<?php
//Force mobile over secure connection
		if ($_SERVER['HTTPS'] != "on") {
			$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
			header("Location: $url");
		}

// Include main config file
include ($_SERVER['DOCUMENT_ROOT'] . "/includes/config.inc.php");

// Include database class
include ($_SERVER['DOCUMENT_ROOT'] . "/includes/classes/database.class.php");

session_start();

if ($_GET['logout']) {
unset($_SESSION['member_id']);

		// delete remember me cookie
		setcookie ('rememberme', "", time() - 3600, '/', '.gps100.com');

header("Location: index.php");
}

//check for remember me cookie and process

if (isset($_COOKIE['rememberme']) && !is_numeric($_SESSION['member_id'])) {
parse_str($_COOKIE['rememberme']);

$do_email = $r_email_address;
$do_pass = $r_hash;
$sql = "SELECT * FROM members WHERE email_address = '".$do_email."' AND pass_enc_sec = '".$do_pass."' AND disabled = 0 LIMIT 1";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);
if (mysql_num_rows($result) == 1) { //the following will log the user in if the cookie matches a record in the database

				// Setup session variables
				$_SESSION['member_id'] = $row['id'];
				$_SESSION['user_id'] = $row['id']; //new "user_" convention for all user variables - hence repeat
				$_SESSION['user_first_name'] = $row['first_name'];
				$_SESSION['user_last_name'] = $row['last_name'];
				$_SESSION['user_ug_school_id'] = $row['ug_school_id'];
				$_SESSION['user_is_analyst'] = $row['analyst'];
				$_SESSION['user_is_member'] = $row['member'];
				$_SESSION['user_is_um'] = $row['upper_management'];
				$_SESSION['user_is_board'] = $row['board_manager'];
				$_SESSION['user_is_alumni'] = $row['alumni'];
				$_SESSION['user_is_admin'] = $row['admin'];
				$_SESSION['user_gps_position_held'] = $row['gps_position_held'];

				// Add log
				$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('user_login_remember_me','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".$_SERVER['SCRIPT_NAME']."?".$_SERVER['QUERY_STRING']."','".$_SERVER['REMOTE_ADDR']."')";
				$result = mysql_query($sql) or die(mysql_error());
}
}

if (is_numeric($_SESSION['member_id'])) {
$logged_in = 1;
}

if ($_POST['submitted']) {

$db_connection = new db_connection('members');

$_POST['email'] = addslashes($_POST['email']);
$_POST['password'] = addslashes($_POST['password']);

if (empty($_POST['email'])) {
$error++;
$errormsg .= "Please enter your email address.<BR>";
}

elseif (empty($_POST['password'])) {
$error++;
$errormsg .= "Please enter your password.<BR>";
}

else { //both email and password not empty

//sanitise and check
$results = $db_connection->select_array("email_address = '" . mysql_real_escape_string($_POST['email']) . "' AND pass_enc_sec = sha1('" . sha1(mysql_real_escape_string($_POST['password'])) . "3l0bsbTdC3x8gS79F9jK3piAHxBtg5aJMrT05sp9C1VsdX6v8')");

if (is_array($results) && $results[0]['disabled'] != 1) { //row found, do log in


				// Setup session variables
				if (!session_is_registered('member_id')) {
					session_register('member_id');
				}
				$_SESSION['member_id'] = $results[0]['id'];
				$_SESSION['user_id'] = $results[0]['id']; //new "user_" convention for all user variables - hence repeat
				$_SESSION['user_first_name'] = $results[0]['first_name'];
				$_SESSION['user_last_name'] = $results[0]['last_name'];
				$_SESSION['user_ug_school_id'] = $results[0]['ug_school_id'];
				$_SESSION['user_is_member'] = $results[0]['member'];
				$_SESSION['user_is_um'] = $results[0]['upper_management'];
				$_SESSION['user_is_board'] = $results[0]['board_manager'];
				$_SESSION['user_is_alumni'] = $results[0]['alumni'];
				$_SESSION['user_is_admin'] = $results[0]['admin'];

				// Add log
				$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('mobile_login','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','mobile_site','".$_SERVER['REMOTE_ADDR']."')";
				$result = mysql_query($sql) or die(mysql_error());

				// Check for remember me, if checked set remember me cookie
				if ($_POST['remember_me']) {
				$cookie_path = "/";
				$cookie_timeout = 60 * 60 * 24 * 30; 
				$user_nohash = $_POST['email_address'];
				$pass_hash = sha1(sha1(mysql_real_escape_string($_POST['password'])) . "3l0bsbTdC3x8gS79F9jK3piAHxBtg5aJMrT05sp9C1VsdX6v8");
				setcookie ('rememberme', 'r_email_address='.$user_nohash.'&r_hash='.$pass_hash, time() + $cookie_timeout, "/", '.gps100.com');
				}

				header("Location: index.php");



} //end row found, do log in

elseif (!is_array($results)) {
$error++;
$errormsg .= "Username and password combination not valid.";
}

elseif ($results[0]['disabled'] == 1) {
$error++;
$errormsg .= "Account disabled.";
	$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('mobile_login_fail_disabled','".$results[0]['id']."','".date("Y-m-d H:i:s")."','mobile_site','".$_SERVER['REMOTE_ADDR']."')";
	$result = mysql_query($sql) or die(mysql_error());
}

} //end both email and password not empty
} //end form posted


$tab = $_GET['tab'];
if ($tab != 'directory' && $tab != 'newsfeed' && $tab != 'birthdays') { $tab = 'directory'; }

if (!$logged_in) { $tab = "login"; }

if ($_GET['directory_form'] || is_numeric($_GET['fetchid'])) {
$fetch_id = $_POST['userString'];
if (!is_numeric($fetch_id)) { $fetch_id = $_GET['fetchid']; }
if (!is_numeric($fetch_id)) { die("Fatal Error 1"); }
//$sql = "SELECT id, CONCAT(first_name, ' ', last_name) fullname, contact_number_country_code, contact_number, email_address, current_photo FROM members WHERE id = '" . $fetch_id . "' LIMIT 1";
//$sql = "SELECT m.id, CONCAT(m.first_name, ' ', m.last_name) fullname, m.contact_number_country_code, m.contact_number, m.email_address, m.current_photo, j.employer, j.title FROM members m, jobs j WHERE m.id = '" . $fetch_id . "' AND m.current_job_id = j.id LIMIT 1";
$sql = "SELECT m.id, CONCAT(m.first_name, ' ', m.last_name) fullname, m.contact_number_country_code, m.contact_number, m.email_address, m.current_photo, j.title, j.employer FROM members m LEFT JOIN jobs as j ON (m.current_job_id = j.id) WHERE m.id = '" . $fetch_id . "' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array($result)) {
$fetch = $row;
if (substr($fetch['contact_number'], 0, 1) == '0') { $fetch['contact_number'] = substr($fetch['contact_number'], 1); }
$fetch['found'] = 1;
}
}

if ($tab == 'newsfeed') {

// Include image manipulation script
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/simpleimage.php");

// Face detection
include($_SERVER['DOCUMENT_ROOT']."/includes/FaceDetector.php");

//Check for entries
//$sql = "SELECT * FROM logs WHERE type='profile_update' OR type='survey_vote' OR type='research_add' ORDER BY time DESC LIMIT 100";
$sql = "SELECT logs.id, logs.type, logs.uid, logs.time, logs.data, logs.ip_address, members.gender_male FROM logs JOIN members WHERE members.id = logs.uid AND (type='profile_update' OR type='survey_vote' OR type='research_add' OR type='dms_file_upload') ORDER BY logs.time DESC LIMIT 100";

$result = mysql_query($sql) or die(mysql_error());

$i = 0;

while ($row = mysql_fetch_array($result)) {
if (!$fed[$row['uid']]) {
$fed[$row['uid']] = 1;
$feed[$i]['id'] = $row['id'];
$feed[$i]['type'] = $row['type'];
$feed[$i]['uid'] = $row['uid'];
$feed[$i]['time'] = $row['time'];

//get gender
if ($row['gender_male'] == 1) { $gender_possessive = "his"; }
else { $gender_possessive = "her"; }

//generate text
if ($row['type'] == 'profile_update') { $feed[$i]['content'] = " updated the <a href='/portal/network/view/".$row['data'].".php?id=".$row['uid']."'>".ucfirst($row['data']). "</a> area of ".$gender_possessive." profile."; }

if ($row['type'] == 'survey_vote') {

$sql1 = "SELECT question FROM vote_questions WHERE id = '".$row['data']."' LIMIT 1";
$result2 = mysql_query($sql1) or die(mysql_error());
while ($row2 = mysql_fetch_array($result2)) { $question = $row2['question']; }
$feed[$i]['content'] = " voted on <a href='/portal/vote/'>".$question."</a>";

}

if ($row['type'] == 'research_add' && $row['data'] == 'pipeline') { $feed[$i]['content'] = " added a company to the <a href='/portal/research/'>research pipeline</a>."; }
if ($row['type'] == 'research_add' && $row['data'] == 'wishlist') { $feed[$i]['content'] = " added a company to the <a href='/portal/research/research.php?wishlist=1'>PM's Wishlist</a>."; }

if ($row['type'] == 'dms_file_upload') {
//check file exists
$sql3 = "SELECT file.name as file_name, file.id_file as id_file, file.id_folder, folder.name as folder_name FROM dms_file as file, dms_folder as folder WHERE file.id_file = '".$row['data']."' AND file.id_folder = folder.id_folder";
$result3 = mysql_query($sql3) or die(mysql_error());
$row3 = mysql_fetch_array($result3);

//add content
$feed[$i]['content'] = " uploaded <a href='/portal/documents/file/downloadFile/" . $row3['id_file'] . "/" . $row3['id_folder'] . "'>" . $row3['file_name'] . "</a> in <a href='/portal/documents/folder/folders/" . $row3['id_folder'] . "'>" . $row3['folder_name'] . "</a>";
}


// Get user photo and name
if(is_numeric($row['uid'])) {
$sql2 = "SELECT current_photo, first_name, last_name FROM members WHERE id = '" . $row['uid'] . "' LIMIT 1";
$result2 = mysql_query($sql2) or die(mysql_error());
$row2 = mysql_fetch_array($result2);

//photo
$img_path = $row2['current_photo'];
$img_path_cropped = str_replace("current","cropped",$img_path);

if(strlen($img_path) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'] . $img_path)) {

if(!file_exists($_SERVER['DOCUMENT_ROOT'] . $img_path_cropped)) {
$detector = new FaceDetector();
$detector->scan($_SERVER['DOCUMENT_ROOT'] . $img_path);
$faces = $detector->getFaces();
foreach($faces as $face) {
$x = $face['x'];
$y = $face['y'];
$width = $face['width'];
$height = $face['height'];
}

			   $image = new SimpleImage();
			   $image->load($_SERVER['DOCUMENT_ROOT']. $img_path);
if (is_numeric($width)) { 
			   $image->crop($width,$height,$x,$y);
}
			   $image->resizeToHeight(100);
			   $image->resizeToWidth(50);
			   $image->crop(50, 50, 0, 0);
//			   $image->resize(50, 50);
			   $image->save($_SERVER['DOCUMENT_ROOT']. $img_path_cropped);
}

$feed[$i]['thumb'] = $img_path_cropped;
}
else {
$feed[$i]['thumb'] = "/images/male_thumb.png";
}

//name

$feed[$i]['name'] = $row2['first_name'] . " " . $row2['last_name'];

} //end check there is a user id (for user photo)

//generate time text

$item_time = strtotime($row['time']);
$now = date(U);
$difference = $now - $item_time;

//check if less than 60 seconds
if ($difference < 60) { $feed[$i]['timetext'] = round($difference,0) . " seconds ago"; }

elseif ($difference < 90) { $feed[$i]['timetext'] = "1 minute ago"; }

elseif ($difference < 3600) { $feed[$i]['timetext'] = round(($difference/60),0) . " minutes ago"; }

elseif ($difference < 5400) { $feed[$i]['timetext'] = "1 hour ago"; }

elseif ($difference < 84600) { $feed[$i]['timetext'] = round(($difference/3600),0) . " hours ago"; }

elseif ($difference < 129600) { $feed[$i]['timetext'] = "1 day ago"; } //169200 (the old value)

elseif ($difference < 30240000) { $feed[$i]['timetext'] = round(($difference/84600),0) . " days ago"; }

else { $feed[$i]['timetext'] = $difference; }


$i++;
} //end check if fed
} //end fetching feed

}

if ($tab == 'birthdays') {
function calculateAge($birthday){
$age = floor((time() - strtotime($birthday))/31556926);
if ($age < 120) { return $age; }
else { return "?"; }
}
$look_forward = 30;
$look_limit = 10;
$sql = "SELECT id, first_name, last_name, birth_date FROM members WHERE (DAYOFYEAR( curdate( ) ) <= dayofyear( birth_date ) AND DAYOFYEAR( curdate( ) ) +".$look_forward." >= dayofyear( birth_date ) OR DAYOFYEAR( curdate( ) ) <= dayofyear( birth_date ) +365 AND DAYOFYEAR( curdate( ) ) +".$look_forward." >= dayofyear( birth_date ) +365) ORDER BY dayofyear( birth_date ) LIMIT " . $look_limit;
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$day = substr($row['birth_date'],8,2);
$tmonth = substr($row['birth_date'],5,2);
$birthday[$tmonth.$day][] = $row;
}
}
?>

<?php echo '<?xml version="1.0" encoding="UTF-8" ?>'; ?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" id="bp-doc">


<head>

<title>GPS Mobile Portal

</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style type="text/css">
.pageTab { width: 33%; padding: 12px 8px 8px; text-align: center; font-size: 93%; font-weight: bold; white-space: nowrap }
#tabs-19 .tabLinks td { width: 33.3333% }
body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6 { padding:0 ;margin:0 }
h1,h2,h3,h4,h5,h6 { display:inline; font-weight:normal }
img{ border:0 }
table { border:0; border-collapse:collapse;border-spacing:0;width:auto }
pre,code,kbd,samp,tt { font-family:monospace;line-height:99%;font-size:108% }
a, a:visited, a:hover, a:active { color:navy;text-decoration:none }
a:hover { text-decoration: underline }
ul, li{list-style-type:none}
body{font-size:16px;-webkit-text-size-adjust:none;font-family:Helvetica, 'Nokia Sans', Arial, Sans-serif}
.collection{background:#c5ccd7;color:#3e3e3e}
.list{background:#fff;color:#000}
.collection #pageHeader{margin-bottom:5px}
.small{font-size:93%}
.title{font-size:115%;font-weight:bold}
.article{font-family:Georgia, serif}
.article-title{font-weight:normal;font-size:120%}
.collection .title{color:#000}
.collection .uim .title{color:#000}
.collection .featured-uim .title{color:#000}.collection .uim .uim .title{color:#000}.collection .subdued{color:#7c7c7d}.collection .uim .subdued{color:#7c7c7d}.collection .featured-uim .subdued{color:#7c7c7d}.collection .uim .uim .subdued{color:#7c7c7d}.collection .subtext{color:#446b94}.collection .uim .subtext{color:#6271a0}.collection .featured-uim .subtext{color:#446b94}.collection .uim .uim .subtext{color:#6271a0}.collection .positive{color:#007800}.collection .uim .positive{color:#056005}.collection .featured-uim .positive{color:#00a400}.collection .uim .uim .positive{color:#056005}.collection .negative{color:#b30200}.collection .uim .negative{color:#b30200}.collection .featured-uim .negative{color:#b30200}.collection .uim .uim .negative{color:#b30200}.collection .important{color:#b30200;font-weight:bold}.collection .uim .important{color:#b30200}.collection .featured-uim .important{color:#b30200}.collection .uim .uim .important{color:#b30200}.collection .value{color:#405288}.collection .uim .value{color:#405288}.collection .featured-uim .value{color:#405288}.collection .uim .uim .value{color:#405288}.collection .disabled{color:#bcc5cc}.collection .uim .disabled{color:#bcc5cc}.collection .featured-uim .disabled{color:#bcc5cc}.collection .uim .uim .disabled{color:#bcc5cc}.collection .link{color:#006ec2}.collection .uim .link{color:#006ec2}.collection .featured-uim .link{color:#006ec2}.collection .uim .uim .link{color:#006ec2}.collection .url{color:#398f08}.collection .uim .url{color:#398f08}.collection .featured-uim .url{color:#398f08}.collection .uim .uim .url{color:#398f08}.list .title{color:#000}.list .uim .title{color:#000}.list .featured-uim .title{color:#000}.list .uim .uim .title{color:#000}.list .subdued{color:#7c7c7d}.list .uim .subdued{color:#7c7c7d}.list .featured-uim .subdued{color:#7c7c7d}.list .uim .uim .subdued{color:#7c7c7d}.list .subtext{color:#6271a0}.list .uim .subtext{color:#6271a0}.list .featured-uim .subtext{color:#446b94}.list .uim .uim .subtext{color:#6271a0}.list .positive{color:#056005}.list .uim .positive{color:#056005}.list .featured-uim .positive{color:#00a400}.list .uim .uim .positive{color:#056005}.list .negative{color:#b30200}.list .uim .negative{color:#b30200}.list .featured-uim .negative{color:#b30200}.list .uim .uim .negative{color:#b30200}.list .important{color:#b30200;font-weight:bold}.list .uim .important{color:#b30200}.list .featured-uim .important{color:#b30200}.list .uim .uim .important{color:#b30200}.list .value{color:#405288}.list .uim .value{color:#405288}.list .featured-uim .value{color:#405288}.list .uim .uim .value{color:#405288}.list .disabled{color:#bcc5cc}.list .uim .disabled{color:#bcc5cc}.list .featured-uim .disabled{color:#bcc5cc}.list .uim .uim .disabled{color:#bcc5cc}.list .link{color:#006ec2}.list .uim .link{color:#006ec2}.list .featured-uim .link{color:#006ec2}.list .uim .uim .link{color:#006ec2}.list .url{color:#398f08}.list .uim .url{color:#398f08}.list .featured-uim .url{color:#398f08}.list .uim .uim .url{color:#398f08}.uim .subtext.description, .uim-featured .subtext.description{font-size:100%;color:#000}.url{word-break:break-all;word-wrap:break-word}#pageBranding{background:#455681 url(http://www.gps100.com/mobile/images/steel.cf.png) bottom left repeat-x}#pageHeader .searchBox{padding:0.3em 4px}#pageHeader .searchBox input{margin:0}#toolbar{background:#fff url(http://www.gps100.com/mobile/images/masthead.png) bottom left repeat-x;padding-bottom:4px}#toolbar .link{color:#006ec2}#toolbar .url{color:#398f08}#toolbar .title{color:#16387c}#toolbar .subtext{color:#6271a0}#toolbar .subdued{color:#7c7c7d}#toolbar .important{color:#b30200}#toolbar .negative{color:#b30200}#toolbar .positive{color:#00a400}#toolbar .value{color:#405288}#toolbar .template .uic img{vertical-align:text-top}#toolbar .sc-hidden, #toolbar.hidden{display:none}.collection #pageTabs{border-bottom:4px solid #fff}#pageTabs table{margin:0 auto;width:90%}#pageTabs .wide{padding:12px 2px 8px;min-width:130px;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;-o-text-overflow:ellipsis}#pageTabs .ptactive{background:#fff;padding:12px 20px 8px;margin:0 10px}.pageTab a, .pageTab a.inline{color:#fff;margin-right:0}#pageTabs .ptactive, #pageTabs .ptactive a{color:#000}div.tabs-compact{font-size:110%;padding:6px 0 8px}div.tabs-compact .tabLinks, #gotos{width:auto;text-align:center;margin:0 auto}body div.tabs-compact .tabLinks td.tabLink, #gotos .goto{background:transparent;border:0;padding:0 12px}body div.tabs-compact .tabLinks td a, body div.tabs-compact .tabLinks td span, #gotos .goto a{color:#3b4056;font-weight:bold}body div.tabs-compact .tabLinks td a, #gotos .goto a{padding:4px 0}body div.tabs-compact table.tabLinks .active a, body div.tabs-compact table.tabLinks .active span, #gotos .active a{color:#000}body div.tabs-compact table.tabLinks .active, #gotos .active{background:#fff;border:1px solid #ccc;padding-top:1px}#gotos{font-size:85%}#titlebar{padding:8px}#titlebar .icon img{vertical-align:middle}#titlebar .widgetTitle{vertical-align:middle;color:#fff;font-weight:bold;margin-left:4px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;text-shadow:rgba(0,0,0,1) 0 1px 1px;max-width:192px;display:inline-block}#titlebar #uiCommands{position:absolute;top:10px;right:8px;max-width:40%;text-align:right}#titlebar #uiCommands a{color:#fff;text-decoration:none;text-shadow:rgba(0,0,0,0.7) 0 -1px 1px;font-size:85%;font-weight:bold}img.left{padding-right:.4em}img.right{padding-left:.4em}.favicon img{margin-right:4px;vertical-align:bottom}.image-block{display:block}.caption{display:block;padding:0 1px;clear:both;font-size:72%;margin-top:2px;text-align:center}.cleft{text-align:left}.cright{text-align:right}.sprite{overflow:hidden;display:inline-block;background-repeat:no-repeat}.uic span.sprite.inline{vertical-align:middle}.photoSize{display:block;width:100%;text-align:center}.photoSize img{max-width:295px}.collection .photoSize img{max-width:285px}.uim .uiCommand a{font-size:85%;font-weight:bold;color:#481060}.suim .uiCommand a{color:#674670}.featured .uiCommand a{color:#fff}#titlebar .uiCommand a{color:#fff}.uiCommand.as a, #allSites .uiCommand a{display:block;width:100%;height:100%}.tabs{padding:0}table.tabLinks{width:100%}table.tabLinks td{background:#fff url(http://www.gps100.com/mobile/images/tab_bkg.png) bottom left repeat-x;text-align:center;color:#000;font-size:75%;font-weight:bold;border:1px solid #b7c1c8;border-left:0;white-space:nowrap}.uim-featured table.tabLinks td{background:#7c97b6 url(http://www.gps100.com/mobile/images/tab_featured_bkg.png) bottom left repeat-x;border-color:#627390;color:#343d5b}table.tabLinks td.last{border-right:none}body table.tabLinks td.first{border:1px solid #b7c1c8;border-bottom:1px solid #b7c1c8;border-left:0}#content table.tabLinks td.first{border-top:1px solid #b7c1c8}body .uim-featured table.tabLinks td.first{border-color:#627390;border-top:1px solid #627390}table.tabLinks td a.inline{display:block;width:100%;height:100%;padding:8px 0;margin-right:0;color:#16387c}.uim-featured table.tabLinks td a.inline{color:#fff}table.tabLinks td a.inline, table.tabLinks td a.inline:hover, table.tabLinks td a.inline:active, table.tabLinks td a.inline:focus{outline:none}body table.tabLinks td.active{background:#fff;color:#000;border-bottom-color:#fff}body .uim-featured table.tabLinks td.active{background:#eff2fd;border-bottom-color:#eff2fd}table.tabLinks td, table.tabLinks td a{color:#16387c;cursor:pointer}.uim-featured table.tabLinks td, .uim-featured table.tabLinks td a{color:#fff}table.tabLinks td.active a{color:#000}.uim-featured table.tabLinks td.active a{color:#343d5b}body #content .first table.tabLinks td{border-top:0}.tabSection{display:none}.tabSectionShown{display:block}body .tabs .tabSectionShown .uim:first-child, #content .tabs .tabSectionShown div.first{border-top:none}a.inline{color:#006ec2}.uic{display:block}.uim .searchBox-first{padding-top:4px}.searchBox table{width:100%}.searchBox td.input{width:100%}.searchBox td.input input{width:100%;margin-right:0;padding:1px 2px;background:#fff;color:#000;border:1px solid #ababab}.searchBox td.input input.disabled{color:#bcc5cc}.searchBox table td.submit{padding:1px 1px 1px 10px;font-size:105%}input[type="text"], input[type="password"], textarea{font-size:85%;border-top:1px solid #c6c6c6;border-bottom:1px solid #808080;border-left:1px solid #a6a6a6;border-right:1px solid #a6a6a6}.formField input[type="text"], .formField input[type="password"], .formField textarea{width:89%}.formField input[type="submit"], .formField button{font-size:93%;font-weight:bold;border-top:1px solid #c6c6c6;border-bottom:1px solid #808080;border-left:1px solid #a6a6a6;border-right:1px solid #a6a6a6;background:#f0f0f0}form{margin:0}form.inline, form.inline .formField, .formField.inline{display:inline}.formField input, .formField textarea, .formField select, .formField button{font-size:inherit;font-weight:inherit;margin:0 8px 0.3em}.formField textarea{margin-top:.3em;width:89%}.formField input.disabled, .formField textarea.disabled{color:#bcc5cc}#toolbar .formField input.disabled, #toolbar .formField textarea.disabled{color:#bcc5cc}.uim .formField input.disabled, .uim .formField textarea.disabled{color:#bcc5cc}.formField{padding:0}.formField-first{padding-top:0.3em}.formField .uic{color:#3e3e3e}.uim .formField .uic{color:#000}.suim .formField .uic{color:#000}.formLabel{padding:0.3em 8px;color:#4d4d4d}.formField.compact, form.inline .formField.compact{display:block}.compact .formLabel, .compact input, .compact input.text{display:inline}.compact .formLabel{padding-right:0}.formField .item{display:block}input.text{display:block}#content .uim{background:#fff;color:#000}#content .uim-featured, #content .uim-featured .uim{background:#eff2fd;color:#3e3e3e}.uim, #content .list .uim{border:none;border-top:1px solid #dfdfdf;margin:0}.first, #content .list .first{border-top:0}.uim-featured, .uim-featured .uim, #content .list .uim-featured, #content .list .uim-featured .uim{border-color:#7f9ab8}.collection .uim{margin:5px;border:1px solid #a9b0b8;border-top:1px solid #a9b0b8}.collection .uim-featured{border-bottom:1px solid #7f9ab8}.collection .uim .uim{border-width:1px 0 0 0;margin:0}.collection .uim .bd div.first, .collection .uim .hd{border-top:0}.collection .uim .hd:last-child{border-bottom:0}.collection .uim .bd form:first-child{border:0}.collection .doBdrTop{border-top:1px solid #a9b0b8}.collection .doBdrBottom{border-bottom:1px solid #a9b0b8}.collection .doBdrTop .bd div, .collection .doBdrTop .bd a{border-left:0;border-right:0}.collection .uim .bd .uim:first-child .hd, .collection .uim .bd .uim:first-child .hd{border-top:0}.collection .uim .hd + .bd > *:first-child{border-top:0}.collection .uim:last-child{margin-bottom:0}.groupCompact{border-top:1px solid #a9b0b8;padding:0}.groupCompact-first{border-top:0;padding-top:0.3em}.groupCompact .uic, .groupCompact .uip, .uim .groupCompact .uic, .uim .groupCompact .uip{border-top:0;padding:1px 8px}.groupCompact .uip .uic, .uim .groupCompact .uip .uic{padding:1px 0}.groupCompact-last{padding-bottom:0.3em}.groupCompact .uip:last-child, .groupCompact .uic:last-child{padding-bottom:4px}.uip{padding:0.3em 8px}.uip .uic{padding:0.3em 0}.linked .items{background:transparent url(http://www.gps100.com/mobile/images/arrow_blue.png) no-repeat;background-position:center right}.uim-featured .hd-linked .items{background:transparent url(http://www.gps100.com/mobile/images/arrow_white.png) no-repeat;background-position:center right}.linked .items td.blocks, .linked .items td.imgR{padding-right:8px}#bp-bd .callout{background:#13428b url(http://www.gps100.com/mobile/images/callout_medium) bottom left repeat-x}#bp-bd .callout *{color:#d3ecff}#bp-bd .callout-linked .items{background:transparent url(http://www.gps100.com/mobile/images/arrow_white) no-repeat;background-position:center right}#bp-bd .callout .title{color:#fff}#bp-bd .callout .subtext{color:#fff}#bp-bd .callout .subdued{color:#378ca4}#bp-bd .callout .value{color:#fff}#bp-bd .callout-low{background:#f2fafe url(http://www.gps100.com/mobile/images/callout_subdued) bottom left repeat-x}#bp-bd .callout-low *{color:#272627}#bp-bd .callout-low-linked .items{background:transparent url(http://www.gps100.com/mobile/images/arrow_blue) no-repeat;background-position:center right}#bp-bd .callout-low .title{color:#16387c}#bp-bd .callout-low .subtext{color:#16387c}#bp-bd .callout-low .subdued{color:#656a6d}#bp-bd .callout-low .value{color:#16387c}#bp-bd .callout-strong{background:#fdf099 url(http://www.gps100.com/mobile/images/callout_strong) bottom left repeat-x}#bp-bd .callout-strong *{color:#272627}#bp-bd .callout-strong-linked .items{background:transparent url(http://www.gps100.com/mobile/images/arrow_blue) no-repeat;background-position:center right}#bp-bd .callout-strong .title{color:#16387c}#bp-bd .callout-strong .subtext{color:#50201e}#bp-bd .callout-strong .subdued{color:#7d6d58}#bp-bd .callout-strong .value{color:#16387c}#bp-bd .callout-alert{background:#b91315 url(http://www.gps100.com/mobile/images/callout_alert) bottom left repeat-x}#bp-bd .callout-alert *{color:#ffe6a2}#bp-bd .callout-alert-linked .items{background:transparent url(http://www.gps100.com/mobile/images/arrow_white) no-repeat;background-position:center right}#bp-bd .callout-alert .title{color:#fff}#bp-bd .callout-alert .subtext{color:#fff}#bp-bd .callout-alert .subdued{color:#e79576}#bp-bd .callout-alert .value{color:#fff}table.template{width:100%}.uim .uip table.template tr td .uic{padding:1px 0}table.template td{vertical-align:top}table.template.single td{vertical-align:middle}.template td.icon{padding:1px 0 0 0;text-align:center}.template .uic img{margin-left:3px}.template .uip, .template .uic{padding:0}.template .value-cell{white-space:nowrap;text-align:right}.linked .template .value-cell{padding-right:12px}.template .value-cell .uic{white-space:nowrap;color:#405288}.uim .template .value-cell .uic{color:#405288}.uim-featured .template .value-cell .uic{color:#405288}.template .description-cell .floatL{float:none}.uic, .uip{display:block;padding:0.3em 8px}.uic img + a.inline{margin-left:2px}.uic img, .uic img + a.inline{vertical-align:middle}.uim .uic, .uim .uip{border-top:1px solid #dfdfdf}.uic.first, .uip.first, .collection .uim .uic.first, .collection .uim .uip.first, .collection .uim .uim .uic.first{border-top:0}.uim .uic{border-top:1px solid #dfdfdf}.uim-featured .uic{border-top:1px solid #7991a7}.uim .uic.first, .uim .uim .uic.first{border-top:0}.center{text-align:center}.left, .natural{text-align:left}.right, .opposite{text-align:right}#content strong, #content .uim strong{font-weight:bold}table.items{width:100%}.items .blocks, .items .imgR{vertical-align:middle}.items .imgL{vertical-align:top}.placardSet .items .imgL{vertical-align:middle}.imgL, .tpl .icon{padding-right:4px}.tpl .icon{text-align:center}.imgL span.sprite{display:block}.imgR{padding:1px 12px 1px 4px;white-space:nowrap}.blocks{width:100%}.blocks .uic, .uim .uip .uic{padding:0;border-top:none}.hd{position:relative;font-size:93%}.section .hd{background-color:transparent;background-image:none}.section .hd .uic{padding:0.3em 7px;color:#4e5980}.section .uim .hd .uic{padding:0.3em 8px}.section .hd .uic a{color:#4e5980}.section .hd .uiCommand a{font-size:85%;font-weight:bold;color:#4e5980}.section .items{padding:0}.section .uim .items{padding:0.3em 0}.section .hd-linked{padding-right:7px}.hd .uiCommand{display:block;text-align:right;position:absolute;right:8px;top:0.4em}.section .hd-uicommand-short .uiCommand{top:0.2em}.section .hd-linked .uiCommand{right:15px}.hd .uiCommand a{font-size:85%;font-weight:bold;color:#481060;text-decoration:none}.uim-featured .hd .uiCommand a{color:#fff}.uim .hd{padding:0.4em 8px 0.3em;background:#fff url(http://www.gps100.com/mobile/images/module_header) bottom left repeat-x;border-bottom:1px solid #b7c1c8}.uim .hd-uicommand{padding-right:4em}.uim .hd .uic{border-top:0;color:#000;font-weight:bold}.uim .uim .hd{background:#fff url(http://www.gps100.com/mobile/images/submodule_header) bottom left repeat-x}.uim .uim .hd .uic{color:#596c7b}.uim-featured .hd, .uim-featured .uim .hd, .uim .uim-featured .hd{background:#98adc6 url(http://www.gps100.com/mobile/images/module_featured_header) bottom left repeat-x;border-bottom:1px solid #446b94}.uim-featured .hd .uic, #content .uim-featured .hd .uic{color:#fff}.uim .hd .title{color:#000}.uim .uim .hd .title{color:#000}.uim-featured .hd .title, .uim-featured .uim .hd .title{color:#fff}.uim .hd .subdued{color:#7c7c7d}.uim .uim .hd .subdued{color:#676867}.uim-featured .hd .subdued, .uim-featured .uim .hd .subdued{color:#d6e0ee}.uim .hd .subtext{color:#6271a0}.uim .uim .hd .subtext{color:#446b94}.uim-featured .hd .subtext, .uim-featured .uim .hd .subtext{color:#ffe294}.uim .hd .positive{color:#056005}.uim .uim .hd .positive{color:#056005}.uim-featured .hd .positive, .uim-featured .uim .hd .positive{color:#5bf65d}.uim .hd .negative{color:#b30200}.uim .uim .hd .negative{color:#b30200}.uim-featured .hd .negative, .uim-featured .uim .hd .negative{color:#b30200}.uim .hd .important{color:#b30200}.uim .uim .hd .important{color:#b30200}.uim-featured .hd .important, .uim-featured .uim .hd .important{color:#ffe400}.uim .hd .value{color:#405288}.uim .uim .hd .value{color:#405288}.uim-featured .hd .value, .uim-featured .uim .hd .value{color:#c6fb29}.uim .hd .link{color:#006ec2}.uim .uim .hd .link{color:#006ec2}.uim-featured .hd .link, .uim-featured .uim .hd .link{color:#ffdb79}#content .navbar{background:#9aa6b6;padding:0;text-align:center}#content .navbar:after{content:".";display:block;clear:both;visibility:hidden;height:0}#content .navbar-tabs{padding-bottom:4px}#content .navbar-tabs .nav-box{background:#c5ccd7}#content .uim .navbar .nav-box span, #content .uim-featured div.navbar-tabs .nav-box span, #content .uim .navbar .nav-back span, body #content .uim-featured .navbar-first .nav-back span, #content .uim .uim .navbar .nav-back span, body #content .uim .navbar-tabs .back span, #bp-doc .list .navbar-tabs .nav-box span, #bp-doc .collection .navbar-first .nav-box span, #bp-doc .collection .navbar-tabs-first .nav-back .back span{color:#405288}#content .uim .navbar, #content .uim .navbar-first{background:#f4f5f6}#content .uim .navbar-tabs .nav-box{background:#fff;border:1px solid #dfdfdf;border-top:0}#content .uim-featured .navbar{background:#97acc6}#content .navbar .nav-box span, #content .navbar .nav-back .back span, #content .uim-featured .navbar .nav-box span, #content .uim-featured .navbar .nav-back span, #content .uim-featured .uim .navbar .nav-back span, body #content .navbar-tabs-first span, body #content .uim-featured .navbar-tabs .back span{color:#fff}#content .uim-featured .navbar-tabs .nav-box{background:#eff2fd;border:1px solid #7991a7;border-top:0}#content .navbar-tabs-first{padding:4px 0 0 0;margin-bottom:4px}#content .uim .navbar-tabs-first, #bp-doc .list .navbar-tabs-first{margin-bottom:0}#bp-doc .list .navbar-first{background:#a2acc2 url(http://www.gps100.com/mobile/images/nav_bkg_top) left bottom repeat-x}#bp-doc .collection .navbar-first{border-bottom:1px solid #7991a7}#content .uim .navbar-first{background:#f4f5f6}#bp-doc .collection .uim .navbar-first{border-bottom:1px solid #dfdfdf;background:#f4f5f6}#bp-doc .collection .uim .navbar-tabs-first .nav-box{background:#fff}#bp-doc .list .navbar-tabs-first + .uim, #bp-doc .collection .uim .navbar-tabs-first + .uim{border-top:0}#content .navbar .nav-box, #content .navbar .nav-back{margin-top:-1px;padding:6px 12px}#content .navbar .nav-back, #content .navbar .disabled{margin-top:0}#content .navbar .nav-left, #content .uim .navbar .nav-left, #content .navbar .nav-back{display:block;float:left;text-align:left;padding-left:8px;border-left:0;border-top:0}#content .navbar .nav-right, #content .uim .navbar div.nav-right{float:right;text-align:right;border-right:0}#content .navbar .nav-right.nav-both .prev{margin-right:6px;border-right:1px solid #dfdfdf;padding-right:6px}#content .uim-featured .navbar .nav-right .prev{border-right:1px solid #7991a7}#bp-doc .list .navbar-tabs-first .nav-box{border:none}#content .navbar-tabs-first .nav-box, #content .uim .navbar-tabs-first .nav-box{margin:0 0 -1px;border:1px solid #7991a7;border-bottom:0}#content .navbar-tabs-first .nav-right .prev{border-color:#7991a7}#content .navbar-tabs-first .nav-right{border-right:0}#content .navbar-tabs-first .nav-left, #content .navbar-tabs-first .nav-back{border-left:0 !important}#content .uim .navbar-tabs-first .nav-box{border-color:#dfdfdf}#content .uim .groupCompact div + .navbar-tabs{border-top:1px solid #dfdfdf}#content div.uim-featured .groupCompact div + .navbar-tabs{border-top:1px solid #7991a7}#toolbar table.header-nav{width:100%;height:20px}#toolbar table.header-nav td{padding:5px 0 4px}#toolbar table.header-nav td.back{width:30%;white-space:nowrap;padding-left:6px}#toolbar table.header-nav td.back span.back{background:transparent url(http://www.gps100.com/mobile/images/page_nav_back_arrow_outline) no-repeat;background-position:left top;display:inline-block;height:20px;width:12px;overflow:hidden;vertical-align:middle}#toolbar table.header-nav td.back a{border:1px solid #b8babf;border-left:0;padding:1px 6px 0 0;height:17px;display:inline-block;min-width:20px;vertical-align:middle;text-align:center;overflow:hidden;text-overflow:ellipsis;-o-text-overflow:ellipsis;line-height:1.4em;color:#313131;font-size:77%;font-weight:bold}#toolbar table.header-nav td.back a.empty{width:auto;padding-right:4px}#toolbar table.header-nav td.page-title{text-align:center;font-size:105%;font-weight:bold;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;-o-text-overflow:ellipsis;width:40%;color:#3d5465}#toolbar table.hn-min td.page-title{white-space:normal;overflow:visible}#toolbar table.header-nav td.nav{width:30%;padding-right:6px;text-align:right}#toolbar table.header-nav td.nav span{border:1px solid #b8babf;margin:0;display:inline-block;line-height:0}#toolbar table.header-nav td.nav span img{padding:8px}#toolbar table.header-nav td.nav span.last{border-left:0}#toolbar table.header-nav td.buttons span{line-height:inherit;padding:2px 4px;margin-bottom:1px;color:#313131;font-size:85%;font-weight:bold}#toolbar table.header-nav td.buttons span a{display:inline-block;width:100%;white-space:nowrap;overflow:hidden;vertical-align:bottom}#toolbar table.header-nav td.buttons span.last{border-left:1px solid #b8babf;margin-left:4px}#toolbar table.header-nav td.buttons span.disabled{display:none}.navbar .prev, .navbar .next, .navbar .back, .pageNav .prev, .pageNav .next{font-size:85%;font-weight:bold;white-space:nowrap}#content .navbar *, #content .pageNav *{vertical-align:inherit}.navbar .back img, .navbar .nav-box .prev img, .pageNav .prev img{padding-right:4px}.navbar .nav-box .next img, .pageNav .next img{padding-left:4px}#content .more a span{margin-right:2px}#content .more a{color:#405288}#content .uim-featured .more a{color:#fff}#content .pageNav .disabled span, #content .navbar .disabled span{color:#bcc5cc !important}.uim .pageNav .disabled, .uim .navbar .disabled{color:#bcc5cc}.navbar span.label{padding:8px 3px}
.uim a.inline{color:#006ec2}
.uim-featured a.inline{color:#006ec2}
#pageFooter{clear:both;padding:0.8em 0 1em 0;border-top:1px solid #b4bac5;background-color:#c5ccd7}
.collection #pageFooter{margin-top:5px}
#pageFooter div#defaultFooter{margin-bottom:0;border:none;text-align:center;line-height:1.2em;font-size:85%}
div#defaultFooter div.bd{background:transparent;border:none;min-height:0}
div#defaultFooter div.uic{margin:0;padding:0;border:none;color:#000;font-weight:bold}
div.username a, div.links a{margin:0 4px}div#defaultFooter a{font-weight:bold;color:#006ec2}
div#footer div.subtext{font-size:85%;color:#a4a3a4}
#pageFooter #brandFooter{background-color:#f2f2f2;color:#000;border:none;text-align:left}
#pageFooter #brandFooter a, #pageFooter #brandFooter a.inline{color:inherit}
#pageFooter #brandFooter .title{color:#000}
#pageFooter #brandFooter .subdued{color:#7c7c7d}#pageFooter #brandFooter .subtext{color:#a4a3a4}
#pageFooter #brandFooter .positive{color:#056005}#pageFooter #brandFooter .negative{color:#b30200}
#pageFooter #brandFooter .important{color:#b30200;font-weight:bold}
#pageFooter #brandFooter .link{color:#006ec2}#pageFooter #brandFooter .url{color:#398f08}
.thumbb { float:left; margin: 7px 10px 0 0 }
</style>

<script type="text/javascript">
//var Bp={};Bp.get=function(a){
//if(typeof(a)=="object"){return a}return document.getElementById(a)};Bp.queryAll=function(b){var a=document.body;if(arguments.length>1){a=Bp.get(arguments[1])}return a.querySelectorAll(b)};Bp.on=function(f,e,d){f=Bp.get(f);var c=this;if(arguments.length>3){c=arguments[3]}var b=false;if(Bp.env.Webkit&&typeof(window.ontouchstart)==="undefined"){switch(e){case"touchstart":e="mousedown";b=true;break;case"touchmove":e="mousemove";b=true;break;case"touchend":e="mouseup"}}var a=function(g){var g=g||window.event;if(!g.target&&g.srcElement){g.target=g.srcElement}if(b&&!g.touches){g.touches=[g]}d.call(c,g)};if(typeof(f.addEventListener)!="undefined"){f.addEventListener(e,a,false)}else{if(f.attachEvent){f.attachEvent("on"+e,a)}else{f["on"+e]=a}}};Bp.preventDefault=function(a){if(a.preventDefault){a.preventDefault()}else{a.returnValue=false}};Bp.stopPropagation=function(a){if(a.stopPropagation){a.stopPropagation()}else{a.cancelBubble=true}};Bp.simulateClick=function(b){var a=document.createEvent("MouseEvents");a.initMou//seEvent("click",true,true,window,0,0,0,0,0,false,false,false,false,0,null);b.dispatchEvent(a)};Bp.dom={addClass:function(b,a){b=Bp.get(b);if(!b.className.match(a)){if(b.className!=""){a=" "+a}b.className+=a}},removeClass:function(b,a){b=Bp.get(b);b.className=b.className.replace(a,"").replace(/^\s+|\s+$/g,"")},hasClass:function(b,a){b=Bp.get(b);return b.className.match(new RegExp("(?:^|\\s+)"+a+"(?:\\s+|$)"))!==null},getXY:function(a){a=Bp.get(a);var c=0,b=0;if(a.offsetParent){do{c+=a.offsetLeft;b+=a.offsetTop}while(a=a.offsetParent)}return[c,b]},getStyle:function(b,a){b=Bp.get(b);return window.getComputedStyle?document.defaultView.getComputedStyle(b,null).getPropertyValue(a):(b.currentStyle?b.currentStyle[a]:b.style[a])},insertAfter:function(b,a){a=Bp.get(a);a.parentNode.insertBefore(b,a.nextSibling)},getAncestorBy:function(c,b,a){c=Bp.get(c);a=a||b;if(b.call(a,c)){return c}else{if(c===document.body){return false}else{return this.getAncestorBy(c.parentNode,b,a)}}},getAncestorByTagName:function(b,a){return t//his.getAncestorBy(b,function(c){return c.nodeName.toLowerCase()===a})}};Bp.env=(function(){var a={};a.RIM=RegExp("BlackBerry").test(navigator.userAgent);a.RIM6=a.RIM&&RegExp("Version/6").test(navigator.userAgent);a.IE=RegExp("MSIE ").test(navigator.userAgent);a.OperaMobile=RegExp("Opera Mobi").test(navigator.userAgent);a.Webkit=RegExp(" AppleWebKit/").test(navigator.userAgent);a.Apple=RegExp("iPhone").test(navigator.userAgent)||RegExp("iPad").test(navigator.userAgent);a.iPhone3=a.Apple&&RegExp("OS 3").test(navigator.userAgent);a.iPhone4=a.Apple&&RegExp("OS 4").test(navigator.userAgent);a.Android=RegExp("Android").test(navigator.userAgent);a.Android2=RegExp("Android 2").test(navigator.userAgent);a.WebOS=RegExp("webOS").test(navigator.userAgent);return a})();Bp.locationHref=function(c,b,a){Bp.on(c,b,function(d){if(!(d.target.tagName=="A"||Bp.dom.getAncestorByTagName(d.target,"a"))){location.href=a}})};Bp.getTextDirection=function(){return document.body.getAttribute("dir")||"ltr"};Bp.defaultFocus=false;;

</script>

<script type="text/javascript">
function update() {
document.directory_form.submit();
}
</script>

</head>


<body class="collection" id="bp-bd">


<div id="page">


<div id="pageHeader">


<div id="pageBranding">


<div id="titlebar" class="full">

<div class="logoAndTitle">

<? if ($logged_in) { ?>
<div id="uiCommands">
<span class="uiCommand"><a href="?logout=1">Logout</a>
</span>
</div>
<? } ?>

<span class="icon"><a href="http://m.gps100.com"><img src="http://m.gps100.com/images/logo.gif"/></a>
</span>
</div>
</div>

<div id="pageTabs">
<table>
<tr>
<td class="pageTab<? if ($tab == 'directory') { echo ' ptactive'; } ?> first"><a class="inline" href="?tab=directory" id="tab-directory">

<span>Directory
</span></a>
</td>

<td class="pageTab<? if ($tab == 'newsfeed') { echo ' ptactive'; } ?>"><a class="inline" href="?tab=newsfeed" id="tab-newsfeed" x-bp-load-page="true">

<span>Feed
</span></a>
</td>

<td class="pageTab<? if ($tab == 'birthdays') { echo ' ptactive'; } ?> last"><a class="inline" href="?tab=birthdays" x-bp-load-page="true">

<span>Birthdays
</span></a>
</td>
</tr>
</table>
</div>
</div>

<? if ($tab == 'login') { ?>
<form name="loginform" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<input type="hidden" name="submitted" value="1">
<div class="uim bdr first">

<div class="hd first">
<table class="items">
<tr>
<td class="blocks">

<div class="uic first">LOGIN
</div>
</td>
</tr>
</table>
</div>

<div class="bd">

<div class="uip">
<? if ($error) { echo "<div class='small'>".$errormsg."</div>"; } ?>
<table class="template">
<tr>
<td class="input">
<input type="text" name="email" placeholder="email" style="width: 100%"><BR/>
<input type="password" name="password" placeholder="password" style="width: 100%"><BR/>
<input type="checkbox" name="remember_me" value="1"> <span class="small">Remember me</span><BR/>
<input type="submit" value="Login">
</td>
</tr>
</table>
</div>

</div>
</div>

</form>

<? } ?>

<? if ($tab == 'directory') { ?>

<div id="toolbar">

<div id="markets-sub-content" class="">

<form action="<?php echo $_SERVER['PHP_SELF']; ?>?tab=directory&directory_form=1" method="post" id="search-box-model" name="directory_form">

<div class="formField searchBox">
<table>
<tr>
<td class="input">
<? //<input type="text" name="userString" id="input-9" placeholder="Select Name"/>?>
<select name="userString" id="input-9" placeholder="Select Name"/ style="width:100%; margin-top:6px" onChange='update();'>
<?php
echo "<option value='0' SELECTED>** Please Select **</option>";
$sql = "SELECT id, CONCAT(first_name, ' ', last_name) fullname, contact_number_country_code, contact_number, email_address FROM members WHERE disabled != 1 AND LENGTH(contact_number) > 0 AND LENGTH(contact_number_country_code) > 0 ORDER BY first_name ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
if (substr($row['contact_number'], 0, 1) == '0') { $row['contact_number'] = substr($row['contact_number'], 1); }
$entry[] = $row;
echo "<option value='".$row['id']."'";
if ($row['id'] == $fetch_id) { echo " SELECTED"; }
echo ">".$row['fullname']."</option>";
}
?>
</select>
</td>
<td class="submit">
<input type="submit" class="submit" value="Search"/>
</td>
</tr>
</table>
</div>
</form>
</div>
</div>
<? } ?>
</div>

<div id="content">

<? if ($tab == 'directory' && $fetch['found']) { ?>

<div class="uim bdr first">

<div class="bd">

<div class="tabs first" id="tabs-19">

<div class="tabContents">

<div class="tabSection tabSectionShown">

<div class="group first">

<div class="uip first">
<table class="template single one-column">
<tr>
<td>
<table class="template">
<tr>
<td class="title-cell">

<div class="uic small first"> 

<span class="title"><? echo $fetch['fullname']; ?>
</span><br/> 

<span class="small"><a href="tel:+<? echo $fetch['contact_number_country_code'].$fetch['contact_number']; ?>">+<? echo $fetch['contact_number_country_code'].$fetch['contact_number']; ?></a><BR>
<a href="mailto:<?php echo $fetch['email_address']; ?>"><?php echo $fetch['email_address']; ?></a>
</span> 
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>

<div class="uip">
<table class="template single one-column">
<tr>
<td>
<table class="template">
<tr>
<td class="title-cell">

<div class="uic small"> 
<img src="http://www.gps100.com/
<?php
if (strlen($fetch['current_photo']) > 1) {
echo $fetch['current_photo'];
}
else {
echo "images/male.png";
}
?>
">
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>

</div>

<? if (strlen($fetch['employer']) > 0) { ?>

<div class="uip last">
<table class="items">
<tr>
<td class="blocks">

<div class="uic small first"> <strong><? echo $fetch['employer']; ?></strong> 
</div>

<div class="uic subdued small last"> 

<span class="small"><? echo $fetch['title']; ?>
</span> 
</div>
</td>
</tr>
</table>
</div>

<? } ?>

</div>

</div>
</div>
</div>
</div>

<? } ?>

<? if ($tab == 'directory' && !$fetch['found']) { ?>

<div class="uim bdr first">

<div class="bd">

<div class="tabs first" id="tabs-19">

<div class="tabContents">

<div class="tabSection tabSectionShown">

<div class="group first">

<div class="uip first">
<table class="template single one-column">
<tr>
<td>
<table class="template">
<tr>
<td class="title-cell">

<div class="uic small first"> 

<span class="small">Select a member from the dropdown menu...
</span>

</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>

</div>
</div>

</div>
</div>
</div>
</div>

<? } ?>

<? if ($tab == 'newsfeed') { ?>

<div class="uim bdr first">

<div class="hd first">
<table class="items">
<tr>
<td class="blocks">

<div class="uic first">RECENT ACTIVITY
</div>
</td>
</tr>
</table>
</div>

<div class="bd">

<?php

foreach ($feed as $key => $value) {

?>
<div class="uip">
<table class="template">
<tr>
<td>
<div class="thumbb"><a href='?tab=directory&fetchid=<?php echo $feed[$key]['uid']; ?>'><img src="<? echo $feed[$key]['thumb'] ?>"></a></div>

<div class="uic title article article-title first"><? echo $feed[$key]['name'] ?>
</div>


<div class="uic small first">

<span class="subdued"><? echo $feed[$key]['timetext'] ?>
</span> <? echo $feed[$key]['content'] ?>
</div>
</td>
</tr>
</table>
</div>

<?php
}
?>

</div>
</div>

<? } ?>

<? if ($tab == 'birthdays') { ?>

<?
$startmonth = date("n");
$endmonth = $startmonth + 1;
$startday = date("j");
$today = $startday;
$count = 0;
$endcount = $look_forward;
$month = $startmonth;
while($count < $endcount) {

if (strlen($month) < 2) { $month = "0" . $month; }
if (strlen($today) < 2) { $today = "0" . $today; }

//echo $month.$today."<BR>"; //uncomment to debug
if(isset($birthday[$month.$today])) {
?>
<div class="uim bdr first" id="anchor_birthdays">
<div class="hd first">
<table class="items">
<tr>
<td class="blocks">

<div class="uic first">
<?php echo "<b>".date("jS F",mktime(0,0,0,$month,$today,date("Y")))."</B>"; ?>
</div>
</td>
</tr>
</table>
</div>
<?
foreach ($birthday[$month.$today] as $i) {
$name = $i['first_name'] . " " . $i['last_name'];
?>
<div class="bd">

<div class="uip">
<table class="template single two-column">
<tr>
<td>
<table class="template">
<tr>
<td class="title-cell">

<div class="uic">
<?php echo "<a href='?tab=directory&fetchid=".$i['id']."'>".substr($name,0,30)."</a>"; ?>
</div>
</td>
<td class="value-cell">

<div class="uic"> <?php echo calculateAge($i['birth_date'])+1; ?>
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>

</div>


<?
}
echo "</div>";
}

if ($today == 28 && $month == 2) { $today = 0; $month++; $count++; }
elseif ($today == 30 && ($month == 4 || $month == 6 || $month == 9 || $month == 11)) { $today = 0; $month++; $count++; }
elseif ($today == 31 && ($month == 1 || $month == 3 || $month == 5 || $month == 7 || $month == 8 || $month == 10 || $month == 12)) { $today = 0; $month++; $count++; }
else { $count++; }
$today++;

}
?>

<? } ?>

<div class="uic small center"> <a class="inline" href="/w/yfinance/quotesdelayed/?.ts=1311450925&amp;.ysid=eeSwdLZcgFKFykPJw8vRoVRW&amp;.intl=BB&amp;.lang=en">
</a> 
</div>

<div class="uic title last center"> 

<span>View: Mobile |
</span> <a class="inline" href="http://www.gps100.com/portal/">

<span> Desktop
</span></a> 
</div>
</div>

<div id="pageFooter">

<div class="uim first last" id="defaultFooter">

<div class="bd">

<?php
/*

<div class="uic small"><a class="inline" href="#">

<span>Logout
</span></a>

</div>
*/
?>

<div class="uic small last">A Benjamin Wigoder Production
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">Bp.Script = Bp.Script || {};if(typeof YAHOO=="undefined"){YAHOO={}}if(!YAHOO.ULT){YAHOO.ULT={}}if(!YAHOO.ULT.BEACON){YAHOO.ULT.BEACON="http://geo.yahoo.com/t"}if(!YAHOO.ULT.IMG){YAHOO.ULT.IMG=new Image()}YAHOO.ULT.SRC_SPACEID_KEY="_S";YAHOO.ULT.DEST_SPACEID_KEY="_s";YAHOO.ULT.YLC_LIBSRC=2;YAHOO.ULT.CTRL_C="\x03";YAHOO.ULT.CTRL_D="\x04";YAHOO.ULT.BASE64_STR="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789._-";(function(){YAHOO.ULT.track_click=function(c,g){if(!c||!g){return c}g._r=YAHOO.ULT.YLC_LIBSRC;var h=[];var d=0;for(var b in g){var a=g[b];if(typeof(a)=="undefined"){a=g[b]=""}if(b.length<1){return c}if(b.length>8){return c}if(b.indexOf(" ")!=-1){return c}if(YAHOO.ULT.has_ctrl_char(b)||YAHOO.ULT.has_ctrl_char(a)){return c}h[d++]=b}h=h.sort();var e=[];for(d=0;d<h.length;d++){e[d]=h[d]+YAHOO.ULT.CTRL_C+g[h[d]]}e=e.join(YAHOO.ULT.CTRL_D);if(e.length<1||e.length>1024){return c}e=";_ylc="+YAHOO.ULT.encode64(e);d=c.indexOf("/*");if(d==-1){d=c.indexOf("/?")}if(d==-1){d=c.indexOf("?")}if(d==-1){return c+e}else{return c.substr(0,d)+e+c.substr(d)}};YAHOO.ULT.beacon_click=function(c,b){if(!b){b=YAHOO.ULT.IMG}if(c){var a=YAHOO.ULT.track_click(YAHOO.ULT.BEACON,c);a+="?t="+Math.random();b.src=a}return true};YAHOO.ULT.has_ctrl_char=function(b){for(var a=0;a<b.length;a++){if(b.charCodeAt(a)<32){return true}}return false};YAHOO.ULT.encode64=function(c){var a="";var k,h,f="";var j,g,e,d="";var b=0;do{k=c.charCodeAt(b++);h=c.charCodeAt(b++);f=c.charCodeAt(b++);j=k>>2;g=((k&3)<<4)|(h>>4);e=((h&15)<<2)|(f>>6);d=f&63;if(isNaN(h)){e=d=64}else{if(isNaN(f)){d=64}}a=a+YAHOO.ULT.BASE64_STR.charAt(j)+YAHOO.ULT.BASE64_STR.charAt(g)+YAHOO.ULT.BASE64_STR.charAt(e)+YAHOO.ULT.BASE64_STR.charAt(d);k=h=f="";j=g=e=d=""}while(b<c.length);return a}})();;(function(){Bp.get_script=function(){var a=arguments;return function(d,b){var e=b||new Bp.ExecContext(d);for(var c=0;c<a.length;c++){a[c].call(this,e)}if(!b){e.postTrack()}}}})();;(function(){Bp.EvHistory=Bp.EvHistory||{};Bp.assign_script=function(g,e,j,i,c,k){var f=Bp.get(g);if(typeof e!=="function"&&typeof j!=="function"&&typeof i!=="function"){return}var h=function(l){Bp.preventDefault(l);if(!c){Bp.stopPropagation(l)}if(typeof Bp.EvHistory[g]==="undefined"){if(typeof j==="function"){j.call(this,k)}Bp.EvHistory[g]="ac"}else{if(typeof i==="function"){i.call(this,k)}}if(typeof e==="function"){e.call(this,k)}};if(f){if(f.tagName=="DIV"&&f.className=="formField trigger"){var d=f.parentNode;while(d.tagName!="FORM"&&d.parentNode){d=d.parentNode}var b=d.getAttribute("action");var a=d.getAttribute("method");Bp.on(d,"submit",h)}else{if(f.tagName=="A"||f.tagName=="TD"||f.tagName=="TR"||f.tagName=="DIV"){if(f.tagName=="A"&&f.parentNode.tagName=="BUTTON"){Bp.on(f.parentNode,"click",h)}else{Bp.on(f,"click",h)}if(Bp.env.Apple&&f.tagName=="A"){f.removeAttribute("href")}}}}}})();;(function(){Bp.initTracking=function(b,a){Bp.TrackingConf={spaceid:b,beaconurl:a}};Bp.ExecContext=function(a){this._tracking={};this._trackingCnt=0;this._ult=a};Bp.ExecContext.prototype={track:function(a){for(var b in a){if(a.hasOwnProperty(b)){this._tracking[b]=a[b];this._trackingCnt++}}},postTrack:function(){if(this._trackingCnt===0){return}this._tracking[YAHOO.ULT.SRC_SPACEID_KEY]=Bp.TrackingConf.spaceid;if(typeof Bp.TrackingConf.beaconurl!=="undefined"&&Bp.TrackingConf.beaconurl!==""){var b=Bp.TrackingConf.beaconurl;if(this._ult){b=b+this._ult}var c=YAHOO.ULT.track_click(b,this._tracking);var a=new Image();a.src=c}else{YAHOO.ULT.beacon_click(this._tracking)}},addYLC:function(a){if(this._trackingCnt===0){return}return YAHOO.ULT.track_click(a,this._tracking)},cleanup:function(){this._trackingCnt=0;this._tracking={}}}})();;Bp.initTracking("954001073","http://m.yahoo.com/be");(function(){Bp.Timers=Bp.Timers||{};Bp.TimersNN=Bp.TimersNN||[];Bp.Timer=function(d,a,b,c){if(d){Bp.Timers[d]=this}else{Bp.TimersNN.push(this)}this._acts=b;this._inter=a*1000;if(c){this.startTimer()}};Bp.Timer.prototype={startTimer:function(){if(!this._active){this._uid=setInterval(this._acts,this._inter)}this._active=true},stopTimer:function(){if(this._active){clearInterval(this._uid)}this._active=false;this._enbl=false}}})();;(function(){var b="http://l.yimg.com/a/i/w/go/web/spinner-1.0.0.gif",a="https://s.yimg.com/lq/i/w/go/web/spinner-1.0.0.gif";Bp.PageTabs=function(c){this._tabs=Bp.get(c);Bp.on(this._tabs,"click",this._handleChange,this);this._spinner=(window.location.protocol==="https:")?a:b;(new Image()).src=this._spinner};Bp.PageTabs.prototype={_spinner:"",_tabs:null,_handleChange:function(j){if(j.target.tagName!="A"&&j.target.tagName!="SPAN"){return}var m=j.target;while(m.tagName!="TD"&&m.parentNode){m=m.parentNode}var k=m.getElementsByTagName("a");if(k&&k[0].getAttribute("x-bp-load-page")==="true"){return}if(Bp.dom.hasClass(m,"pageTab")&&!Bp.dom.hasClass(m,"ptactive")){var g=Bp.get("toolbar");var f=m.firstChild.id.substring(4);var l=m.parentNode.getElementsByTagName("td");for(var h=0;h<l.length;h++){var c=l[h].firstChild.id.substring(4);var d=Bp.get(c+"-sub-content");if(m===l[h]){Bp.dom.addClass(l[h],"ptactive");if(typeof(Bp.TabsSettings)!=="undefined"&&Bp.TabsSettings._sendTabs.length>0){for(var h=0;h<Bp.TabsSettings._sendTabs.length;h++){if(Bp.TabsSettings._sendTabs[h].event==="first-activate"){Bp.get("content").innerHTML="

<div class='loading-spinner'><img src='"+this._spinner+"' alt='' />
</div>"}}}if(d){Bp.dom.removeClass(d,"sc-hidden");Bp.dom.removeClass(g,"hidden")}else{if(g){Bp.dom.addClass(g,"hidden")}}}else{Bp.dom.removeClass(l[h],"ptactive");if(d){Bp.dom.addClass(d,"sc-hidden")}}}}else{if(Bp.dom.hasClass(m,"tabLink")&&!Bp.dom.hasClass(m,"active")){var l=m.parentNode.getElementsByTagName("td");for(var h=0;h<l.length;h++){if(m==l[h]){Bp.dom.addClass(l[h],"active")}else{Bp.dom.removeClass(l[h],"active")}}}}}}})();;new Bp.PageTabs('pageTabs');Bp.DefaultInput=function(a){this._el=Bp.get(a);if(arguments.length>1){this._dt=arguments[1]}else{this._dt=this._el.getAttribute("placeholder")}this._blur();Bp.on(this._el,"blur",this._blur,this);Bp.on(this._el,"focus",this._focus,this);var c=this._el.parentNode;while(c.tagName!="FORM"&&c.parentNode){c=c.parentNode}var b=Bp.DefaultInputForm.GetInstance(c);b.addInput(this)};Bp.DefaultInput.prototype={_el:null,_dt:null,_dClass:"disabled",_blur:function(){if(this._el.value==""||this.isDefault()){this._el.value=this._dt;Bp.dom.addClass(this._el,this._dClass)}},_focus:function(){if(this._el.value==""||this.isDefault()){this.blank();Bp.dom.removeClass(this._el,this._dClass)}},isDefault:function(){return this._el.value==this._dt},blank:function(){this._el.value=""}};Bp.DefaultInputForm=function(a){this._formEl=a;this._defaultInputs=[];Bp.on(this._formEl,"submit",this._onSubmit,this);this._formEl.defaultInputForm=this};Bp.DefaultInputForm.prototype={_defaultInputs:null,_formEl:null,_onSubmit:function(b){if(this._defaultInputs.length==0){return}for(var a=0;a
<this._defaultInputs.length;a++){if(this._defaultInputs[a].isDefault()){this._defaultInputs[a].blank()}}},addInput:function(a){if(typeof(a)=="object"){this._defaultInputs.push(a)}}};Bp.DefaultInputForm.GetInstance=function(a){a=Bp.get(a);if(typeof(a.defaultInputForm)!="undefined"){return a.defaultInputForm}return new Bp.DefaultInputForm(a)};;new Bp.DefaultInput( 'input-9' );Bp.Tabs=function(b){this._container=Bp.get(b.id);this._id=b.id;var d=this._container.firstChild.getElementsByTagName("td");this._tabs=[];this._tabContents=[];for(var a=0;a<d.length;a++){if(!d[a].id){continue}var c=d[a].id.split("-");this._tabs.push(d[a]);this._tabContents.push(Bp.get(c.slice(0,c.length-2).join("-")+"-tab-content-"+this._id))}Bp.on(this._container.firstChild,"click",this._tabChange,this)};Bp.Tabs.prototype={_id:"",_container:null,_tabs:null,_tabContents:null,_tabChange:function(g){var b=g.target;while(b.parentNode&&b.tagName!="TD"){b=b.parentNode}var f=b.getElementsByTagName("a");if(f&&f[0].getAttribute("x-bp-load-page")=="true"){return}Bp.preventDefault(g);var h=this._container.firstChild.getElementsByTagName("td"),a=0;for(var c=0;c<h.length;c++){if(!Bp.dom.hasClass(h[c],"tabLink")){continue}if(h[c]==b){a=c;Bp.dom.addClass(h[c],"active")}else{Bp.dom.removeClass(h[c],"active")}}var d=this._container.lastChild.firstChild,c=0;while(d){if(c==a){Bp.dom.addClass(d,"tabSectionShown")}else{Bp.dom.removeClass(d,"tabSectionShown")}c++;d=d.nextSibling}}};;new Bp.Tabs({ id: 'tabs-19' });
</script>
</body>
</html>