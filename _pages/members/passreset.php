<div><img src="/content_files/headers/members.gif" width="800" height="90"></div>
<div>
	<h2>Password Reset</h2>

<% if $request_reset == 1 %>
<% if $error == 1 %>
<p style="color:red"><% $error_message %></p>
<% /if %>
<% if $email_success != 1 %>
<p>Please enter your GPS email address to request a password reset.</p>
<form id="request_reset" action="passreset.php?request_reset=1" method="post">
<input type="hidden" name="request_reset" value="1">
<input type="text" id="email" name="email" value="<% $r_email %>" style="width: 180px"><BR>
<input type="submit" value="Submit">
</form>
<% else %>
<p>Email found - please check your email for further instructions.</p>
<% /if %>
<% else %>

	<p style="color: #FF0000; text-align: left;">
		<% $error_message %>
	</p>
<% if $success == 1 %>
<p>
Password successfully updated.
</p>
<% elseif $code_match == 1 %>

	<form id="form" action="<% $php_self %>?code=<% $code %>&email=<% $email %>" method="post" enctype="multipart/form-data">
		<fieldset>
		<legend>Password Change</legend>
<div style="float:left">
		<p style="float: left;">New Password:<br />
			<input type="password" id="new_password" name="new_password" value="<% $new_password %>" style="width: 230px;" />
		</p>
		<br style="clear: both;" />
		<p style="float: left;">New Password Again:<br />
			<input type="password" id="new_password_2" name="new_password_2" value="<% $new_password_2 %>" style="width: 230px;" />
		</p>
</div>
<div style="float:left; margin-left: 30px; text-align: center; border: 1px dotted black; padding: 4px">
<b>Rules</b><BR>
6 - 14 characters<BR>
1+ lower case letters<BR>
1+ upper case letters<BR>
1+ numbers
</div>
		</fieldset>
<input type="submit" value="Submit New Password">
	</form>

<% else %>

To reset your password you'll need a valid code.

<% /if %>

<% /if %>

</div>