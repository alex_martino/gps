<div><img src="/content_files/headers/members.gif" width="800" height="90" alt="members" /></div>
<div>
	<h2>GPS Member Login</h2>

<% if $session_expired == 1 %>
Sorry, your session has expired. Please log in again.
<% /if %>

<% if $update_required == "true" %>
You are required to update your password. Please check your email for further instructions.<BR>
If you have trouble please contact: admin@gps100.com.
<% else %>

	<p style="color: #FF0000; text-align: center;"><% $error_message %></p>
	<form id="form" action="<% $php_self %>?referrer=<% $referrer %>" method="post">
		<table align="center">
		<tr>
		<td align="right"><strong>Email Address:</strong></td>
		<td><input type="text" id="l_email_address" name="email_address" value="<% $smarty.post.email_address %>" />
		</td>
		</tr>
		<tr>
		<td align="right"><strong>Password:</strong></td>
		<td><input type="password" id="l_password" name="password" value="<% $smarty.post.password %>" onKeyPress="return submitenter(this,event)" />
		</td>
		</tr>
		<tr>
		<td>&nbsp;</td>
		<td style="padding-top: 2px; padding-bottom: 2px"><input type="checkbox" name="remember_me" value="1" onKeyPress="return submitenter(this,event)" /> Remember me</td>
		</tr>
		<tr>
		<td>&nbsp;</td>
		<td><a href="javascript: submitform('form');" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('login','','/content_files/images/buttons/login_over.gif',1)"><img name="login" border="0" src="/content_files/images/buttons/login_out.gif" width="57" height="17" alt="login" /></a></td>
		</tr>
		</table>
		<input type="hidden" id="action" name="action" value="login" /><br>
		<div style="text-align: center">(<a href="/members/passreset.php?request_reset=1">Forgotten Password?</a>)</div>
	</form>

<% /if %>
</div>