<div><img src="/content_files/headers/mission_statement.gif" width="800" height="90"></div>
<div>
	<h2>Mission Statement</h2>
	<p>Global Platinum Securities&trade; will  strive to provide a comprehensive investment education to the future generation  of passionate and intellectually curious business leaders in an ethical and  socially responsible fashion.</p>
	<h2>The GPS Creed</h2>
	<p>We believe that ethics should take absolute precedence <br />
		<img src="/images/pixel.png" width="20" height="10" align="left"> and profit at the price of integrity is no profit at all.</p>
	<p>We believe that those who have been blessed with prosperity <br />
		<img src="/images/pixel.png" width="20" height="10" align="left">should 
		help those in need <br />
		<img src="/images/pixel.png" width="20" height="10" align="left">and 
		that donating time and knowledge <br />
		<img src="/images/pixel.png" width="20" height="10" align="left">is 
		more important than simply writing a check.</p>
	<p>We believe that learning through experience <br />
		<img src="/images/pixel.png" width="20" height="10" align="left">is 
		just as important as learning the theory <br />
		<img src="/images/pixel.png" width="20" height="10" align="left">and 
		that the process of learning <br />
		<img src="/images/pixel.png" width="20" height="10" align="left">should 
		be interesting and intellectually engaging.</p>
	<p>We believe that through friendship, <br />
		<img src="/images/pixel.png" width="20" height="10" align="left">we 
		can learn from each other, <br />
		<img src="/images/pixel.png" width="20" height="10" align="left">and 
		through mentorship <br />
		<img src="/images/pixel.png" width="20" height="10" align="left">wisdom 
		can be passed down so youth can learn from history.</p>
	<p>We believe that people should be active investors <br />
		<img src="/images/pixel.png" width="20" height="10" align="left">having 
		input in their investments <br />
		<img src="/images/pixel.png" width="20" height="10" align="left">learning 
		about the companies they invest in <br />
		<img src="/images/pixel.png" width="20" height="10" align="left">learning 
		about the industries they invest in <br />
		<img src="/images/pixel.png" width="20" height="10" align="left">purchasing 
	the underlying business rather than the stock.</p>
	<p>We believe that by doing what we love <br />
		<img src="/images/pixel.png" width="20" height="10" align="left">by 
		being well-informed <br />
		<img src="/images/pixel.png" width="20" height="10" align="left">by 
		being well-educated <br />
		<img src="/images/pixel.png" width="20" height="10" align="left">by 
		doing due diligence we can profit.<br />
	</p>
</div>
