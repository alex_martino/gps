<div><img src="/content_files/headers/philanthropy.gif" alt="" width="800" height="90" /></div>
<div>
<p>Past events have ranged from visiting inner-city schools to partnering up with local charity organizations to teach students and individuals about basic financial concepts. These events include empowering individuals with valuable life tools such as how to obtain student loans and financial aid for college, saving and budgeting for the homeless to get back on track, and stock simulations at elementary schools to teach basic stock knowledge for those who are intellectually curious from an early age.</p>
<p>The events put on by the pods this past year were:</p>
<ul>
<li>Colorado gave a presentation to men in a transitional community in Denver on basic financial planning and skills.</li>
<li>Harvard and MIT gave a powerpoint presentation teaching elementary school students the basics of investing, the stock market, how to invest, and the current market conditions today</li>
<li>NYU gave a presentation to students in Long Island on the basics of commerce</li>
<li>LSE presented to 150 7th grade students to highlight how their school could be viewed as a business and the skills necessary to succeed now and in the future</li>
</ul>
</div>