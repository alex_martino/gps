<div><img src="/content_files/headers/recruiting.gif" alt="" width="800" height="90" /></div>
<div>
<h1>Apply Online
<% if $cycles != 'closed' %>
- Stage 2/4</h1>
<BR>
<p class="btxt"><b>Covering letter</b>
<% if $coverlimit > 0 %>
(max <% $coverlimit %> words)
<% /if %>
<BR>
<% if $error != 0 %>
<% $errormsg %>
<% /if %>
<form name="app2" action="<% $phpself %>" method="post">
<textarea name="coverletter" style="width: 600px; height: 200px" onkeyup="cnt(this,document.app2.c)"><% $coverletter %></textarea>
<p class="btxt" style="margin-top:10px">
Total words: <input type="text" name="c" value="0" onkeyup="cnt(document.app2.coverletter,this)"
style="border:0; background: #EDEDED" readonly><BR>
<a href="#" onClick="javascript:document.app2.submit();">Continue to next stage</a> | <a href="app_delete.php">Cancel
application</a>
</p>
</form>

<% else %>
</h1>
We are not currently accepting applications. Please check back later.
<% /if %>

</div>
<script>cnt(document.app2.coverletter,document.app2.c);</script>