<div><img src="/content_files/headers/analyst_program.gif" alt="" width="800" height="90" /></div>
<div>
<p>The GPS education program is designed to introduce analysts to basic business and financial concepts, with a particular focus on evaluating the prospects of the common stock of publicly listed enterprises. Over the summer, GPS analysts learn about the qualitative and quantitative aspects of business through a variety of avenues including reading materials, conference calls with industry professionals, and interaction with fellow analysts and GPS members. Key concepts covered during the program include:</p>
<p>
<ul>
<li>Financial ratios&nbsp;</li>
<li>Basic accounting principles (including balance sheet, income statement, and cash flow statement analysis)&nbsp;</li>
<li>Porter&rsquo;s Five Forces&nbsp;</li>
<li>SWOT analysis</li>
<li>Discounted cash flow analysis</li>
</ul>
</p>
<p>These concepts are used to analyze and understand businesses qualitatively in preparation for determining their value based on quantitative methodologies. Within sector groups, analysts apply what they learn during the program to public companies with the aid of SEC (Securities and Exchange Commission) filings (i.e. 10-K, 10-Q, etc.), as well as other publicly available resources including press releases, industry reports, and government-generated statistics.</p>
<p>While the program focuses extensively on printed material for education, one of the greatest resources for education is the depository of knowledge contained by fellow analysts and members. Interaction between members and analysts creates an environment conducive to learning and makes for an invaluable summer experience.</p>
</div>