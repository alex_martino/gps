<div><img src="/content_files/headers/recruiting.gif" alt="" width="800" height="90" /></div>
<div>
<p>Global Platinum Securities recruits freshmen and sophomore analysts from its eight chapter schools to participate in a ten-week analyst program. Upon culmination of the program, successful analysts are invited to become full-time members. The analyst program is highly selective and seeks candidates that exhibit unique skill sets, leadership capabilities, intellectual curiosity, and a dynamic personality.</p>
<p>Recruitment for our US-based chapter schools, which occurs during the spring semester, follows the timeline outlined below:</p>
<ul>
	<li><b>On-campus information sessions:</b> Early February</li>
	<li>Resume and Cover Letter Deadlines by School:</li>
			<ul>
				<% foreach from=$universities item=i %>
				<li><b><% $i.SNAME %>:</b> <% $i.RECRUITING_DEADLINE|date_format:"%A, %e %B %Y" %></li>
				<% /foreach %>
			</ul>
	<li><b>First and second round interviews:</b> Mid-Late February</li>
	<li><b>Decisions made by the end of February</b></li>
</ul>
<p><a href="/contact/recruiting.php">For more information please see the contact page for recruiting</a>.</p>
</div>
