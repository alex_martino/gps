<div><img src="/content_files/headers/booklist.gif" width="800" height="90"></div>
<div>
	<style>
	#sortable { list-style-type: none; margin: 0; padding: 0; width: 758px; }
	#sortable li { list-style-image: none; margin: 0 0 10px 0; font-size: 1.4em; height: 55px }
	#sortable div.position { float: left; width: 50px; text-align: center; font-size: 2em; background: #091c5a; color: #fff; padding-top: 7px; height: 48px }
	#sortable div.bookname { float: left; margin: 18px 0 0 30px; width: 240px }
	#sortable div.actions { float: left; width: 320px; margin: 0 0 0 10px }
	#sortable div.actions div.text { float: left; margin: 22px 0 0 10px; font-size: 12px; color: #091c5a }
	#sortable div.actions div.text a { color: #516683; text-decoration: underline }
	.bookworm { width: 50px; height: 50px; margin: 2px 0 0 8px; float: left }
	#sortable div.who { float: left; padding: 2px 0 0 0; width: 104px }
	#sortable div.who img { float: left; width: 25px; height: 25px; margin: 0 1px 1px 0 }
	#sortable div.who .more { float: left; font-size: 11px; margin: 6px 0 0 4px }
	</style>
	<script>
	$(function() {
		$( "#sortable" ).sortable({ disabled:true });
		$( "#sortable" ).disableSelection();
	});
	</script>
<div style="height: 10px">&nbsp;</div>
<div style="float: left; padding: 5px 0 0 0; font-size: 15px; font-weight: bold"><% $smarty.session.user_first_name %>'s Top 10 Progress:
</div>
<div class="progress-outer" style="float: left; margin: 0 0 0 20px"><div class="progress-inner" style="width: <% $topcount %>%"><% $topcount %>%</div></div>
<div style="float: left; margin: 5px 0 0 0; text-align: right; width: 225px"><a href="/portal/network/view/interests.php?id=<% $smarty.session.user_id %>">Your other interests...</a></div>
<div style="clear: both; height: 30px">&nbsp;</div>

<ul id="sortable">
<% foreach from=$booklist key=k item=i %>
	<li class="ui-state-default">
		<div class="position"><% $k+1 %></div>
		<div class="bookname"<% if $i.name|strlen > 29 %> style="margin: 8px 0 0 30px;"<% /if %>>
			<a type="amzn" search="<% $i.name %>" category="books">
				<% $i.name %>
			</a>
			</div>
		<div class="actions">
		<% if ($i.read_flag) %>
		<img src="/images/book-worm.gif" class="bookworm">
		<div class="text">You've read this!</div>
		<% else %>
		<div class="text"><a href="booklist.php?add_to_list=<% $i.name|urlencode %>">I've read this too!</a></div>
		<% /if %>
		</div>
		<div class="who">
		<% assign var="users" value=","|explode:$i.users %>
		<% assign var="users_fullnames" value=","|explode:$i.user_fullname %>
		<% assign var="more" value=0 %>
		<% foreach from=$users key=l item=uid %>
		    <% if $l < 5 %>
		    <a href="/portal/network/view/interests.php?id=<% $uid %>">
		    <img src="/content_files/member_photos/<% $uid %>-cropped.jpg" alt="" title="<% $users_fullnames.$l %>">
		    </a>
		    <% else %>
		    <% assign var="more" value=$more+1 %>
		    <% /if %>
		<% /foreach %>
		<div class="more">
			<a href="javascript:void(0);" onClick="popup_display_users('<% foreach from=$users item=j %>,<% $j %><% /foreach %>','Readers of <% $i.name %>');">
				<% if ($more > 1) %>
				+ <% $more %> others
				<% elseif ($more > 0) %>
				+ 1 other
			<% else %>
				<div style="float: left; width: 74px; text-align: right"><% $users|@count %> total</div>
		    <% /if %>
		    </a>
		    </div>
		</div>
	</li>
<% /foreach %>
</ul>

</div>