<div><img src="/content_files/headers/payments.gif" width="800" height="90"></div>
<div>
<% if ($invoice_count > 0) %>
<h2>Summary</h2>
You are making <b>$<% $payment_total %></b> payment for <b><% $description %></b>.

<BR><BR>

<h2>Payment Options</h2>

<img src="/images/payment_options_exp.png" style="width: 384px; height: 34px; margin: 10px 0 0 0">

<BR><BR>

<% if ($active_card) %>

<h2>Confirm card details</h2>

<i>If you would like to use a different card please <a href="<% $smarty.server.PHP_SELF %>?id=<% $invoice_list %>&new_card=1">click here</a>.</i>

<BR><BR>

<form name="card-exists-payment-form" action="<% $smarty.server.PHP_SELF %>?id=<% $invoice_list %>" method="post" id="card-exists-payment-form" onsubmit="if (this.getAttribute('submitted')) return false; this.setAttribute('submitted','true'); document.getElementById('submit-button').disabled = true;">
<input type="hidden" name="saved_card" value="1">

<div style="float: left; line-height: 1.5">
Card type:<BR>
Card number:<BR>
Expiry date:<BR>
Security code:
</div>

<div style="float: left; margin: 0 0 0 5px; line-height: 1.5; font-weight: bold">
<% $active_card.type %><BR>
XXXX-XXXX-XXXX-<% $active_card.last4 %><BR>
<% $active_card.expiry_month %>-<% $active_card.expiry_year %><BR>
<input type="text" name="security_code" id="security_code" value="" autocomplete="Off" style="width: 32px" maxlength=4>
<% if ($error_s.security_code) %> <b style="color: red; font-weight: normal; margin: 0 0 0 8px">Please enter a valid security code</b><% /if %>
</div>

<div style="clear:both; padding: 15px 0 0 0">
<button type="submit" name="submit-button" id="submit-button">Confirm Payment</button>
</form>

<script>document.getElementById('security_code').focus();</script>

<% else %>

<h2>Add new card</h2>

<% if $smarty.get.new_card %>
<i>If you would like to use a previous card please <a href="<% $smarty.server.PHP_SELF %>?id=<% $invoice_list %>">click here</a>.</i>
<BR><BR>
<% /if %>

      <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
        <script type="text/javascript" src="https://js.stripe.com/v1/"></script>
        <script type="text/javascript">
    // this identifies your website in the createToken call below
Stripe.setPublishableKey('pk_6cpoz6uoTE5ESmnq3sDS1TQJeSe8d');

            $(document).ready(function() {
                function addInputNames() {
                    // Not ideal, but jQuery's validate plugin requires fields to have names
                    // so we add them at the last possible minute, in case any javascript 
                    // exceptions have caused other parts of the script to fail.
                    $(".card-number").attr("name", "card-number")
                    $(".card-cvc").attr("name", "card-cvc")
                    $(".card-expiry-year").attr("name", "card-expiry-year")
                }

                function removeInputNames() {
                    $(".card-number").removeAttr("name")
                    $(".card-cvc").removeAttr("name")
                    $(".card-expiry-year").removeAttr("name")
                }

                function submit(form) {
                    // remove the input field names for security
                    // we do this *before* anything else which might throw an exception
                    removeInputNames(); // THIS IS IMPORTANT!

                    // given a valid form, submit the payment details to stripe
                    $(form['submit-button']).attr("disabled", "disabled")

                    Stripe.createToken({
                        number: $('.card-number').val(),
                        cvc: $('.card-cvc').val(),
                        exp_month: $('.card-expiry-month').val(), 
                        exp_year: $('.card-expiry-year').val()
                    }, 100, function(status, response) {
                        if (response.error) {
                            // re-enable the submit button
                            $(form['submit-button']).removeAttr("disabled")
        
                            // show the error
                            $(".payment-errors").html(response.error.message);

                            // we add these names back in so we can revalidate properly
                            addInputNames();
                        } else {
                            // token contains id, last4, and card type
                            var token = response['id'];

                            // insert the stripe token
                            var input = $("<input name='stripeToken' value='" + token + "' style='display:none;' />");
                            form.appendChild(input[0])

                            // and submit
                            form.submit();
                        }
                    });
                    
                    return false;
                }
                
                // add custom rules for credit card validating
                jQuery.validator.addMethod("cardNumber", Stripe.validateCardNumber, "Please enter a valid card number");
                jQuery.validator.addMethod("cardCVC", Stripe.validateCVC, "Please enter a valid security code");
                jQuery.validator.addMethod("cardExpiry", function() {
                    return Stripe.validateExpiry($(".card-expiry-month").val(), 
                                                 $(".card-expiry-year").val())
                }, "Please enter a valid expiration");

                // We use the jQuery validate plugin to validate required params on submit
                $("#payment-form").validate({
                    submitHandler: submit,
                    rules: {
                        "card-cvc" : {
                            cardCVC: true,
                            required: true
                        },
                        "card-number" : {
                            cardNumber: true,
                            required: true
                        },
                        "card-expiry-year" : "cardExpiry" // we don't validate month separately
                    }
                });

                // adding the input field names is the last step, in case an earlier step errors                
                addInputNames();
            });
        </script>
<style>
.form-row { height: 25px }
#payment-form label { float: left; margin: 2px 0 0 0 }
#payment-form input, #payment-form select { margin: 0 5px 0 5px }
</style>
       <form action="<% $smarty.server.PHP_SELF %>?id=<% $invoice_list %>" method="post" id="payment-form" style="display: none;">

            <div class="form-row">
                <label>Card Number</label>
                <input type="text" maxlength="20" autocomplete="off" class="card-number stripe-sensitive required" />
            </div>
            
            <div class="form-row">
                <label>CVC</label>
                <input type="text" maxlength="4" autocomplete="off" class="card-cvc stripe-sensitive required" style="width: 35px" />
            </div>
            
            <div class="form-row">
                <label>Expiration</label>
                <div class="expiry-wrapper">
                    <select class="card-expiry-month stripe-sensitive required">
                    </select>
                    <script type="text/javascript">
                        var select = $(".card-expiry-month"),
                            month = new Date().getMonth() + 1;
                        for (var i = 1; i <= 12; i++) {
                            select.append($("<option value='"+i+"' "+(month === i ? "selected" : "")+">"+i+"</option>"))
                        }
                    </script>
                    <span> / </span>
                    <select class="card-expiry-year stripe-sensitive required"></select>
                    <script type="text/javascript">
                        var select = $(".card-expiry-year"),
                            year = new Date().getFullYear();

                        for (var i = 0; i < 12; i++) {
                            select.append($("<option value='"+(i + year)+"' "+(i === 0 ? "selected" : "")+">"+(i + year)+"</option>"))
                        }
                    </script>
                </div>
            </div>
<BR>
            <button type="submit" name="submit-button">Confirm Payment</button>
            <span class="payment-errors"></span>
        </form>

        <!-- 
            The easiest way to indicate that the form requires JavaScript is to show
            the form with JavaScript (otherwise it will not render). You can add a
            helpful message in a noscript to indicate that users should enable JS.
        -->
        <script>if (window.Stripe) $("#payment-form").show()</script>
        <noscript><p>JavaScript is required for the registration form.</p></noscript>
<% /if %>

<div style="clear: both; margin: 12px 0 0 0">&nbsp;</div>

<div style="border-top: 1px solid navy; padding: 5px 0">
<B>Security:</b> Your credit card information is securely sent directly to the payment processor using 256-bit SSL encryption. It is <b>never</b> stored on GPS servers. We use tokenization to retrieve your previously used cards.
</div>

<% else %>
<h2>Unable to locate invoices</h2>
No unpaid invoices found, please wait...<BR><BR>
<a href="https://www.gps100.com/portal/payments/">Click here</a> if you are not automatically redirected.
<script>
function redirect() {
window.location = "https://www.gps100.com/portal/payments/";
}
setTimeout(redirect,5000);
</script>
<% /if %>

</div>