<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>

<% if !$popup %>

<script>
function showlog(id) {
window.location='activity_log.php?popup='+id;
}

	function toggle(fields) {
		t_fields = new Array();
		t_fields = fields.split(",");
		boxnum = t_fields.length;
		if(document.getElementsByName(t_fields[0])[0].checked) {
for (var i = 1; i < boxnum; i++) { document.getElementsByName(t_fields[i])[0].checked = true; }
		}
		else {
for (var i = 1; i < boxnum; i++) { document.getElementsByName(t_fields[i])[0].checked = false; }
		}
	}
</script>

<h2>Activity Log</h2>
<% if $error > 0 %>
<div style="color:navy">
<% $errormsg %>
</div>
<BR>
<% /if %>

<p class="note">Click on a log for more details...</p>

<% $pagination %>
<BR>
<table class="admin_table" style="width: 700px; border: 1px solid black">
<tr style="border-bottom: 1px solid black">
<th>Date</th>
<th>Time</th>
<th>Type</th>
<th>User</th>
<th>IP Address</th>
</tr>
<% foreach from=$logs item=i key=id %>
<tr class="row" onClick="showlog(<% $i.id %>);">
<td><% $i.time|date_format:"%D" %></td>
<td><% $i.time|date_format:"%H:%M:%S" %></td>
<td><% $i.type|truncate:20:"...":true %></td>
<td><% if $i.user %><% $i.user|truncate:20:"...":TRUE:TRUE %><% else %>-<% /if %></td>
<td><% $i.ip_address|truncate:20:"...":true %></td>
</tr>
<% /foreach %>
</table>
<BR>
<h2>Filters</h2>
<BR>
<form name="filterform" action="activity_log.php" method="GET">
<input type="hidden" name="submitted" value="1">
<div class="searchmenu" id="searchmenu">

<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="admin" value=1 checked onClick="toggle('admin,r[admin_add],r[admin_delete],r[admin_loginas],r[admin_user_add]');"> Admin</div>
	<div class="submenu">
	<input type="checkbox" name="r[admin_add]" value=1 checked> Add<BR>
	<input type="checkbox" name="r[admin_delete]" value=1 checked> Delete<BR>
	<input type="checkbox" name="r[admin_loginas]" value=1 checked> Log In As<BR>
	<input type="checkbox" name="r[admin_user_add]" value=1 checked> User Add
	</div>
</div>
</div>

<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="db" value=1 checked onClick="toggle('db,r[db_download],r[db_edit],r[db_search]');"> Database</div>
	<div class="submenu">
	<input type="checkbox" name="r[db_download]" value=1 checked> DB Download<BR>
	<input type="checkbox" name="r[db_edit]" value=1 checked> DB Edit<BR>
	<input type="checkbox" name="r[db_search]" value=1 checked> DB Search
	</div>
</div>
</div>

<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="dms" value=1 checked onClick="toggle('dms,r[dms_file_delete],r[dms_file_download],r[dms_file_upload],r[dms_folder_create],r[dms_folder_delete]');"> Documents</div>
	<div class="submenu">
	<input type="checkbox" name="r[dms_file_delete]" value=1 checked> File Delete<BR>
	<input type="checkbox" name="r[dms_file_download]" value=1 checked> File Download<BR>
	<input type="checkbox" name="r[dms_file_upload]" value=1 checked> File Upload<BR>
	<input type="checkbox" name="r[dms_folder_create]" value=1 checked> Folder Create<BR>
	<input type="checkbox" name="r[dms_folder_delete]" value=1 checked> Folder Delete
	</div>
</div>
</div>

<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="research" value=1 checked onClick="toggle('research,r[research_add],r[research_delete],r[research_update]');"> Research</div>
	<div class="submenu">
	<input type="checkbox" name="r[research_add]" value=1 checked> Add<BR>
	<input type="checkbox" name="r[research_delete]" value=1 checked> Delete<BR>
	<input type="checkbox" name="r[research_update]" value=1 checked> Update
	</div>
</div>
</div>

<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="resume" value=1 checked onClick="toggle('resume,r[resume_download],r[resume_search],r[resume_upload]');"> Resumes</div>
	<div class="submenu">
	<input type="checkbox" name="r[resume_download]" value=1 checked> Download<BR>
	<input type="checkbox" name="r[resume_search]" value=1 checked> Search<BR>
	<input type="checkbox" name="r[resume_upload]" value=1 checked> Upload
	</div>
</div>
</div>

<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="survey" value=1 checked onClick="toggle('survey,r[survey_add],r[survey_download],r[survey_vote]');"> Surveys</div>
	<div class="submenu">
	<input type="checkbox" name="r[survey_add]" value=1 checked> Add<BR>
	<input type="checkbox" name="r[survey_download]" value=1 checked> Download<BR>
	<input type="checkbox" name="r[survey_vote]" value=1 checked> Vote
	</div>
</div>
</div>

<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="user" value=1 checked onClick="toggle('user,r[user_login],r[user_login_fail],r[profile_update],r[user_pw_change],r[user_pw_reset_req],r[user_login_remember_me]');"> Users</div>
	<div class="submenu">
	<input type="checkbox" name="r[user_login]" value=1 checked> Login<BR>
	<input type="checkbox" name="r[user_login_fail]" value=1 checked> Login Fail<BR>
	<input type="checkbox" name="r[profile_update]" value=1 checked> Profile Update<BR>
	<input type="checkbox" name="r[user_pw_change]" value=1 checked> PW Change<BR>
	<input type="checkbox" name="r[user_pw_reset_req]" value=1 checked> PW Reset Request<BR>
	<input type="checkbox" name="r[user_login_remember_me]" value=1 checked> Remember Me Login
	</div>
</div>
</div>

<div style="clear: both"><span class="button white strong"><input type="submit" value="Update Filters"></span></div>

</div>
</form>

<script>
// set up check boxes
count = document.filterform.elements.length;
    for (i=0; i < count; i++) 
	{
document.filterform.elements[i].checked = 0;
}

var somethingchecked = 0;

<% foreach from=$fieldsi item=i key=key %>
document.getElementsByName('r[<% $i %>]')[0].checked = true;
somethingchecked = 1;
<% /foreach %>

if (somethingchecked != 1) {
    for (i=0; i < count; i++) 
	{
document.filterform.elements[i].checked = 1;
}
}

//check boss checkboxes if children are checked
if(document.getElementsByName('r[admin_add]')[0].checked && document.getElementsByName('r[admin_delete]')[0].checked && document.getElementsByName('r[admin_loginas]')[0].checked && document.getElementsByName('r[admin_user_add]')[0].checked) {
document.getElementsByName('admin')[0].checked = true
}
if(document.getElementsByName('r[db_download]')[0].checked && document.getElementsByName('r[db_edit]')[0].checked && document.getElementsByName('r[db_search]')[0].checked) {
document.getElementsByName('db')[0].checked = true
}
if(document.getElementsByName('r[dms_file_delete]')[0].checked && document.getElementsByName('r[dms_file_download]')[0].checked && document.getElementsByName('r[dms_file_upload]')[0].checked && document.getElementsByName('r[dms_folder_create]')[0].checked && document.getElementsByName('r[dms_folder_delete]')[0].checked) {
document.getElementsByName('dms')[0].checked = true
}
if(document.getElementsByName('r[research_add]')[0].checked && document.getElementsByName('r[research_delete]')[0].checked && document.getElementsByName('r[research_update]')[0].checked) {
document.getElementsByName('research')[0].checked = true
}

if(document.getElementsByName('r[resume_download]')[0].checked && document.getElementsByName('r[resume_search]')[0].checked && document.getElementsByName('r[resume_upload]')[0].checked) {
document.getElementsByName('resume')[0].checked = true
}

if(document.getElementsByName('r[survey_add]')[0].checked && document.getElementsByName('r[survey_download]')[0].checked && document.getElementsByName('r[survey_vote]')[0].checked) {
document.getElementsByName('survey')[0].checked = true
}
if(document.getElementsByName('r[user_login]')[0].checked && document.getElementsByName('r[user_login_fail]')[0].checked && document.getElementsByName('r[profile_update]')[0].checked && document.getElementsByName('r[user_pw_change]')[0].checked && document.getElementsByName('r[user_pw_reset_req]')[0].checked && document.getElementsByName('r[user_login_remember_me]')[0].checked) {
document.getElementsByName('user')[0].checked = true
}

</script>

<div style="clear:both">&nbsp;</div>

<% else %>
<h2>Activity Log: Details</h2>
<style>
.title { font-weight: bold; width: 80px }
.field, .title { padding: 3px 0 }
</style>
<table>
<tr>
<td class="title">Date:</td>
<td class="field"><% $popup.time|date_format:"%D" %></td>
</tr>
<tr>
<td class="title">Time:</td>
<td class="field"><% $popup.time|date_format:"%H:%M:%S" %></td>
</tr>
<tr>
<td class="title">Action:</td>
<td class="field"><% $popup.type %></td>
</tr>
<tr>
<td class="title">User:</td>
<td class="field"><% $popup.user %></td>
</tr>
<tr>
<td class="title">IP Address:</td>
<td class="field"><% $popup.ip_address %></td>
</tr>
<% if ($popup.type == 'db_edit') %>
<% foreach from=$db_edit_fields item=i %>
<tr>
<td class="title"><% $i.col %>:</td>
<td class="field">"<% $i.oldval %>" to "<% $i.newval %>" (<a href="http://www.gps100.com/portal/network/view/overview.php?id=<% $i.rowkey %>">User: <% $i.rowkey %></a>)</td>
</tr>
<% /foreach %>
<% else %>
<tr>
<td class="title">Data:</td>
<td class="field"><% $popup.data|replace:":::::":"<BR><BR>"|wordwrap:110:"<BR>":TRUE %></td>
</tr>
<% /if %>
</table>
<hr>
<b>Interpretation:</b>
<% if ($popup.type == 'admin_add') %>
<% $popup.user %> added <% $popup.data %> as an admin at <% $popup.time|date_format:"%l:%M%p" %> on <% $popup.time|date_format:"%e %B %Y" %>.

<% elseif ($popup.type == 'admin_delete') %>
<% $popup.user %> removed <% $popup.data %>'s admin priviledges at <% $popup.time|date_format:"%l:%M%p" %> on <% $popup.time|date_format:"%e %B %Y" %>.

<% elseif ($popup.type == 'admin_loginas') %>
<% $popup.user %> logged in as <% $popup.uname %> at <% $popup.time|date_format:"%l:%M%p" %> on <% $popup.time|date_format:"%e %B %Y" %>.

<% elseif ($popup.type == 'db_download') %>
<% $popup.user %> downloaded custom search results at <% $popup.time|date_format:"%l:%M%p" %> on <% $popup.time|date_format:"%e %B %Y" %>.


<% elseif ($popup.type == 'hack_attempt_db_download') %>
<% $popup.user %> attempted to hack the custom search results script to download data he/she was not entitled to.

<% elseif ($popup.type == 'db_edit') %>
<% $popup.user %> made changes to the database using admin priviledges.

<% elseif ($popup.type == 'dms_file_delete') %>
<% $popup.user %> deleted the file: "<% $popup.file_name %>"

<% elseif ($popup.type == 'dms_file_download') %>
<% $popup.user %> downloaded the file: "<% $popup.file_name %>"

<% elseif ($popup.type == 'dms_file_upload') %>
<% $popup.user %> uploaded the file: "<% $popup.file_name %>"

<% elseif ($popup.type == 'dms_folder_create') %>
<% $popup.user %> created the folder: "<% $popup.folder_name %>"

<% elseif ($popup.type == 'dms_folder_delete') %>
<% $popup.user %> deleted the folder: "<% $popup.folder_name %>"

<% elseif ($popup.type == 'profile_update') %>
<% $popup.user %> updated the <a href="/portal/network/view/<% $popup.data %>.php?id=<% $popup.uid %>"><% $popup.data %></a> area of their profile at <% $popup.time|date_format:"%l:%M%p" %> on <% $popup.time|date_format:"%e %B %Y" %>.

<% elseif ($popup.type == 'survey_add') %>
<% $popup.user %> added the survey "<% $popup.survey_name %>" at <% $popup.time|date_format:"%l:%M%p" %> on <% $popup.time|date_format:"%e %B %Y" %>.

<% elseif ($popup.type == 'survey_download') %>
<% $popup.user %> downloaded the survey results for "<% $popup.survey_name %>" at <% $popup.time|date_format:"%l:%M%p" %> on <% $popup.time|date_format:"%e %B %Y" %>.

<% elseif ($popup.type == 'survey_vote') %>
<% $popup.user %> voted on the survey "<% $popup.survey_name %>" at <% $popup.time|date_format:"%l:%M%p" %> on <% $popup.time|date_format:"%e %B %Y" %>.

<% elseif ($popup.type == 'user_login') %>
<% $popup.user %> logged in at <% $popup.time|date_format:"%l:%M%p" %> on <% $popup.time|date_format:"%e %B %Y" %>.

<% elseif ($popup.type == 'user_login_fail') %>
Someone failed to log into the account with email address "<b><% $popup.data %></b>" at <% $popup.time|date_format:"%l:%M%p" %> on <% $popup.time|date_format:"%e %B %Y" %>.
<% if (strlen($popup.user) > 1) %>
The user who corresponds with this email address is <b><% $popup.user %></b>.
<% /if %>

<% elseif ($popup.type == 'user_pw_change') %>
<% $popup.user %> changed their password at <% $popup.time|date_format:"%l:%M%p" %> on <% $popup.time|date_format:"%e %B %Y" %>.

<% elseif ($popup.type == 'user_login_remember_me') %>
<% $popup.user %> logged in using a "remember me" cookie at <% $popup.time|date_format:"%l:%M%p" %> on <% $popup.time|date_format:"%e %B %Y" %>.

<% else %>
No interpretation available.
<% /if %>
<hr>
<BR>
<a href="javascript:history.go(-1);">Back</a>

<% /if %>

</div>