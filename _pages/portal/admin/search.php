<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

// check if user is um or board
if($_SESSION['user_is_board']) { $smarty->assign('user_is_board', 1); }
if($_SESSION['user_is_um']) { $smarty->assign('user_is_um', 1); }

//check if search
if ($_GET['s'] || $_GET['qs'] || $_GET['cs']) { $is_search = 1; $smarty->assign('is_search', $is_search); }

//get sorting variables
$sortby = $_GET['sortby'];
if(empty($sortby)) { $sortby = 'uni'; }

if ($is_search) {

//check if quick search
if ($_GET['qs']) {
if ($_GET['tag']) { //search by tag
$search_type = "by_tag";
$ttype_f = $_GET['ttype']; if ($ttype_f == 'pitch') { $ttype_f = "pitche"; } //fix formatting of tag type for plurals
$search_title = "&quot;".$_GET['tag']."&quot; in ".ucwords($ttype_f)."s";
$search_tag = mysql_real_escape_string($_GET['tag']);
$ttype = mysql_real_escape_string($_GET['ttype']);
if(strlen($ttype) < 1) { $ttype = "sector"; }

//then I get the database results

$sql = "SELECT      m.id,
                    m.first_name,
                    m.last_name,
                    m.graduation_year,
                    m.current_employer,
                    m.ug_school_id,
                    u.FNAME AS undergraduate_school
        FROM        members m,
					universities u,
					tags t
        WHERE       m.ug_school_id = u.ID
		AND			m.id = t.uid
		AND			t.tag = '$search_tag'
		AND			t.ttype = '$ttype'
        AND    	    NOT hidden
        AND         NOT disabled
        ORDER BY    u.FNAME, m.first_name";

$result = mysql_query($sql) or die(mysql_error());
$results_num = mysql_num_rows($result);

if($results_num < 1) { $no_results = 1; }

if(!$no_results) {

//while ($row = mysql_fetch_assoc($result)) {
//    $members_array[] = $row;
//}

while ($row = mysql_fetch_assoc($result)) {
//check for thumbnail, the second line is for backwards-compatibility with legacy set up
if (file_exists($_SERVER['DOCUMENT_ROOT']."/content_files/member_photos/".$row['id']."-cropped.jpg")) { $row['cropped_photo'] = "/content_files/member_photos/".$row['id']."-cropped.jpg"; }
elseif (file_exists($_SERVER['DOCUMENT_ROOT']."/content_files/member_photos/".$row['first_name']."-".$row['last_name']."-cropped.jpg")) { $row['cropped_photo'] = "/content_files/member_photos/".$row['first_name']."-".$row['last_name']."-cropped.jpg"; }
else { $row['cropped_photo'] = "/images/male_thumb.png"; }
    $members_array[] = $row;
}

if ($sortby == 'uni') {
//retrieve universities
foreach ($members_array as $temp) {
	if ($temp['undergraduate_school'] != '') {
		$unis[$temp['undergraduate_school']][] = $temp;
	}
}
}

if ($sortby == 'year') {
//retrieve years
foreach ($members_array as $temp) {
	if ($temp['graduation_year'] != '') {
		$years[$temp['graduation_year']][] = $temp;
	}
}
krsort($years);
}

} //end if not no results

} //end if search by tag

else { //tag is unknown

$search_term = explode("_", mysql_real_escape_string(str_replace("'", "", ($_POST['quicksearch']))));
$s_type = $search_term[0];
$s_id = $search_term[1];
$s_value = $search_term[2];

// adjust term for GET variable (having an '&' messes up the query string)
$s_value = str_replace("&","%26",$s_value);

if ($s_type == 'member') { //search by member
header("Location:/portal/network/view/overview.php?id=$s_id");
} //end search by member

elseif ($s_type == 'uni') { //search by member
header("Location:search.php?s=1&uni=$s_id");
} //end search by member

elseif ($s_type == 'class') { //search by member
header("Location:search.php?s=1&year=$s_id");
} //end search by member

elseif ($s_type == 'book' || $s_type == 'expertise' || $s_type == 'hobby' || $s_type == 'hometown' || $s_type == 'investor' || $s_type == 'movie' || $s_type == 'philosopher' || $s_type == 'pitch' || $s_type == 'sector' || $s_type == 'television') { //search by tag
header("Location:search.php?qs=1&ttype=$s_type&tag=$s_value");
} //end search by tag

$no_results = 1;

}

}

//check if custom search
elseif ($_GET['cs']) {

//check if logged in
if ($logged_in) {

if(isset($_GET['f_employer'])) { foreach($_GET['f_employer'] as $key=>$value) { $criteria[] = 'current_employer:'.$value; } }
if(isset($_GET['f_expertise'])) { foreach($_GET['f_expertise'] as $key=>$value) { $criteria[] = 'tag:expertise:'.$value; } }
if(isset($_GET['f_gps_stage'])) { foreach($_GET['f_gps_stage'] as $key=>$value) { $criteria[] = 'stage:'.$value; } }
if(isset($_GET['f_graduation_year'])) { foreach($_GET['f_graduation_year'] as $key=>$value) { $criteria[] = 'graduation_year:'.$value; } }
if(isset($_GET['f_hobby'])) { foreach($_GET['f_hobby'] as $key=>$value) { $criteria[] = 'tag:hobby:'.$value; } }
if(isset($_GET['f_hometown'])) { foreach($_GET['f_hometown'] as $key=>$value) { $criteria[] = 'tag:hometown:'.$value; } }
if(isset($_GET['f_industry'])) { foreach($_GET['f_industry'] as $key=>$value) { $criteria[] = 'current_industry:'.$value; } }
if(isset($_GET['f_sector'])) { foreach($_GET['f_sector'] as $key=>$value) { $criteria[] = 'tag:sector:'.$value; } }
if(isset($_GET['f_undergraduate_school'])) { foreach($_GET['f_undergraduate_school'] as $key=>$value) { $criteria[] = 'undergraduate_school:'.$value; } }

if((count($criteria) < 1) && !$_GET['results_view']) { header("Location: search.php"); }

if (!$_GET['results_view']) {

$sql = "SELECT id FROM members WHERE disabled = 0";
$sql2_default = "SELECT uid FROM tags WHERE 1 = 1";
$sql2 = $sql2_default;

//set root statements
$sql_t['current_employer'] = "1 = 0";
$sql_t['current_industry'] = "1 = 0";
$sql_t['gps_stage'] = "1 = 0";
$sql_t['graduation_year'] = "1 = 1";
$sql_t['undergraduate_school'] = "1 = 0";

//set root statements for tags
$sql_tag['expertise'] = "1 = 0"; $sql_tag_count['expertise'] = 0;
$sql_tag['hobby'] = "1 = 0"; $sql_tag_count['hobby'] = 0;
$sql_tag['hometown'] = "1 = 0"; $sql_tag_count['hometown'] = 0;
$sql_tag['sector'] = "1 = 0"; $sql_tag_count['sector'] = 0;

foreach ($criteria as $key => $value) {
//echo ($key.": ".$value);
$value = mysql_real_escape_string($value);

//add employer criteria
if (substr($value,0,17) == 'current_employer:') { $sql_t['current_employer'] .= " OR current_employer = '" . substr($value,17) . "'"; }

//add expertise criteria
elseif (substr($value,0,13) == "tag:expertise") { $sql_tag['expertise'] .= " OR (ttype = 'expertise' AND tag = '" . str_replace('_',', ',substr($value,14)) . "')"; }

//add gps stage criteria
elseif ($value == 'stage:analyst') { $sql_t['gps_stage'] .= " OR analyst = '1'"; }
elseif ($value == 'stage:member') { $sql_t['gps_stage'] .= " OR member = '1'"; }
elseif ($value == 'stage:alumni') { $sql_t['gps_stage'] .= " OR alumni = '1'"; }

//add graduation year criteria
elseif (substr($value,0,16) == 'graduation_year:') { $sql_t['graduation_year'] .= " AND graduation_year " . substr($value,21) . " '" . substr($value,16,4) ."'"; }

//add hobby criteria
elseif (substr($value,0,9) == "tag:hobby") { $sql_tag['hobby'] .= " OR (ttype = 'hobby' AND tag = '" . str_replace('_',', ',substr($value,10)) . "')"; }

//add hometown criteria
elseif (substr($value,0,12) == "tag:hometown") { $sql_tag['hometown'] .= " OR (ttype = 'hometown' AND tag = '" . str_replace('_',', ',substr($value,13)) . "')"; }

//add industry criteria
elseif (substr($value,0,17) == 'current_industry:') { $sql_t['current_industry'] .= " OR current_industry = '" . substr($value,17) . "'"; }

//add sector criteria
elseif (substr($value,0,10) == "tag:sector") { $sql_tag['sector'] .= " OR (ttype = 'sector' AND tag = '" . str_replace('_',', ',substr($value,11)) . "')"; }

//add undergraduate school criteria
elseif (is_numeric(substr($value,21))) {
if (substr($value,0,21) == 'undergraduate_school:') { $sql_t['undergraduate_school'] .= " OR ug_school_id = '" . substr($value,21) . "'"; }
}

}

//build statements
foreach ($sql_t AS $value) {
if ($value != "1 = 0" && $value != "1 = 1") { $sql .=  " AND (" . $value .")"; }
}

foreach ($sql_tag AS $key => $value) {
if ($sql_tag_count[$key] < 2) {
if ($value != "1 = 0" && $value != "1 = 1") { $sql2 .=  " AND uid IN (SELECT uid FROM tags WHERE (" . $value ."))"; $sql_tag_count[$key]++; }
}
else {
if ($value != "1 = 0" && $value != "1 = 1") { $sql2 .=  " AND (" . $value .")"; $sql_tag_count[$key]++; }
}
}
$sql2 .= " GROUP BY uid";

//SELECT uid FROM tags WHERE 1=1 AND (1 = 0 OR (ttype = 'hometown' AND tag = 'Wayland, MA') OR (ttype = 'hometown' AND tag = 'London, UK')) AND uid IN (SELECT uid FROM tags WHERE 1=1 AND (1 = 0 OR (ttype = 'sector' AND tag = 'Technology')))


//get results

if ($sql2 == $sql2_default . " GROUP BY uid") { $sql3 = $sql; }
else { $sql3 = $sql . " AND id IN(". $sql2 .")"; }

$sql3 = str_replace("1 = 0 OR","",$sql3);
$sql3 = str_replace("1 = 1 AND","",$sql3);

//die($sql3); //uncomment to debug

$time_start = microtime(true);
$result = mysql_query($sql3) or die(mysql_error());
$time_end = microtime(true);
$query_time = $time_end - $time_start;

$num_rows = mysql_num_rows($result);
$num_rows_adj = $num_rows + 1;

if($num_rows < 1) { die("No matches found - remove some criteria."); }
else { $resultstxt = mysql_num_rows($result)." matches found."; }

//create array of result ids
while ($row = mysql_fetch_array($result)) {
$r_ids[] = $row[0];
}

if (!$_GET['download']) { //begin fetch number of results

echo "<div style='float:right'>Query time: " . number_format($query_time,7) . " seconds.</div>";

echo $resultstxt;

echo ' <a href="#" onClick="view_online(\''.implode("_",$r_ids).'\'); return false;">View Online</a>';

if ($_SESSION['user_is_um'] || $_SESSION['user_is_board']) { echo ' | <a href="#" onClick="view_results();return false;">Download Results</a>'; }

}

else { //begin fetch results for download

//transfer fields from JS to PHP
$fields = array();
foreach ($_GET['r'] as $key => $value) {
$fields[$key] = 1;
}

//set field count to 0

$valid_fields = array("first_name", "last_name", "email_address", "gmail_address", "biography", "birth_date", "undergraduate_school", "undergraduate_major", "graduation_year", "gps_position_held",
"graduate_school", "graduate_major", "alumni_mentor", "current_employer", "member_advocate", "contact_number", "contact_number_country_code", "skype", "philanthropy", "entrepreneurship", "address", "current_industry");
$tag_fields = array("book", "expertise", "hobby", "hometown", "investor", "movie", "philosopher", "pitch", "sector", "television");

$sql_fetch = "SELECT m.id";

foreach($valid_fields as $key => $value) {
if (isset($fields[$value])) { $sql_fetch .= ", m." . $value; $field_list[] = $value; }
} //end foreach


//$sql_fetch = str_replace("SELECT m.id,","SELECT",$sql_fetch); //always include the user id so there is definitely going to be a column

foreach($tag_fields as $key => $value) {
if (isset($fields[$value])) { $sql_fetch .= ", MAX(CASE WHEN t.ttype = '" . $value."' THEN t.tag END) AS ".$value; $field_list[] = $value; }
} //end foreach


$sql_fetch .= " FROM members m 
         LEFT JOIN (  SELECT uid, 
                        ttype, 
                        GROUP_CONCAT(tag) tag 
                   FROM tags 
               GROUP BY uid, 
                        ttype) t 
           ON m.id = t.uid 
WHERE m.id IN (".implode(",", $r_ids).")
GROUP BY m.id";

foreach($valid_fields as $key => $value) {
if (isset($fields[$value])) { $sql_fetch .= ", m." . $value; }
} //end foreach


if ($_GET['format'] == 'excel') { //begin Excel

date_default_timezone_set('America/New_York');

/** PHPExcel */
require_once ($_SERVER['DOCUMENT_ROOT'] . '/includes/classes/PHPExcel.php');

// Define User
$user = $_SESSION['user_first_name'] . " " . $_SESSION['user_last_name'];

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Load Template
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($_SERVER['DOCUMENT_ROOT']."/includes/db_excel.xlsx");

// Set properties
$objPHPExcel->getProperties()->setCreator($user)
							 ->setLastModifiedBy($user)
							 ->setTitle("GPS Database Search Results")
							 ->setSubject("GPS Database Search Results")
							 ->setDescription("Data generated by internal database search using GPS100.com Portal")
							 ->setKeywords("gps")
							 ->setCategory("GPS Database");

// Cover Sheet

// Add generation info
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D8', $user);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D9', date("jS F Y"));
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D10', date("g:i A"));
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D11', date("e"));

// Add criteria
$row = 14;

//foreach ($criteria as $key => $value) {
//$value = mysql_real_escape_string($value);
//}

if ($sql_t['current_employer'] != "1 = 0") {
$sql_t['current_employer'] = str_replace("1 = 0 OR ","",$sql_t['current_employer']);
$sql_t['current_employer'] = ucwords($sql_t['current_employer']);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$row, $sql_t['current_employer']);
$row++;
}

if ($sql_t['current_industry'] != "1 = 0") {
$sql_t['current_industry'] = str_replace("1 = 0 OR ","",$sql_t['current_industry']);
$sql_t['current_industry'] = ucwords($sql_t['current_industry']);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$row, $sql_t['current_industry']);
$row++;
}

if ($sql_t['gps_stage'] != "1 = 0") {
$sql_t['gps_stage'] = str_replace("1 = 0 OR ","",$sql_t['gps_stage']);
$sql_t['gps_stage'] = ucwords($sql_t['gps_stage']);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$row, $sql_t['gps_stage']);
$row++;
}

if ($sql_t['graduation_year'] != "1 = 1") {
$sql_t['graduation_year'] = str_replace("1 = 1 AND ","",$sql_t['graduation_year']);
$sql_t['graduation_year'] = str_replace("graduation_year","Graduation Year",$sql_t['graduation_year']);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$row, $sql_t['graduation_year']);
$row++;
}


foreach ($sql_tag as $key => $value) {
if ($value != "1 = 0") {
$sql_tag[$key] = str_replace("1 = 0 OR ","",$sql_tag[$key]);
$sql_tag[$key] = str_replace("ttype = '".$key."' AND tag =","",$sql_tag[$key]);
$sql_tag[$key] = str_replace("(","",$sql_tag[$key]);
$sql_tag[$key] = str_replace(")","",$sql_tag[$key]);
$sql_tag[$key] = str_replace("  "," ",$sql_tag[$key]);
//$sql_tag[$key] = str_replace("'","",$sql_tag[$key]);
$sql_tag[$key] = $key." =" . $sql_tag[$key];
$sql_tag[$key] = ucwords($sql_tag[$key]);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$row, $sql_tag[$key]);
$row++;
}
}

if ($sql_t['undergraduate_school'] != "1 = 0") {
$sql_t['undergraduate_school'] = str_replace("1 = 0 OR ","",$sql_t['undergraduate_school']);
$sql_t['undergraduate_school'] = str_replace("ug_school_id","Undergraduate School",$sql_t['undergraduate_school']);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$row, $sql_t['undergraduate_school']);
$row++;
}


// Format sheet
$objPHPExcel->getActiveSheet()->getStyle('A1:AZ80')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:AZ80')->getFill()->getStartColor()->setARGB('FFFFFFFF');
$objPHPExcel->getActiveSheet()->getStyle('B7')->getFill()->getStartColor()->setARGB('FF000080');
$objPHPExcel->getActiveSheet()->getStyle('B7')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);

// Rename sheet
//$objPHPExcel->getActiveSheet()->setTitle('Cover');


// Add fields
$objPHPExcel->setActiveSheetIndex(1)->setCellValue('A1', 'id');
$col = 'A';
foreach ($field_list as $value) {
$objPHPExcel->setActiveSheetIndex(1)->setCellValue(++$col.'1', $value);
}


// Add data
$result = mysql_query($sql_fetch) or die(mysql_error());
$i = 2; $num_fields = count($field_list);
while ($row = mysql_fetch_array($result)) {

$j = 0; $col = "A";

while ($j <= $num_fields) {

//remove html
$row[$j] = strip_tags($row[$j]);
$row[$j] = str_replace("&nbsp;","",$row[$j]);

$cell1 = $col.$i;
$objPHPExcel->setActiveSheetIndex(1)->setCellValue($cell1, $row[$j]);
$j++; ++$col;
}

$i++;

}

// Do formatting
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->getStyle('A1:AZ'.$num_rows_adj)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:AZ'.$num_rows_adj)->getFill()->getStartColor()->setARGB('FFFFFFFF');
//$objPHPExcel->getActiveSheet()->getStyle('A1:Z1')->getFont()->setBold(true);
//$objPHPExcel->getActiveSheet()->getStyle('A1:Z'.$num_rows_adj)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Set file name
$filename = "GPS Database Search (" . date("Y-m-d") . ")";

if ($_SESSION['user_is_board'] || $_SESSION['user_is_um']) { //check that user is on board of um

// Log download permanently
$part1 = $sql_fetch;
$part2 = $sql3;
$data_store = mysql_real_escape_string($part1 . ":::::" . $part2);
$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('db_download','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".$data_store."','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql) or die("There was a problem logging the download. Please contact an admin.");

// Redirect output to a client's web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

} //end if user is board or um

else { // if not board or um, and they have got to here, they have attempted to hack, log and email admin
$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('hack_attempt_db_download','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".mysql_real_escape_string($sql_fetch)."','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql) or die("There was a problem logging the download. Please contact an admin.");
				//now email code
				require_once "Mail.php";
				
				$from = "GPS Admin <admin@".$domain.">";
				$to = "bwigoder@gps100.com";
				$subject = "Hack Attempt logged";
				$body = "Dear Benjamin,\n\nA hack attempt has been logged on the GPS Portal, details below.\n\n";
				$body .= "Type: Database Download\n";
				$body .= "User ID: ". $_SESSION['user_id'] . "\n";
				$body .= "Date/time: ". date("Y-m-d H:i:s") . "\n";
				$body .= "IP Address: ". $_SERVER['REMOTE_ADDR'] . "\n\n";
				$body .= "Query: " . mysql_real_escape_string($sql_fetch);
				$body .= "\n\nMany thanks,\nBenjamin Wigoder";

				$host = "localhost";
				$username = "admin@".$domain;
				$password = "101dalmations";

				$headers = array ('From' => $from,
				  'To' => $to,
				  'Subject' => $subject);
				$smtp = Mail::factory('smtp',
				  array ('host' => $host,
				    'auth' => true,
				    'username' => $username,
				    'password' => $password));

				$mail = $smtp->send($to, $headers, $body);

				if (PEAR::isError($mail)) {
				  $error++; $errormsg = "Email Error";
				 } else {
				  $email_success=1;
				 }
die("Hack attempt logged.");
}

}

}

die(); //to prevent rest of search page loading

} //end check if not viewing results online

else {
$smarty->assign('results_view', 1);
$smarty->assign('criteria',$criteria);
$z_ids = str_replace("_",",",mysql_real_escape_string($_GET['ids']));
if(!is_numeric(implode('', explode(',',$z_ids)))) { die("Fatal error code:1. Please contact an admin if you believe this is in error."); }

$sql = "SELECT      m.id,
                    m.first_name,
                    m.last_name,
                    m.graduation_year,
                    m.current_employer,
                    m.ug_school_id,
                    u.FNAME AS undergraduate_school
        FROM        members m,
					universities u
        WHERE       m.ug_school_id = u.ID
        AND    	    NOT hidden
        AND         NOT disabled
        AND m.id IN (" . $z_ids . ")";
if ($sortby == 'uni') { $sql .= " ORDER BY u.FNAME, m.first_name"; }
if ($sortby == 'year') { $sql .= " ORDER BY m.graduation_year DESC, m.first_name"; }

$result = mysql_query($sql) or die(mysql_error());

//while ($row = mysql_fetch_assoc($result)) {
//    $members_array[] = $row;
//}

while ($row = mysql_fetch_assoc($result)) {
//check for thumbnail, the second line is for backwards-compatibility with legacy set up
if (file_exists($_SERVER['DOCUMENT_ROOT']."/content_files/member_photos/".$row['id']."-cropped.jpg")) { $row['cropped_photo'] = "/content_files/member_photos/".$row['id']."-cropped.jpg"; }
elseif (file_exists($_SERVER['DOCUMENT_ROOT']."/content_files/member_photos/".$row['first_name']."-".$row['last_name']."-cropped.jpg")) { $row['cropped_photo'] = "/content_files/member_photos/".$row['first_name']."-".$row['last_name']."-cropped.jpg"; }
else { $row['cropped_photo'] = "/images/male_thumb.png"; }
    $members_array[] = $row;
}

if ($sortby == 'uni') {
//retrieve universities
foreach ($members_array as $temp) {
	if ($temp['undergraduate_school'] != '') {
		$unis[$temp['undergraduate_school']][] = $temp;
	}
}
}

if ($sortby == 'year') {
//retrieve years
foreach ($members_array as $temp) {
	if ($temp['graduation_year'] != '') {
		$years[$temp['graduation_year']][] = $temp;
	}
}
krsort($years);
}



}

} //end if logged in

else {
die("Your session has expired. Please <a href='#' onClick='javascript:location.reload(true); return false'>refresh the page</a>.");
}

} //end cs search


//if neither of these then is a popular search
elseif ($_GET['year']) {
//search by year
$search_type = "by_year";

$s_year = $_GET['year'];
if (!is_numeric($s_year)) { die("hack attempt"); }

$search_title = "Class of ".$s_year;

//then I get the database results

$sql = "SELECT      m.id,
                    m.first_name,
                    m.last_name,
                    m.graduation_year,
                    m.current_employer,
                    m.ug_school_id,
                    u.FNAME AS undergraduate_school
        FROM        members m
        JOIN        universities u
        ON          m.ug_school_id = u.ID
        WHERE       NOT hidden
        AND         NOT disabled
        AND         graduation_year = '".$s_year."'
        ORDER BY    u.FNAME";

$result = mysql_query($sql) or die(mysql_error());

if(mysql_num_rows($result) < 1) { $no_results = 1; }

if(!$no_results) {

//while ($row = mysql_fetch_assoc($result))
//    $members_array[] = $row;

while ($row = mysql_fetch_assoc($result)) {
//check for thumbnail, the second line is for backwards-compatibility with legacy set up
if (file_exists($_SERVER['DOCUMENT_ROOT']."/content_files/member_photos/".$row['id']."-cropped.jpg")) { $row['cropped_photo'] = "/content_files/member_photos/".$row['id']."-cropped.jpg"; }
elseif (file_exists($_SERVER['DOCUMENT_ROOT']."/content_files/member_photos/".$row['first_name']."-".$row['last_name']."-cropped.jpg")) { $row['cropped_photo'] = "/content_files/member_photos/".$row['first_name']."-".$row['last_name']."-cropped.jpg"; }
else { $row['cropped_photo'] = "/images/male_thumb.png"; }
    $members_array[] = $row;
}

//retrieve universities
foreach ($members_array as $temp) {
	if ($temp['undergraduate_school'] != '') {
		$unis[$temp['undergraduate_school']][] = $temp;
	}
}

} //end if not no results

} // end is a search by year

elseif ($_GET['uni']) {
// search by university
$search_type = "by_uni";
$s_uni = $_GET['uni'];
if (!is_numeric($s_uni)) { die("hack attempt"); }
load_unis();
$s_uni_sname = $university[$s_uni]['sname'];
$s_uni_fname = $university[$s_uni]['fname'];

$search_title = $s_uni_fname;

//Check for entries
$sql = "SELECT      m.id,
                    m.first_name,
                    m.last_name,
                    m.graduation_year,
                    m.current_employer,
                    m.ug_school_id,
                    u.FNAME AS undergraduate_school
        FROM        members m
        JOIN        universities u
        ON          m.ug_school_id = u.ID
        WHERE       NOT hidden
        AND         NOT disabled
	AND         m.ug_school_id = $s_uni
        ORDER BY    m.graduation_year DESC,
	            m.first_name ASC";

$result = mysql_query($sql) or die(mysql_error());

if(mysql_num_rows($result) < 1) { $no_results = 1; }

if(!$no_results) {

//while ($row = mysql_fetch_assoc($result))
//    $members_array[] = $row;

while ($row = mysql_fetch_assoc($result)) {
//check for thumbnail, the second line is for backwards-compatibility with legacy set up
if (file_exists($_SERVER['DOCUMENT_ROOT']."/content_files/member_photos/".$row['id']."-cropped.jpg")) { $row['cropped_photo'] = "/content_files/member_photos/".$row['id']."-cropped.jpg"; }
elseif (file_exists($_SERVER['DOCUMENT_ROOT']."/content_files/member_photos/".$row['first_name']."-".$row['last_name']."-cropped.jpg")) { $row['cropped_photo'] = "/content_files/member_photos/".$row['first_name']."-".$row['last_name']."-cropped.jpg"; }
else { $row['cropped_photo'] = "/images/male_thumb.png"; }
    $members_array[] = $row;
}

//retrieve years
foreach ($members_array as $temp) {
	if ($temp['graduation_year'] != '') {
		$years[$temp['graduation_year']][] = $temp;
	}
}

krsort($years);

} //end if not no results

} //end is a search by uni

} // end is conducting a search

else { //default search page


//Check for entries
$sql = "SELECT id, first_name, last_name, graduation_year, current_employer, ug_school_id FROM members WHERE hidden = 0 AND disabled = 0 ORDER BY graduation_year DESC, first_name ASC";
$result = mysql_query($sql) or die(mysql_error());

//generate classes
$sql = "SELECT graduation_year FROM members WHERE hidden = 0 AND disabled = 0";
$result = mysql_query($sql) or die(mysql_error());
$i = 0;
while ($row = mysql_fetch_array($result)) {
$members_array[$i]['graduation_year'] = $row[0];
$i++;
}
//retrieve years
foreach ($members_array as $temp) {
	if ($temp['graduation_year'] != '') {
		$years[$temp['graduation_year']][] = $temp;
	}
}

krsort($years);

//generate universities
$sql = "SELECT ID,SNAME FROM universities ORDER BY SNAME ASC";
$result = mysql_query($sql) or die(mysql_error());
$i=0;
while ($row = mysql_fetch_array($result)) {
$uni[$i]['id'] = $row[0];
$uni[$i]['sname'] = $row[1];
$i++;
}

$smarty->assign('uni', $uni);

//generate sector
$sql = "SELECT tag FROM tags WHERE ttype = 'sector' GROUP BY tag ORDER BY tag ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$sectors[] = $row[0];

$smarty->assign('sectors', $sectors);

//fetch employers
$sql = "SELECT employer FROM jobs WHERE current = 1 GROUP BY employer ORDER BY employer ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$employers[] = $row[0];

//fetch expertise
$sql = "SELECT tag FROM tags WHERE ttype = 'expertise' GROUP BY tag ORDER by tag ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$tags['expertise'][] = $row[0];

//fetch hobbies
$sql = "SELECT tag FROM tags WHERE ttype = 'hobby' GROUP BY tag ORDER by tag ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$tags['hobbies'][] = $row[0];

//fetch hometowns
$sql = "SELECT tag FROM tags WHERE ttype = 'hometown' GROUP BY tag ORDER by tag ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$tags['hometowns'][] = $row[0];

//fetch industries
$sql = "SELECT jobs_industries.name as name FROM jobs JOIN jobs_industries ON jobs.i_id = jobs_industries.id WHERE jobs.i_id != '0' AND jobs.current = '1' GROUP BY name ORDER by name ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$industries[] = $row['name'];

//fetch sectors
$sql = "SELECT tag FROM tags WHERE ttype = 'sector' GROUP BY tag ORDER by tag ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$tags['sectors'][] = $row[0];

$smarty->assign('tags', $tags);

//fetch years
$sql = "SELECT graduation_year FROM members WHERE hidden != 1 AND disabled != 1 GROUP BY graduation_year ORDER by graduation_year ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
if (is_numeric($row[0])) { $year[] = $row[0]; }
}

$smarty->assign('year', $year);

$smarty->assign('search_title', $search_title);
$smarty->assign('search_type', $search_type);
$smarty->assign('sortby', $sortby);

$smarty->assign('ttype', $ttype);
$smarty->assign('search_tag', $search_tag);

$smarty->assign('years', $years);
$smarty->assign('unis', $unis);
$smarty->assign('employers', $employers);
$smarty->assign('industries', $industries);

$smarty->assign('members_array', $members_array);

$smarty->assign('no_results', $no_results);

?>