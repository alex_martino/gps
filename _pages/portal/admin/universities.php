<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>
<h2>Settings: Universities</h2>

<% if ($display == 'default') %>
<table class="admin_table" style="border: 1px solid black; margin-top: 20px">
<tr style="border-bottom: 1px solid black">
<th>ID</th>
<th>Short Name</th>
<th>Full Name</th>
</tr>
<% foreach from=$universities item=i %>
<tr class="row">
<td><% $i.ID %></td>
<td><% $i.SNAME %></td>
<td><% $i.FNAME %></td>
</tr>
<% /foreach %>
</table>
<BR>
<input type="button" value="Add University" onClick="window.location='universities.php?display=add';">
<% else %>
<% if ($display == 'add') && ($error != 0) %>
<div class="error"><% $errormsg %></div>
<div style="clear:both">&nbsp;</div>
<% /if %>
<form name="adduni" action="universities.php" method="post">
<input type="hidden" name="addsubmitted" value="1">
<div style="float: left; line-height: 1.4; width: 140px;">
Abbreviated Name:<BR>
Full Name:<BR><BR>
<input type="submit" value="Save">
</div>
<div style="float: left; line-height: 1.4; width: 155px;">
<input type="text" name="sname" value="<% $sname %>"><BR>
<input type="text" name="fname" value="<% $fname %>">
</div>
<div style="float: left; line-height: 1.4">
E.g. Harvard or LSE<BR>
E.g. Harvard University or London School of Economics
</div>
</form>
<% /if %>

</div>