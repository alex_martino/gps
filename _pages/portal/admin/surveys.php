<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>
<h2>Surveys</h2>

<table class="admin_table" style="border: 1px solid black; margin-top: 20px">
<tr style="border-bottom: 1px solid black">
<th>ID</th>
<th>Deadline</th>
<th>Question</th>
<th>Votes</th>
<th>Creator</th>
<th><img src="/content_files/images/icons/excel.png" style="width:14px; height: 14px"></th>
<th><img src="/content_files/images/bin.gif" style="width:14px; height: 14px"></th>
</tr>
<% foreach from=$surveys item=i key=id %>
<tr>
<td><% $i.id %></td>
<td><% $i.deadline %></td>
<td><% $i.question|truncate:60 %></td>
<td><% $i.total %></td>
<td><% $i.user|truncate:20 %></td>
<td><a href="surveys.php?download=<% $i.id %>"><img src="/content_files/images/icons/download.gif" style="width:14px; height: 14px"></a></td>
<td><a href="surveys.php?del=<% $i.id %>"><img src="/content_files/images/delete.png" style="width: 12px; height: 12px; margin-top: 2px" onClick="javascript:return confirm('Are you sure you wish to permanently delete this survey?');"></a></td>
</tr>
<% /foreach %>
</table>

<div style="clear:both">&nbsp;</div>
</div>