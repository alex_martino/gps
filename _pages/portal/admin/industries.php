<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>
<h2>Settings: Industries</h2>

<% if ($display == 'default') %>
<table class="admin_table" style="border: 1px solid black; margin-top: 20px">
<tr style="border-bottom: 1px solid black">
<th>ID</th>
<th>Name</th>
</tr>
<% foreach from=$industries item=i %>
<tr class="row">
<td><% $i.id %></td>
<td><% $i.name %></td>
</tr>
<% /foreach %>
</table>
<BR>
<input type="button" value="Add Industry" onClick="window.location='industries.php?display=add';">
<% else %>
<% if ($display == 'add') && ($error != 0) %>
<div class="error" style="margin-left:0"><% $errormsg %></div>
<div style="clear:both">&nbsp;</div>
<% /if %>
<form name="addindustry" action="industries.php" method="post">
<input type="hidden" name="addsubmitted" value="1">
<div style="float: left; line-height: 1.4; width: 140px;">
Industry Name:<BR><BR>
<input type="submit" value="Save">
</div>
<div style="float: left; line-height: 1.4; width: 155px;">
<input type="text" name="name" value="<% $name %>">
</div>
<div style="float: left; line-height: 1.4">
E.g. Private Equity
</div>
</form>
<% /if %>

</div>