<div><img src="/content_files/headers/admin.gif" width="800" height="90">

<style>
.d_table td { padding-right: 8px }
.cp_menu { height: 20px; background: grey; border-right: 1px dotted black; border-left: 1px dotted black }
.cp_option { float: left; padding: 2px 7px; color: white; border-right: 1px dotted black; height: 16px }
.cp_option_cur { float: left; padding: 2px 7px; color: black; border-right: 1px dotted black; background: #ededed; height: 16px }
.cp_option a { color: white }
.cp_option_cur a { color: black }
.cp_option a, .cp_option_cur a { font-weight: normal; text-decoration: none }
.cp_option a:hover, .cp_option_cur a:hover { text-decoration: underline }
#successbar { background: #95B9C7; color: black; padding: 3px; margin: 2px 0 0 0 }
.progress { width: 380px; height: 85px; overflow: none; border: none }
</style>
<script>
//Define variables

//counts
var count = new Array();
count['docs'] = 4;
count['payments'] = 4;

function switch_tab(item) {
item = item.split('_');
item_name = item[0];
item_num = item[1];
id_current = item_name + "_" + item_num + "_menu";
new_content = document.getElementById(item_name + "_" + item_num + "_content").innerHTML;
content_div = item_name + "_content";

//loop through each tab
for (x=1;x<=count[item_name];x=x+1) {
id_menu = item_name + "_" + x + "_menu";
document.getElementById(id_menu).setAttribute("class", "cp_option");
}

//Edit current tab
document.getElementById(id_current).setAttribute("class", "cp_option_cur");
document.getElementById(content_div).innerHTML = new_content;
document.getElementById(content_div).focus(); document.getElementById(content_div).blur(); //this line ensures focus is not on link
}

//Mailing list management
function open_list(listid) {

listid_db = listid.replace("_gps100.com","");

	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
// Create a function that will receive data sent from the server
ajaxRequest.onreadystatechange = function(){
		listpass = "Please wait...";
	if(ajaxRequest.readyState == 4){
		listpass = ajaxRequest.responseText;

listwin = "http://gps100.com/mailman/admin/"+listid+"/members/list/";

var form = document.createElement("form");
form.setAttribute("method", "post");
form.setAttribute("action", listwin);

// setting form target to a window named 'formresult'
form.setAttribute("target", "formresult");

var hiddenField = document.createElement("input");              
hiddenField.setAttribute("name", "adminpw");
hiddenField.setAttribute("value", listpass);
listpass = null;
form.appendChild(hiddenField);
document.body.appendChild(form);

// creating the 'formresult' window with custom features prior to submitting the form
var w=screen.width*0.8;
var h=screen.height*0.8;

if (w > 1200) { w = 1200; }
if (h > 800) { h = 800; }

var left = (screen.width/2)-(w/2);
var top = (screen.height/2)-(h/2);

popupwin = window.open('popup.php', 'formresult', 'scrollbars=yes,menubar=no,height='+h+',width='+w+',resizable=yes,toolbar=no,status=no, top='+top+', left='+left);

form.submit();

	}
}

var theurl = "index.php?fl=1&listname=" + listid_db;
ajaxRequest.open("GET", theurl, true);
ajaxRequest.send(null);

}

var ef_form_disp = 0;
function show_add_fwd_form() {

if (ef_form_disp != 1) {
document.getElementById('email_fwd_add_restore').innerHTML = document.getElementById('email_fwd_div').innerHTML;
document.getElementById('email_fwd_div').innerHTML = document.getElementById('email_fwd_add_1').innerHTML;
ef_form_disp = 1;
}

else {
restore_email_forward_list();
}

}

function restore_email_forward_list() {
document.getElementById('email_fwd_div').innerHTML = document.getElementById('email_fwd_add_restore').innerHTML;
ef_form_disp = 0;
}

function clear_default() {
if(document.getElementById('add_gps_email').value == 'E.g. tshannon@gps100.com') {
document.getElementById('add_gps_email').value = '';
}
}

function clear_default_dest() {
if(document.getElementById('add_email_dest').value == 'E.g. bob@gmail.com') {
document.getElementById('add_email_dest').value = '';
}
}



function add_fwd_step1() {

email_try = document.getElementById('add_gps_email').value;

	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
// Create a function that will receive data sent from the server
ajaxRequest.onreadystatechange = function(){

document.getElementById('resultstxt').innerHTML = '';
document.getElementById('resultstxt').style.display = 'none';

		
	if(ajaxRequest.readyState == 4){
		email_check = ajaxRequest.responseText;

if (email_check == "EMAIL_INVALID") {
document.getElementById('resultstxt').innerHTML = 'Email invalid - please enter a valid email';
document.getElementById('resultstxt').style.display = 'block';
}

else if (email_check == "FORWARD_EXISTS") {
document.getElementById('resultstxt').innerHTML = 'An email forward already exists for this user';
document.getElementById('resultstxt').style.display = 'block';
}

else if (email_check == "MAILING_LIST_EXISTS") {
document.getElementById('resultstxt').innerHTML = 'A mailing list exists with this name';
document.getElementById('resultstxt').style.display = 'block';
}

else {

document.getElementById('email_fwd_div').innerHTML = document.getElementById('email_fwd_add_2').innerHTML;
document.getElementById('add_gps_email').value = ajaxRequest.responseText;

}

  }
}

var theurl = "index.php?ef=1&gps_email=" + email_try;
ajaxRequest.open("GET", theurl, true);
ajaxRequest.send(null);

}

function add_fwd_step2() {

gps_email = email_try;

dest_try = document.getElementById('add_email_dest').value;

	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
// Create a function that will receive data sent from the server
ajaxRequest.onreadystatechange = function(){

document.getElementById('resultstxt').innerHTML = '';
document.getElementById('resultstxt').style.display = 'none';

		
	if(ajaxRequest.readyState == 4){
		email_check = ajaxRequest.responseText;

if (email_check == "EMAIL_INVALID") {
document.getElementById('resultstxt').innerHTML = 'Email invalid - please enter a valid email';
document.getElementById('resultstxt').style.display = 'block';
}

else {


var form = document.createElement("form");
form.setAttribute("method", "post");
form.setAttribute("action", "index.php");

var hiddenField = document.createElement("input");              
hiddenField.setAttribute("name", "gps_email");
hiddenField.setAttribute("value", gps_email);

var hiddenField2 = document.createElement("input");              
hiddenField2.setAttribute("name", "email_dest");
hiddenField2.setAttribute("value", email_check);

form.appendChild(hiddenField);
form.appendChild(hiddenField2);
document.body.appendChild(form);

form.submit();


}

  }
}

var theurl = "index.php?ef=1&dest_email=" + dest_try;
ajaxRequest.open("GET", theurl, true);
ajaxRequest.send(null);

}

var ml_form_disp = 0;
function show_add_mailing_list_form() {

if (ml_form_disp != 1) {
document.getElementById('mailing_list_add_restore').innerHTML = document.getElementById('mailing_list_div').innerHTML;
document.getElementById('mailing_list_div').innerHTML = document.getElementById('mailing_list_add_1').innerHTML;
ml_form_disp = 1;
}

else {
restore_mailing_list_list();
}

}

function restore_mailing_list_list() {
document.getElementById('mailing_list_div').innerHTML = document.getElementById('mailing_list_add_restore').innerHTML;
document.getElementById('mailing_list_div').style.overflow = "auto";
ml_form_disp = 0;
}

function clear_mailing_list_default() {
if(document.getElementById('add_mailing_list').value == 'E.g. industrials@gps100.com') {
document.getElementById('add_mailing_list').value = '';
}
}

function add_mailing_list_step1() {

list_name_try = document.getElementById('add_mailing_list').value;

	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
// Create a function that will receive data sent from the server
ajaxRequest.onreadystatechange = function(){

document.getElementById('resultstxt').innerHTML = '';
document.getElementById('resultstxt').style.display = 'none';

		
	if(ajaxRequest.readyState == 4){
		list_name_check = ajaxRequest.responseText;

if (list_name_check == "LIST_NAME_INVALID") {
document.getElementById('ml_resultstxt').innerHTML = 'List name invalid - please enter a valid list name';
document.getElementById('ml_resultstxt').style.display = 'block';
}

else if (list_name_check == "FORWARD_EXISTS") {
document.getElementById('ml_resultstxt').innerHTML = 'An email forward already exists with this name';
document.getElementById('ml_resultstxt').style.display = 'block';
}

else if (list_name_check == "MAILING_LIST_EXISTS") {
document.getElementById('ml_resultstxt').innerHTML = 'A mailing list already exists with this name';
document.getElementById('ml_resultstxt').style.display = 'block';
}

else {

document.getElementById('mailing_list_div').innerHTML = document.getElementById('mailing_list_add_2').innerHTML;
document.getElementById('add_mailing_list').value = ajaxRequest.responseText;

}

  }
}

var theurl = "index.php?ml=1&list_name=" + list_name_try;
ajaxRequest.open("GET", theurl, true);
ajaxRequest.send(null);

}

function add_mailing_list_step2() {

document.getElementById('mailing_list_div').style.overflow = "hidden";

list_name = list_name_try;

password_try = document.getElementById('list_password').value;

	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
// Create a function that will receive data sent from the server
ajaxRequest.onreadystatechange = function(){

document.getElementById('resultstxt').innerHTML = '';
document.getElementById('resultstxt').style.display = 'none';

		
	if(ajaxRequest.readyState == 4){
		password_check = ajaxRequest.responseText;

if (password_check == "PASSWORD_INVALID") {
document.getElementById('ml_resultstxt').innerHTML = 'Password invalid - please enter a valid password';
document.getElementById('ml_resultstxt').style.display = 'block';
}

else {


var form = document.createElement("form");
form.setAttribute("method", "post");
form.setAttribute("action", "index.php");

var hiddenField = document.createElement("input");              
hiddenField.setAttribute("name", "list_name");
hiddenField.setAttribute("value", list_name);

var hiddenField2 = document.createElement("input");              
hiddenField2.setAttribute("name", "list_password");
hiddenField2.setAttribute("value", password_check);

form.appendChild(hiddenField);
form.appendChild(hiddenField2);
document.body.appendChild(form);

form.submit();


}

  }
}

var theurl = "index.php?ml=1&list_password=" + password_try;
ajaxRequest.open("GET", theurl, true);
ajaxRequest.send(null);

}




</script>

<% if $smarty.get.successmsg %>
<div id="successbar">
<img src="/content_files/images/icons/notification.png" style="vertical-align: text-bottom; width: 18px; height: 15px">

<% if $smarty.get.message == "email_fwd_deleted" %>
Email forward deleted
<% /if %>

<% if $smarty.get.message == "email_fwd_added" %>
Email forward added
<% /if %>

<% if $smarty.get.message == "mailing_list_added" %>
Mailing list added
<% /if %>

</div>
<% /if %>

<!-- begin segment -->

<div style="float: left; width:262px; line-height:normal; margin: 2px">
<div style="padding:5px; margin:0 auto; text-align: center; background: black; color: white">Hosting Details</div>

<div style="padding: 5px; border: 1px dotted black; border-top: none; height: 75px; overflow:auto">
<% if !$error.hosting_details %>
<table class="d_table">
<tr>
<td><b>Hosting Package:</b></td>
<td><a href="http://www.mddhosting.com/hosting.php"><% $cp_admin.hosting.package_name %></a></td>
</tr>
<tr>
<td><b>Disk Space Used:</b></td>
<td>
<% if ($cp_admin.hosting.disk_space_low) %><b style="color:red"><% /if %>
<% $cp_admin.hosting.disk_space_used %> / <% $cp_admin.hosting.disk_space_quota %>
<% if ($cp_admin.hosting.disk_space_low) %></b><% /if %>
</td>
</tr>
<tr>
<td><b>Bandwidth Used:</b></td>
<td>
<% if ($cp_admin.hosting.bandwidth_low) %><b style="color:red"><% /if %>
<% $cp_admin.hosting.bandwidth_used %> / <% $cp_admin.hosting.bandwidth_quota %>
<% if ($cp_admin.hosting.bandwidth_low) %></b><% /if %>
</td>
</tr>
<tr>
<td><b>Monthly Cost:</b></td>
<td>$6.38</td>
</tr>
</table>
<% else %>
There was an error fetching the hosting details - script maintenance is required.
<% /if %>
</div>

</div>

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:262px; line-height:normal; margin: 2px">
<div style="padding: 5px 5px 5px 37px; margin:0 auto; text-align: center; background: black; color: white">
<div style="float:right"><a href="admin_list.php"><img src="/content_files/images/icon_edit.gif"></a></div>
Portal Admins</div>

<div style="padding: 5px; border: 1px dotted black; border-top: none; height: 75px; overflow:auto">
<table class="d_table">
<% foreach from=$admin_list item=i %>
</tr>
<td><% $i.first_name %> <% $i.last_name %></td>
</tr>
<% /foreach %>
</table>
</div>

</div>

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:262px; line-height:normal; margin: 2px">
<div style="padding: 5px 5px 5px 23px; margin:0 auto; text-align: center; background: black; color: white">
<div style="float:right"><a href="activity_log.php?submitted=1&r[user_login]=1&r[user_login_remember_me]=1"><img src="/content_files/images/arrow_double_right.png" style="width:18px; height: 14.6px"></a></div>
Recent Logins</div>

<div style="padding: 5px; border: 1px dotted black; border-top: none; height: 75px; overflow:auto">
<table class="d_table">
<% foreach from=$login_list item=i %>
<tr>
<td><b><% $i.time|date_format:"%H:%M:%S" %></b></td>
<td><% $i.first_name %> <% $i.last_name %></td>
</tr>
<% /foreach %>
</table>
</div>

</div>

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:395px; line-height:normal; margin: 2px">
<div style="padding: 5px; margin:0 auto; text-align: center; background: black; color: white">
User Management</div>

<div style="padding: 7px 5px 3px 5px; border: 1px dotted black; border-top: none; height: 75px; overflow:auto">

<div style="float:left; width: 95px; text-align: center">
<a href="user_add.php" style="text-decoration:none">
<img src="/content_files/images/icons/add-user.png" style="width:56px; height: 56px"><BR>
Add User</a>
</div>

<div style="float:left; width: 95px; text-align: center">
<a href="loginas.php" style="text-decoration:none">
<img src="/content_files/images/icons/login.png" style="width:56px; height: 56px"><BR>
Login As</a>
</div>

<div style="float:left; width: 95px; text-align: center">
<a href="user_edit.php" style="text-decoration:none">
<img src="/content_files/images/icons/edit-user.png" style="width:56px; height: 56px"><BR>
Edit User</a>
</div>

<div style="float:left; width: 95px; text-align: center">
<a href="user_groups.php" style="text-decoration:none">
<img src="/content_files/images/icons/group.png" style="width:56px; height: 56px"><BR>
Groups
</a>
</div>

</div>

</div>

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:395px; line-height:normal; margin: 2px">
<div style="padding: 5px 5px 5px 23px; margin:0 auto; text-align: center; background: black; color: white">
<div style="float:right"><a href="/portal/transcripts/"><img src="/content_files/images/arrow_double_right.png" style="width:18px; height: 14.6px"></a></div>
Recent Transcripts</div>

<div style="padding: 5px; border: 1px dotted black; border-top: none; height: 75px; overflow:auto">
<table class="d_table">
<% foreach from=$transcript_list item=i %>
<tr>
<td><b><% $i.date|date_format:"%Y-%m-%d" %></b></td>
<td><% $i.sector|truncate:30:"...":TRUE %></td>
<td>(<a href="/portal/transcripts/index.php?sector=<% $i.sector_s %>&date=<% $i.date|date_format:"%Y-%m-%d" %>"><% $i.message_num %> messages</a>)</td>
</tr>
<% /foreach %>
</table>
</div>

</div>

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:395px; line-height:normal; margin: 2px">
<div style="padding: 5px; margin:0 auto; text-align: center; background: black; color: white">
<% if !$error.emailfwd_list %>
	<div style="float:right"><a href="#" onClick="show_add_fwd_form(); return false;"><img src="/content_files/images/icons/plus.png" style="width:16px; height: 16px"></a></div>
<% /if %>
Email Forwarders</div>

<div id="email_fwd_div" style="padding: 5px; border: 1px dotted black; border-top: none; height: 75px; overflow:auto">
<% if $error.emailfwd_list %>
There was an error fetching the list, most likely caused by an update to cPanel. Script maintenance is required.
<% else %>
<table>
<% foreach from=$emailfwd_list item=i %>
<tr>
<td><a href="index.php?del_fwd=1&del_gps_email=<% $i.dest %>&del_dest_email=<% $i.forward %>" onClick="return confirm('Are you sure you want to delete: <% $i.dest %> -> <% $i.forward %>?');"><img src="/content_files/images/delete.png" style="margin-right:3px"></a></td>
<td><b><% $i.dest|replace:"@gps100.com":"" %></b></td>
<td style="text-align: right"><% $i.forward %></td>
</tr>
<% /foreach %>
</table>
<% /if %>
</div>

</div>

<!-- hidden divs -->

<div id="email_fwd_add_1" style="display:none">
<div id="resultstxt" style="display:none; color:red; margin-bottom: 4px">&nbsp;</div>
GPS Email: <input type='text' name='gps_email' value='E.g. tshannon@gps100.com' style='width: 280px' onClick='clear_default();' id="add_gps_email"><BR><BR>
<span class='button default strong' onClick='restore_email_forward_list();'><input type='button' value='Cancel'></span>
<span class='button default strong' style='float:right'><input type='button' value='Next' onClick='add_fwd_step1();'></span>
</div>

<div id="email_fwd_add_restore" style="display:none">
&nbsp;
</div>

<div id="email_fwd_add_2" style="display:none">
<div id="resultstxt" style="display:none; color:red; margin-bottom: 4px">&nbsp;</div>
<input type='text' name='gps_email' value='' style='width: 165px; background: #ededed; border:0' id="add_gps_email" readonly> ->
<input type='text' name='email_dest' value='E.g. bob@gmail.com' style='width: 190px; margin-left: 5px' id="add_email_dest" onClick="clear_default_dest()";><BR><BR>
<span class='button default strong' onClick='restore_email_forward_list();'><input type='button' value='Cancel'></span>
<span class='button default strong' style='float:right'><input type='button' value='Save' onClick='add_fwd_step2();'></span>
</div>

<!-- end hidden divs -->

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:395px; line-height:normal; margin: 2px">
<div style="padding: 5px; margin:0 auto; text-align: center; background: black; color: white">
<% if !$error.emailfwd_list %>
	<div style="float:right"><a href="#" onClick="show_add_mailing_list_form(); return false;"><img src="/content_files/images/icons/plus.png" style="width:16px; height: 16px"></a></div>
<% /if %>
Mailing Lists</div>

<div id="mailing_list_div" style="padding: 5px; border: 1px dotted black; border-top: none; height: 75px; overflow:auto">
<% if $error.emailfwd_list %>
There was an error fetching the list, most likely caused by an update to cPanel. Script maintenance is required.
<% else %>
<table>
<% foreach from=$mailinglists_list item=i %>
<tr>
<td><a href="#" onClick="open_list('<% $i.listid %>');return false;"><% $i.list|replace:"@gps100.com":"" %></td>
</tr>
<% /foreach %>
</table>
<% /if %>
</div>

</div>

<!-- hidden divs -->

<div id="mailing_list_add_1" style="display:none">
<div id="ml_resultstxt" style="display:none; color:red; margin-bottom: 4px">&nbsp;</div>
<% if !$mailman_curl_error %>
List name: <input type='text' name='list_name' value='E.g. industrials@gps100.com' style='width: 280px' onClick='clear_mailing_list_default();' id="add_mailing_list"><BR><BR>
<span class='button default strong' onClick='restore_mailing_list_list();'><input type='button' value='Cancel'></span>
<span class='button default strong' style='float:right'><input type='button' value='Next' onClick='add_mailing_list_step1();'></span>
<% else %>
<b>Important Notice:</b> Adding mailing lists through the admin control panel has been automatically disabled.<BR><BR>
<b>Technical details:</b> Script uses PHP cURL to interface with Mailman 2.1.14 (no API), it is likely a software upgrade is culpable. Script maintenance required - contact bwigoder@gps100.com for further info.
<% /if %>
</div>

<div id="mailing_list_add_restore" style="display:none">
&nbsp;
</div>

<div id="mailing_list_add_2" style="display:none">
<div id="ml_resultstxt" style="display:none; color:red; margin-bottom: 4px">&nbsp;</div>
List name: <input type='text' name='list_name' value='' style='width: 280px; background: #ededed; border:0' id="add_mailing_list" readonly><BR>
Password: <input type='text' name='list_password' value='' style='width: 190px; margin-left: 5px' id="list_password"><BR>
<span class='button default strong' onClick='restore_mailing_list_list();'><input type='button' value='Cancel'></span>
<span class='button default strong' style='float:right'><input type='button' value='Save' onClick='add_mailing_list_step2();'></span>
</div>

<!-- end hidden divs -->

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:395px; line-height:normal; margin: 2px">
<div style="padding: 5px; margin:0 auto; text-align: center; background: black; color: white">
Resumes</div>

<div style="float: left; border: 1px dotted black; border-top: none; border-right: none; padding: 5px; height: 75px; overflow:auto; width: 283px">
<table class="d_table">
<% foreach from=$resume_list item=i %>
<tr>
<td><b><% $i.resume_date|date_format:"%Y-%m-%d" %></b></td>
<td><a href="/portal/toolbox/resume_upload.php?download=1&id=<% $i.id %>"><% $i.first_name %> <% $i.last_name %></a></td>
</tr>
<% /foreach %>
</table>
</div>
<div style="float: left; border: 1px dotted black; border-top: none; border-left: none; padding: 7px 5px 3px 5px; height: 75px; width: 90px; text-align: center">
<a href="resume_booklets.php">
<img src="/content_files/images/icons/book_icon.png" style="width:72px; height: 55px"><BR>
<div style="padding-top: 2px">Create Books</div>
</a>
</div>

</div>

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:395px; line-height:normal; margin: 2px">
<div style="padding: 5px; margin:0 auto; text-align: center; background: black; color: white">
Documents</div>
<div class="cp_menu">
<div class="cp_option_cur cp_option_l" id="docs_1_menu"><a href="#" onClick="switch_tab('docs_1'); return false;">Overview</a></div>
<div class="cp_option" id="docs_2_menu"><a href="#" onClick="switch_tab('docs_2'); return false;">User Stats</a></div>
<div class="cp_option" id="docs_3_menu"><a href="#" onClick="switch_tab('docs_3'); return false;">File stats</a></div>
<div class="cp_option" id="docs_4_menu"><a href="#" onClick="switch_tab('docs_4'); return false;">Recycle Bin</a></div>
</div>

<div style="padding: 5px; border: 1px dotted black; border-top: none; height: 55px; overflow:auto; clear: left" id="docs_content">

</div>

<div style="display:none" id="docs_1_content">
<table class="d_table">
<tr>
<td><b>Disk Usage:</b></td>
<td><% $documents_total_disk_usage %> MB</td>
</tr>
<tr>
<td><b>Files:</b></td>
<td><% $documents_file_count %> (<% $documents_disk_usage %> MB)</td>
</tr>
<tr>
<td><b>Recycle Bin:</b></td>
<td><a href="#" onClick="switch_tab('docs_4'); return false;"><% $documents_recycled_file_count %> files (<% $documents_recycled_disk_usage %> MB)</a></td>
</tr>
</table>
</div>

<div style="display:none" id="docs_2_content">
<table class="d_table">
<% foreach from=$dms_user_stats item=i %>
<tr>
<td><b><% $i.totalbytes|file_size %></b></td>
<td><a href="/portal/network/view/overview.php?id=<% $i.id %>"><% $i.first_name %> <% $i.last_name %></a></td>
</tr>
<% /foreach %>
</table>
</div>

<div style="display:none" id="docs_3_content">
<table class="d_table">
<% foreach from=$dms_file_stats item=i %>
<tr>
<td><b><% $i.download_count %></b></td>
<td><a href="/portal/documents/folder/folders/<% $i.id_folder %>"><% $i.file_name|truncate:50:'...':true:false %></a></td>
</tr>
<% /foreach %>
</table>
</div>

<div style="display:none" id="docs_4_content">
<table class="d_table">
<tr>
<td><b>Disk Usage:</b></td>
<td><% $documents_recycled_disk_usage %> MB</td>
</tr>
<tr>
<td><b>Files:</b></td>
<td><% $documents_recycled_file_count %></td>
</tr>
<tr>
<td><b>Restore Files:</b></td>
<td><a href="/portal/documents/file/recycled_file_folders">View Recycle Bin</a></td>
</tr>
</table>
</div>

<script>
//load first tab into content div
switch_tab('docs_1');
</script>

</div>

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:395px; line-height:normal; margin: 2px">
<div style="padding: 5px 5px 5px 23px; margin:0 auto; text-align: center; background: black; color: white">
<div style="float:right"><a href="surveys.php"><img src="/content_files/images/arrow_double_right.png" style="width:18px; height: 14.6px"></a></div>
Recent Surveys</div>

<div style="padding: 5px; border: 1px dotted black; border-top: none; height: 105px; overflow:auto">
<table class="d_table">
<% foreach from=$survey_list item=i %>
<tr>
<td><b><% $i.deadline|date_format:"%Y-%m-%d" %></b></td>
<td><% $i.question|truncate:35:"":TRUE %></td>
<td>(<a href="surveys.php?download=<% $i.id %>"><% $i.total %> votes</a>)</td>
</tr>
<% /foreach %>
</table>
</div>

</div>

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:395px; line-height:normal; margin: 2px">
<div style="padding: 5px; margin:0 auto; text-align: center; background: black; color: white">
Payments</div>

<div class="cp_menu">
<div class="cp_option_cur cp_option_l" id="payments_1_menu"><a href="#" onClick="switch_tab('payments_1'); return false;">Menu</a></div>
<div class="cp_option" id="payments_2_menu"><a href="#" onClick="switch_tab('payments_2'); return false;">Analysts</a></div>
<div class="cp_option" id="payments_3_menu"><a href="#" onClick="switch_tab('payments_3'); return false;">Members</a></div>
<div class="cp_option" id="payments_4_menu"><a href="#" onClick="switch_tab('payments_4'); return false;">Alumni</a></div>
</div>

<div style="padding: 5px; border: 1px dotted black; border-top: none; height: 85px; overflow:auto; clear: left" id="payments_content">

</div>

<div style="display:none" id="payments_1_content">
<table class="d_table" style="text-align: center; margin-top: 4px">
<tr>
  <td>
    <div style="float: left; width: 95px">
      <a href="payments_conferences.php" style="text-decoration: none"><img src="/content_files/images/icons/conferences.png" style="width:57px; height: 61px:"><BR>Conferences</a>
    </div>
    <div style="float: left; width: 95px">
      <a href="payments_invoices.php" style="text-decoration: none"><img src="/content_files/images/icons/invoices.png" style="width:57px; height: 61px:"><BR>Invoices</a>
    </div>
    <div style="float: left; width: 95px">
      <a href="payments_reports.php" style="text-decoration: none"><img src="/content_files/images/icons/reports.png" style="width:57px; height: 61px:"><BR>Reports</a>
    </div>
  </td>
</tr>
</table>
</div>

<div style="display:none" id="payments_2_content">
  <iframe src="/includes/payments/progress.php?group=analyst" class="progress" scrolling="no"></iframe>
</div>

<div style="display:none" id="payments_3_content">
  <iframe src="/includes/payments/progress.php?group=member" class="progress" scrolling="no"></iframe>
</div>

<div style="display:none" id="payments_4_content">
  <iframe src="/includes/payments/progress.php?group=alumni" class="progress" scrolling="no"></iframe>
</div>


<script>
//load first tab into content div
switch_tab('payments_1');
</script>

</div>

<!-- end segment -->


</div>
<div style="clear:both">

<!-- further text goes here -->

<!-- end further text goes here -->

</div>