<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>

<div class="editlink">
<% if $is_editing %>
<a href="#" onClick="update_groups(); return false;">Save</a> |
<a href="/portal/admin/user_groups.php">Cancel</a>
<% else %>
<a href="/portal/admin/user_groups.php?edit=1">Edit</a>
<% /if %>
</div>
<h2>User Groups</h2>

<% if $is_editing %>
<script type="text/javascript">
	window.onload = function() {
		var list = document.getElementById("analysts");
		DragDrop.makeListContainer( list );
		list.onDragOver = function() { this.style["border"] = "1px dashed #AAA"; };
		list.onDragOut = function() {this.style["border"] = "1px solid white"; };
		
		list = document.getElementById("members");
		DragDrop.makeListContainer( list );
		list.onDragOver = function() { this.style["border"] = "1px dashed #AAA"; };
		list.onDragOut = function() {this.style["border"] = "1px solid white"; };

		list = document.getElementById("alumni");
		DragDrop.makeListContainer( list );
		list.onDragOver = function() { this.style["border"] = "1px dashed #AAA"; };
		list.onDragOut = function() {this.style["border"] = "1px solid white"; };
	};
</script>
<style>
ul.boxy { cursor:move; }
</style>
<% /if %>

<div class="listdiv">
<div class="title">Analysts</div>
<div class="thelist">
<ul id="analysts" class="sortable boxy" style="margin-left: 1em;">
<% foreach from=$group.analysts item=i %>
<li><% $i.first_name %> <% $i.last_name %><i><% $i.id %></i></li>
<% /foreach %>
</ul>
</div>
</div>

<div class="listdiv">
<div class="title">Members</div>
<div class="thelist">
<ul id="members" class="sortable boxy" style="margin-right: 1em;">
<% foreach from=$group.members item=i %>
<li><% $i.first_name %> <% $i.last_name %><i><% $i.id %></i></li>
<% /foreach %>
</ul>
</div>
</div>

<div class="listdiv">
<div class="title">Alumni</div>
<div class="thelist">
<ul id="alumni" class="sortable boxy" style="margin-right: 1em;">
<% foreach from=$group.alumni item=i %>
<li><% $i.first_name %> <% $i.last_name %><i><% $i.id %></i></li>
<% /foreach %>
</ul>
</div>
</div>

<div style="clear:both">&nbsp;</div>

</div>