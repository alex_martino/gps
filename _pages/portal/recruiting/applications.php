<div><img src="/content_files/headers/recruiting.gif" width="800" height="90"></div>
<div>

<p class="btxt">Please select a recruiting cycle from the list below.
<% if ($can_cycle_create) %>
, or <a href="new_cycle.php">create a new one</a>
<% /if %>

</p>

<table style="width: 600px; border-spacing:0" class="applications">
<tr style="background: #182e72; color: white; font-weight: bold">
<td>University</td><td>Deadline</td><td>Applications</td>
</tr>


<% foreach from=$rowdetails item=i key=key %>
<tr class="<% $i.class %>">

<% if ($i.appsnumber > 0) %>

<td><a href='viewapplications.php?cycle=<% $i.cycle %>'><% $i.uni_sname %></a></td>
<td><a href='viewapplications.php?cycle=<% $i.cycle %>'><% $i.deadline_f %></a></td>
<td><a href='viewapplications.php?cycle=<% $i.cycle %>'><% $i.appsnumber %></a></td>

<% else %>
<td><% $i.uni_sname %></td>
<td><% $i.deadline_f %></td>
<td>0</td>

<% /if %>

</tr>
<% /foreach %>

</table>

</div>