<div><img src="/content_files/headers/account.gif" width="800" height="90"></div>
<div>
	<h2>Email Settings</h2>
	<p style="color: #FF0000; text-align: left;">
		<% $error_message %>
	</p>
<% if $success == 1 %>
<p>
<b><% $success_message %></b>
</p>
<% /if %>

<form name="email_preferences" id="email_preferences" action="emails.php" method="POST">
<div style="float: left; font-size: 15px; line-height: 20px; margin-right: 10px; padding-top: 2px">
Articles Digest
</div>

<div style="line-height: 20px">
<select name="articles_digest">
<option value="1"<% if ($articles_digest_on) %> SELECTED<% /if %>>On</option>
<option value="0"<% if (!$articles_digest_on) %> SELECTED<% /if %>>Off</option>
</select>
</div>

<div style="clear:both; margin-top: 20px">

<span class="button default strong"><input type="submit" value="Save Changes"></span>

</div>

</form>

</div>
