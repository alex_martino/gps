<div><img src="/content_files/headers/transcripts.gif" width="800" height="90"></div>
<div>
<% if !$sector %>
<h2>Introduction</h2>
<p>Browse by sector using the menu on the left or search using the feature below.</p>
<BR>
<h2>Search</h2>
<form name="transcript_search" action="index.php?ts=1" method="post">
<div style="float:left; margin-right: 5px">
<div style="height: 22px">Search terms:</div>
<div style="height: 22px">Sector:</div>
<div style="height: 22px">
<span class="button default strong"><input type="button" onClick="document.transcript_search.submit()" value="Search Database"></span>
</div>
</div>
<div style="float:left">
<div style="height: 22px">
<input type="text" name="searchbox" value="<% $smarty.post.searchbox %>" style="width: 200px">
</div>
<div style="height: 22px">
<select name="filter_sector">
<option value="0">* All Sectors *</option>
<% foreach from=$sectors item=i %>
<option value="<% $i.sname %>"<% if $smarty.post.filter_sector == $i.sname %> SELECTED<% /if %>><% $i.sname %></option>
<% /foreach %>
</select>
</div>
</div>
</form>
<div style="clear:both">&nbsp;</div><BR>
<% elseif !$date %>

<% if ($sector != "gm") %>
<h2><% $sector|capitalize %></h2>
<% else %>
<h2>GM</h2>
<% /if %>

<% if isset($dates) %>
<% foreach from=$dates item=i %>
<a href="index.php?sector=<% $sector %>&date=<% $i.date %>"><% $i.date|date_format:"%B %e, %Y" %></a><BR>
<% /foreach %>
<% else %>
<p>No transcripts have been stored yet.</p>
<% /if %>

<% else %>

<% if ($sector != "gm") %>
<h2><% $sector|capitalize %>: <% $date|date_format:"%A, %B %e, %Y" %></h2>
<% else %>
<h2>GM: <% $date|date_format:"%A, %B %e, %Y" %></h2>
<% /if %>

<% if $smarty.get.message_id > 0 %>
<a href="javascript:history.go(-1);">Return to Search Results</a><BR><BR>
<% /if %>

<div style="width: 700px">
<table class="admin_table_2">
<tr style="border-bottom: 1px solid black">
<th style="width:30px">Time</th>
<th style="width:100px">Member</th>
<th>Message</th>
</tr>
<% foreach from=$messages item=i %>
<% if $smarty.get.message_id == $i.id %>
<tr style="background:yellow">
<% else %>
<tr>
<% /if %>
<td><% $i.timestamp|date_format:"%H:%M" %></td>
<td><a href="/portal/network/view/overview.php?id=<% $i.userid|truncate:20:"..":TRUE %>" name="<% $i.id %>"><% $i.username %></a></td>
<td><% $i.message %></td>
</tr>
<% /foreach %>
</table>
</div>
<% /if %>

<% if $smarty.get.ts %>
<h2>Search Results: "<% $s_query %>"</h2>

<script>
function show_message(id) {
window.location="index.php?show_message="+id;
}
</script>

<% if $error %>
<% $errormsg %>
<% else %>

<% if $results_count > 0 %>

<% $pagination %>

<table class="admin_table" style="margin-top: 15px">
<tr style="border-bottom: 1px solid black">
<th style="width: 50px">Date</th>
<th>Sector</th>
<th style="width: 100px">User</th>
<th>Message</th>
</tr>
<% foreach from=$results_r item=i %>
<tr class="row" onClick="show_message(<% $i.id %>);">
<td><% $i.sent|date_format:"%D" %></td>
<td><% $i.sector %></td>
<td><% $i.username|truncate:20:'..':true:true %></td>
<td><% $i.message %></td>
</tr>
<% /foreach %>
</table>

<% else %>

No results found for "<% $s_query %>"

<% /if %>

<% /if %>
<% /if %>

</div>