<div><img src="/content_files/headers/profile.gif" width="800" height="90"></div>
<div>
	<h2>GPS Member Information</h2>
	<p style="color: #FF0000; text-align: center;">
		<% $error_message %>
	</p>
	<form id="form" action="<% $php_self %>" method="post" enctype="multipart/form-data">
		<fieldset>
		<legend>Personal Information</legend>
		<p style="float: left;">First Name:<br />
			<input type="text" id="first_name" name="first_name" value="<% $member.first_name %>" style="width: 230px;" />
		</p>
		<p style="float: right;">Last Name:<br />
			<input type="text" id="last_name" name="last_name" value="<% $member.last_name %>" style="width: 230px;" />
		</p>
		<br style="clear: both;" />
		<p>Biography:<br />
			<textarea id="biography" name="biography" style="width: 100%"><% $member.biography %></textarea>
		</p>
		<p style="float: left;">Location:<br />
			<input type="text" id="location" name="location" value="<% $member.location %>" style="width: 230px;" />
		</p>
		<p style="float: right">Hometown:<br />
			<input type="text" id="hometown" name="hometown" value="<% $member.hometown %>" style="width: 230px;" />
		</p>
		<br style="clear: both;" />
		<p>Hobbies:<br />
			<input type="text" id="hobbies" name="hobbies" value="<% $member.hobbies %>" style="width: 100%;" />
		</p>
		<p>Birth Date:<br />
			<input type="text" id="birth_date" name="birth_date" value="<% $member.birth_date %>" style="width: 230px;" />
		</p>
		</fieldset>
		<fieldset>
		<legend>School & Professional Information</legend>
		<p style="float: left;">Undergraduate School:<br />
			<input type="text" id="undergraduate_school" name="undergraduate_school" value="<% $member.undergraduate_school %>" style="width: 230px;" />
		</p>
		<p style="float: right;">Undergraduate Major:<br />
			<input type="text" id="undergraduate_major" name="undergraduate_major" value="<% $member.undergraduate_major %>" style="width: 230px;" />
		</p>
		<br style="clear: both;" />
		<p style="float: left;">Graduation Year:<br />
			<input type="text" id="graduation_year" name="graduation_year" value="<% $member.graduation_year %>" style="width: 230px;" />
		</p>
		<p style="float: right;">Graduate School:<br />
			<input type="text" id="graduate_school" name="graduate_school" value="<% $member.graduate_school %>" style="width: 230px;" />
		</p>
		<br style="clear: both;" />
		<p>Professional Designations:<br />
			<input type="text" id="professional_designations" name="professional_designations" value="<% $member.professional_designations %>" style="width: 230px;" />
		</p>
		<p style="float: left;">Current Employer:<br />
			<input type="text" id="current_employer" name="current_employer" value="<% $member.current_employer %>" style="width: 230px;" />
		</p>
		<p style="float: right;">Previous Employers:<br />
			<input type="text" id="previous_employers" name="previous_employers" value="<% $member.previous_employers %>" style="width: 230px;" />
		</p>
		<br style="clear: both;" />
		</fieldset>
		<fieldset>
		<legend>GPS Information</legend>
		<p>GPS Position Held:<br />
			<input type="text" id="gps_position_held" name="gps_position_held" value="<% $member.gps_position_held %>" style="width: 230px;" />
		</p>
		<p>Favorite GPS Memory:<br />
			<input type="text" id="favorite_gps_memory" name="favorite_gps_memory" value="<% $member.favorite_gps_memory %>" style="width: 100%;" />
		</p>
		<p>Analyst Stock Pitch:<br />
			<input type="text" id="analyst_stock_pitch" name="analyst_stock_pitch" value="<% $member.analyst_stock_pitch %>" style="width: 230px;" />
		</p>
		<p style="float: left;">Alumni Mentor:<br />
			<input type="text" id="alumni_mentor" name="alumni_mentor" value="<% $member.alumni_mentor %>" style="width: 230px;" />
		</p>
		<p style="float: right;">Member Advocate:<br />
			<input type="text" id="member_advocate" name="member_advocate" value="<% $member.member_advocate %>" style="width: 230px;" />
		</p>
		<br style="clear: both;" />
		</fieldset>
		<fieldset>
		<legend>Photos</legend>
		<p style="float: left;">Current Photo:<br />
			<input type="file" id="current_photo" name="current_photo" value="" style="width: 230px;" />
		</p>
		<p style="float: right;">School Photo:<br />
			<input type="file" id="school_photo" name="school_photo" value="" style="width: 230px;" />
		</p>
		<br style="clear: both;" />
		<p><em>* Images should be approximately 150px in width</em></p>
		<br style="clear: both;" />
		<div style="float: left; width: 230px;">
			<img src="<% $member.current_photo %>" width="150" alt="" />
		</div>
		<div style="float: right; width: 230px;">
			<img src="<% $member.school_photo %>" width="150" alt="" />
		</div>
		<br style="clear: both;" />
		</fieldset>
		<div style="text-align: center;"><a href="javascript: submitform('form');" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('submit_button','','/content_files/images/buttons/submit_over.gif',1)"><img name="submit_button" border="0" src="/content_files/images/buttons/submit_out.gif" width="66" height="17"></a> <a href="/members/index.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('cancel_button','','/content_files/images/buttons/cancel_over.gif',1)"><img name="cancel_button" border="0" src="/content_files/images/buttons/cancel_out.gif" width="66" height="17"></a></div>
		<input type="hidden" id="current_photo" name="current_photo" value="<% $member.current_photo %>" />
		<input type="hidden" id="school_photo" name="school_photo" value="<% $member.school_photo %>" />
		<input type="hidden" id="founding_member" name="founding_member" value="<% $member.founding_member %>" />
		<input type="hidden" id="chapter_founder" name="chapter_founder" value="<% $member.chapter_founder %>" />
		<input type="hidden" id="board_manager" name="board_manager" value="<% $member.board_manager %>" />
		<input type="hidden" id="upper_management" name="upper_management" value="<% $member.upper_management %>" />
		<input type="hidden" id="member" name="member" value="<% $member.member %>" />
		<input type="hidden" id="alumni" name="alumni" value="<% $member.alumni %>" />
		<input type="hidden" id="advisory_board" name="advisory_board" value="<% $member.advisory_board %>" />
		<input type="hidden" id="required" name="required" value="first_name,last_name,email_address" />
		<input type="hidden" id="action" name="action" value="update" />
	</form>
</div>
