<div><img src="/content_files/headers/network.gif" width="800" height="90">

<% if $not_viewable %>
<div style="margin-top: 15px; margin-left: 10px">
<p>This profile is not available. <a href="javascript:history.go(-1)">Go back.</a></p>

<% else %>

<div id="network_t_menu">
  <ul>
<% foreach from=$network_t_menu item=i %>
    <li<% if $i.current == "1" %> id="current"<% /if %>><a href="<% $i.url %>?id=<% $profile.id %>"><% $i.name %></a></li>
<% /foreach %>
  </ul>
</div>
</div>
<div style="margin-top: 25px">

<% if $can_edit %>
<div class="editlink">
<% if $is_editing %>
<a href="#" onClick="do_edit.submit(); return false;">Save</a> | <a href="academia.php?id=<% $profile_id %>">Cancel</a>
<% else %>
<a href="academia.php?id=<% $profile_id %>&edit=1">Edit</a>
<% /if %>
</div>
<% /if %>

<h2><% $profile.first_name %> <% $profile.last_name %></h2>

<div style="float:left" style="width: 150px">

<% if $display_photo %>

<img name="profile_img" src="<% $profile.current_photo %>" width="150" alt="Photo of <% $profile.first_name %> <% $profile.last_name %>"
<% if $profile.school_photo %>
<%* onmouseover="this.src='<% $profile.school_photo %>'" onmouseout="this.src='<% $profile.current_photo %>'" *%>
<% /if %>
/>

<% else %>

<img src="/images/male.png" width="150" alt="Photo missing" />

<% /if %>

</div>

<% if ($is_editing) %>
<form name="do_edit" action="academia.php?id=<% $profile_id %>&edit=1" method="POST">
<input type="hidden" name="submitted" value="1">

<div class="network_data">
		<fieldset>
		<legend>Undergraduate</legend>
		<p style="float: left;">University:<br />
			<% $profile.undergraduate_school %>
		</p>
		<p style="float: left; clear:left">Graduation Year:<br />
			<input type="text" id="graduation_year" name="graduation_year" value="<% $profile.graduation_year %>" style="width: 40px;" />
		</p>
		<p style="float: left; clear:left">Major:<br />
			<input type="text" id="undergraduate_major" name="undergraduate_major" value="<% $profile.undergraduate_major %>" style="width: 230px;" />
		</p>
		</fieldset>
</div>

<div class="network_data">
		<fieldset>
		<legend>Graduate</legend>
		<p style="float: left;">Graduate School:<br />
			<input type="text" id="graduate_school" name="graduate_school" value="<% $profile.graduate_school %>" style="width: 230px;" />
		</p>
		<p style="float: left; clear:left">Graduate Major:<br />
			<input type="text" id="graduate_major" name="graduate_major" value="<% $profile.graduate_major %>" style="width: 230px;" />
		</p>
		</fieldset>
</div>

<% else %>

<div class="network_data">

<div id="university">
<div style="float:left" class="data_title">
University
</div>
<div style="float:left" class="job_info">
<b style="font-size: 15px"><a href="/portal/network/search.php?s=1&uni=<% $profile.ug_school_id %>"><% $profile.uni_fname %></a></b>
<span style="font-size: 15px"><% if ($profile.graduation_year) %>| <a href="/portal/network/search.php?s=1&year=<% $profile.graduation_year %>"><% $profile.graduation_year %></a></span><% /if %>
<% if ($profile.undergraduate_major) %>
<BR><i style="font-size: 13px"><% $profile.undergraduate_major %>
<% /if %>
</div>
</div>

<% if ($profile.graduate_school) %><BR>
<div id="gradschool">
<div style="float:left" class="data_title">
Graduate School
</div>
<div style="float:left" class="job_info">
<b style="font-size: 15px"><% $profile.graduate_school %></b>
<% if ($profile.graduate_major) %>
<BR><i style="font-size: 13px"><% $profile.graduate_major %>
<% /if %>
</div>
</div>
<% /if %>
<% if (($prof_des > 1)) %>
<BR><BR>
<div id="professional_designations" class="network_block">
<div style="float:left" class="data_title">
Professional Designations
</div>
<div style="float:left" class="data_data">
<% $profile.professional_designations %><BR>
</div>
</div>
<% /if %>

</div>

<% /if %>

<div style="clear:both">&nbsp;</div>

<% /if %>

</div>
