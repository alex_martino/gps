<div><img src="/content_files/headers/network.gif" width="800" height="90">

<% if $not_viewable %>
<div style="margin-top: 15px; margin-left: 10px">
<p>This profile is not available. <a href="javascript:history.go(-1)">Go back.</a></p>

<% else %>
<div id="network_t_menu">
  <ul>
<% foreach from=$network_t_menu item=i %>
    <li<% if $i.current == "1" %> id="current"<% /if %>><a href="<% $i.url %>?id=<% $profile.id %>"><% $i.name %></a></li>
<% /foreach %>
  </ul>
</div>
</div>
<div style="margin-top: 25px">

<% if $can_edit %>
<div class="editlink">
<% if $is_editing %>
<a href="#" onClick="document.do_edit.submit(); return false;">Save</a> |
<a href="employment.php?id=<% $profile_id %>">Cancel</a>
<% else %>
<a href="employment.php?id=<% $profile_id %>&edit=1">Edit</a>
<% /if %>
</div>
<% /if %>

<h2><% $profile.first_name %> <% $profile.last_name %></h2>

<div style="float:left" style="width: 150px">

<% if $display_photo %>

<img name="profile_img" src="<% $profile.current_photo %>" width="150" alt="Photo of <% $profile.first_name %> <% $profile.last_name %>"
<% if $profile.school_photo %>
<%* onmouseover="this.src='<% $profile.school_photo %>'" onmouseout="this.src='<% $profile.current_photo %>'" *%>
<% /if %>
/>

<% else %>

<img src="/images/male.png" width="150px" alt="Photo missing" />

<% /if %>

<% if $linkedin %>

<div style="width: 150px; text-align: center; padding: 5px 0 0 0">
<a href="<% $profile.linkedin %>"><img src="/content_files/images/icons/linkedin.png" style="width: 18px; height: 18px"></a>
</div>



<% /if %>

<% if $is_editing %>
<script>
			window.addEvent('load', function(){
				
				// Autocomplete initialization
				var t1 = new TextboxList('employer_add', {unique: true, max: 1, plugins: {autocomplete: {}}});
<% foreach from = $tags.sector item=i key=ttype %>
t1.add('<% $i.tag|capitalize %>');
<% /foreach %>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t1.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_jobs.php', onSuccess: function(r){
					t1.plugins['autocomplete'].setValues(r);
					t1.container.removeClass('textboxlist-loading');
				}}).send();
			});
</script>
<BR>
<div id="addbutton"<% if $error %> style="display:none"<% /if %> style="text-align: center">
<form name="addjob">
&nbsp; <input type="button" value="Add Job" onClick="showform();">
</form>
</div>
<% /if %>

</div>

<div class="network_data" style="width: 580px">

<% if !$jobs %>
No jobs listed
<% /if %>

	<% foreach from=$jobs item=i %>
<div style="clear:left">
<div style="float:left" class="data_title">
<% $i.start|substr:0:7|date_format:"%b %Y" %> -
<% if $i.current %>Present<% else %>
<%  $i.end|substr:0:7|date_format:"%b %Y" %>
<% /if %>
</div>
<div class="data_data">
<b style="font-size: 15px"><% $i.employer %></b>
<% if $i.division %>|<% /if %> <span style="font-size: 14px"><% $i.division %></span>
<% if $is_editing %>
<a href="#" onClick="show_edit_form('<% $i.id %>','<% $i.employer %>','<% $i.division %>','<% $i.title %>','<% $i.start|substr:0:7|date_format:"%m" %>','<% $i.start|substr:0:7|date_format:"%Y" %>','<% $i.end|substr:0:7|date_format:"%m" %>','<% $i.end|substr:0:7|date_format:"%Y" %>','<% $i.current %>','<% $i.i_id %>');"><img src="/content_files/images/edit.png" class="edit"></a>
<a href="employment.php?id=<% $profile.id %>&deljob=<% $i.id %>"><img src="/content_files/images/delete.gif" class="delete"
onclick="return confirm('Are you sure you want to delete this job?')"></a>
<% /if %><BR>
<i style="font-size: 13px; line-height: 13px"><% $i.title %></i><BR><BR>
</div>
</div>
<% /foreach %>

<% if $is_editing %>
<form name="do_edit" action="employment.php?id=<% $profile_id %>" method="POST">
<input type="hidden" name="submitted" value=1>
<div style="clear:both">
<div style="float:left" class="data_title">
LinkedIn URL:
</div>
<div style="float:left" class="job_info">
<input type="text" name="linkedinurl" value="<% if $profile.linkedin %><% $profile.linkedin %><% else %>Your Public LinkedIn URL<% /if %>" style="width: 350px" onClick="this.value='';" onFocus="this.value='';">
</div>
</div>
  <% if $jobs %>
<BR><BR>
<div style="clear:both">
<div style="float:left" class="data_title">
Current Job:
</div>
<div style="float:left" class="job_info">
<select name="currentjob">
<option value="0">* None *</option>
<% foreach from = $jobs item=i %>
<option value="<% $i.id %>" <% if ($i.id == $profile.current_job_id) %>SELECTED<% /if %>><% $i.employer %></option>
<% /foreach %>
</select><BR>
<p class="note">This job will show in search results and in your public profile</p>
<BR>
</div>
</div>
  <% /if %>

</form>
<% /if %>

</div>
<div style="clear:both">&nbsp;</div>

<% if $is_editing %>

    <div id="page_screen">
        &nbsp;
    </div>

<div id="addform" style="display:<% if $error %>block<% else %>none<% /if %>;border-radius:15px; -moz-border-radius: 15px">
<% if $error %>
<div class="error" id="error" style="text-align: center; width: 100%; margin-top: 5px"><% $errormsg %></div>
<% /if %>
<form name="edit_profile" action="/portal/network/view/employment.php?id=<% $profile_id %>&edit=1" method="POST">
<div class="network_block_edit">
<div class="dataform" style="border-bottom: 0; padding-bottom: 0">
<div class="data_title_r">
Employer
</div>
<div style="float:left" class="data_data_edit">
<input type="text" name="employer" class="edittextw" value="" id="employer_add"><BR>
</div>
</div>
<div class="dataform">
<div class="data_title_r">
Division<BR>
Title<BR>
Industry<BR>
Start Date<BR>
End Date<BR>
Current Job?
</div>
<div style="float:left" class="data_data_edit">
<input type="text" name="division" class="edittextw" value="<% $do_division %>"><BR>
<input type="text" name="title" class="edittextw" value="<% $do_title %>"><BR>
<select name="industry">
<option value="0">** Please Select **</option>
<% foreach from = $industries item=i %>
<option value="<% $i.id %>"><% $i.name %></option>
<% /foreach %>
</select><BR>
<input type="text" name="startmonth" id="startmonth" class="edittext" size=2 value="<% if isset($do_start_month) %><% $do_start_month %><% else %>MM<% /if %>" onClick="this.value='';">
<input type="text" name="startyear" id="startyear" class="edittext" size=4 value="YYYY" onClick="this.value='';"><BR>
<div style="display:inline" id="end"><input type="text" name="endmonth" class="edittext" size=2 value="MM" onClick="this.value='';">
<input type="text" name="endyear" class="edittext" size=4 value="YYYY" onClick="this.value='';"><BR></div>
<input type="checkbox" name="current" value="1" onClick="togglecurrent();">
</div>
<hr class="editline">
<div style="float:left;"><input type="button" value="Cancel" onClick="hideform();"></div>
<div style="float:right"><input type="submit" value="Add Job"></div>
</div>

</div>
</form>
</div>

<div id="editform" style="display:<% if $edit_error %>block<% else %>none<% /if %>; border-radius:15px; -moz-border-radius: 15px">
<form name="edit_job" action="/portal/network/view/employment.php?id=<% $profile_id %>&edit=1&e_job=1" method="POST">
<input type="hidden" name="id" value="">
<div class="network_block_edit" style="background-color:#ededed">
<div class="dataform">
<div class="data_title_r">
Employer<BR>
Division<BR>
Title<BR>
Industry<BR>
Start Date<BR>
End Date<BR>
Current Job?
</div>
<div style="float:left" class="data_data_edit">
<input type="text" name="employer" class="edittextw" value="" id="employer_edit" readonly style="background: #ededed; border: 0"><BR>
<input type="text" name="division" class="edittextw" value=""><BR>
<input type="text" name="title" class="edittextw" value=""><BR>
<select name="industry">
<option value="0">** Please Select **</option>
<% foreach from = $industries item=i %>
<option value="<% $i.id %>"><% $i.name %></option>
<% /foreach %>
</select><BR>
<input type="text" name="startmonth" class="edittext" size=2 value="">
<input type="text" name="startyear" class="edittext" size=4 value=""><BR>
<div style="display:inline" id="edit_end"><input type="text" name="endmonth" class="edittext" size=2 value="" onClick="">
<input type="text" name="endyear" class="edittext" size=4 value="" onClick=""><BR></div>
<input type="checkbox" name="current" value="1" onClick="toggle_edit_current();">
</div>
<hr class="editline">
<div style="float:left;"><input type="button" value="Cancel" onClick="hide_edit_form();"></div>
<div style="float:right"><input type="submit" value="Save"></div>
</div>

</div>
</form>

<% /if %>

</div>

<% if $error %>
<script>
showform();
</script>
<% /if %>

<% /if %>
