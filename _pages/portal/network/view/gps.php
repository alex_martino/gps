<div><img src="/content_files/headers/network.gif" width="800" height="90">

<% if $not_viewable %>
<div style="margin-top: 15px; margin-left: 10px">
<p>This profile is not available. <a href="javascript:history.go(-1)">Go back.</a></p>

<% else %>
<div id="network_t_menu">
  <ul>
<% foreach from=$network_t_menu item=i %>
    <li<% if $i.current == "1" %> id="current"<% /if %>><a href="<% $i.url %>?id=<% $profile.id %>"><% $i.name %></a></li>
<% /foreach %>
  </ul>
</div>
</div>
<% if $is_editing %>
<script>
			window.addEvent('load', function(){
				
				// Autocomplete initialization
				var t1 = new TextboxList('sectors', {unique: true, plugins: {autocomplete: {}}});
<% foreach from = $tags.sector item=i key=ttype %>
t1.add('<% $i.tag|capitalize %>');
<% /foreach %>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t1.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_sectors.php', onSuccess: function(r){
					t1.plugins['autocomplete'].setValues(r);
					t1.container.removeClass('textboxlist-loading');
				}}).send();	

				// Autocomplete initialization
				var t2 = new TextboxList('pitches', {unique: true, plugins: {autocomplete: {}}});
<% foreach from = $tags.pitch item=i key=ttype %>
t2.add('<% $i.tag|upper %>');
<% /foreach %>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t2.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_pitches.php', onSuccess: function(r){
					t2.plugins['autocomplete'].setValues(r);
					t2.container.removeClass('textboxlist-loading');
				}}).send();	

				// Autocomplete initialization
				var t3 = new TextboxList('mentor', {unique: true, max: 1, plugins: {autocomplete: {}}});
<% if $profile.alumni_mentor_id %>
t3.add('<% $profile.alumni_mentor|capitalize %>');
<% /if %>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t3.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_members.php', onSuccess: function(r){
					t3.plugins['autocomplete'].setValues(r);
					t3.container.removeClass('textboxlist-loading');
				}}).send();

				// Autocomplete initialization
				var t4 = new TextboxList('advocate', {unique: true, max: 1, plugins: {autocomplete: {}}});
<% if $profile.member_advocate_id %>
t4.add('<% $profile.member_advocate|capitalize %>');
<% /if %>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t4.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_members.php', onSuccess: function(r){
					t4.plugins['autocomplete'].setValues(r);
					t4.container.removeClass('textboxlist-loading');
				}}).send();

				// Autocomplete initialization
				var t5 = new TextboxList('expertise', {unique: true, plugins: {autocomplete: {}}});

<% foreach from = $tags.expertise item=i key=ttype %>
t5.add('<% $i.tag|capitalize %>');
<% /foreach %>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response example
				t5.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_expertise.php', onSuccess: function(r){
					t5.plugins['autocomplete'].setValues(r);
					t5.container.removeClass('textboxlist-loading');
					document.getElementById('savelink').innerHTML = '<a href="#" onClick="document.do_edit.submit(); return false;">Save</a>';
				}}).send();
			
								
			});


</script>
<% /if %>
<div style="margin-top: 25px">

<% if $can_edit %>
<div class="editlink">
<% if $is_editing %>
<span id="savelink">Please wait...</span> |
<a href="gps.php?id=<% $profile_id %>">Cancel</a>
<% else %>
<a href="gps.php?id=<% $profile_id %>&edit=1">Edit</a>
<% /if %>
</div>
<% /if %>

<h2><% $profile.first_name %> <% $profile.last_name %></h2>

<div style="float:left" style="width: 150px">

<% if $display_photo %>

<img name="profile_img" src="<% $profile.current_photo %>" width="150" alt="Photo of <% $profile.first_name %> <% $profile.last_name %>"
<% if $profile.school_photo %>
<%* onmouseover="this.src='<% $profile.school_photo %>'" onmouseout="this.src='<% $profile.current_photo %>'" *%>
<% /if %>
/>

<% else %>

<img src="/images/male.png" width="150" alt="Photo missing" />

<% /if %>
</div>

<div style="float: right; width: 610px">

<% if $error %>
<div class="error"><% $errormsg %></div>
<% /if %>

<% if ($is_editing) %>

<form name="do_edit" action="gps.php?id=<% $profile_id %>&edit=1" method="POST" accept-charset="utf-8">
<input type="hidden" name="submitted" value="1">

<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Sectors</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="sectors" value="" id="sectors" />
<BR>
		<p class="note">Type the tag (one or more words) and press enter. Use left/right arrows, backspace, delete to navigate/remove boxes, and up/down/enter to navigate/add suggestions.</p>
</div>
</div>

<BR><BR>


		</fieldset>
</div>
<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Pitches</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="pitches" value="" id="pitches" />
<BR>
		<p class="note">Enter the <b>1 - 4 letter</b> stock market ticker</p>
</div>
</div>

<BR><BR>


		</fieldset>
</div>
<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Research Experience</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="expertise" value="" id="expertise" />
<BR>
		<p class="note">Please enter any research areas you have been exposed to. E.g. "Insurance". You can include experience gained during employment.</p>
</div>
</div>

<BR><BR>


		</fieldset>
</div>
<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Alumni Mentor</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="mentor" value="" id="mentor" />
</div>
</div>

<BR><BR>


		</fieldset>
</div>

<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Member Advocate</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="advocate" value="" id="advocate" />
</div>
</div>

<BR><BR>


		</fieldset>
</div>

</form>

		
<% else %>

<div class="network_data">

<div id="title">
<div style="float:left" class="data_title">
Title
</div>
<div style="float:left" class="data_data">
<% $profile.title %>
</div>
</div>
<BR><BR>
<% if $tags.sector %>
<div id="sectors" class="network_block_top">
<div style="float:left" class="data_title">
Sectors
</div>
<div style="float:left" class="data_data">
<% foreach from=$tags.sector item=i key=key %>
<span class="button blue small"><a href="../search.php?qs=1&ttype=sector&tag=<% $i.tag|replace:"&":"%26" %>"><% $i.tag|capitalize %></a></span>
<% /foreach %>
</div>
</div>
<BR><BR>
<% /if %>
<% if $tags.pitch %>
<div id="pitches" class="network_block">
<div style="float:left" class="data_title">
Pitches
</div>
<div style="float:left" class="data_data">
<% foreach from = $tags.pitch item=i key=ttype %>
<span class="button green small"><a href="../search.php?qs=1&ttype=pitch&tag=<% $i.tag %>"><% $i.tag|upper %></a></span>
<% /foreach %>
</div>
</div>
<BR><BR>
<% /if %>
<% if $tags.expertise %>
<div id="expertise" class="network_block">
<div style="float:left" class="data_title">
Research Experience
</div>
<div style="float:left" class="data_data">
<% foreach from = $tags.expertise item=i key=ttype %>
<span class="button red small"><a href="../search.php?qs=1&ttype=expertise&tag=<% $i.tag %>"><% $i.tag|capitalize %></a></span>
<% /foreach %>
</div>
</div>
<BR><BR>
<% /if %>
<% if $profile.alumni_mentor_id %>
<div id="mentor" class="network_block">
<div style="float:left" class="data_title">
Alumni Mentor
</div>
<div style="float:left" class="data_data">
<a href="overview.php?id=<% $profile.alumni_mentor_id %>"><% $profile.alumni_mentor %></a><BR>
</div>
</div>
<BR><BR>
<% /if %>
<% if $profile.member_advocate_id %>
<div id="Advocate" class="network_block">
<div style="float:left" class="data_title">
Member Advocate
</div>
<div style="float:left" class="data_data">
<a href="overview.php?id=<% $profile.member_advocate_id %>"><% $profile.member_advocate %></a><BR>
</div>
</div>
<BR><BR>
<% /if %>
<% if $profile.mentees %>
<div id="Mentees" class="network_block">
<div style="float:left" class="data_title">
Mentees
</div>
<div style="float:left" class="data_data">
<% foreach from=$profile.mentees item=i key=key %>
<a href="overview.php?id=<% $i.id %>"><% $i.first_name %> <% $i.last_name %></a>&nbsp;
<% /foreach %>
<BR><BR>
</div>
</div>
<% /if %>

</div>

<% /if %>

</div> <!--end float right -->

<div style="clear:both">&nbsp;</div>

<% /if %>

</div>
