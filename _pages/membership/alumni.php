<div><img src="/content_files/headers/alumni.gif" width="800" height="90"></div>
<div>
	<% foreach from=$years item=alumni key=year %>
		<h2>Class of <% $year %></h2>
		<% foreach from=$alumni item=member %>
			<div style="float: left; height: 50px; margin-bottom: 15px; width: 253px;">
				<% if $member.biography != '' %><a href="/membership/member.php?id=<% $member.id %>"><% /if %><strong><% $member.first_name %> <% $member.last_name %></strong><% if $member.biography != '' %></a><% /if %><br />
				<% if $member.current_employer != '' %><strong><% $member.current_employer %></strong><br /><% /if %>
				<% $member.undergraduate_school %>
			</div>
		<% /foreach %>
		<div style="clear: both;"></div>
	<% /foreach %>
</div>