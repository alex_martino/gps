<?php
//Additional stuff from me

//config - manual as on subdomain
require("config.php");

$db_connection = 1;

$domain = "gps100.com";

function check_authenticated($thefunction) {

	$success = FALSE;
	$error = FALSE;

	$error = 'MISSING_API_KEY';
	if (isset($_REQUEST['api_key'])) {
		$key = mysql_real_escape_string($_REQUEST['api_key']);
		$sql = "SELECT * FROM api_keys WHERE api_key = '".$key."' AND active = 1 LIMIT 1";
		$result = mysql_query($sql) or die(mysql_error());
		$keyok = mysql_num_rows($result);
		$error = 'BAD_API_KEY';
		if ($keyok == 1) {
			$error = 'MISSING_ACCESS_TOKEN';
			if (isset($_REQUEST['access_token'])) {
				$access_token = mysql_real_escape_string($_REQUEST['access_token']);
				$sql = "SELECT a.id_user FROM api_sessions as a, api_keys as k WHERE a.app_key = k.api_key AND k.active = 1 AND a.expiry >= NOW() AND a.app_key ='".$key."' AND a.access_token ='".$access_token."'";
				$result = mysql_query($sql) or die(mysql_error());
				$tokenok = mysql_num_rows($result);
				$error = 'BAD_ACCESS_TOKEN';
				if ($tokenok == 1) {
					$row = mysql_fetch_row($result);
					$userid = $row[0];
					log_access($userid, $thefunction, $key, $access_token);
					$success = TRUE;
				}
			}
		}
	}

	$response = array('success'=>$success,'error'=>$error);
	return $response;
	
}

function output_results($results) {
	if (gettype($results) == 'array') { $results = array2xml($results); }
	$output = check_output();
	if ($output == 'xml') {
		header( "Content-type: application/xml; encoding=UTF-8",true );
		echo $results;
	}
	else {
		$xml = simplexml_load_string($results);
		$json = json_encode($xml);
		header( "Content-type: application/json; encoding=UTF-8",true );
		echo $json;
	}
}

function check_output() {
	$output='json';
	if (isset($_GET['output']) && $_GET['output'] == 'xml') { $output = 'xml'; }
	elseif (isset($_POST['output']) && $_POST['output'] == 'xml') { $output = 'xml'; }
	return $output;
}

function log_access($userid,$function,$app_key,$access_token) {
	$time_f = date("Y-m-d H:i:s");
	
	//get IP
	if (!empty($_SERVER['HTTP_CLIENT_IP'])){
	$ip=$_SERVER['HTTP_CLIENT_IP'];
		}elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
		$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
		$ip=$_SERVER['REMOTE_ADDR'];
		}
		$ip = ip2long($ip);
		
	$sql = "INSERT INTO api_logs (user_id, event, date_time, ip_address, app_key, access_token) VALUES ('".$userid."','".$function."','".$time_f."','".$ip."','".$app_key."','".$access_token."')";
	$result = mysql_query($sql);
}

function do_login() {
    if (isset($_POST['username']) && isset($_POST['password'])) {
    	//attempt login here
    	//////
        if (1==1) {
        	return array('success'=>TRUE,'sessionkey'=>'123abc');
        }
        else {
        	return array('success' => "FALSE", 'error' => 'BAD_CREDENTIALS');
        }
    }
    else {
       return array('success' => "FALSE", 'error' => 'CREDENTIALS_NOT_PROVIDED');
    }
}

function array2xml($array, $wrap='response', $upper=true) {
    // set initial value for XML string
    $xml = '';
    // wrap XML with $wrap TAG
    if ($wrap != null) {
        $xml .= "<$wrap>\n";
    }
    // main loop
    foreach ($array as $key=>$value) {
        // set tags in uppercase if needed
        if ($upper == true) {
            $key = strtoupper($key);
        }
        // append to XML string
        $xml .= "<$key>" . htmlspecialchars(trim($value)) . "</$key>";
    }
    // close wrap TAG if needed
    if ($wrap != null) {
        $xml .= "\n</$wrap>\n";
    }
    // return prepared XML string
    return $xml;
}
?>