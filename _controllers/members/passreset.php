<?php

if ($_GET['request_reset'] == 1) { //requesting reset
$smarty->assign('request_reset', 1);

if ($_POST['request_reset'] == 1) {
$r_email = $_POST['email'];
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/email_validate.php");
if (!check_email_address($r_email)) { $error++; $errormsg .= "Invalid email<BR>"; }

else {
$sql = "SELECT id, email_address FROM members WHERE email_address = '".$r_email."' LIMIT 1";
$result=mysql_query($sql) or die(mysql_error());
$num_rows = mysql_num_rows($result);
$row = mysql_fetch_array($result);
if ($num_rows != 1) { $error++; $errormsg .= "Email address not found"; }
}

if ($error < 1) {
				function rand_string( $length ) {
				$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";	

				$size = strlen( $chars );
					for( $i = 0; $i < $length; $i++ ) {
						$str .= $chars[ rand( 0, $size - 1 ) ];
					}

					return $str;
				}

				$c_code = rand_string(16);
				$c_time = date("Y-m-d H-i-s");
				$c_email = $r_email;

				mysql_query("INSERT INTO pass_reset (gps_email,code,time) VALUES ('$c_email','$c_code','$c_time') ON DUPLICATE KEY UPDATE code = '$c_code'") or die(mysql_error());

				//now email code
				require_once "Mail.php";
				
				$from = "GPS Admin <admin@".$domain.">";
				$to = $c_email;
				$subject = "Action Required: GPS Password Reset";
				$body = "Dear GPSer,\n\nPlease click the following link to reset your password: http://www.".$domain."/members/passreset.php?code=".$c_code."&email=".$c_email;
				$body .= "\n\nMany thanks,\nBenjamin Wigoder";

				$host = "localhost";
				$username = "admin@".$domain;
				$password = "101dalmations";

				$headers = array ('From' => $from,
				  'To' => $to,
				  'Subject' => $subject);
				$smtp = Mail::factory('smtp',
				  array ('host' => $host,
				    'auth' => true,
				    'username' => $username,
				    'password' => $password));

				$mail = $smtp->send($to, $headers, $body);

				if (PEAR::isError($mail)) {
				  $error++; $errormsg = "Email Error";
				 } else {
				  $email_success=1;
				 }

				// Add log
				$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('user_pw_reset_req','".$row['id']."','".date("Y-m-d H:i:s")."','".$c_email."','".$_SERVER['REMOTE_ADDR']."')";
				$result = mysql_query($sql) or die(mysql_error());

}

}

}

if($_GET['success'] == 1) {

$success = 1;

}

elseif(isset($_GET['email'])) {

$submitted_code = $_GET['code'];
$submitted_email = $_GET['email'];

$error = 0;
$code_match = 0;

//check that information submitted is valid
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/email_validate.php");
if (!check_email_address($submitted_email)) { $error++; $errormsg .= "Invalid email<BR>"; }
elseif (!ctype_alnum($submitted_code)) { $error++; $errormsg .= "Invalid code<BR>"; }
elseif (strlen($submitted_code) != 16) { $error++; $errormsg .= "Invalid code<BR>"; }

//valid submission, now check for match
else {

require_once($_SERVER['DOCUMENT_ROOT'] . "/config.php");

$sql = "SELECT code FROM pass_reset WHERE gps_email = '$submitted_email' LIMIT 1";
$results = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($results);


//if code matches...
if ($submitted_code == $row[0]) {

$code_match = 1;

//now do new password validation

$s_new_password_1 = $_POST['new_password'];
$s_new_password_2 = $_POST['new_password_2'];

//check that the two new passwords match
if ($s_new_password_1 != $s_new_password_2) {
$error++;
$errormsg .= "New passwords do not match.<BR>";
}

elseif(strlen($s_new_password_1)<1) // anything entered?
{ $error++; $errormsg .= "Please enter a new password.<BR>"; }

elseif(!ctype_alnum($s_new_password_1)) // numbers & digits only
{ $error++; $errormsg .= "Password must consist of letters and numbers only.<BR>"; }

elseif(strlen($s_new_password_1)<6) // at least 6 chars
{ $error++; $errormsg .= "Password must be at least 6 characters.<BR>"; } 

elseif(strlen($s_new_password_1)>14) // at most 14 chars
{ $error++; $errormsg .= "Password must be at most 14 characters.<BR>"; } 

elseif(!preg_match('`[A-Z]`',$s_new_password_1)) // at least one upper case
{ $error++; $errormsg .= "Password must contain at least one upper case letter.<BR>"; } 

elseif(!preg_match('`[a-z]`',$s_new_password_1)) // at least one lower case
{ $error++; $errormsg .= "Password must contain at least one lower case letter.<BR>"; } 

elseif(!preg_match('`[0-9]`',$s_new_password_1)) // at least number
{ $error++; $errormsg .= "Password must contain at least one number.<BR>"; } 

else { //now update database with new password

$sql = "UPDATE members SET pass_enc_sec = sha1(concat(sha1('$s_new_password_1'),'3l0bsbTdC3x8gS79F9jK3piAHxBtg5aJMrT05sp9C1VsdX6v8')), password = 'updated' WHERE email_address = '".$submitted_email."'";
if ($results = mysql_query($sql, $db)) { $success = 1; $successmsg = "Password successfully changed."; }
else { $error++; $errormsg .= "There was an error updating the database. Please try again, or contact an admin."; }

$sql = "DELETE FROM pass_reset WHERE gps_email = '".$submitted_email."' AND code = '".$submitted_code."'";
mysql_query($sql, $db);

//Find uid
$sql = "SELECT id FROM members WHERE email_address = '".$submitted_email."' LIMIT 1";
$result = mysql_query($sql, $db);
$row = mysql_fetch_array($result);

// Add log
$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('user_pw_change','".$row[0]."','".date("Y-m-d H:i:s")."','".$submitted_email."','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql, $db);

unset($s_new_password_1);
unset($s_new_password_2);

header("Location: passreset.php?success=1");

}


} //end when matched




} //end check for match


} //end check for if





$smarty->assign('error', $error);
$smarty->assign('error_message', $errormsg);
$smarty->assign('code_match', $code_match);

$smarty->assign('code', $submitted_code);
$smarty->assign('email', $submitted_email);

$smarty->assign('success', $success);
$smarty->assign('email_success', $email_success);

$smarty->assign('r_email', $r_email);
?>