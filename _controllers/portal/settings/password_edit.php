<?
$error = 0;

if(isset($_POST['old_password'])) {

if (strlen($_POST['old_password'])>0) {

$s_old_password = $_POST['old_password'];
$s_new_password_1 = $_POST['new_password'];
$s_new_password_2 = $_POST['new_password_2'];

//check that the two new passwords match
if ($s_new_password_1 != $s_new_password_2) {
$error++;
$errormsg .= "New passwords do not match.<BR>";
}

elseif(strlen($s_new_password_1)<1) // anything entered?
{ $error++; $errormsg .= "Please enter a new password.<BR>"; }

elseif(!ctype_alnum($s_new_password_1)) // numbers & digits only
{ $error++; $errormsg .= "Password must consist of letters and numbers only.<BR>"; }

elseif(strlen($s_new_password_1)<6) // at least 6 chars
{ $error++; $errormsg .= "Password must be at least 6 characters.<BR>"; } 

elseif(strlen($s_new_password_1)>14) // at most 14 chars
{ $error++; $errormsg .= "Password must be at most 14 characters.<BR>"; } 

elseif(!preg_match('`[A-Z]`',$s_new_password_1)) // at least one upper case
{ $error++; $errormsg .= "Password must contain at least one upper case letter.<BR>"; } 

elseif(!preg_match('`[a-z]`',$s_new_password_1)) // at least one lower case
{ $error++; $errormsg .= "Password must contain at least one lower case letter.<BR>"; } 

elseif(!preg_match('`[0-9]`',$s_new_password_1)) // at least number
{ $error++; $errormsg .= "Password must contain at least one number.<BR>"; } 

else
{
// password meets specs

// Include site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/config.php");

$user_id = $_SESSION['member_id'];

//check old password is valid

$sql = "select pass_enc_sec FROM members WHERE id = ".$user_id." LIMIT 1";
$results = mysql_query($sql, $db);
$row = mysql_fetch_array($results);

if ($row[0] != sha1(sha1($s_old_password)."3l0bsbTdC3x8gS79F9jK3piAHxBtg5aJMrT05sp9C1VsdX6v8")) { $error++; $errormsg = "Old password incorrect.<BR>"; }

else { //now update password

$sql = "UPDATE members SET pass_enc_sec = sha1(concat(sha1('$s_new_password_1'),'3l0bsbTdC3x8gS79F9jK3piAHxBtg5aJMrT05sp9C1VsdX6v8')) WHERE id = '".$user_id."'";
if ($results = mysql_query($sql, $db)) { $success = 1; $successmsg = "Password successfully changed.";

// Add log
$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('user_pw_change','".$user_id."','".date("Y-m-d H:i:s")."','password_edit.php','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql, $db);

}
else { $error++; $errormsg .= "There was an error updating the database. Please try again, or contact an admin."; }

unset($s_old_password);
unset($s_new_password_1);
unset($s_new_password_2);


}



}


}

else {

$error++; $errormsg .= "Please enter your old password.<BR>";

}

}

$smarty->assign('error', $error);
$smarty->assign('error_message', $errormsg);

$smarty->assign('success', $success);
$smarty->assign('success_message', $successmsg);

$smarty->assign('old_password', $s_old_password);
$smarty->assign('new_password', $s_new_password_1);
$smarty->assign('new_password_2', $s_new_password_2);

?>