<?php
if ($_SESSION['user_is_um'] || $_SESSION['user_is_board'] || $_SESSION['user_is_admin']) { $user_can_add_survey = 1; }
if (($_SESSION['user_is_um'] && $_SESSION['user_gps_position_held'] == "Portfolio Manager") || ($_SESSION['user_is_admin'])) { $user_can_download_survey = 1; }
if (($_SESSION['user_is_um'] && $_SESSION['user_gps_position_held'] == "Portfolio Manager") || ($_SESSION['user_is_admin'])) { $user_can_extend_survey = 1; }

//define variables
$error = 0;
$errormsg = "";
$add_success = 0;
$nosurveys = 0;
$now = date("Y-m-d H:i:s");

//handle form submission
$addsurvey = 0;
if (isset($_GET['addsurvey'])) {
	$addsurvey = 1;
}
if ($addsurvey && $user_can_add_survey == 1) { //add form submitted

$do_question = mysql_real_escape_string($_POST['question']);
$do_cat_id = $_POST['category'];
$do_deadline_day = $_POST['deadline_day'];
$do_deadline_month = $_POST['deadline_month'];
$do_deadline_year = $_POST['deadline_year'];
$do_deadline_hour = $_POST['deadline_hour'];
$do_deadline_minute = $_POST['deadline_minute'];
$do_response_1 = mysql_real_escape_string($_POST['response1']);
$do_response_2 = mysql_real_escape_string($_POST['response2']);
$do_response_3 = mysql_real_escape_string($_POST['response3']);
$do_response_4 = mysql_real_escape_string($_POST['response4']);
$do_response_5 = mysql_real_escape_string($_POST['response5']);
$do_type = $_POST['type'];

//check all fields which should be numeric are numeric or die
if (!is_numeric($do_cat_id.$do_deadline_day.$do_deadline_month.$do_deadline_year.$do_deadline_hour.$do_deadline_minute)) { header("Location: index.php"); }

if ($do_type != "radio" && $do_type != "checkbox") { header("Location: index.php"); }

//now do error handling
if (strlen($do_question) < 1) { $error++; $errormsg.="Please enter a question. &nbsp; "; }
if ($do_cat_id == 0) { $error++; $errormsg .= "Please select a category. &nbsp; "; }
if (strlen($do_response_1) < 1 && strlen($do_response_2) < 1 ) { $error++; $errormsg.="Please enter at least two responses. &nbsp; "; }

if (!is_numeric($_SESSION['user_id']) || $_SESSION['user_id'] == 0) {
$error++;
header("Location: /members/login.php?logout=1");
}

if ($error < 1) { //no errors, add survey

//format deadline
$do_deadline = $do_deadline_year . "-" . $do_deadline_month . "-" . $do_deadline_day . " " . $do_deadline_hour . ":" . $do_deadline_minute . ":00";
$do_created = date("Y-m-d H:i:s");
$do_uid = $_SESSION['user_id'];
if ($do_type == "checkbox") { $do_multiple = "1"; } else { $do_multiple = "0"; }

$sql = "INSERT INTO vote_questions (uid, cat_id, created, deadline, multiple, question) VALUES ('$do_uid', '$do_cat_id', '$do_created', '$do_deadline', '$do_multiple', '$do_question')";
$result = mysql_query($sql) or die(mysql_error());

//get question id
$sql = "SELECT id FROM vote_questions WHERE created = '$do_created' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$qid = $row['id'];
}

if (strlen($do_response_1) > 0) {
$sql = "INSERT INTO vote_options (qid, name) VALUES ('$qid','$do_response_1')";
$result = mysql_query($sql) or die(mysql_error());
}

if (strlen($do_response_2) > 0) {
$sql = "INSERT INTO vote_options (qid, name) VALUES ('$qid','$do_response_2')";
$result = mysql_query($sql) or die(mysql_error());
}

if (strlen($do_response_3) > 0) {
$sql = "INSERT INTO vote_options (qid, name) VALUES ('$qid','$do_response_3')";
$result = mysql_query($sql) or die(mysql_error());
}

if (strlen($do_response_4) > 0) {
$sql = "INSERT INTO vote_options (qid, name) VALUES ('$qid','$do_response_4')";
$result = mysql_query($sql) or die(mysql_error());
}

if (strlen($do_response_5) > 0) {
$sql = "INSERT INTO vote_options (qid, name) VALUES ('$qid','$do_response_5')";
$result = mysql_query($sql) or die(mysql_error());
}

$add_success = 1;

//log survey add
$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('survey_add','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".$qid."','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql) or die("There was a problem logging the survey add. Please contact an admin.");


} //end no errors

} //end if form submitted

//handle extend survey
if (isset($_GET['extend_survey'])) {
	if (is_numeric($_GET['extend_survey']) && is_numeric($_GET['extend']) && $user_can_extend_survey) {
		$sql = "SELECT timestampdiff(SECOND,deadline,'$now') difference FROM vote_questions WHERE id = '".$_GET['extend_survey']."' ORDER BY DEADLINE DESC LIMIT 1";
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		$difference = $row['difference'];
		if (abs($difference) < 604800) {
			$sql = "SELECT deadline FROM vote_questions WHERE id = '".$_GET['extend_survey']."' LIMIT 1";
			$result = mysql_query($sql);
			while ($row = mysql_fetch_array($result)) {
				$deadline = strtotime($row[0]);
				$deadline_new = $deadline + $_GET['extend'] * 86400;
				$deadline_new_f = date("Y-m-d h:i:s", $deadline_new);
				$sql = "UPDATE vote_questions SET deadline = '".$deadline_new_f."' WHERE id = '".$_GET['extend_survey']."' LIMIT 1";
				$result = mysql_query($sql);
				header("Location: ./");
			}
		}
	}
}

//handle vote submitted
if (isset($_GET['pollid'])) {
	if (is_numeric($_GET['pollid']) && (!empty($_POST['vote']) || !empty($_POST['vote[]']))) { //start if vote submitted
		$pollid = $_GET['pollid'];
		$uid = $_SESSION['user_id'];
		if (!is_array($_POST['vote'])) {
			$response = mysql_real_escape_string($_POST['vote']);
			if (!is_numeric($response)) { die("Fatal error 222. Contact an admin."); }
		}
	else {
		$response = $_POST['vote'];
		foreach ($response as $key => $value) {
			$response_clean[] = mysql_real_escape_string($value);
			if (!is_numeric($value)) { die("Fatal error 333. Contact an admin."); }
		}
	}
}

$sql = "SELECT * FROM vote_responses WHERE qid = '$pollid' AND uid = '$uid' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
if (mysql_num_rows($result) < 1) { //check no rows (i.e. this user hasn't voted yet)

if (!is_array($response)) {
$sql = "INSERT INTO vote_responses (uid, qid, response, updated) VALUES ('$uid', '$pollid', '$response', '$now')";
$result = mysql_query($sql) or die(mysql_error());
$sql = "UPDATE vote_options SET total=total+1 WHERE id='$response' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
}
else {
foreach ($response_clean as $key => $value) {
$sql = "INSERT INTO vote_responses (uid, qid, response, updated) VALUES ('$uid', '$pollid', '$value', '$now')";
$result = mysql_query($sql) or die(mysql_error());
$sql = "UPDATE vote_options SET total=total+1 WHERE id='$value' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
} //end foreach
}

//log vote
$sql2 = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('survey_vote','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','$pollid','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql2) or die("There was a problem logging the vote. Please contact an admin.");

} //end check no rows yet
} //end if vote submitted


$sql = "SELECT * FROM vote_categories ORDER BY id ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$categories[] = $row;
}

	for($day=1; $day <= 31; ++$day) {
		if ($day <= 9) { $day = "0" . $day; }
		$days[] = $day;
	}

		$months[] = array("01","January");
		$months[] = array("02","February");
		$months[] = array("03","March");
		$months[] = array("04","April");
		$months[] = array("05","May");
		$months[] = array("06","June");
		$months[] = array("07","July");
		$months[] = array("08","August");
		$months[] = array("09","September");
		$months[] = array("10","October");
		$months[] = array("11","November");
		$months[] = array("12","December");

	$year = date("Y");
	for ($i = $year; $i <= date("Y")+1; ++$i) {
			$years[] = $year; ++$year;
	}

	$hour = 0;
	for ($i = $hour; $i <= 23; ++$i) {
		if ($hour <= 9) { $hour = "0" . $hour; }
			$hours[] = $hour; ++$hour;
	}

	$minute = 0;
	for ($i = $minute; $i <= 59; ++$i) {
		if ($minute <= 9) { $minute = "0" . $minute; }
			$minutes[] = $minute; ++$minute;
	}

	$today['day'] = date("d");
	$today['month'] = date("m");

$previous = 0;
if (isset($_GET['previous'])) {
	$previous = 1;
}
	
if ($previous) {
	$sql = "SELECT *, timestampdiff(SECOND,deadline,'$now') difference FROM vote_questions WHERE deadline <= '$now' ORDER BY DEADLINE DESC";
}
else {
	$sql = "SELECT *, timestampdiff(SECOND,deadline,'$now') difference FROM vote_questions WHERE deadline > '$now' ORDER BY DEADLINE DESC";
}
$result = mysql_query($sql) or die(mysql_error());
if (mysql_num_rows($result) < 1) { $nosurveys = 1; }
else {
while ($row = mysql_fetch_array($result)) {
$surveys[] = $row;
}

foreach ($surveys as $key => $value) {
$qid = $value['id'];
$cid = $value['cat_id'];
$vresults[$qid]['expiry'] = $value['difference'];
$difference = abs($value['difference']);

//check if less than 60 seconds
if ($difference < 60) { $difftext[$qid] = round($difference,0) . " seconds"; }

elseif ($difference < 90) { $difftext[$qid] = "1 minute remaining"; }

elseif ($difference < 3600) { $difftext[$qid] = round(($difference/60),0) . " minutes"; }

elseif ($difference < 5400) { $difftext[$qid] = "1 hour"; }

elseif ($difference < 84600) { $difftext[$qid] = round(($difference/3600),0) . " hours"; }

elseif ($difference < 129600) { $difftext[$qid] = "1 day"; } //169200 (the old value)

elseif ($difference < 31536000) { $difftext[$qid] = round(($difference/84600),0) . " days"; }

elseif ($difference >= 31536000 && $difference < 63072000) { $difftext[$qid] = "1 year"; }

else { $difftext[$qid] = round(($difference/31536000),0) . " years"; }

//else { $difftext[$qid] = $difference; }

if ($previous) {
$difftext[$qid] .= " ago";
}
else {
$difftext[$qid] .= " remaining";
}

$sql = "SELECT * FROM vote_options WHERE qid = '$qid'";
$result = mysql_query($sql);

while($row=mysql_fetch_array($result)) {
$options[$qid][$row['id']]['name'] = $row['name'];
}


} //end foreach
} //end if no surveys

//check previous votes
$uid = $_SESSION['user_id'];

if ($previous) { //override behaviour if previous view
$sql = "SELECT * FROM vote_responses GROUP BY qid";
}
else {
$sql = "SELECT * FROM vote_responses WHERE uid = '$uid' GROUP BY qid";
}

$result = mysql_query($sql);
while ($row = mysql_fetch_array($result)) {
$qid = $row['qid'];
$already_voted[$qid] = 1;
}

if (is_array($already_voted)) {
foreach ($already_voted as $key => $value) {

$i=0;
$g_width = "200";
$colors = array("#000000", "#D0D0D0", "#585858", "#B8B8B8", "#888888");
$fcolors = array("#ffffff", "#000000", "#ffffff", "#000000", "#ffffff");

$sql = "SELECT SUM(total) as total FROM vote_options WHERE qid = '$key'";
$result = mysql_query($sql);
while ($row = mysql_fetch_array($result)) {
$vresults[$key]['total_votes'] = $row['total'];
}

$sql = "SELECT * FROM vote_options WHERE qid = '$key' ORDER BY total DESC";
$result = mysql_query($sql);

while ($row = mysql_fetch_array($result)) {
$vresults[$key]['option'][$row['id']]['name'] = $row['name'];
$vresults[$key]['option'][$row['id']]['total'] = $row['total'];

$decimal = $row['total']/$vresults[$key]['total_votes'];
$vresults[$key]['option'][$row['id']]['percentage'] = round($decimal*100);
$vresults[$key]['option'][$row['id']]['width'] = $decimal*$g_width;

$vresults[$key]['option'][$row['id']]['color'] = $colors[$i];
$vresults[$key]['option'][$row['id']]['fcolor'] = $fcolors[$i];
$i++;


}

}
} //end if is array

$smarty->assign('categories', $categories);

$smarty->assign('days', $days);
$smarty->assign('months', $months);
$smarty->assign('years', $years);

$smarty->assign('hours', $hours);
$smarty->assign('minutes', $minutes);

$smarty->assign('user_can_add_survey', $user_can_add_survey);
$smarty->assign('user_can_download_survey', $user_can_download_survey);

$smarty->assign('today', $today);

$smarty->assign('error', $error);
$smarty->assign('errormsg', $errormsg);

$smarty->assign('post', $_POST);

$smarty->assign('add_success', $add_success);

$smarty->assign('surveys', $surveys);
$smarty->assign('options', $options);

$smarty->assign('difftext', $difftext);

$smarty->assign('already_voted', $already_voted);
$smarty->assign('vresults', $vresults);

$smarty->assign('previous', $previous);
$smarty->assign('nosurveys', $nosurveys);

?>