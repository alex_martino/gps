<?php
$profile_id = $_GET['id'];
if (!isset($_GET['id'])) { header("Location: ./"); }
if (!is_numeric($profile_id)) { die("Hack attempt"); }

//check if your profile
if ($_SESSION['member_id'] == $profile_id) { $can_edit = 1; }

//check if editing
if ($can_edit == 1 && $_GET['edit'] == 1) {
$is_editing = 1;
}

require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

//include nav nav menu
$page = "INTERESTS"; include ($_SERVER['DOCUMENT_ROOT'] . "/includes/network_t_menu.php"); $smarty->assign('network_t_menu', $network_t_menu);

if ($can_edit && $_POST['submitted']) {
//edit form submitted

$do_investors = explode("_", mysql_real_escape_string(str_replace("'", "", ($_POST['investors']))));
$do_philosophers = explode("_", mysql_real_escape_string(str_replace("'", "", ($_POST['philosophers']))));
$do_books = explode("_", mysql_real_escape_string(str_replace("'", "", ($_POST['books']))));
$do_movies = explode("_", mysql_real_escape_string(str_replace("'", "", ($_POST['movies']))));
$do_television = explode("_", mysql_real_escape_string(str_replace("'", "", ($_POST['television']))));

//first delete all the tags which match for this user
$sql = "DELETE FROM tags WHERE uid = '$profile_id' AND (ttype = 'investor' OR ttype = 'philosopher' OR ttype = 'book' OR ttype = 'movie' OR ttype = 'television')";
$result = mysql_query($sql) or die(mysql_error());
//now add the tags
foreach ($do_investors as $i) {
if(strlen($i) > 1) {
$i = ucwords($i);
$sql = "INSERT INTO tags (uid,ttype,tag) VALUES ('$profile_id','investor','$i')";
$result = mysql_query($sql) or die(mysql_error());
}
}
foreach ($do_philosophers as $i) {
if(strlen($i) > 1) {
$i = ucwords($i);
$sql = "INSERT INTO tags (uid,ttype,tag) VALUES ('$profile_id','philosopher','$i')";
$result = mysql_query($sql) or die(mysql_error());
}
}
foreach ($do_books as $i) {
if(strlen($i) > 1) {
$i = ucwords($i);
$sql = "INSERT INTO tags (uid,ttype,tag) VALUES ('$profile_id','book','$i')";
$result = mysql_query($sql) or die(mysql_error());
}
}
foreach ($do_movies as $i) {
if(strlen($i) > 1) {
$i = ucwords($i);
$sql = "INSERT INTO tags (uid,ttype,tag) VALUES ('$profile_id','movie','$i')";
$result = mysql_query($sql) or die(mysql_error());
}
}
foreach ($do_television as $i) {
if(strlen($i) > 1) {
$i = ucwords($i);
$sql = "INSERT INTO tags (uid,ttype,tag) VALUES ('$profile_id','television','$i')";
$result = mysql_query($sql) or die(mysql_error());
}
}

//add logs
$sql2 = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('profile_update','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','interests','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql2) or die("There was a problem logging the download. Please contact an admin.");

header("Location:interests.php?id=$profile_id");
}

$sql = "SELECT * FROM members WHERE id = '$profile_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$profile = mysql_fetch_array($result);

//check if account has been disabled, or elected to be hidden
if ($profile['hidden'] == 1 || $profile['disabled'] == 1) {
$not_viewable = 1;
}

if ($not_viewable != 1) {

if(strlen($profile['current_photo']) > 0) { $display_photo = 1; }

//get tags
$sql = "SELECT * FROM tags WHERE uid = '$profile_id' ORDER BY ttype,tag ASC";
$result = mysql_query($sql) or die(mysql_error());
$i=0;
while ($row = mysql_fetch_array($result)) {
$tid = $row[0];
$uid = $row[1];
$ttype = $row[2];
$tag = $row[3];
$tmore = $row[4];
$tags[$ttype][$i]['tid'] = $tid;
$tags[$ttype][$i]['uid'] = $uid;
$tags[$ttype][$i]['ttype'] = $ttype;
$tags[$ttype][$i]['tag'] = $tag;
$tags[$ttype][$i]['tmore'] = $tmore;
$i++;
}

$title = "Member";
if ($profile['analyst']) { $title = "Analyst"; }
if ($profile['gps_position_held']) { $title = $profile['gps_position_held']; }
if ($profile['alumni']) { $title = "Alumni"; }
if ($profile['chapter_founder']) { $title = "Chapter Founder"; }
if ($profile['founding_member']) { $title = "Founder"; }
if ($profile['advisory_board']) { $title = "Advisor"; }

$title = $profile['first_name'] . " " . $profile['last_name'] . " | ". $title;

$smarty->assign('can_edit', $can_edit);
$smarty->assign('is_editing', $is_editing);
$smarty->assign('profile_id', $profile_id);

$smarty->assign('tags', $tags);

$smarty->assign('profile', $profile);
$smarty->assign('display_photo', $display_photo);
$smarty->assign('title', $title);

$smarty->assign('error', $error);
$smarty->assign('errormsg', $errormsg);
}

$smarty->assign('not_viewable', $not_viewable);

?>