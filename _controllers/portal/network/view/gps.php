<?php
$profile_id = $_GET['id'];
if (!isset($_GET['id'])) { header("Location: ./"); }
if (!is_numeric($profile_id)) { die("Hack attempt"); }

//check if your profile
if ($_SESSION['member_id'] == $profile_id) { $can_edit = 1; }

//check if editing
if ($can_edit == 1 && $_GET['edit'] == 1) {
$is_editing = 1;
}

require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

//include nav nav menu
$page = "GPS"; include ($_SERVER['DOCUMENT_ROOT'] . "/includes/network_t_menu.php"); $smarty->assign('network_t_menu', $network_t_menu);

//this piece of code is redundant - but may be useful elsewhere
if ($can_edit && $_GET['deltag']) {
$delnum = $_GET['deltag'];
if (!is_numeric($delnum)) { die("Fatal error"); }
$sql = "SELECT uid FROM tags WHERE tid = '$delnum' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_row($result);
if ($row[0] == $profile_id) {
$sql = "DELETE FROM tags WHERE tid ='$delnum' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
}
}

if ($can_edit && $_POST['submitted']) {

//edit form submitted
$do_sectors = explode("_", mysql_real_escape_string(str_replace("'", "", ($_POST['sectors']))));
$do_pitches = explode("_", mysql_real_escape_string(str_replace("'", "", ($_POST['pitches']))));
$do_expertise = explode("_", mysql_real_escape_string(str_replace("'", "", ($_POST['expertise']))));
$do_mentor = explode("_", mysql_real_escape_string(str_replace("'", "", ($_POST['mentor']))));
$do_advocate = explode("_", mysql_real_escape_string(str_replace("'", "", ($_POST['advocate']))));

//first delete all the tags which match for this user
$sql = "DELETE FROM tags WHERE uid = '$profile_id' AND (ttype = 'sector' OR ttype = 'pitch' OR ttype = 'expertise')";
$result = mysql_query($sql) or die(mysql_error());

//and delete all mentors
$sql = "UPDATE members SET alumni_mentor = '', alumni_mentor_id = '', member_advocate = '', member_advocate_id = '' WHERE id = '$profile_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());

//now add the tags
foreach ($do_sectors as $i) {
if(strlen($i) > 2) {
$sql = "INSERT INTO tags (uid,ttype,tag) VALUES ('$profile_id','sector','$i')";
$result = mysql_query($sql) or die(mysql_error());
}
}
foreach ($do_pitches as $i) {
$i = str_replace("/",".",$i);
if(strlen($i) > 0 && strlen($i) < 5) {
$sql = "INSERT INTO tags (uid,ttype,tag) VALUES ('$profile_id','pitch','$i')";
$result = mysql_query($sql) or die(mysql_error());
}
}
foreach ($do_expertise as $i) {
if(strlen($i) > 2) {
$sql = "INSERT INTO tags (uid,ttype,tag) VALUES ('$profile_id','expertise','$i')";
$result = mysql_query($sql) or die(mysql_error());
}
}


//add mentor

if (strlen($do_mentor[0])>0) {
$do_mentor_split = explode(" ", $do_mentor[1]);
$do_mentor_first_name = $do_mentor_split[0];
$do_mentor_last_name = $do_mentor_split[1];
$sql = "SELECT id FROM members WHERE id = '" . $do_mentor[0] . "' LIMIT 1";
if (!is_numeric($do_mentor[0])) {
$sql = "SELECT id, first_name, last_name FROM members WHERE CONCAT(first_name, ' ', last_name) = '" . $do_mentor[0] . "' LIMIT 1";
}
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
if (!is_numeric($do_mentor[0])) { $do_mentor[1] = $row['first_name'] . " " . $row['last_name']; }
$do_mentor_id = $row[0];
if (mysql_num_rows($result) < 1) { $error++; $errormsg .= "Alumni mentor not found"; }
else {
$sql = "UPDATE members SET alumni_mentor_id = '$do_mentor_id', alumni_mentor = '$do_mentor[1]' WHERE id = '$profile_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
}
}

//add advocate
if (strlen($do_advocate[0])>0) {
$sql = "SELECT id FROM members WHERE id = '" . $do_advocate[0] . "' LIMIT 1";
if (!is_numeric($do_advocate[0])) {
$sql = "SELECT id, first_name, last_name FROM members WHERE CONCAT(first_name, ' ', last_name) = '" . $do_advocate[0] . "' LIMIT 1";
}
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
if (!is_numeric($do_advocate[0])) { $do_advocate[1] = $row['first_name'] . " " . $row['last_name']; }
$do_advocate_id = $row[0];
if (mysql_num_rows($result) < 1) { $error++; $errormsg .= "Member advocate not found"; }
else {
$sql = "UPDATE members SET member_advocate_id = '$do_advocate_id', member_advocate = '$do_advocate[1]' WHERE id = '$profile_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
}
}

//add logs
$sql2 = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('profile_update','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','gps','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql2) or die("There was a problem logging the download. Please contact an admin.");

header("Location:gps.php?id=$profile_id");

}

$sql = "SELECT * FROM members WHERE id = '$profile_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$profile = mysql_fetch_array($result);

//check if account has been disabled, or elected to be hidden
if ($profile['hidden'] == 1 || $profile['disabled'] == 1) {
$not_viewable = 1;
}

if ($not_viewable != 1) {

if(strlen($profile['current_photo']) > 0) { $display_photo = 1; }


$title = "Member";
if ($profile['analyst']) { $title = "Analyst"; }
if ($profile['alumni']) { $title = "Alumni"; }
if ($profile['gps_position_held']) { $title = $profile['gps_position_held']; }
if ($profile['chapter_founder']) { $title = "Chapter Founder"; }
if ($profile['founding_member']) { $title = "Founder"; }
if ($profile['advisory_board']) { $title = "Advisor"; }


//get tags
$sql = "SELECT * FROM tags WHERE uid = '$profile_id' ORDER BY ttype,tag ASC";
$result = mysql_query($sql) or die(mysql_error());
$i=0;
while ($row = mysql_fetch_array($result)) {
$tid = $row[0];
$uid = $row[1];
$ttype = $row[2];
$tag = $row[3];
$tmore = $row[4];
$tags[$ttype][$i]['tid'] = $tid;
$tags[$ttype][$i]['uid'] = $uid;
$tags[$ttype][$i]['ttype'] = $ttype;
$tags[$ttype][$i]['tag'] = $tag;
$tags[$ttype][$i]['tmore'] = $tmore;
$i++;
}

//get mentees
$sql = "SELECT id, first_name, last_name FROM members WHERE (member_advocate_id = '$profile_id' OR alumni_mentor_id = '$profile_id') and disabled = 0 ORDER BY first_name ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$profile['mentees'][] = $row;
}

$title = "Member";
if ($profile['analyst']) { $title = "Analyst"; }
if ($profile['gps_position_held']) { $title = $profile['gps_position_held']; }
if ($profile['alumni']) { $title = "Alumni"; }
if ($profile['chapter_founder']) { $title = "Chapter Founder"; }
if ($profile['founding_member']) { $title = "Founder"; }
if ($profile['advisory_board']) { $title = "Advisor"; }

$profile['title'] = $title;

$title = $profile['first_name'] . " " . $profile['last_name'] . " | ". $title;

$smarty->assign('can_edit', $can_edit);
$smarty->assign('is_editing', $is_editing);
$smarty->assign('profile_id', $profile_id);

$smarty->assign('tags', $tags);

$smarty->assign('profile', $profile);
$smarty->assign('display_photo', $display_photo);
$smarty->assign('title', $title);

$smarty->assign('error', $error);
$smarty->assign('errormsg', $errormsg);

}

$smarty->assign('not_viewable', $not_viewable);

?>