<?php
$profile_id = $_GET['id'];
if (!isset($_GET['id'])) { header("Location: ./"); }
if (!is_numeric($profile_id)) { die("Hack attempt"); }

//check if your profile
if ($_SESSION['member_id'] == $profile_id) { $can_edit = 1; }

//check if editing
if ($can_edit == 1 && $_GET['edit'] == 1) {
$is_editing = 1;

if (isset($_POST['submitted'])) { $do_edit = 1; }

}

require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

//include nav nav menu
$page = "ACADEMIA"; include ($_SERVER['DOCUMENT_ROOT'] . "/includes/network_t_menu.php"); $smarty->assign('network_t_menu', $network_t_menu);

$sql = "SELECT * FROM members WHERE id = '$profile_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$profile = mysql_fetch_array($result);

load_unis(); //loads universities into university array
$profile['uni_sname'] = $university[$profile['ug_school_id']]['sname'];
$profile['uni_fname'] = $university[$profile['ug_school_id']]['fname'];

//check if account has been disabled, or elected to be hidden
if ($profile['hidden'] == 1 || $profile['disabled'] == 1) {
$not_viewable = 1;
}


if ($do_edit == 1) {
$do_undergraduate_major = mysql_real_escape_string($_POST['undergraduate_major']);
$do_graduation_year = mysql_real_escape_string($_POST['graduation_year']);
$do_graduate_school = mysql_real_escape_string($_POST['graduate_school']);
$do_graduate_major = mysql_real_escape_string($_POST['graduate_major']);
$error = 0;
$errormsg = "";
if (strlen($do_undergraduate_major) > 50) { $error++; $errormsg .= "Undergraduate major exceeds 50 character limit.<BR>"; }
if (isset($do_graduation_year) && !is_numeric($do_graduation_year)) { $error++; $errormsg .= "Graduation year must be a number.<BR>"; }
if (strlen($do_graduate_school) > 50) { $error++; $errormsg .= "Graduate school exceeds 50 character limit.<BR>"; }
if (strlen($do_graduate_major) > 50) { $error++; $errormsg .= "Graduate major exceeds 50 character limit.<BR>"; }
if (!$error) {
$sql = "UPDATE members SET undergraduate_major='$do_undergraduate_major', graduation_year='$do_graduation_year', graduate_school='$do_graduate_school', graduate_major='$do_graduate_major'
WHERE id = '$profile_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
unset($do_undergraduate_major,$do_graduation_year,$do_graduate_school,$do_graduate_major);

//add logs
$sql2 = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('profile_update','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','academia','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql2) or die("There was a problem logging the download. Please contact an admin.");

header("Location: academia.php?id=$profile_id");

}

}

if ($not_viewable != 1) {


if(strlen($profile['current_photo']) > 0) { $display_photo = 1; }

$title = "Member";
if ($profile['analyst']) { $title = "Analyst"; }
if ($profile['gps_position_held']) { $title = $profile['gps_position_held']; }
if ($profile['alumni']) { $title = "Alumni"; }
if ($profile['chapter_founder']) { $title = "Chapter Founder"; }
if ($profile['founding_member']) { $title = "Founder"; }
if ($profile['advisory_board']) { $title = "Advisor"; }

$title = $profile['first_name'] . " " . $profile['last_name'] . " | ". $title;


$smarty->assign('can_edit', $can_edit);
$smarty->assign('is_editing', $is_editing);
$smarty->assign('profile_id', $profile_id);

$smarty->assign('profile', $profile);
$smarty->assign('display_photo', $display_photo);
$smarty->assign('title', $title);

$smarty->assign('error', $error);
$smarty->assign('errormsg', $errormsg);

}

$smarty->assign('not_viewable', $not_viewable);

?>