<?php

$profile_id = $_GET['id'];
if (!isset($_GET['id'])) { header("Location: ../"); }
if (!is_numeric($profile_id)) { die("Hack attempt"); }

//check if your profile
if ($_SESSION['member_id'] == $profile_id) { $can_edit = 1; }

//check if editing
if ($can_edit == 1 && $_GET['edit'] == 1) {
$is_editing = 1;

if (isset($_POST['employer'])) { $do_edit = 1; }

}

require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

//include nav nav menu
$page = "EMPLOYMENT"; include ($_SERVER['DOCUMENT_ROOT'] . "/includes/network_t_menu.php"); $smarty->assign('network_t_menu', $network_t_menu);

if ($can_edit && $_GET['deljob']) {
$delnum = $_GET['deljob'];
if (!is_numeric($delnum)) { die("Fatal error"); }
$sql = "SELECT uid FROM jobs WHERE id = '$delnum' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_row($result);
if ($row[0] == $profile_id) {
$sql = "DELETE FROM jobs WHERE id ='$delnum' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
}
}

if ($can_edit && $_POST['submitted']) { //edit form submitted

$do_cji = $_POST['currentjob'];
if (isset($_POST['currentjob']) && is_numeric($do_cji)) {
//now get job name
if ($do_cji != 0) {
//$sql = "SELECT employer, i_id FROM jobs WHERE id ='$do_cji' LIMIT 1";
$sql = "SELECT jobs.employer, jobs.i_id, jobs_industries.name as i_name FROM jobs JOIN jobs_industries ON jobs.i_id = jobs_industries.id WHERE jobs.id ='$do_cji' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
$do_job_name = $row['employer'];
$do_industry_id = $row['i_id'];
$do_industry_name = $row['i_name'];
}
$sql = "UPDATE members SET current_job_id = '$do_cji', current_employer = '$do_job_name', current_industry_id = '$do_industry_id', current_industry = '$do_industry_name' WHERE id = '$profile_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
}

if(isset($_POST['linkedinurl'])) {
$s_linkedin = $_POST['linkedinurl'];
$s_linkedin = filter_var($s_linkedin, FILTER_SANITIZE_URL);
$sql = "UPDATE members SET linkedin = '$s_linkedin' WHERE id = '$profile_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
}


}

if ($can_edit && isset($_GET['editjob'])) {
$editjob = $_GET['editjob'];
if (!is_numeric($editjob)) { die("Fatal error"); }
$sql = "SELECT uid FROM jobs WHERE id = '$editjob' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_row($result);
if ($row[0] != $profile_id) {
$editjob = 0;
}
}

if ($do_edit && $can_edit) { //form submitted

//first load up variables
$do_id = mysql_real_escape_string($_POST['id']);
$do_employer = mysql_real_escape_string($_POST['employer']);
$do_division = mysql_real_escape_string($_POST['division']);
$do_title = mysql_real_escape_string($_POST['title']);
$do_industry_id = mysql_real_escape_string($_POST['industry']);
$do_startmonth = mysql_real_escape_string($_POST['startmonth']);
$do_startyear = mysql_real_escape_string($_POST['startyear']);
$do_endmonth = mysql_real_escape_string($_POST['endmonth']);
$do_endyear = mysql_real_escape_string($_POST['endyear']);
$do_current = mysql_real_escape_string($_POST['current']);

$error = 0;
$errormsg = "";

if (strlen($do_employer) > 50) { $error++; $errormsg .= "Employer name exceeds 50 character limit.<BR>"; }
if (strlen($do_employer) < 2) { $error++; $errormsg .= "Please enter the employer name. (Make sure you hit enter after typing it!)<BR>"; }
if (!is_numeric($do_startmonth+$do_startyear+$do_endmonth+$do_endyear)) { $error++; $errormsg .= "Dates not formatted correctly."; }
if ($do_current != 1 && $do_current != 0) { die("Session Timeout"); }
if (!is_numeric($do_industry_id)) { die("Session Timeout"); }

$do_start = $do_startyear."-".$do_startmonth."-00";
$do_end = $do_endyear."-".$do_endmonth."-00";

if ($do_current) { $do_end = date("Y-m-00"); }

//if no errors, load into database
if (!$error) {


//if editing existing job
if ($_GET['e_job']) {

$sql = "UPDATE jobs SET employer='$do_employer', division='$do_division', title='$do_title', start='$do_start', end='$do_end', current='$do_current', i_id='$do_industry_id' WHERE id = '$do_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
unset($do_employer,$do_division,$do_title,$do_startmonth,$do_endmonth,$do_startyear,$do_endyear,$do_current,$do_industry_id);

//add logs
$sql2 = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('profile_update','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','employment','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql2) or die("There was a problem logging the download. Please contact an admin.");

header("Location: employment.php?id=$profile_id&edit=1");

}

//if adding new job
else {

$sql = "INSERT INTO jobs (uid, employer, division, title, start, end, current, i_id)
VALUES ('$profile_id', '$do_employer', '$do_division', '$do_title', '$do_start', '$do_end', '$do_current', '$do_industry_id')";
$result = mysql_query($sql) or die(mysql_error());
if ($do_current) {
$sql = "SELECT id FROM jobs WHERE uid = '".$profile_id."' ORDER BY id DESC LIMIT 1";
$result = mysql_query($sql) or die (mysql_error());
$row = mysql_fetch_array($result);
$do_cji = $row[0];
$sql = "SELECT jobs.employer, jobs.i_id, jobs_industries.name as i_name FROM jobs JOIN jobs_industries ON jobs.i_id = jobs_industries.id WHERE jobs.id ='$do_cji' LIMIT 1";
$result = mysql_query($sql) or die (mysql_error());
$row = mysql_fetch_array($result);
$do_industry_name = $row['i_name'];
$do_industry_id = $row['i_id'];
$sql = "UPDATE members SET current_job_id = '$do_cji', current_employer = '$do_employer', current_industry = '$do_industry_name', current_industry_id = '$do_industry_id' WHERE id = '$profile_id' LIMIT 1";
$result = mysql_query($sql) or die (mysql_error());
}
unset($do_employer,$do_division,$do_title,$do_startmonth,$do_endmonth,$do_startyear,$do_endyear,$do_current,$do_industry_id);

//add logs
$sql2 = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('profile_update','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','employment','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql2) or die("There was a problem logging the download. Please contact an admin.");

header("Location: employment.php?id=$profile_id");

}


}

}

$sql = "SELECT * FROM members WHERE id = '$profile_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$profile = mysql_fetch_array($result);

//check if account has been disabled, or elected to be hidden
if ($profile['hidden'] == 1 || $profile['disabled'] == 1) {
$not_viewable = 1;
}

if ($not_viewable != 1) {

//load up jobs - orders by current jobs at the top, then end dates desc, then start dates desc, then employer names
$sql = "SELECT * FROM jobs WHERE uid = '$profile_id' ORDER BY current DESC, end DESC, start DESC, employer ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_assoc($result))
    $jobs[] = $row;

//load up employment industries
$sql = "SELECT * FROM jobs_industries ORDER BY name ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_assoc($result))
    $industries[] = $row;

if(strlen($profile['current_photo']) > 0) { $display_photo = 1; }

if (strlen($profile['linkedin']) > 0) { $smarty->assign('linkedin', 1); }

$title = "Member";
if ($profile['analyst']) { $title = "Analyst"; }
if ($profile['gps_position_held']) { $title = $profile['gps_position_held']; }
if ($profile['alumni']) { $title = "Alumni"; }
if ($profile['chapter_founder']) { $title = "Chapter Founder"; }
if ($profile['founding_member']) { $title = "Founder"; }
if ($profile['advisory_board']) { $title = "Advisor"; }

$title = $profile['first_name'] . " " . $profile['last_name'] . " | ". $title;

$smarty->assign('can_edit', $can_edit);
$smarty->assign('is_editing', $is_editing);
$smarty->assign('profile_id', $profile_id);

$smarty->assign('profile', $profile);
$smarty->assign('display_photo', $display_photo);
$smarty->assign('title', $title);

$smarty->assign('do_employer', $do_employer);
$smarty->assign('do_division', $do_division);
$smarty->assign('do_title', $do_title);
$smarty->assign('do_startmonth', $do_startmonth);
$smarty->assign('do_startyear', $do_endyear);
$smarty->assign('do_endmonth', $do_endmonth);
$smarty->assign('do_endyear', $do_endyear);
$smarty->assign('current', $do_current);

$smarty->assign('error', $error);
$smarty->assign('errormsg', $errormsg);

$smarty->assign('jobs', $jobs);
$smarty->assign('industries', $industries);

$smarty->assign('editjob', $editjob);

}

$smarty->assign('not_viewable', $not_viewable);

?>