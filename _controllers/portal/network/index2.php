<?php
require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

// Include image manipulation script
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/simpleimage.php");

// Face detection
include($_SERVER['DOCUMENT_ROOT']."/includes/FaceDetector.php");

//Get data from logs and send into array, grouped by user
$sql ="
SELECT l.type, timestampdiff(SECOND,now(),l.time) as t_minus,
if (l.type='survey_vote',(SELECT question FROM vote_questions WHERE id=l.data LIMIT 1),
if (l.type='dms_file_upload',(SELECT cast(concat(id_folder,',',name) as char) FROM dms_file WHERE id_file=l.data LIMIT 1),l.data)) as data,
m.id as user_id,
concat(m.first_name,' ',m.last_name) as user_fullname,
m.gender_male as user_male,
m.current_photo
FROM logs AS l
JOIN members AS m
WHERE m.id = l.uid
AND l.type
IN (
'book_add',  'dms_file_upload',  'profile_update',  'research_add',  'survey_vote'
)
ORDER BY l.time DESC 
LIMIT 120
";
$data_feed = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($data_feed)) {
if ($row['data'] != "") { //clear dud rows, e.g. deleted documents
	$feed_grouped[$row['user_id']][] = $row;
}

//create list of user ids in order
if ($user_fed[$row['user_id']] != 1) {
	$users_in_feed_arr[] = $row['user_id'];
}
$user_fed[$row['user_id']] = 1;

}

$f_count=0;

foreach ($users_in_feed_arr as $value) {
	
	//user information
	$feed[$f_count]['user_id'] = $value;
	$feed[$f_count]['user_fullname'] = $feed_grouped[$value][0]['user_fullname'];
	if ($feed_grouped[$value][0]['user_male']) { $feed[$f_count]['gender_male'] = 1; }
	
	//each piece of news grouped together
	foreach ($feed_grouped[$value] as $value2) {
		$feed[$f_count]['news'][$value2['type']][] = $value2['data'];
	}
	
	$content = '';
	$count['book_add'] = count($feed[$f_count]['news']['book_add']);
	$count['dms_file_upload'] = count($feed[$f_count]['news']['dms_file_upload']);
	$count['profile_update'] = count($feed[$f_count]['news']['profile_update']);
	$count['research_add'] = count($feed[$f_count]['news']['research_add']);
	$count['survey_vote'] = count($feed[$f_count]['news']['survey_vote']);
	
	//book add
	if ($count['book_add'] > 0) {
		$content .= " read";
		foreach ($feed[$f_count]['news']['book_add'] as $key => $value3) {
			if ($key != $count['book_add'] -1) {
			$content .= " <a href='/portal/network/view/interests.php?id=".$feed[$f_count]['user_id']."'>".$value3."</a>,";
			}
			else {
			$content .= "and <a href='/portal/network/view/interests.php?id=".$feed[$f_count]['user_id']."'>".$value3."</a>";
			}
		}
		$content.= "...";
	}
	
	//file upload
	if ($count['dms_file_upload'] > 0) {
		$content .= " uploaded";
		foreach ($feed[$f_count]['news']['dms_file_upload'] as $key => $value3) {
			if ($fed[$feed[$f_count]['user_id']][$value3] != 1) {
				$doc = explode(",",$value3);
				if ($key != $count['dms_file_upload'] -1) {
				$content .= " <a href='/portal/documents/folder/folders/".$doc[0]."'>".$doc[1]."</a>,";
				}
				else if ($count['dms_file_upload'] == 1) {
				$content .= " <a href='/portal/documents/folder/folders/".$doc[0]."'>".$doc[1]."</a>";
				}
				else {
				$content .= "and <a href='/portal/documents/folder/folders/".$doc[0]."'>".$doc[1]."</a>";
				}
			}
			$fed[$feed[$f_count]['user_id']][$value3] = 1;
		}
		$content.= "...";
	}
	
	//profile update
	$i=0;
	if ($count['profile_update'] > 0) {
		$content .= " updated the";
		foreach ($feed[$f_count]['news']['profile_update'] as $key => $value3) {
			if ($fed[$feed[$f_count]['user_id']][$value3] != 1) {
				if ($key != $count['profile_update'] -1) {
					$content .= " <a href='/portal/network/view/".$value3.".php?id=".$feed[$f_count]['user_id']."'>".ucwords($value3)."</a>,";
				}
				else if ($count['profile_update'] == 1) {
					$content .= " <a href='/portal/network/view/".$value3.".php?id=".$feed[$f_count]['user_id']."'>".ucwords($value3)."</a>";
				}
				else {
					$content .= "and <a href='/portal/network/view/".$value3.".php?id=".$feed[$f_count]['user_id']."'>".ucwords($value3)."</a>";
				}
			$i++;
			}
			$fed[$feed[$f_count]['user_id']][$value3] = 1;
		}
		$content = rtrim($content, ',');
		$content .= " section";
		if ($i > 1) {
			$content .= "s";
		}
		$content .= " of ";
		if ($feed[$f_count]['gender_male']) { $content .= "his"; } else { $content .= "her"; }
		$content .= " profile";
		$content.= "...";
	}
	
	//research add
	if ($count['research_add'] > 0) {
		$content .= " added research on";
		$content.= "...";
	}
	
	//survey vote
	if ($count['survey_vote'] > 0) {
		$content .= " voted on";
		foreach ($feed[$f_count]['news']['survey_vote'] as $key => $value3) {
			if ($key != $count['survey_vote'] -1) {
			$content .= " <a href='/portal/vote/'>".$value3."</a>,";
			}
			else if ($count['survey_vote'] == 1) {
			$content .= " <a href='/portal/vote/'>".$value3."</a>";
			}
			else {
			$content .= "and <a href='/portal/vote/'>".$value3."</a>";
			}
		}
		$content.= "...";
	}
	
	$content = str_replace(",and"," and",$content);
	
	
	$feed[$f_count]['content'] = $content;
	
	
	//other stuff
		//thumbnail: photo of user
		$img_path = $feed_grouped[$value][0]['current_photo'];
		$img_path_cropped = str_replace("current","cropped",$img_path);

		if(strlen($img_path) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'] . $img_path)) {

			if(!file_exists($_SERVER['DOCUMENT_ROOT'] . $img_path_cropped)) {
				$detector = new FaceDetector();
				$detector->scan($_SERVER['DOCUMENT_ROOT'] . $img_path);
				$faces = $detector->getFaces();
				foreach($faces as $face) {
				$x = $face['x'];
				$y = $face['y'];
				$width = $face['width'];
				$height = $face['height'];
			}

				$image = new SimpleImage();
				$image->load($_SERVER['DOCUMENT_ROOT']. $img_path);
				if (is_numeric($width)) { 
					$image->crop($width,$height,$x,$y);
				}
				$image->resizeToHeight(100);
				$image->resizeToWidth(50);
				$image->crop(50, 50, 0, 0);
				//$image->resize(50, 50);
				$image->save($_SERVER['DOCUMENT_ROOT']. $img_path_cropped);
			}

				$feed[$f_count]['thumb'] = $img_path_cropped;
		}
		else {
			$feed[$f_count]['thumb'] = "/images/male_thumb.png";
		}
		
		//t_minus (how long ago)
			//get seconds ago from db
			$t_minus = abs($feed_grouped[$value][0]['t_minus']);
		
			//format nicely
			if ($t_minus < 60) {
			$feed[$f_count]['timetext'] = round($t_minus) . " seconds ago";
			}

			if ($t_minus < 3600) {
			$feed[$f_count]['timetext'] = round($t_minus/60) . " minutes ago";
			}
			
			if ($t_minus < 86400) {
			$feed[$f_count]['timetext'] = round($t_minus/3600) . " hours ago";
			}
			
			else {
			$feed[$f_count]['timetext'] = round($t_minus/86400) . " days ago";
			}
	
	$f_count++;

}

// Assign variables
$smarty->assign('feed', $feed);

?>