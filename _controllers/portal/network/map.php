<?php
require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

//share location if requested
if ($_GET['share_location']) {
	$sql = "UPDATE members SET address_private = '0' WHERE id = '".$_SESSION['user_id']."'";
	$result = mysql_query($sql);
}

//update address
if ($_POST) {
	$do_address = mysql_real_escape_string($_POST['address']);
	$url = "http://maps.google.com/maps/geo?q=".urlencode($do_address)."&output=csv&key=AIzaSyDuG7ufZ1rLsxu_KstE-A21mhsDgEEimsY";
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER,0); //Change this to a 1 to return headers
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 
	$data = curl_exec($ch);
	
	//Check our Response code to ensure success
	if (substr($data,0,3) == "200"){
		$data = explode(",",$data);
			$precision = $data[1];
			$latitude = $data[2];
			$longitude = $data[3];
			if (is_numeric($latitude) && is_numeric($longitude)) {
				$sql = "UPDATE members SET address = '".$do_address."', address_lat_long = POINT(".$latitude.", ".$longitude.") WHERE id = '".$_SESSION['user_id']."' LIMIT 1";
				$result2 = mysql_query($sql) or die(mysql_error());
			}
			else {
				$address_not_found = 1;
			}
	}
	else {
		$address_not_found = 1;
	}
	curl_close($ch);
}

//Check for entries
$sql = "SELECT      m.id,
					m.email_address,
                    m.first_name,
                    m.last_name,
                    m.graduation_year,
                    m.current_employer,
                    m.ug_school_id,
					m.current_photo,
					m.contact_number,
					m.contact_number_country_code,
					m.address,
					m.address_private,
					x(m.address_lat_long) as address_lat,
					y(m.address_lat_long) as address_long,
                    u.FNAME AS undergraduate_school,
		    u.SNAME as undergraduate_school_s
        FROM        members m
        JOIN        universities u
        ON          m.ug_school_id = u.ID
        WHERE       NOT m.hidden
        AND         NOT m.disabled
		AND			m.address <> ''
		AND			m.address_lat_long <> ''
        ORDER BY    m.graduation_year DESC,
		    m.first_name ASC";

$result = mysql_query($sql) or die(mysql_error());

while ($row = mysql_fetch_assoc($result)) {
//check for thumbnail, the second line is for backwards-compatibility with legacy set up
if (file_exists($_SERVER['DOCUMENT_ROOT']."/content_files/member_photos/".$row['id']."-cropped.jpg")) { $row['cropped_photo'] = "/content_files/member_photos/".$row['id']."-cropped.jpg"; }
elseif (file_exists($_SERVER['DOCUMENT_ROOT']."/content_files/member_photos/".$row['first_name']."-".$row['last_name']."-cropped.jpg")) { $row['cropped_photo'] = "/content_files/member_photos/".$row['first_name']."-".$row['last_name']."-cropped.jpg"; }
else { $row['cropped_photo'] = "/images/male_thumb.png"; }
	if ($row['address_private'] != 1) {
		$members_array[] = $row;
	}
}

//retrieve years
foreach ($members_array as $temp) {
	if ($temp['graduation_year'] != '') {
		$years[$temp['graduation_year']][] = $temp;
	}
}

//get user details
$sql = "SELECT id, address, address_private, x(address_lat_long) as address_lat, y(address_lat_long) as address_long FROM members WHERE id = '".$_SESSION['user_id']."' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_assoc($result)) {
	$user_details = $row;
}
if (isset($_POST['address'])) {
	$user_details['address'] = $_POST['address'];
}

foreach ($members_array as $value) {
//echo $value['address']."<BR>";
}

// Assign variables
$smarty->assign('members_array', $members_array);
$smarty->assign('user_details', $user_details);
$smarty->assign('address_not_found', $address_not_found);
$smarty->assign('years', $years);

?>