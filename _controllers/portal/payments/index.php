<?php
//create payments array
$payments['active'] = array();
$payments['past'] = array();

$sql = "SELECT * FROM payments WHERE user_id = '".$_SESSION['member_id']."' AND (status = 'unpaid' OR status = 'overdue') ORDER BY id DESC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$payments['active'][] = $row;
}

$sql = "SELECT * FROM payments WHERE user_id = '".$_SESSION['member_id']."' AND (status != 'unpaid' AND status != 'overdue') ORDER BY id DESC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$payments['past'][] = $row;
}

// Annual dues paid
$payments['past_annual_sum'] = 0;
$sql = "SELECT amount FROM payments WHERE user_id = '".$_SESSION['member_id']."' AND annual_dues_flag = 1 AND (status != 'unpaid' AND status != 'overdue') ORDER BY id DESC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$payments['past_annual_sum'] += $row[0];
}

// Get dues cap, and dues paid not via the Portal (e.g. via PayPal)
$sql = "SELECT m.non_portal_dues_paid, d.cap_amount FROM members as m, dues_cap as d WHERE m.dues_cap_id = d.id AND m.id = '".$_SESSION['member_id']."'";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
	$payments['non_portal_dues_paid'] = $row['non_portal_dues_paid'];
	$payments['dues_cap_amount'] = $row['cap_amount'];
}

$payments['total_annual_paid'] = $payments['non_portal_dues_paid'] + $payments['past_annual_sum'];
$payments['total_annual_dues_remaining'] = $payments['dues_cap_amount'] - $payments['total_annual_paid'];

// If user has paid dues not via the Portal, add this to the list of past payments
if ($payments['non_portal_dues_paid'] > 0) {
	$payments['past'][] = array(
		'id' => "-1",
		'user_id' => $_SESSION['member_id'],
		'annual_dues_flag' => "0",
		'description' => "Dues paid via PayPal",
		'amount' => $payments['non_portal_dues_paid'],
		'due_date' => "",
		'status' => "paid",
		'created' => "",
		'updated' => ""
		);
}

$active_payments_count = count($payments['active']);
$past_payments_count = count($payments['past']);

// Assign variables
$smarty->assign('payments', $payments);

$smarty->assign('active_payments_count', $active_payments_count);
$smarty->assign('past_payments_count', $past_payments_count);
?>