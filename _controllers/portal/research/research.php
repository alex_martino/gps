<?php
// Include site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.inc.php");

if (!$user_is_analyst) { $can_delete = 1; }
if ($_SESSION['user_is_admin'] || $_SESSION['user_is_um'] || $_SESSION['user_is_board']) { $can_restore = 1; }

$error = 0;

$sectorid = $_GET['sector'];
if (isset($_GET['sector']) && !is_numeric($sectorid)) { $sectorid = 0; }

$research_item = $_GET['item'];
if (isset($_GET['item']) && !is_numeric($research_item)) { die("Fatal error"); }

if ($_GET['restore'] == 1 && $can_delete == 1 && is_numeric($_GET['item'])) { //Delete item
$item = $_GET['item'];
$sql = "UPDATE research_pipeline SET deleted = '0' WHERE id = '".$item."' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());

//log research restored
$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('research_restore','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql) or die("There was a problem logging the restoration. Please contact an admin.");

if ($sectorid != 0 ) { header("Location: research.php?sector=$sectorid"); }
else { header("Location: research.php?wishlist=1"); }
}

if ($_GET['delete'] == 1 && $can_delete == 1 && is_numeric($_GET['item'])) { //Delete item
$item = $_GET['item'];
$sql = "UPDATE research_pipeline SET deleted = '1' WHERE id = '".$item."' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());

//log research deleted
$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('research_delete','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".$item."','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql) or die("There was a problem logging the deletion. Please contact an admin.");

if ($sectorid != 0 ) { header("Location: research.php?sector=$sectorid"); }
else { header("Location: research.php?wishlist=1"); }
}

if ($_GET['edit_form'] == 1 && is_numeric($_GET['item'])) { //Edit research item form submitted

$do_description = mysql_real_escape_string($_POST['description']);
$do_why_interesting = mysql_real_escape_string($_POST['why_interesting']);
$do_other_information = mysql_real_escape_string($_POST['other_information']);
$do_ticker = mysql_real_escape_string($_POST['ticker']);
$item = $_GET['item'];

$sql = "UPDATE research_pipeline SET description = '$do_description', why_interesting = '$do_why_interesting', other_information = '$do_other_information', ticker = '$do_ticker' WHERE id = '$item' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());

//log research update
$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('research_update','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".$item."','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql) or die("There was a problem logging the update. Please contact an admin.");

if ($sectorid != 0 ) { header("Location: research.php?sector=$sectorid&item=$item"); }
else { header("Location: research.php?wishlist=1"); }

}

if ($_GET['addcompany'] == 1) { //add form submitted

$do_company_name = mysql_real_escape_string($_POST['company_name']);
$do_sector_id = mysql_real_escape_string($_POST['sector']);
$do_ticker = mysql_real_escape_string($_POST['ticker']);
$do_description = mysql_real_escape_string($_POST['description']);
$do_why_interesting = mysql_real_escape_string($_POST['why_interesting']);
$do_other_information = mysql_real_escape_string($_POST['other_information']);

$do_type = mysql_real_escape_string($_POST['type']);
$do_user = mysql_real_escape_string($_POST['user']);

//check all fields which should be numeric are numeric or die
if (!is_numeric($do_sector_id)) { header("Location: index.php"); }

if ($do_type != "pipeline" && $do_type != "wishlist") { header("Location: index.php"); }

//now do error handling
if (strlen($do_company_name) < 1) { $error++; $errormsg.="Please enter the company name. &nbsp; "; }
if ($do_sector_id == 0) { $error++; $errormsg .= "Please select a sector. &nbsp; "; }
if (strlen($do_description) < 30) { $error++; $errormsg .= "The description needs to be at least 30 characters. &nbsp; "; }
if (strlen($do_why_interesting) < 30) { $error++; $errormsg .= "Please explain why this company is interesting. &nbsp; "; }

if (!is_numeric($_SESSION['user_id']) || $_SESSION['user_id'] == 0) {
$error++;
header("Location: /members/login.php?logout=1");
die();
}

if ($error < 1) { //no errors, add survey

//format deadline
$do_created = date("Y-m-d");

//get share price
if(strlen($do_ticker) > 0) { //get finance data
$stock_data = file_get_contents("http://finance.google.com/finance/info?client=ig&q=".$do_ticker);
$stock_data_array = explode('"', $stock_data);
$do_current_price = $stock_data_array[15];
}
if (!is_numeric($do_current_price) || $do_current_price == 0) {
$do_current_price = "";
}

//wishlist or pipeline
if ($do_type == "pipeline") { $do_wishlist = 0; } else { $do_wishlist = 1; }

if ($do_user == 0 || !is_numeric($do_user)) { $do_user = $_SESSION['user_id']; }

$sql = "INSERT INTO research_pipeline (company_name, ticker, sector_id, description, entry_date, entry_price, why_interesting, other_information, user, wishlist, deleted)
VALUES ('$do_company_name', '$do_ticker', '$do_sector_id', '$do_description', '$do_created', '$do_current_price', '$do_why_interesting', '$do_other_information', '$do_user', '$do_wishlist', '0')";
$result = mysql_query($sql) or die(mysql_error());

$add_success = 1;

//log research add
$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('research_add','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".$do_type."','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql) or die("There was a problem logging the research add. Please contact an admin.");

} //end no errors



}


$sql = "SELECT r.id, r.entry_date, r.company_name, r.ticker, r.sector_id, s.sname, r.user, m.first_name, m.last_name FROM research_pipeline r JOIN sectors s ON r.sector_id = s.id JOIN members m ON r.user = m.id WHERE wishlist = 1 AND deleted = 0";
if (is_numeric($sectorid)) { $sql .= " AND r.sector_id = '$sectorid'"; }
$sql .= " ORDER BY r.entry_date DESC, r.company_name ASC";
$result = mysql_query($sql) or die(mysql_error());
if(mysql_num_rows($result) > 0) { $display_w = 1; }
while($row = mysql_fetch_array($result)) {
$wishlist[] = $row;
}

$sql = "SELECT r.id, r.entry_date, r.company_name, r.ticker, r.sector_id, s.sname, r.user, m.first_name, m.last_name FROM research_pipeline r JOIN sectors s ON r.sector_id = s.id JOIN members m ON r.user = m.id WHERE wishlist = 0 AND deleted = 0";
if (is_numeric($sectorid)) { $sql .= " AND r.sector_id = '$sectorid'"; }
$sql .= " ORDER BY r.entry_date DESC, r.company_name ASC";
$result = mysql_query($sql) or die(mysql_error());
if(mysql_num_rows($result) > 0) { $display_p = 1; }
while($row = mysql_fetch_array($result)) {
$pipeline[] = $row;
}



$research_cat = "PM's Wishlist";
if (is_numeric($sectorid)) {
$sql = "SELECT fname, description FROM sectors where id = '$sectorid'";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array($result)) {
$research_cat = $row['fname'];
$research_des = $row['description'];
}
}

if (is_numeric($research_item)) {
$sql = "SELECT * FROM research_pipeline where id = '$research_item' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array($result)) {
$item_details = $row;
}
if (strlen($item_details['description']) < 1) { $item_details['description'] = "[No description entered]"; }
if (strlen($item_details['why_interesting']) < 1) { $item_details['why_interesting'] = "[Information not entered]"; }
if (strlen($item_details['other_information']) < 1) { $item_details['other_information'] = "[Other information not entered]"; }
$research_cat .= ": ".$item_details['company_name'];

if(strlen($item_details['ticker']) > 0) { //get finance data
$stock_data = file_get_contents("http://finance.google.com/finance/info?client=ig&q=".$item_details['ticker']);
$stock_data_array = explode('"', $stock_data);
$item_details['current_price'] = $stock_data_array[15];
}

//process price
if (!is_numeric($item_details['current_price'])) {
$item_details['current_price'] = "?";
$item_details['price_change'] = "?";
}
else {
$item_details['price_change'] = round(($item_details['current_price']-$item_details['entry_price'])/$item_details['entry_price'] * 100,2);
}

}

$sql = "SELECT * FROM sectors ORDER BY fname ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$sectors[] = $row;
}

$sql = "SELECT id, CONCAT(first_name,' ', last_name) name FROM members WHERE disabled = 0 AND (analyst = 1 OR member = 1) ORDER BY name ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$users[] = $row;
}

$smarty->assign('can_delete', $can_delete);
$smarty->assign('can_restore', $can_restore);

$smarty->assign('research_cat', $research_cat);
$smarty->assign('research_description', $research_des);
$smarty->assign('research_item', $research_item);
$smarty->assign('item_details', $item_details);

$smarty->assign('sectors', $sectors);
$smarty->assign('users', $users);

$smarty->assign('wishlist', $wishlist);
$smarty->assign('pipeline', $pipeline);

$smarty->assign('display_wishlist', $display_w);
$smarty->assign('display_pipeline', $display_p);
$smarty->assign('sectorid', $sectorid);

$smarty->assign('error', $error);
$smarty->assign('errormsg', $errormsg);

?>