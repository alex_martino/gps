<?php
// Include site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/config.php");

//Check we are an admin
if (!$_SESSION['user_is_admin']) { die("Fatal error."); }

//Delete admin
$del_id = $_GET['del'];
if (is_numeric($del_id) && ($del_id != 103)) {
$sql = "UPDATE members SET admin = 0 WHERE id = '$del_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());

// Get details

				$sql = "SELECT first_name, last_name FROM members WHERE id = '$del_id' LIMIT 1";
				$result = mysql_query($sql) or die(mysql_error());
				while($row = mysql_fetch_array($result)) { $this_name = $row[0] . " " . $row[1]; }

// Log deletion permanently
$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('admin_delete','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".$this_name ."','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql) or die("There was a problem logging the admin delete. Please contact an admin.");

// Send email

				//Get email Address

				//now email
				require_once "Mail.php";
				
				$from = "GPS Admin <admin@".$domain.">";
				$to = "bwigoder@gps100.com";
				$subject = "Admin Access Removed";
				$body = "Dear Benjamin,\n\n" . $_SESSION['user_first_name'] . " " . $_SESSION['user_last_name'] . " removed " . $this_name . "'s admin access.";
				$body .= "\n\nMany thanks,\nYour loyal GPS Portal";

				$host = "localhost";
				$username = "admin@".$domain;
				$password = "101dalmations";

				$headers = array ('From' => $from,
				  'To' => $to,
				  'Subject' => $subject);
				$smtp = Mail::factory('smtp',
				  array ('host' => $host,
				    'auth' => true,
				    'username' => $username,
				    'password' => $password));

				$mail = $smtp->send($to, $headers, $body);

				if (PEAR::isError($mail)) {
				  $error++; $errormsg = "Email Error";
				 } else {
				  $email_success=1;
				 }
}

// Add admin
if ($_POST['add_admin']) {
$add_id = explode("_", mysql_real_escape_string($_POST['add_admin']));
$add_name = explode(" ",$add_id[1]);

// Check user is not already an admin
$sql = "SELECT id FROM members WHERE first_name = '$add_name[0]' AND last_name = '$add_name[1]' AND admin = 0 LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
if (mysql_num_rows($result) > 0) {

$sql = "UPDATE members SET admin = 1 WHERE first_name = '$add_name[0]' AND last_name = '$add_name[1]' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$add_success = 1;

// Send email

				//Get email Address
				$sql = "SELECT email_address FROM members WHERE first_name = '$add_name[0]' AND last_name = '$add_name[1]' LIMIT 1";
				$result = mysql_query($sql) or die(mysql_error());
				while($row = mysql_fetch_array($result)) { $c_email = $row[0]; }

				if(strlen($c_email) > 0) {

				//now email
				require_once "Mail.php";
				
				$from = "GPS Admin <admin@".$domain.">";
				$to = $c_email.",bwigoder@gps100.com";
				$subject = "Admin Access Granted";
				$body = "Dear ". $add_name[0] .",\n\nYou have been provided admin access for the GPS Portal by " . $_SESSION['user_first_name'] . " " . $_SESSION['user_last_name'] . ". To view the admin area please visit: http://www.gps100.com/portal/admin/";
				$body .= "\n\nMany thanks,\nBenjamin Wigoder";

				$host = "localhost";
				$username = "admin@".$domain;
				$password = "101dalmations";

				$headers = array ('From' => $from,
				  'To' => $to,
				  'Subject' => $subject);
				$smtp = Mail::factory('smtp',
				  array ('host' => $host,
				    'auth' => true,
				    'username' => $username,
				    'password' => $password));

				$mail = $smtp->send($to, $headers, $body);

				if (PEAR::isError($mail)) {
				  $error++; $errormsg = "Email Error";
				 } else {
				  $email_success=1;
				 }

				}

// Add log

// Log add permanently
$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('admin_add','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".$add_name[0] . " " . $add_name[1] ."','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql) or die("There was a problem logging the admin add. Please contact an admin.");


} //end row count

else {

$error = 1; $errormsg = "User is already an admin!";

}

}

// Generate list of admins
$sql = "SELECT id, first_name, last_name FROM members WHERE admin=1 AND disabled != 1 ORDER BY first_name ASC";
$result = mysql_query($sql) or die(mysql_error());
$i=0;
while ($row = mysql_fetch_array($result)) {
$admin_list[$i]['id'] = $row['id'];
$admin_list[$i]['first_name'] = $row['first_name'];
$admin_list[$i]['last_name'] = $row['last_name'];
$i++;
}

$smarty -> assign('add_success', $add_success);
$smarty -> assign('admin_list', $admin_list);
$smarty -> assign('error', $error);
$smarty -> assign('errormsg', $errormsg);
?>