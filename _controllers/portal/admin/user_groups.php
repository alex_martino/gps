<?php

if ($_GET['edit']) {
$is_editing = 1;
}

if ($_GET['update_groups']) {
$ids['analyst'] = explode(",",substr($_GET['analyst_ids'],10));
$ids['member'] = explode(",",substr($_GET['member_ids'],10));
$ids['alumni'] = explode(",",substr($_GET['alumni_ids'],10));

foreach ($ids['analyst'] as $key => $value) {
if (is_numeric($value)) {
$v_id['analyst'][] = $value;
}
}

foreach ($ids['member'] as $key => $value) {
if (is_numeric($value)) {
$v_id['member'][] = $value;
}
}

foreach ($ids['alumni'] as $key => $value) {
if (is_numeric($value)) {
$v_id['alumni'][] = $value;
}
}

$analyst_ids = implode($v_id['analyst'],",");
$member_ids = implode($v_id['member'],",");
$alumni_ids = implode($v_id['alumni'],",");

$sql = "SELECT id FROM members WHERE (analyst = 1 OR member = 1 OR alumni = 1) AND disabled = 0";
$result = mysql_query($sql) or die(mysql_error());
$num_users = mysql_num_rows($result);
$num_ids_counted = count($v_id['analyst']) + count($v_id['member']) + count($v_id['alumni']);

if ($num_users == $num_ids_counted) { //proceed to manipulate

$sql = "UPDATE members SET analyst=1,member=0,alumni=0 WHERE id IN('".$analyst_ids."')";
$result = mysql_query($sql) or die(mysql_error());
$sql = "UPDATE members SET analyst=0,member=1,alumni=0 WHERE id IN('".$member_ids."')";
$result = mysql_query($sql) or die(mysql_error());
$sql = "UPDATE members SET analyst=0,member=0,alumni=1 WHERE id IN('".$alumni_ids."')";
$result = mysql_query($sql) or die(mysql_error());

$sql = "INSERT INTO logs (type, uid, time, ip_address) VALUES ('admin_groups_edit','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql) or die("There was a problem logging the group change. Please contact an admin.");

}

else {
$error++;
$errormsg .= "There was an error attempting to process the code - no changes were made to the database. Please contact an admin.";
}

}


//get list of analysts
$sql = "SELECT id, first_name, last_name, analyst, member, alumni FROM members WHERE disabled = 0 ORDER BY first_name ASC";
$result = mysql_query($sql) or die(mysql_error());
while($row=mysql_fetch_array($result)) {
$allgroups[] = $row;
}

foreach ($allgroups as $key => $value) {
if ($allgroups[$key]['analyst']) {
$group['analysts'][] = $value;
}
if ($allgroups[$key]['member']) {
$group['members'][] = $value;
}
if ($allgroups[$key]['alumni']) {
$group['alumni'][] = $value;
}
}


$smarty->assign('group', $group);
$smarty->assign('is_editing', $is_editing);

$smarty->assign('error', $error);
$smarty->assign('error_msg', $error_msg);
?>