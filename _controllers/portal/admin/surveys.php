<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

$d_id = $_GET['download'];

//run checks that user is admin or PM
if ($logged_in && (($_SESSION['user_is_um'] && $_SESSION['user_gps_position_held'] == "Portfolio Manager") || ($_SESSION['user_is_admin']))) { $user_can_download_survey = 1; }

if (is_numeric($d_id) && $user_can_download_survey == 1) {
$sql = "SELECT * FROM vote_questions WHERE id = '$d_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
if (mysql_num_rows($result) > 0) {

date_default_timezone_set('America/New_York');

/** PHPExcel */
require_once ($_SERVER['DOCUMENT_ROOT'] . '/includes/classes/PHPExcel.php');

// Define User
$user = $_SESSION['user_first_name'] . " " . $_SESSION['user_last_name'];

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Load Template
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($_SERVER['DOCUMENT_ROOT']."/includes/db_excel_surveys.xlsx");

// Set properties
$objPHPExcel->getProperties()->setCreator($user)
							 ->setLastModifiedBy($user)
							 ->setTitle("GPS Survey Results")
							 ->setSubject("GPS Survey Results Download")
							 ->setDescription("Download of GPS Portal Survey")
							 ->setKeywords("gps")
							 ->setCategory("GPS Surveys");

// Cover Sheet

// Add generation info
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D8', $user);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D9', date("jS F Y"));
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D10', date("g:i A"));
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D11', date("e"));

// Add criteria
$row_n = 15;

$sql = "SELECT * FROM vote_options WHERE qid = '$d_id'";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$row_n, $row['id']);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$row_n, $row['name']);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$row_n, $row['total']);
$row_n++;
}

// Format sheet
$objPHPExcel->getActiveSheet()->getStyle('A1:AZ80')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:AZ80')->getFill()->getStartColor()->setARGB('FFFFFFFF');
$objPHPExcel->getActiveSheet()->getStyle('B7')->getFill()->getStartColor()->setARGB('FF000080');
$objPHPExcel->getActiveSheet()->getStyle('B7')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);

$field_list = array("userid","first name","last name","response id","response", "date time");

//Add fields
$objPHPExcel->setActiveSheetIndex(1)->setCellValue('A1', 'id');
$col = 'A';
foreach ($field_list as $value) {
$objPHPExcel->setActiveSheetIndex(1)->setCellValue(++$col.'1', $value);
}


//Add data
$sql = "SELECT vote_responses.id, vote_responses.uid, members.first_name, members.last_name, vote_responses.response, vote_options.name, vote_responses.updated FROM vote_responses INNER JOIN members on vote_responses.uid = members.id INNER JOIN vote_options on vote_responses.response = vote_options.id WHERE vote_responses.qid = '$d_id'";
$result = mysql_query($sql) or die(mysql_error());
$i = 2; $num_fields = count($field_list);
while ($row = mysql_fetch_array($result)) {

$j = 0; $col = "A";

while ($j <= $num_fields) {

//remove html
$row[$j] = strip_tags($row[$j]);
$row[$j] = str_replace("&nbsp;","",$row[$j]);

$cell1 = $col.$i;
$objPHPExcel->setActiveSheetIndex(1)->setCellValue($cell1, $row[$j]);
$j++; ++$col;
}

$i++;

}


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Set file name
$filename = "GPS Survey Results (ID ".$d_id.") (" . date("Y-m-d") . ")";

// Log download permanently
$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('survey_download','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".$d_id."','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql) or die("There was a problem logging the download. Please contact an admin.");

// Redirect output to a client's web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;


} //end check row found
} //end if is numeric download id

$sql = "SELECT * FROM vote_questions ORDER BY deadline DESC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {

//get total
$qid = $row['id'];
$sql = "SELECT sum(total) as total FROM vote_options WHERE qid = '$qid'";
$result2 = mysql_query($sql) or die(mysql_error());
while ($row2 = mysql_fetch_array($result2)) {
$row['total'] = $row2['total'];
}

$uid = $row['uid'];
$sql = "SELECT first_name, last_name FROM members WHERE id = '$uid' LIMIT 1";
$result3 = mysql_query($sql) or die(mysql_error());
while ($row3 = mysql_fetch_array($result3)) {
$row['user'] = $row3['first_name']." ".$row3['last_name'];
}
$surveys[] = $row;
}

$smarty->assign('surveys', $surveys);
?>