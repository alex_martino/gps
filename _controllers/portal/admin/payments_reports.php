<?php

//The group
$sql = 'SELECT IF(p.annual_dues_flag =1,CONCAT(  "Annual dues: ", p.description ),p.description) AS Item FROM payments as p LEFT JOIN members m ON m.id = p.user_id WHERE p.description <> "Test" GROUP BY Item ORDER BY p.created DESC';
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
	$due_cat[] = $row[0];
}

if ($_GET['download'] == 1) { //download report

//Get the submitted filters
  $s_due_cat = $_GET['due_cat'];
  $s_pod = $_GET['uni'];
  $s_who = $_GET['who'];

  //now validate
  if (!in_array($s_due_cat,$due_cat) && ($s_due_cat != 'all')) { die("Fatal error 1."); }
  if (!is_numeric($s_pod) && ($s_pod != "all")) { die("Fatal error 2."); }
  if (!in_array($s_who,array("all","analyst","member","alumni"))) { die("Fatal error 3"); }
  
  //build the query
  $sql = 'SELECT p.id, p.user_id, p.annual_dues_flag, p.description, p.amount, p.status, p.created, p.updated, m.first_name, m.last_name, CONCAT(m.first_name, " ", m.last_name) name,';
  $sql .= ' m.graduation_year, u.SNAME university FROM payments p LEFT JOIN members m ON p.user_id = m.id LEFT JOIN universities u ON m.ug_school_id = u.ID WHERE p.description <> "Test"';
  if ($s_due_cat != "all") { $sql .= ' AND IF(p.annual_dues_flag,CONCAT(  "Annual dues: ", p.description ),p.description) = "'.$s_due_cat.'"'; }
  if ($s_who != 'all') { $sql .= ' AND m.'.$s_who.' = 1'; }
  if (is_numeric($s_pod)) { $sql .= ' AND u.ID = "'.$s_pod.'"'; }
  $sql .= " ORDER BY name ASC";
  
  $result = mysql_query($sql) or die(mysql_error());
  while ($row = mysql_fetch_array($result)) {
	$payment_record[] = $row;
  }
  
  //get the totals also
  $sql = 'SELECT p.status, sum(p.amount) as total FROM payments p LEFT JOIN members m ON p.user_id = m.id LEFT JOIN universities u ON m.ug_school_id = u.ID WHERE p.description <> "Test"';
  if ($s_due_cat != "all") { $sql .= ' AND IF(p.annual_dues_flag,CONCAT(  "Annual dues: ", p.description ),p.description) = "'.$s_due_cat.'"'; }
  if ($s_who != 'all') { $sql .= ' AND m.'.$s_who.' = 1'; }
  if (is_numeric($s_pod)) { $sql .= ' AND u.ID = "'.$s_pod.'"'; }
  $sql .= ' GROUP BY(p.status)';
  $result = mysql_query($sql) or die(mysql_error());
  while ($row = mysql_fetch_array($result)) {
	$total[$row['status']] = $row['total'];
  }
  
  //get uni name
  if (is_numeric($s_pod)) { $sql = "SELECT SNAME FROM universities WHERE ID = '".$s_pod."' LIMIT 1"; $result = mysql_query($sql) or die(mysql_error()); $row = mysql_fetch_row($result); $s_pod_name = $row[0]; }
  if (!isset($s_pod_name)) { $s_pod_name = "All"; }
  
  //prepare text for cover page
  $write['pods'] = $s_pod_name;
  $write['subset'] = ucwords($s_who); if ($write['subset'] == "Analyst" || $write['subset'] == "Member") { $write['subset'] = $write['subset']."s"; }
  
  setlocale(LC_MONETARY, 'en_US');
  $write['overdue'] = number_format(max($total['overdue'],"0"),0);
  $write['paid'] = number_format(max($total['paid'],"0"),0);
  $write['unpaid'] = number_format(max($total['unpaid'],"0"),0);
  
  
require_once($_SERVER['DOCUMENT_ROOT']."/includes/tcpdf/config/lang/eng.php");
require_once($_SERVER['DOCUMENT_ROOT']."/includes/tcpdf/tcpdf.php");

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('GPS Portal');
$pdf->SetTitle('GPS Payments Report');
$pdf->SetSubject('Payment of dues');
$pdf->SetKeywords('GPS, Portal, Payments, Invoices');

// set custom header
$pdf->SetHeaderData("logo.gif", PDF_HEADER_LOGO_WIDTH, 'GPS Payments Report', 'Generated: '.date("d/m/y H:i:s"));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// Set some content to print
$html = '
<h1>Summary</h1>
<p>Pods: '.$write['pods'].'<BR>Subset: '.$write['subset'].'</p>
<p>Paid: $'.$write['paid'].'<BR>Unpaid: $'.$write['unpaid'].'<BR>Overdue: $'.$write['overdue'].'</p>
';

$html .= '<p><h2>Paid</h2><table><tr><th><b>Name</b></th><th><b>Amount</b></th></tr>';
foreach ($payment_record as $value) {
	if ($value['status'] == "paid") { $is_paid = 1;
		$html .= "<tr><td>".$value['name']."</td><td>$".number_format($value['amount'],0)."</td></tr>";
	}
}
	if (!$is_paid) { $html .= "No paid invoices"; }
$html .= '</table></p>';

$html .= '<p><h2>Unpaid</h2><table><tr><th><b>Name</b></th><th><b>Amount</b></th></tr>';
foreach ($payment_record as $value) {
	if ($value['status'] == "unpaid") { $is_unpaid = 1;
		$html .= "<tr><td>".$value['name']."</td><td>$".number_format($value['amount'],0)."</td></tr>";
	}
}
	if (!$is_unpaid) { $html .= "No unpaid invoices"; }
$html .= '</table></p>';

$html .= '<p><h2>Overdue</h2><table><tr><th><b>Name</b></th><th><b>Amount</b></th></tr>';
foreach ($payment_record as $value) {
	if ($value['status'] == "overdue") { $is_overdue = 1;
		$html .= "<tr><td>".$value['name']."</td><td>$".number_format($value['amount'],0)."</td></tr>";
	}
}
	if (!$is_unpaid) { $html .= "No overdue invoices"; }
$html .= '</table></p>';

// Print text using writeHTMLCell()
$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('GPS Payment Report.pdf', 'D');

die();
}

$universities = array();
$sql = "SELECT * FROM universities ORDER BY SNAME ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
	$universities[] = $row;
}

$smarty->assign('universities',$universities);
$smarty->assign('due_cat',$due_cat);

?>