<?php

if ($_GET['ir']) {
$str = explode("_",$_POST['quickresume']);
if(is_numeric($str[0])) { header("Location:/portal/toolbox/resume_upload.php?download=1&id=".$str[0]); }
}

if ($_GET['retry']) {

$ids = $_SESSION['resume_ids'];
$ids=substr($ids,1);
$uids = explode("_",$ids);
$removeid=$_GET['removeid'];
if (!is_numeric($removeid)) { $die(); }

foreach ($uids as $key => $value) {
if (is_numeric($value) && ($value != $removeid)) {
$clean_uid[] = $value;
}
}

//update members table for user with bad resume
$sql = "UPDATE members SET resume_problem = '1' WHERE id = '$removeid' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());

$retry_ids = implode("_", $clean_uid);
header("Location:resume_booklets.php?make_booklet=1&ids=".$retry_ids);

}

if ($_GET['make_booklet']) {

$uids = explode("_",$_GET['ids']);

$_SESSION['resume_ids'] = "";

foreach ($uids as $key => $value) {
if (is_numeric($value)) {
$clean_uid[] = $value;
$_SESSION['resume_ids'] .= "_".$value;
}
}

//This function is a quick check pdf is valid
define('PDF_MAGIC', "\x25\x50\x44\x46\x2D");
function is_pdf($filename) {
    return (file_get_contents($filename, false, null, 0, strlen(PDF_MAGIC)) === PDF_MAGIC) ? true : false;
}

// just require TCPDF instead of FPDF
//require_once($_SERVER['DOCUMENT_ROOT'].'/includes/tcpdf/tcpdf.php');
//currently use fpdp - below

require_once($_SERVER['DOCUMENT_ROOT'].'/includes/fpdf/fpdf.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/includes/fpdi/fpdi.php');

class concat_pdf extends FPDI {

    var $files = array();

    function setFiles($files) {
        $this->files = $files;
    }

    function concat() {
        foreach($this->files AS $file) {
	if (!is_pdf($_SERVER['DOCUMENT_ROOT']."/portal/resumes/".$file.".pdf")) {
	$id = $file;
	header("Location:resume_booklets.php?retry=1&removeid=".$id);
	}
            $pagecount = $this->setSourceFile($_SERVER['DOCUMENT_ROOT']."/portal/resumes/".$file.".pdf");
            for ($i = 1; $i <= $pagecount; $i++) {
                 $tplidx = $this->ImportPage($i);
                 $s = $this->getTemplatesize($tplidx);
                 $this->AddPage('P', array($s['w'], $s['h']));
                 $this->useTemplate($tplidx);
            }
        }
    }

}

$pdf =& new concat_pdf();
$pdf->setFiles($clean_uid);

//add cover page
$pdf->AddPage();
$pdf->setSourceFile($_SERVER['DOCUMENT_ROOT'].'/portal/resumes/cover_page.pdf');
$tplIdx = $pdf->importPage(1);
$pdf->useTemplate($tplIdx);

//get inside pages
$pdf->concat();

$pdf->Output('GPS Resume Book ('.date("jS M Y").').pdf', 'D');


die();
}

elseif ($_GET['rs']) {

if(isset($_GET['f_employer'])) { foreach($_GET['f_employer'] as $key=>$value) { $criteria[] = 'current_employer:'.$value; } }
if(isset($_GET['f_expertise'])) { foreach($_GET['f_expertise'] as $key=>$value) { $criteria[] = 'tag:expertise:'.$value; } }
if(isset($_GET['f_gender'])) { foreach($_GET['f_gender'] as $key=>$value) { $criteria[] = 'gender:'.$value; } }
if(isset($_GET['f_gps_stage'])) { foreach($_GET['f_gps_stage'] as $key=>$value) { $criteria[] = 'stage:'.$value; } }
if(isset($_GET['f_graduation_year'])) { foreach($_GET['f_graduation_year'] as $key=>$value) { $criteria[] = 'graduation_year:'.$value; } }
if(isset($_GET['f_hobby'])) { foreach($_GET['f_hobby'] as $key=>$value) { $criteria[] = 'tag:hobby:'.$value; } }
if(isset($_GET['f_hometown'])) { foreach($_GET['f_hometown'] as $key=>$value) { $criteria[] = 'tag:hometown:'.$value; } }
if(isset($_GET['f_industry'])) { foreach($_GET['f_industry'] as $key=>$value) { $criteria[] = 'current_industry:'.$value; } }
if(isset($_GET['f_sector'])) { foreach($_GET['f_sector'] as $key=>$value) { $criteria[] = 'tag:sector:'.$value; } }
if(isset($_GET['f_undergraduate_school'])) { foreach($_GET['f_undergraduate_school'] as $key=>$value) { $criteria[] = 'undergraduate_school:'.$value; } }

if((count($criteria) < 1)) { header("resume_booklets.php"); }

$sql = "SELECT id FROM members WHERE disabled = 0 AND resume_uploaded = 1 AND resume_problem = 0";
$sql2_default = "SELECT uid FROM tags WHERE 1 = 1";
$sql2 = $sql2_default;

//set root statements
$sql_t['current_employer'] = "1 = 0";
$sql_t['current_industry'] = "1 = 0";
$sql_t['gender'] = "1 = 0";
$sql_t['gps_stage'] = "1 = 0";
$sql_t['graduation_year'] = "1 = 1";
$sql_t['undergraduate_school'] = "1 = 0";

//set root statements for tags
$sql_tag['expertise'] = "1 = 0"; $sql_tag_count['expertise'] = 0;
$sql_tag['hobby'] = "1 = 0"; $sql_tag_count['hobby'] = 0;
$sql_tag['hometown'] = "1 = 0"; $sql_tag_count['hometown'] = 0;
$sql_tag['sector'] = "1 = 0"; $sql_tag_count['sector'] = 0;

foreach ($criteria as $key => $value) {
//echo ($key.": ".$value);
$value = mysql_real_escape_string($value);

//add employer criteria
if (substr($value,0,17) == 'current_employer:') { $sql_t['current_employer'] .= " OR current_employer = '" . substr($value,17) . "'"; }

//add expertise criteria
elseif (substr($value,0,13) == "tag:expertise") { $sql_tag['expertise'] .= " OR (ttype = 'expertise' AND tag = '" . str_replace('_',', ',substr($value,14)) . "')"; }

//add gender criteria
elseif ($value == 'gender:male') { $sql_t['gender'] .= " OR gender_male = '1'"; }
elseif ($value == 'gender:female') { $sql_t['gender'] .= " OR gender_male = '0'"; }

//add gps stage criteria
elseif ($value == 'stage:analyst') { $sql_t['gps_stage'] .= " OR analyst = '1'"; }
elseif ($value == 'stage:member') { $sql_t['gps_stage'] .= " OR member = '1'"; }
elseif ($value == 'stage:alumni') { $sql_t['gps_stage'] .= " OR alumni = '1'"; }

//add graduation year criteria
elseif (substr($value,0,16) == 'graduation_year:') { $sql_t['graduation_year'] .= " AND graduation_year " . substr($value,21) . " '" . substr($value,16,4) ."'"; }

//add hobby criteria
elseif (substr($value,0,9) == "tag:hobby") { $sql_tag['hobby'] .= " OR (ttype = 'hobby' AND tag = '" . str_replace('_',', ',substr($value,10)) . "')"; }

//add hometown criteria
elseif (substr($value,0,12) == "tag:hometown") { $sql_tag['hometown'] .= " OR (ttype = 'hometown' AND tag = '" . str_replace('_',', ',substr($value,13)) . "')"; }

//add industry criteria
elseif (substr($value,0,17) == 'current_industry:') { $sql_t['current_industry'] .= " OR current_industry = '" . substr($value,17) . "'"; }

//add sector criteria
elseif (substr($value,0,10) == "tag:sector") { $sql_tag['sector'] .= " OR (ttype = 'sector' AND tag = '" . str_replace('_',', ',substr($value,11)) . "')"; }

//add undergraduate school criteria
elseif (is_numeric(substr($value,21))) {
if (substr($value,0,21) == 'undergraduate_school:') { $sql_t['undergraduate_school'] .= " OR ug_school_id = '" . substr($value,21) . "'"; }
}

}

//build statements
foreach ($sql_t AS $value) {
if ($value != "1 = 0" && $value != "1 = 1") { $sql .=  " AND (" . $value .")"; }
}

foreach ($sql_tag AS $key => $value) {
if ($sql_tag_count[$key] < 2) {
if ($value != "1 = 0" && $value != "1 = 1") { $sql2 .=  " AND uid IN (SELECT uid FROM tags WHERE (" . $value ."))"; $sql_tag_count[$key]++; }
}
else {
if ($value != "1 = 0" && $value != "1 = 1") { $sql2 .=  " AND (" . $value .")"; $sql_tag_count[$key]++; }
}
}
$sql2 .= " GROUP BY uid";

//SELECT uid FROM tags WHERE 1=1 AND (1 = 0 OR (ttype = 'hometown' AND tag = 'Wayland, MA') OR (ttype = 'hometown' AND tag = 'London, UK')) AND uid IN (SELECT uid FROM tags WHERE 1=1 AND (1 = 0 OR (ttype = 'sector' AND tag = 'Technology')))

//get results

if ($sql2 == $sql2_default . " GROUP BY uid") { $sql3 = $sql; }
else { $sql3 = $sql . " AND id IN(". $sql2 .")"; }

$sql3 = str_replace("1 = 0 OR","",$sql3);
$sql3 = str_replace("1 = 1 AND","",$sql3);

$sql3 .= "  ORDER BY last_name ASC";

//die($sql3); //uncomment to debug

$bad_resumes = str_replace("resume_problem = 0","resume_problem = 1",$sql3);
$bad_resumes_go = mysql_query($bad_resumes) or die(mysql_error());
$count['bad'] = mysql_num_rows($bad_resumes_go);
$missing_resumes = str_replace("resume_uploaded = 1","resume_uploaded = 0",$sql3);
$missing_resumes_go = mysql_query($missing_resumes) or die(mysql_error());
$count['missing'] = mysql_num_rows($missing_resumes_go);


$time_start = microtime(true);
$result = mysql_query($sql3) or die(mysql_error());
$time_end = microtime(true);
$query_time = $time_end - $time_start;

$num_rows = mysql_num_rows($result);
$num_rows_adj = $num_rows + 1;

if($num_rows < 1) { die("No resumes found - remove some criteria."); }
else { $resultstxt = mysql_num_rows($result)." resumes found"; }

//create array of result ids
while ($row = mysql_fetch_array($result)) {
$r_ids[] = $row[0];
}

echo "<div style='float:right'>Query time: " . number_format($query_time,7) . " seconds.</div>";

echo $resultstxt;
echo " | " . $count['missing'] . " resumes missing | " . $count['bad'] . " unreadable";

echo '<BR><BR><a href="#" onClick="generate_booklet(\''.implode("_",$r_ids).'\'); return false;">Generate Booklet</a>';


//log search run

$sql99 = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('resume_search','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".addslashes($sql3)."','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql99) or die("There was a problem logging the search. Please contact an admin.");

die();

}

else {


//Check for entries
$sql = "SELECT id, first_name, last_name, graduation_year, current_employer, ug_school_id FROM members WHERE hidden = 0 AND disabled = 0 ORDER BY graduation_year DESC, first_name ASC";
$result = mysql_query($sql) or die(mysql_error());

//generate classes
$sql = "SELECT graduation_year FROM members WHERE hidden = 0 AND disabled = 0";
$result = mysql_query($sql) or die(mysql_error());
$i = 0;
while ($row = mysql_fetch_array($result)) {
$members_array[$i]['graduation_year'] = $row[0];
$i++;
}
//retrieve years
foreach ($members_array as $temp) {
	if ($temp['graduation_year'] != '') {
		$years[$temp['graduation_year']][] = $temp;
	}
}

krsort($years);

//generate universities
$sql = "SELECT ID,SNAME FROM universities ORDER BY SNAME ASC";
$result = mysql_query($sql) or die(mysql_error());
$i=0;
while ($row = mysql_fetch_array($result)) {
$uni[$i]['id'] = $row[0];
$uni[$i]['sname'] = $row[1];
$i++;
}

$smarty->assign('uni', $uni);

//generate sector
$sql = "SELECT tag FROM tags WHERE ttype = 'sector' GROUP BY tag ORDER BY tag ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$sectors[] = $row[0];

$smarty->assign('sectors', $sectors);

//fetch employers
$sql = "SELECT employer FROM jobs WHERE current = 1 GROUP BY employer ORDER BY employer ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$employers[] = $row[0];

//fetch expertise
$sql = "SELECT tag FROM tags WHERE ttype = 'expertise' GROUP BY tag ORDER by tag ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$tags['expertise'][] = $row[0];

//fetch hobbies
$sql = "SELECT tag FROM tags WHERE ttype = 'hobby' GROUP BY tag ORDER by tag ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$tags['hobbies'][] = $row[0];

//fetch hometowns
$sql = "SELECT tag FROM tags WHERE ttype = 'hometown' GROUP BY tag ORDER by tag ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$tags['hometowns'][] = $row[0];

//fetch industries
$sql = "SELECT jobs_industries.name as name FROM jobs JOIN jobs_industries ON jobs.i_id = jobs_industries.id WHERE jobs.i_id != '0' AND jobs.current = '1' GROUP BY name ORDER by name ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$industries[] = $row['name'];

//fetch sectors
$sql = "SELECT tag FROM tags WHERE ttype = 'sector' GROUP BY tag ORDER by tag ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$tags['sectors'][] = $row[0];

$smarty->assign('tags', $tags);

//fetch years
$sql = "SELECT graduation_year FROM members WHERE hidden != 1 AND disabled != 1 GROUP BY graduation_year ORDER by graduation_year ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
if (is_numeric($row[0])) { $year[] = $row[0]; }

$smarty->assign('year', $year);

}

$smarty->assign('search_title', $search_title);
$smarty->assign('search_type', $search_type);
$smarty->assign('sortby', $sortby);

$smarty->assign('ttype', $ttype);
$smarty->assign('search_tag', $search_tag);

$smarty->assign('years', $years);
$smarty->assign('unis', $unis);
$smarty->assign('employers', $employers);
$smarty->assign('industries', $industries);

$smarty->assign('members_array', $members_array);

$smarty->assign('no_results', $no_results);
?>