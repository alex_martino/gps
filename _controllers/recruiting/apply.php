<?php
session_start();
require_once($_SERVER['DOCUMENT_ROOT'] . "/config.php");
$error = 0;
if ($_POST['submitapp1'] == '1') {

//Retrieve post variables
$s_fname = $_POST['fname'];
$s_sname = $_POST['sname'];
$s_email = $_POST['email'];
$s_number = $_POST['number'];
$s_uni = $_POST['university'];

//Basic security tests
$valid_fname = preg_match('/^[a-zA-Z\-]+$/', $s_fname);
if ($valid_fname == 0) { $error++; $errormsg .= "Invalid characters entered in first name.<BR>"; }
$valid_sname = preg_match('/^[a-zA-Z\s\-]+$/', $s_sname);
if ($valid_sname == 0) { $error++; $errormsg .= "Invalid characters entered in surname.<BR>"; }
include("email_validate.php");
if (!check_email_address($s_email)) { $error++; $errormsg .= "Invalid email entered.<BR>"; }
if(strlen($s_number) < 4) { $error++; $errormsg .= "Please enter a contact number.<BR>"; } else {
$valid_number = preg_match('/^[0-9\s]+$/', $s_number);
if ($valid_number == 0) { $error++; $errormsg .= "Please only enter numbers in the phone number field.<BR>"; }
}
if ($s_uni == 0) { $error++; $errormsg .= "Please select a university. Only universities which are currently accepting
applications are listed."; }
if (!is_numeric($s_uni)) { die("Fatal error."); }
else {
$sql = "SELECT * FROM cycles WHERE UNI_ID = '".$s_uni."' ORDER BY DEADLINE DESC LIMIT 1";
$result=mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$s_cycle = $row['ID'];
}
}

$_SESSION['fname'] = $s_fname;
$_SESSION['sname'] = $s_sname;
$_SESSION['email'] = $s_email;
$_SESSION['number'] = $s_number;
$_SESSION['uni'] = $s_uni;
$_SESSION['cycle'] = $s_cycle;

$expiry = time()+60*60*24*30;
setcookie('fname', $s_fname, $expiry);
setcookie('sname', $s_sname, $expiry);
setcookie('email', $s_email, $expiry);
setcookie('number', $s_number, $expiry);
setcookie('uni', $s_uni, $expiry);
setcookie('cycle', $s_cycle, $expiry);

if ($error == 0) {
session_write_close();
header('Location: apply2.php');
}
}

if (empty($_SESSION['fname'])) { $_SESSION['fname'] = $_COOKIE['fname']; }
if (empty($_SESSION['sname'])) { $_SESSION['sname'] = $_COOKIE['sname']; }
if (empty($_SESSION['email'])) { $_SESSION['email'] = $_COOKIE['email']; }
if (empty($_SESSION['number'])) { $_SESSION['number'] = $_COOKIE['number']; }
if (empty($_SESSION['uni'])) { $_SESSION['uni'] = $_COOKIE['uni']; }
if (empty($_SESSION['cycle'])) { $_SESSION['cycle'] = $_COOKIE['cycle']; }

$current_time = date("Y-m-d H:i:s");
$sql = "SELECT * FROM cycles WHERE DEADLINE > '".$current_time."' LIMIT 1";
$result=mysql_query($sql) or die(mysql_error());
$num_rows = mysql_num_rows($result);
unset($cycles);
if ($num_rows < 1) { //no open cycles
$cycles = 'closed';
}

if (isset($_REQUEST['uni'])) {

if (isset($_GET['uni'])) { $uni_s = $_GET['uni']; }

else {
$uni_s = $_REQUEST['uni'];
}

if (!is_numeric($uni_s)) { die ("Error."); }

$sql = "SELECT * FROM cycles WHERE UNI_ID = '".$uni_s."' ORDER BY DEADLINE DESC LIMIT 1";
$result=mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {

//format deadline
function convert_datetime($str) {
list($date, $time) = explode(' ', $str);
list($year, $month, $day) = explode('-', $date);
list($hour, $minute, $second) = explode(':', $time);
$timestamp = mktime($hour, $minute, $second, $month, $day, $year);
return $timestamp;
}
$timestamp = convert_datetime($row['DEADLINE']);
$deadline_f = date("jS F Y g:ia", $timestamp);

}

}


$f = 0;

$current_time = date("Y-m-d H:i:s");
$sql = "SELECT * FROM cycles WHERE DEADLINE > '".$current_time."' ORDER BY DEADLINE ASC";
$result=mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
//Convert UniID to name
$UNI_ID = $row['UNI_ID'];
$sql2 = "SELECT ID,FNAME FROM universities WHERE ID = ".$UNI_ID." LIMIT 1";
$result2=mysql_query($sql2) or die(mysql_error());
$row2 = mysql_fetch_array($result2);
$values[$f] = $UNI_ID;
$options[$f] = $row2['FNAME'];
$f++;
}

//assign variables
$smarty->assign('cycles', $cycles);
$smarty->assign('deadline_f', $deadline_f);

// assign options data
$smarty->assign('options',$options);
$smarty->assign('values',$values);
$smarty->assign('uni_s',$uni_s);

//assign error data
$smarty->assign('error',$error);
$smarty->assign('errormsg',$errormsg);
?>