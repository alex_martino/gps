<?php
class db_class{
	var $table_name;
    function insert_records($records,$nsd_fields='',$common=''){
    	global $link;
    	$insert_ids = array();
		foreach($records as $record){
          	$sql = "INSERT INTO ".TABLE_PREFIX.$this->table_name;
			$fields = $values = '';
			$field_array = $this->getFields();
			foreach($record as $key => $value){
				#if(is_array($value)) $value = $value[$i];
				if(!$value){ 
					if(is_array($common) && array_key_exists($key,$common)){
						$value = $common[$key];
					}				
				}				
				if(in_array($key,$field_array)){
					$fields .= $key.',';
					if(is_array($nsd_fields) && in_array($key,$nsd_fields))
						$values .= "$value,";
					else
						$values .= "'$value',";
				} 
			}
			$fields = rtrim($fields,',');
			$values = rtrim($values,',');
			$sql = "$sql($fields)VALUES($values)";
			execute($sql,$err);
			if($err) return $err ;
			$insert_ids[] = mysql_insert_id();			
		}
		$this->insert_ids = $insert_ids;
		return true;
    }	

	function update_records($records,$where,$nsd_fields='',$common=''){
		foreach($records as $record){
			$sql = "UPDATE ".TABLE_PREFIX.$this->table_name;
			$sets = '';
			$field_array = $this->getFields();
			foreach($record as $key => $value){
				#if(is_array($value)) $value = $value[$i];
				if(!$value){ 
					if(is_array($common) && array_key_exists($key,$common)){
						$value = $common[$key];
					}				
				}
				if(in_array($key,$field_array)){
					if(is_array($nsd_fields) && in_array($key,$nsd_fields))
						$sets .= " $key = $value,";
					else
						$sets .= " $key = '$value',";
				}
			}
			$sets= rtrim($sets,',');
			$wheres = '';
			foreach($where as $wher){
				$value = $record[$wher];
				if(!$value){ 
					if(is_array($common) && array_key_exists($key,$common)){
						$value = $common[$key];
					}				
				}
				if(in_array($wher,$field_array)){
					if(is_array($nsd_fields) && in_array($wher,$nsd_fields))
						$wheres .= " $wher = $value AND";
					else
						$wheres .= " $wher = '$value' AND ";
				}					
				#if(is_array($value)) $value = $value[$i];				
			}
			$wheres = rtrim($wheres,' AND ');
			$sql = "$sql SET $sets WHERE $wheres";
			execute($sql,$err);
			if($err) return $err ;
		}
		return true;
	} 
	
	function get_records($wfields="",$sfields='',$order='',$start=0,$count=0){
			$wheres = '';
			foreach($wfields as $key => $val){
					$where = '';
					if(is_array($val)){
							$where = " $key IN (".implode("','",$val).') ';
					}else{
							$where = " $key = '$val' ";
					}
					$wheres .= $where.' AND ';
			}
			$wheres = rtrim($wheres,' AND ');
			if($sfields)
					$fields = implode(',',$sfields);
			else
					$fields = '*';
			if($order){
					$order_set = ' ORDER BY ';
					foreach($order as $key => $v){
							$order_set .= $v['field'].' '.$v['type'].',';
					}
					$order_set = rtrim($order_set,',');
			}
			if($count){
					$limit = " LIMIT $start,$count ";
			}
			$sql = "SELECT  $fields FROM ".TABLE_PREFIX."{$this->table_name} 
					WHERE $wheres $order_set $limit";
			$result = getrows($sql,$err);
			if($err) return $err;
			$this->result = $result;
			return true;
	}

	//New functionality - search

	function search($wfields="",$sfields='',$order='',$start=0,$count=0){
			$wheres = '';
			foreach($wfields as $key => $val){
					$where = '';
					if(is_array($val)){
							$where = " $key IN (".implode("','",$val).') ';
					}else{
			if(preg_match('/^v_/',$key) && $val != ''){
								$where = " $key LIKE '%$val%' ";
							}elseif($val != ''){
									$where = " $key = '$val' ";
							}else{
									$where = " 1 ";
							}
					}
					$wheres .= $where.' AND ';
			}
			$wheres = rtrim($wheres,' AND ');
			if($sfields)
					$fields = implode(',',$sfields);
			else
					$fields = '*';
			if($order){
					$order_set = ' ORDER BY ';
					foreach($order as $key => $v){
							$order_set .= $v['field'].' '.$v['type'].',';
					}
					$order_set = rtrim($order_set,',');
			}
	if($count){
		$limit = " LIMIT $start,$count ";
	}
			$sql = "SELECT  $fields FROM ".TABLE_PREFIX."{$this->table_name}  WHERE $wheres $order_set $limit";
			$result = getrows($sql,$err);
			if($err) return $err;
			$this->result = $result;
			return $result;
	}

	//New functionality - search sql

	function search_sql($wfields="",$sfields='',$order=''){
			$wheres = '';
			foreach($wfields as $key => $val){
					$where = '';
					if(is_array($val)){
							$where = " $key IN (".implode("','",$val).') ';
					}else{
							if(preg_match('/^v_/',$key) && $val != ''){
									$where = " $key LIKE '%$val%' ";
							}elseif($val != ''){
									$where = " $key = '$val' ";
							}else{
									$where = " 1 ";
			}
					}
					$wheres .= $where.' AND ';
			}
			$wheres = rtrim($wheres,' AND ');
			if($sfields)
					$fields = implode(',',$sfields);
			else
					$fields = '*';
			if($order){
					$order_set = ' ORDER BY ';
					foreach($order as $key => $v){
							$order_set .= $v['field'].' '.$v['type'].',';
					}
					$order_set = rtrim($order_set,',');
			}
			$sql = "SELECT  $fields FROM ".TABLE_PREFIX."{$this->table_name}  WHERE $wheres $order_set";
			return $sql;
	}

	
// Inserts dynamic fields and multiple records. Returns inserted ids array on success
    function insert_rows($records,$count=1,$nsd_fields=''){
    	global $link;
    	$insert_ids = array();
		for($i=0;$i<$count;$i++){
          	$sql = "INSERT INTO ".TABLE_PREFIX."{$this->table_name}";
			$fields = $values = '';
			$field_array = $this->getFields();
			foreach($records as $key => $value){
				if(is_array($value)) $value = $value[$i];
				if(in_array($key,$field_array)){
					$fields .= $key.',';
					if(is_array($nsd_fields) && in_array($key,$nsd_fields))
						$values .= "$value,";
					else
						$values .= "'$value',";
				} 
			}
			$fields = rtrim($fields,',');
			$values = rtrim($values,',');
			$sql = "$sql($fields)VALUES($values)";
			execute($sql,$err);
			if($err) return $err ;
			$insert_ids[] = mysql_insert_id();
		}
		$this->insert_ids = $insert_ids;
		return true;
    }	


// Updates dynamic fields and multiple records. Returns the no of updated records on success.
	function update_rows($records,$where,$count=1,$nsd_fields=''){
		for($i=0;$i<$count;$i++){
			$sql = "UPDATE ".TABLE_PREFIX."{$this->table_name}";
			$sets = '';
			$field_array = $this->getFields();
			foreach($records as $key => $value){
				if(is_array($value)) $value = $value[$i];
				if(in_array($key,$field_array)){
					if(is_array($nsd_fields) && in_array($key,$nsd_fields))
						$sets .= " $key = $value,";
					else
						$sets .= " $key = '$value',";
				}
			}
			$sets= rtrim($sets,',');
			$wheres = '';
			foreach($where as $wher){
				$value = $records[$wher];
				if(is_array($value)) $value = $value[$i];				
				$wheres .= " $wher = '{$value}' AND ";	
			}
			$wheres = rtrim($wheres,' AND ');
			$sql = "$sql SET $sets WHERE $wheres";
			execute($sql,$err);
			if($err) return $err ;

		}
		return true;
	}      

	function delete_set($wfields,$count=0,$nsd_fields=''){
			$wheres = '';
			if($count){
					$limit = " LIMIT $count ";
			}
			if(count($wfields)){
					foreach($wfields as $key => $val){
							$where = '';
							if(is_array($val)){
									$where = " $key IN (".implode("','",$val).') ';
							}else{
								if(is_array($nsd_fields) && in_array($key,$nsd_fields))
									$where = " $key = $val";
								else							
									$where = " $key = '$val' ";
							}
							$wheres .= $where.' AND ';
					}
					$wheres = rtrim($wheres,' AND ');
					$sql = "DELETE FROM ".TABLE_PREFIX."{$this->table_name}
							WHERE $wheres $limit";
					execute($sql,$err);
					if($err) return $err;
			}
			return true;
	}

}
