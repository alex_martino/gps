<div id="side_menu_container">
	<? if (stristr($_SERVER['PHP_SELF'], '/about/')) { ?>
	<div><a href="/about/index.php">ABOUT GPS</a></div>
	<div><a href="/about/mission_statement.php">MISSION STATEMENT</a></div>
	<div><a href="/about/ceos_message.php">CEO'S MESSAGE</a></div>
	<div><a href="/about/brochure.php">BROCHURE</a></div>
	<div><a href="/about/annual_reports.php">ANNUAL REPORTS</a></div>
	<div><a href="/about/photos.php">PHOTOS</a></div>
	<div><a href="/about/faqs.php">FAQS</a></div>
	<? } ?>
	
	<? if (stristr($_SERVER['PHP_SELF'], '/membership/')) { ?>
	<div><a href="/membership/index.php">CURRENT MEMBERS</a></div>
	<div><a href="/membership/board_of_managers.php">BOARD OF MANAGERS</a></div>
	<div><a href="/membership/alumni.php">ALUMNI</a></div>
	<div><a href="/membership/founding_members.php">FOUNDING MEMBERS</a></div>
	<div><a href="/membership/advisory_board.php">ADVISORY BOARD</a></div>
	<? } ?>
	
	<? if (stristr($_SERVER['PHP_SELF'], '/approach/')) { ?>
	<div><a href="/approach/index.php">PHILOSOPHY</a></div>
	<div><a href="/approach/objectives.php">OBJECTIVES</a></div>
	<? } ?>
	
	<? if (stristr($_SERVER['PHP_SELF'], '/recruiting/')) { ?>
	<div><a href="/recruiting/index.php">RECRUITMENT</a></div>
	<?php // <div><a href="/recruiting/video.php">VIDEO</a></div> TO ADD THIS LINK BACK REMOVE BOTH PHP BRACKETS AND DOUBLE SLASH ?>
	<div><a href="/recruiting/analyst_program.php">ANALYST PROGRAM</a></div>
<? //	<div><a href="/recruiting/apply.php">APPLY ONLINE</a></div> ?>
	<? } ?>
	
	<? if (stristr($_SERVER['PHP_SELF'], '/philanthropy/')) { ?>
	<div><a href="/philanthropy/index.php">PROGRAM SUMMARY</a></div>
	<div><a href="/philanthropy/examples.php">EXAMPLES</a></div>
	<div><a href="/philanthropy/photos.php">PHOTOS</a></div>
	<? } ?>
	
	<? if (stristr($_SERVER['PHP_SELF'], '/press/')) { ?>
	<? } ?>
	
	<? if (stristr($_SERVER['PHP_SELF'], '/contact/')) { ?>
	<div><a href="/contact/index.php">GENERAL INFO</a></div>
	<div><a href="/contact/press.php">PRESS</a></div>
	<div><a href="/contact/recruiting.php">RECRUITING</a></div>
	<? } ?>

	<? if (stristr($_SERVER['PHP_SELF'], '/members/') and $_SERVER['PHP_SELF'] != '/members/login.php' and $_SERVER['PHP_SELF'] != '/members/passreset.php') { ?>
	<div><a href="/members/index.php">MEMBER HOME</a></div>
	<div><a href="/members/member.php">MY PROFILE</a></div>
	<? } ?>
</div>