<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>GPS Weekly Digest</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
</head>
<body marginheight="0" topmargin="0" marginwidth="0" bgcolor="#c5c5c5" leftmargin="0">
<table cellspacing="0" border="0" height="100%" style="background-image: url('http://www.gps100.com/content_files/mail/bg.gif'); background-color: #fafafa;" cellpadding="0" width="100%">
    <tr>
        <td valign="top">
            <!-- main table -->
            <table cellspacing="0" border="0" align="center" cellpadding="0" width="675">
                <!-- note -->
                <tr>
                    <td valign="top">
                        <table cellspacing="0" border="0" align="center" cellpadding="0" width="500">
                            <tr>
                                <td class="note" valign="top" style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #2b2b2b; line-height: 18px;"> <br />
                                    You're receiving this newsletter because you are a member or alumni of <a href="http://www.gps100.com" style="color: #3b464f; font-weight: bold; text-decoration: none;">Global Platinum Securities</a>. Having trouble reading 
                                    this email? You can
                                    <a href="http://www.gps100.com/portal/digests/" style="color: #3b464f; font-weight: bold; text-decoration: none;">view it in your web browser</a>
                                    <br />
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- / note -->
                <!-- header -->
                <tr>
                    <td valign="top">
                        <table cellspacing="0" border="0" align="center" cellpadding="0" width="675">
                            <!-- top -->
                            <tr>
                                <td height="9" valign="top"> <img src="http://www.gps100.com/content_files/mail/header-top.gif" alt="" style="display: block;" /> </td>
                            </tr>
                            <!-- / top -->
                            <!-- middle -->
                            <tr>
                                <td valign="top">
                                    <table cellspacing="0" border="0" cellpadding="0" width="675">
                                        <tr>
                                            <td valign="top" width="5"> <img src="http://www.gps100.com/content_files/mail/header-side.gif" alt="" style="display: block;" /> </td>
                                            <td height="90" valign="top">
                                                <table cellspacing="0" border="0" height="90" cellpadding="0" width="665">
                                                    <tr>
                                                        <td class="header-content" height="90" valign="top" style="background-color: #bbcedd; background-image: url('http://www.gps100.com/content_files/mail/header-content.gif');">
                                                            <table cellspacing="0" border="0" height="90" cellpadding="0" width="665">
                                                                <tr>
                                                                    <td valign="top"> <img src="http://www.gps100.com/content_files/mail/spacer.gif" height="1" alt="" style="display: block;" width="20" /> </td>
								    <td align="left" valign="middle" width="80"><img src="http://www.gps100.com/images/globe.png"></td>
                                                                    <td class="main-title" align="left" valign="middle" width="465" style="color: #4c545b; font-family: Georgia, serif; font-size: 41px; font-weight: bold; font-style: italic;"> GPS Digest </td>
                                                                    <!-- circle -->
                                                                    <td height="81" align="right" valign="middle" width="81">
                                                                        <table cellspacing="0" border="0" height="81" cellpadding="0" width="101">
                                                                            <tr>
                                                                                <td class="circle" height="56" align="left" valign="top" width="81" style="color: #002868; border: 1px dashed #002868; text-transform: uppercase; font-size: 9px; text-align: center; font-family: Arial, Helvetica, sans-serif; width: 81px; height: 56px; padding: 25px 0 0 0; background-repeat: no-repeat;">
                                                                                    <p style="color: #002868; text-transform: uppercase; font-size: 9px; text-align: center; font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; padding: 0px; -webkit-text-size-adjust: none; clear: both;">
                                                                                        <?php echo date("F"); ?>
                                                                                    </p>
                                                                                    <strong style="font-size: 24px;">
                                                                                    <?php echo date("j"); ?>
                                                                                    </strong>
											  </td>
                                                                                <td rowspan="3"> <img src="http://www.gps100.com/content_files/mail/spacer.gif" height="1" style="display: block;" width="19" /> </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <!-- / circle -->
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td valign="top" width="5"> <img src="http://www.gps100.com/content_files/mail/header-side.gif" alt="" style="display: block;" /> </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- / middle -->
                            <!-- botom -->
                            <tr>
                                <td height="35" valign="top"> <img src="http://www.gps100.com/content_files/mail/header-bottom-left.png" alt="" style="display: block;" /> </td>
                            </tr>
                            <!-- / bottom -->
                        </table>
                    </td>
                </tr>
                <!-- / header -->
                <!-- content -->
                <tr>
                    <td valign="top">
                        <table cellspacing="0" border="0" cellpadding="0">
                            <tr>
                                <td valign="top" width="415">
                                    <table cellspacing="0" border="0" cellpadding="0" width="415">
                                        <tr>
                                            <td valign="top">
                                                <table cellspacing="0" border="0" cellpadding="0">