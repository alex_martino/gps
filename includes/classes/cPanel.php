<?php
//get credentials
include($_SERVER['DOCUMENT_ROOT']."/config_admin.php");

$user = $cpanel_username;
$password = $cpanel_password;

$query = "https://".$domain.":2083/json-api/cpanel?cpanel_jsonapi_module=StatsBar&cpanel_jsonapi_func=stat&display=diskusage&domain=".$domain;

$curl = curl_init();
curl_setopt($curl, CURLOPT_POST, 0);

curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
# Allow self-signed certs
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);
# Allow certs that do not match the hostname
curl_setopt($curl, CURLOPT_HEADER,0);
# Do not include header in output
curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
curl_setopt($curl, CURLOPT_TIMEOUT, 30);
# Return contents of transfer on curl_exec
$header[0] = "Authorization: Basic " . base64_encode($user.":".$password) . "\n\r";

curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
# set the username and password
curl_setopt($curl, CURLOPT_URL, $query);
curl_setopt($curl, CURLOPT_TIMEOUT, 30);
# execute the query
$result = curl_exec($curl);

if ($result == false) {
	error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
	# log error if curl exec fails
}
$curlinfo = curl_getinfo($curl);

$result = preg_replace("(:unlimited)", ":\"unlimited\"", $result);

$data = json_decode($result, 1);

if ($result == false) {
	error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
	# log error if curl exec fails
}
curl_close($curl);

$data = json_decode($result, 1);

$res['diskused'] = $data['cpanelresult']['data'][0]['_count'];
$res['diskquota'] = $data['cpanelresult']['data'][0]['_max'];

if($res['diskquota'] == "unlimited"){
	$res['diskleft'] = "unlimited";

}else{
	$res['diskleft'] = $res['diskquota'] - $res['diskused'];
}

//bandwidth
$query = "http://".$domain.":2082/json-api/cpanel?user=awebsite&cpanel_jsonapi_module=StatsBar&cpanel_jsonapi_func=stat&display=bandwidthusage&domain=".$domain; 

$curl = curl_init();
# Create Curl Object
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
# Allow self-signed certs
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);
# Allow certs that do not match the hostname
curl_setopt($curl, CURLOPT_HEADER,0);
# Do not include header in output
curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
curl_setopt($curl, CURLOPT_TIMEOUT, 30);
# Return contents of transfer on curl_exec
$header[0] = "Authorization: Basic " . base64_encode($user.":".$password) . "\n\r";
curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
# set the username and password
curl_setopt($curl, CURLOPT_URL, $query);
# execute the query
$result = curl_exec($curl);

if ($result == false) {
	error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
	# log error if curl exec fails
}
curl_close($curl);
$result = preg_replace("(:unlimited)", ":\"unlimited\"", $result);
$data = json_decode($result, 1);

if(!$data['cpanelresult']['event']['result']){
	echo "cpanel fail connect";
	//$this->redirect('client', 'fail/api/cPanel/id/'.$this->get->id);
}

$res['bandwidthcount'] = $data['cpanelresult']['data'][0]['_count'];
$res['bandwidthmax'] = $data['cpanelresult']['data'][0]['_max'];

//if($res['bandwidthcount'] == 0 && $res['bandwidthmax'] == 0 && $res['diskused'] == 0 && $res['diskquota'] == 0)
//    $this->redirect('client', 'fail/api/cPanel/id/'.$this->get->id);

if($res['bandwidthmax'] == "unlimited"){
	$res['bandwidthleft'] = "unlimited";

}else{
	$res['bandwidthleft'] = $res['bandwidthmax'] - $res['bandwidthcount'];
}


?>