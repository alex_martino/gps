<?
class db_connection {
	var $table;
	
	// Initialize the class
	function db_connection($table = NULL) {
		$this->table = $table;
		$this->connect();
	}
	
	// Function to connect to the database
	function connect() {
		mysql_connect(DB_HOST, DB_USERNAME, DB_PASSWORD);
		mysql_select_db(DB_NAME);
	}
	
	// Function to execute a mysql query
	function execute($query) {
		$result = mysql_query($query) or die(mysql_error() . " - QUERY: $query");
		return $result;
	}
	
	// Function for getting the columns from a given table
	function get_tables() {
		$query = "SHOW TABLES";
		$result = $this->execute($query);
		
		while ($temp = mysql_fetch_array($result)) {
			$tables[] = $temp[0];
		}
		
		return $tables;
	}
	
	// Function for getting the columns from a given table
	function get_columns() {
		$query = "SHOW COLUMNS FROM " . $this->table;
		$result = $this->execute($query);
		
		while ($temp = mysql_fetch_array($result)) {
			$columns[] = array("name" => $temp["Field"], "type" => $temp["Type"]);
		}
		
		return $columns;
	}
	
	// Function for getting the data from a submitted form
	function get_db_fields($db_fields = false) {
		if ($db_fields == false) {
			$columns = $this->get_columns();
		
			foreach ($columns as $column) {
				// If our data is an array, convert it to a delimited string
				if (is_array($_POST[$column["name"]])) {
					$_POST[$column["name"]] = implode(";", $_POST[$column["name"]]);
				}
				
				$db_fields[$column["name"]] = stripslashes($_POST[$column["name"]]);
			}
		}
	
		return $db_fields;
	}
	
	// Function to get the database info for a particular db field
	function get_field_info($table, $field) {
		$query = "SELECT * FROM _sa_database_admin WHERE table_name = '$table' AND field_name = '$field'";
		$result = $this->execute($query);
		return mysql_fetch_array($result);
	}
	
	// Function for selecting a single value from the database
	function select_single_value($where, $field) {
		$query = "SELECT * FROM " . $this->table . " WHERE $where";
		$result = $this->execute($query);
		$count = mysql_num_rows($result);
		
		if ($result && $count) {
			return mysql_result($result, 0, $field);
		}
	}
	
	// Function for selecting data from a MySQL database and returning a full arrray
	function select_array($where = false, $order_by = false, $limit = false, $array_type = MYSQL_ASSOC) {
		$query = "SELECT * FROM " . $this->table;
		if ($where) {
			$query .= " WHERE $where";
		}
		
		if ($order_by) {
			$query .= " ORDER BY $order_by";
		}
		
		if ($limit) {
			$query .= " LIMIT $limit";
		}
	
		$result = $this->execute($query);
		$count = mysql_num_rows($result);
		
		
		if ($result && $count) {
			// Return all the variables for this query in one array
			while ($temp = mysql_fetch_array($result, $array_type)) {
				$results[] = $temp;
			}
			
			// Return the results
			return $results;
		}
		
		if ($result) {
			if (!$count) {
				return "Query Failed : 0 Records Returned";
			}
		} else {
			return "Query Failed : " . mysql_error();
		}
	}
	
	// Function for selecting unique data from a MySQL database and returning a full arrray
	function select_distinct_array($distinct_field, $where = false, $order_by = false, $limit = false, $array_type = MYSQL_ASSOC) {
		$query = "SELECT DISTINCT $distinct_field FROM " . $this->table;
		if ($where) {
			$query .= " WHERE $where";
		}
		
		if ($order_by) {
			$query .= " ORDER BY $order_by";
		}
		
		if ($limit) {
			$query .= " LIMIT $limit";
		}
	
		$result = $this->execute($query);
		$count = mysql_num_rows($result);
		
		
		if ($result && $count) {
			// Return all the variables for this query in one array
			while ($temp = mysql_fetch_array($result, $array_type)) {
				$results[] = $temp;
			}
			
			// Return the results
			return $results;
		}
		
		if ($result) {
			if (!$count) {
				return "Query Failed : 0 Records Returned";
			}
		} else {
			return "Query Failed : " . mysql_error();
		}
	}
	
	// Function for inserting data in a MySQL database
	function insert($db_fields = false) {
		if ($db_fields == false) {
			$db_fields = $this->get_db_fields();
		}
		
		$query .= "INSERT INTO " . $this->table . " (";
		$i = 0;
		foreach ($db_fields as $db_field => $db_field_value) {
			if ($i > 0) {
				$query .= ",";
			}
			$query .= " $db_field";
			$i++;
		}
		
		$query .=") VALUES (";
		$i = 0;
		foreach ($db_fields as $db_field => $db_field_value) {
			if ($i > 0) {
				$query .= ",";
			}
			$query .= " '" . mysql_real_escape_string(utf8_decode(trim($db_field_value))) . "'";
			$i++;
		}
		$query .= ")";
		
		$result = $this->execute($query);
		
		if ($result) {
			$count = intval(mysql_affected_rows());
			return "Query Successful : " . $count . ($count == 1 ? " Record" : " Records") . " Inserted";
		} else {
			return "Query Failed : " . mysql_error();
		}
	}
	
	// Function for updating data in a MySQL database
	function update($field, $field_value, $and = false) {
		$db_fields = $this->get_db_fields();
		
		if ((!isset($field) or !isset($field_value)) or ($field == '' or $field_value == '')) {
			return "Query Failed : No where statement in update command - Field = $field : Field Value = $field_value ;";
		}
		
		$query .= "UPDATE " . $this->table . " SET";
		$i = 0;
		foreach ($db_fields as $db_field => $db_field_value) {

		// ******** IMPORTANT CODE NOTE - UGLY HACK RIGHT HERE ***********
		//this line is for compatibility with new profile, specify fields which are not to be touched from the profile update page
		//The if statement may interfere with code if you try and use this update function
		//At the time of the hack, the only page using it was the old edit profile, so I had no qualms about implementing this code
		//Questions to Benjamin Wigoder (bwigoder@gps100.com)
		// ******** IMPORTANT CODE NOTE ***********

		   if ($db_field != "pass_enc_sec" && $db_field != "password" && $db_field != "email_address" && $db_field != "gmail_address" && $db_field != "ug_school_id" && $db_field != "alumni_mentor_id"
&& $db_field != "member_advocate_id" && $db_field != "linkedin" && $db_field != "facebook" && $db_field != "contact_number" && $db_field != "contact_number_country_code"
&& $db_field != "contact_number_country_id" && $db_field != "current_job_id" && $db_field != "skype" && $db_field != "entrepreneurship" && $db_field != "philanthropy" && $db_field != "weight"
&& $db_field != "founding_member" && $db_field != "chapter_founder" && $db_field != "board_manager" && $db_field != "upper_management" && $db_field != "analyst" && $db_field != "member" && $db_field != "alumni"
 && $db_field != "weight"  && $db_field != "hidden" && $db_field != "disabled" && $db_field != "address" && $db_field != "address_private") {
			if ($i > 0) {
				$query .= ",";
			}
			$query .= " $db_field = '" . mysql_real_escape_string(utf8_decode(trim($db_field_value))) . "'";
			$i++;
		   }
		}
	
		if ($field && $field_value) {
			$query .= " WHERE $field = '$field_value'";
		} else {
			return "Query Failed : No where statement in update command - Field = $field : Field Value = $field_value ;";
		}
		
		if ($and) {
			$query .= "$and";
		}
		
		$result = $this->execute($query);
		
		if ($result) {
			$count = intval(mysql_affected_rows());
			return "Query Successful : " . $count . ($count == 1 ? " Record" : " Records") . " Updated";
		} else {
			return "Query Failed : " . mysql_error();
		}
	}
	
	// Function for deleting data from a MySQL database
	function delete($field, $field_value) {
		if ((!isset($field) or !isset($field_value)) or ($field == '' or $field_value == '')) {
			return "Query Failed : No where statement in delete command";
		}
		
		$query = "DELETE FROM " . $this->table . " WHERE $field = '$field_value'";
		$result = $this->execute($query);
		
		if ($result) {
			$count = intval(mysql_affected_rows());
			return "Query Successful : " . $count . ($count == 1 ? " Record" : " Records") . " Deleted";
		} else {
			return "Query Failed : " . mysql_error();
		}
	}
}
?>