<?
// Start the session
session_start();

// Use Captcha
$_SESSION['use_captcha'] = true;

// Include reCAPTCHA
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/captcha/lib.php');
?>