To use this captcha (reCAPTCHA) simply include these two lines at the top of the controller:
// Include reCAPTCHA
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/captcha/init.php');

// Assign Variables
$smarty->assign('captcha', recaptcha_get_html($publickey))

Then use <% $captcha %> to place the captcha.

CSS customization is done through:
#recaptcha_container {
	border: 1px solid #dfdfdf;
	padding: 4px;
}

#recaptcha_container #recaptcha_image {
	border: 1px solid #dfdfdf;
}

In the CSS file.