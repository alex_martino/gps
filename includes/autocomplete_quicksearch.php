<?php

session_start(); if (!is_numeric($_SESSION['member_id'])) { die(); } //(quick security check)
require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

$do_names = array();

//get members
$sql = "SELECT id, first_name, last_name, ug_school_id, graduation_year FROM members WHERE disabled != 1 AND hidden != 1";
$result = mysql_query($sql) or die(mysql_error());
$i=0;
while ($row = mysql_fetch_array($result)) {
$do_names[$i] = ucfirst($row[1])." ".ucfirst($row[2]);
$do_type[$i] = 'member';
$do_id[$i] = $row[0];
$i++;
}


//get tags (capitalised)
$sql = "SELECT tag,ttype,tid FROM tags WHERE ttype <> 'pitch' GROUP BY tag ORDER BY tag ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
if (!in_array(ucwords($row[0]),$do_names)) {
$do_names[$i] = ucwords($row[0]);
$do_type[$i] = $row[1];
$do_id[$i] = "";
$i++;
}
}

//get tags (allcaps)
$sql = "SELECT tag,ttype,tid FROM tags WHERE ttype = 'pitch' GROUP BY tag ORDER BY tag ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
if (!in_array(strtoupper($row[0]),$do_names)) {
$do_names[$i] = strtoupper($row[0]);
$do_type[$i] = $row[1];
$do_id[$i] = "";
$i++;
}
}

//get universities
$sql = "SELECT ID,SNAME FROM universities";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$do_names[$i] = ucfirst($row[1]);
$do_type[$i] = 'uni';
$do_id[$i] = $row[0];
$i++;
}

//get classes
$sql = "SELECT graduation_year FROM members";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
if (!in_array($row[0],$do_names)) {
$do_names[$i] = $row[0];
$do_type[$i] = 'class';
$do_id[$i] = $row[0];
$i++;
}
}



// TextboxList Autocomplete sample data for queryRemote: true (server is queried as user types)

// get names (eg: database)
// the format is:
// id, searchable plain text, html (for the textboxlist item, if empty the plain is used), html (for the autocomplete dropdown)

$response = array();
//$names = array('Abraham Lincoln', 'Adolf Hitler', 'Agent Smith', 'Agnus', 'AIAI', 'Akira Shoji', 'Akuma', 'Alex', 'Antoinetta Marie', 'Baal', 'Baby Luigi', 'Backpack', 'Baralai', 'Bardock', 'Baron Mordo', 'Barthello', 'Blanka', 'Bloody Brad', 'Cagnazo', 'Calonord', 'Calypso', 'Cao Cao', 'Captain America', 'Chang', 'Cheato', 'Cheshire Cat', 'Daegon', 'Dampe', 'Daniel Carrington', 'Daniel Lang', 'Dan Severn', 'Darkman', 'Darth Vader', 'Dingodile', 'Dmitri Petrovic', 'Ebonroc', 'Ecco the Dolphin', 'Echidna', 'Edea Kramer', 'Edward van Helgen', 'Elena', 'Eulogy Jones', 'Excella Gionne', 'Ezekial Freeman', 'Fakeman', 'Fasha', 'Fawful', 'Fergie', 'Firebrand', 'Fresh Prince', 'Frylock', 'Fyrus', 'Lamarr', 'Lazarus', 'Lebron James', 'Lee Hong', 'Lemmy Koopa', 'Leon Belmont', 'Lewton', 'Lex Luthor', 'Lighter', 'Lulu');

// make sure they're sorted alphabetically, for binary search tests
//sort($names);
array_multisort($do_names,$do_type,$do_id);

$search = isset($_REQUEST['search']) ? $_REQUEST['search'] : '';

foreach ($do_names as $i => $name)
{
if (!preg_match("/^$search/i", $name)) continue;
if ($do_type[$i] == 'book') { $filename = "images/books.png"; }
if ($do_type[$i] == 'expertise') { $filename = "images/sector.gif"; }
elseif ($do_type[$i] == 'television') { $filename = "images/television.png"; }
elseif ($do_type[$i] == 'movie') { $filename = "images/video.gif"; }
elseif ($do_type[$i] == 'investor') { $filename = "images/investor.png"; }
elseif ($do_type[$i] == 'sector') { $filename = "images/sector.gif"; }
elseif ($do_type[$i] == 'pitch') {
$filename = "images/stocks.png";
$stock_data = file_get_contents("http://finance.google.com/finance/info?client=ig&q=".$name);
$stock_data_array = explode('"', $stock_data);
$info = "<BR>".$stock_data_array[15];
}
elseif ($do_type[$i] == 'member') {
$filename = "member_photos/" . str_replace(' ', '-', strtolower($name)) . "-cropped.jpg";
$sql = "SELECT ug_school_id, graduation_year FROM members WHERE id = '$do_id[$i]' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
if ($row[0]>0) {
$sql = "SELECT SNAME FROM universities WHERE ID = '$row[0]' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row2 = mysql_fetch_array($result);
$info = "<BR>".$row2[0]." '".substr($row[1],2,2);
} //end if numeric school id

if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/content_files/' . $filename)) { $filename = "member_photos/" . str_replace(' ', '-', $do_id[$i]) . "-cropped.jpg"; }

}
else {$filename = "member_photos/" . str_replace(' ', '-', strtolower($name)) . "-thumb.jpg"; }

// Have changed profile photos to the format 'ID-current' or "ID-thumb" in case we have a situation where members have identical names
// As a result, we have a legacy issue, so this code will hopefully help the transition (the old photos are still in the format 'firstname-lastname-thumb.jpg'...

//$response[] = array($do_type[$i]."_".$do_id[$i]."_".$name, $name, null, '<img src="/content_files/'. $filename . (file_exists('/content_files/' . $filename . '.jpg') ? '.jpg' : '.png') .'" /> ' . $name);
$response[] = array($do_type[$i]."_".$do_id[$i]."_".$name, $name, null, '<img src="/content_files/'. $filename . '" /> ' . $name . $info);
$info = "";
}

header('Content-type: application/json');
echo json_encode($response);


?>