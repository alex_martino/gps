<?
$look_forward = 30;
$look_limit = 10;
//old sql statement, replaced with nicer one
//$sql = "SELECT id, first_name, last_name, birth_date FROM members WHERE (DAYOFYEAR( curdate( ) ) <= dayofyear( birth_date ) AND DAYOFYEAR( curdate( ) ) +".$look_forward." >= dayofyear( birth_date ) OR DAYOFYEAR( curdate( ) ) <= dayofyear( birth_date ) +365 AND DAYOFYEAR( curdate( ) ) +".$look_forward." >= dayofyear( birth_date ) +365) AND disabled = 0 AND hidden = 0 ORDER BY dayofyear( birth_date ) LIMIT " . $look_limit;

$sql = "SELECT
  id
  , first_name
  , last_name
  , birth_date
FROM members
WHERE 
    (birth_date >= DATE_SUB(CURDATE(), INTERVAL YEAR(CURDATE()) - YEAR(birth_date) YEAR)
  AND
    birth_date <= DATE_ADD(DATE_SUB(CURDATE(), INTERVAL YEAR(CURDATE()) - YEAR(birth_date) YEAR), INTERVAL ".$look_forward." DAY))
  AND
    disabled = 0
  AND
    hidden = 0
ORDER BY birth_date ASC LIMIT ".$look_limit;

$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$day = substr($row['birth_date'],8,2);
$tmonth = substr($row['birth_date'],5,2);
$birthday[$tmonth.$day][] = $row;
}
//echo $birthday['0506'][0][0];


?>

<style>
.sidebox { text-align: center; font-weight: bold; margin-top: 15px; color: black }
.sideboxrow { text-align:left; font-weight: normal; margin-left: 10px }
.sideboxrow a { color: navy; text-decoration: none; font-weight: normal }
.sideboxrow a:hover { color: navy; text-decoration: underline }
</style>

<div class="sidebox">
<div><img src="/content_files/images/birthday.gif" style="vertical-align: bottom"> Birthdays</div>
<div class="sideboxrow">
<?
$startmonth = date("n");
$endmonth = $startmonth + 1;
$startday = date("j");
$today = $startday;
$count = 0;
$endcount = $look_forward;
$month = $startmonth;

while($count < $endcount) {

if (strlen($month) < 2) { $month = "0" . $month; }
if (strlen($today) < 2) { $today = "0" . $today; }

//echo $month.$today."<BR>"; //uncomment to debug
if(isset($birthday[$month.$today])) {
if (!$set[$month]) {
echo "<BR><b>".date("F",mktime(0,0,0,$month,$today,date("Y")))."</B><BR>";
$set[$month] = 1;
}
foreach ($birthday[$month.$today] as $i) {
$name = $i['first_name'] . " " . $i['last_name'];
echo(date("jS",strtotime($i['birth_date'])))." <a href='/portal/network/view/overview.php?id=".$i['id']."'>".substr($name,0,16	)."</a><BR>";
}
}

if ($today == 28 && $month == 2 && date("L") == 0) { $today = 0; $month++; $count++; }
elseif ($today == 29 && $month == 2) { $today = 0; $month++; $count++; }
elseif ($today == 30 && ($month == 4 || $month == 6 || $month == 9 || $month == 11)) { $today = 0; $month++; $count++; }
elseif ($today == 31 && ($month == 1 || $month == 3 || $month == 5 || $month == 7 || $month == 8 || $month == 10 || $month == 12)) { $today = 0; $month++; $count++; }
else { $count++; }
$today++;

}
?>