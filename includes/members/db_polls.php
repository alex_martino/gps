<?php require_once("db_inc.php"); ?>
<?php
class db_polls {
	
   	var $table_name;
   	var $queryh;

   	var $id;
	var $desc;
	var $isactive;
	var $q1;
	var $q2;
	var $q3;
	var $q4;
	var $q5;

   	function db_polls() {
		$this->table_name = "pollquestions";
		$this->queryh = "";

		$this->id = "";
		$this->desc = "";
		$this->isactive = "";
		$this->q1 = "";
		$this->q2 = "";
		$this->q3 = "";
		$this->q4 = "";
		$this->q5 = "";
	}

   	function selectstmt() {
 		return	"SELECT 
		  		id,
				`desc`,
				isactive,
				q1,
				q2,
				q3,
				q4,
				q5,
				mdate ";		    
   	}

   	function reset_handle($bactiveonly=false) {
		$dbh = dbconnect();
	    $sql = $this->selectstmt();
	    $sql .= " FROM $this->table_name ";
	    if ($bactiveonly) {
	    	$sql .= "WHERE isactive = 'Y' ";
	    }
	    $sql .= "ORDER BY isactive DESC, `desc` ASC ";
	    // $sql .= "ORDER BY sortdate DESC, tb_id DESC ";
 		$this->queryh = @mysql_query($sql, $dbh) or die($sql.mysql_error());
		return mysql_num_rows($this->queryh);
	}

   	function copyrow($row) {
		$this->id = $row['id'];
		$this->desc = $row['desc'];
		$this->isactive = $row['isactive'];
		$this->q1 = $row['q1'];
		$this->q2 = $row['q2'];
		$this->q3 = $row['q3'];
		$this->q4 = $row['q4'];
		$this->q5 = $row['q5'];
	}

   	function copypost() {
		$this->id = trim(stripslashes($_POST['id']));
		$this->desc = trim(stripslashes($_POST['desc']));
		$this->isactive = (isset($_POST['isactive'])) ? "Y" : "N";
		$this->q1 = trim(stripslashes($_POST['q1']));
		$this->q2 = trim(stripslashes($_POST['q2']));
		$this->q3 = trim(stripslashes($_POST['q3']));
		$this->q4 = trim(stripslashes($_POST['q4']));
		$this->q5 = trim(stripslashes($_POST['q5']));
	}

   	function dbseek ($rownum) {
   		if ($rownum > 0)
   			return mysql_data_seek($this->queryh, $rownum);
		else
			return false;   			
   		
	}

   	function next_row() {
		if ($row = mysql_fetch_array($this->queryh))  {
			$this->copyrow($row);
			return true;
		} else {
			return false;
		}
   	}

	function retrieve($id = 0) {
		$this->id = $id;
   		if ($this->id > 0) {
			$dbh = dbconnect();
		    $sql = $this->selectstmt();	    
		    $sql .= " FROM $this->table_name ";
		    $sql .= " WHERE id = '$id' ";
			$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());
			if ($row = mysql_fetch_array($result))  {
				$this->copyrow($row);
			}
		} else {
			$this->id = 0;
		}
	}

	function update() {
		$dbh = dbconnect();
		if ($this->id <= 0) { // insert
	   		$sql = "INSERT INTO $this->table_name 
		  		(id,
				`desc`,
				isactive,
				q1,
				q2,
				q3,
				q4,
				q5,
				mdate)	
		   	VALUES 
		   	(
		   		'', 
   				'".mysql_escape_string($this->desc)."',
   				'".mysql_escape_string($this->isactive)."',
   				'".mysql_escape_string($this->q1)."',
   				'".mysql_escape_string($this->q2)."',
   				'".mysql_escape_string($this->q3)."',
   				'".mysql_escape_string($this->q4)."',
   				'".mysql_escape_string($this->q5)."',
		   		now())";
			$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());
			$this->id = mysql_insert_id();
		} else { //update
			$sql = "UPDATE $this->table_name SET
	   				`desc` = '".mysql_escape_string($this->desc)."',
	   				isactive = '".mysql_escape_string($this->isactive)."',
	   				q1 = '".mysql_escape_string($this->q1)."',
	   				q2 = '".mysql_escape_string($this->q2)."',
	   				q3 = '".mysql_escape_string($this->q3)."',
	   				q4 = '".mysql_escape_string($this->q4)."',
	   				q5 = '".mysql_escape_string($this->q5)."', ";
		   	$sql .= "mdate = now()
		   			WHERE id ='".$this->id."'";
			$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());
		}
		$this->iserror = false;
	}

	function delete($id = 0) {
		if ($id > 0) {
			$this->id = $id;
		}
   		if ($this->id > 0) {
			$dbh = dbconnect();
		    $sql = "DELETE ";
		    $sql .= "FROM $this->table_name ";
		    $sql .= "WHERE id = '$this->id' ";
			$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());

		    $sql = "DELETE "; // also delete poll answers
		    $sql .= "FROM pollanswers ";
		    $sql .= "WHERE poll_id = '$this->id' ";
			$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());
		} else {
			$this->id = 0;
		}
	}	

	function writebutton() {
		echo '<a href="'. $_SERVER['PHP_SELF'] . 
		'?id=' . urlencode($this->id) . '&view" class="arial12greylink">View</a> &nbsp;&nbsp;';
		echo '<a href="'. $_SERVER['PHP_SELF'] . 
		'?id=' . urlencode($this->id) . '" class="arial12greylink">Edit</a> &nbsp;&nbsp;';
		echo '<a href="javascript:form1delete(\''. urlencode($this->id) . '\')" class="arial12greylink">Delete</a>';
	}

}

?>