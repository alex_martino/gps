<?php
// Number encoder (1 -> '41259616' | 2 -> '92519226').
function num_encode($in)
{
	if (is_numeric($in) && $in > 0)
	{
		$key = '0495861273';

		for($i=0; $i < strlen($in); $i++) $out .= $key[substr($in, $i, 1)]; // Encode string according to key.

		if (strlen($out) < 7)
		{
			$padding = (7 - strlen($out));
			$out .= substr(($in * 125961), 0, $padding); // Add padding characters.
			$out .= $padding; // Add number of padding characters.
		}
		else $out .= '0'; // No padding characters.

		return $out;
	}
	return false;
}

// Equivalent decoder function ('41259616' -> 1 | '92519226' -> 2).
function num_decode($in)
{
	if (is_numeric($in) && $in > 0)
	{
		$padding = substr($in, -1);

		if ($padding > 0) $in = substr($in, 0, 0 - (1 + $padding)); // Remove padding characters.
		else $in = substr($in, 0, -1); // Remove the padding.

		$key = '0495861273';
		for($i=0; $i<strlen($in); $i++) $out .= strpos($key, $in[$i]); // Decode string according to key.

		return $out;
	}
	return false;
}
?>