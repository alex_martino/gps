<?php
/////////////////////////////////////////////////////////////////////

///////////////////////   FIRST FUNCTION   //////////////////////////

/////////////////////////////////////////////////////////////////////

// Include our mailto obfuscation script, and parse any email addresses
include ($_SERVER['DOCUMENT_ROOT'] . "/includes/mailto/mailto.inc.php");
ob_start('ob_mailto');

// Include config files and function libraries
include ($_SERVER['DOCUMENT_ROOT'] . "/includes/config.inc.php");

// Include our routing logic, to handle any special rewriting of the URL
include ($_SERVER['DOCUMENT_ROOT'] . "/includes/routing.inc.php");

// Redirect this user to the primary domain name if FORCE_PRIMARY_DOMAIN_NAME is true
if (FORCE_PRIMARY_DOMAIN_NAME == true && !stristr($_SERVER['HTTP_HOST'], PRIMARY_DOMAIN_NAME)) {
	header("Location: http://www." . PRIMARY_DOMAIN_NAME . $_SERVER['PHP_SELF'] . '?' . ($_SERVER['QUERY_STRING'] ? $_SERVER['QUERY_STRING'] . '&' : '') . 'referrer=' . str_replace('www.', '',  $_SERVER['HTTP_HOST']), true, 301);
	exit();
}

//check for remember me cookie and process

if (isset($_COOKIE['rememberme']) && !is_numeric($_SESSION['member_id']) && !stristr($_SERVER['QUERY_STRING'], 'url=members/login.php&session_expired=1')) { //don't run this if logged in or logging out/session expired

parse_str($_COOKIE['rememberme']);

$do_email = $r_email_address;
$do_pass = $r_hash;

$_SESSION['member_id'] = 0; //in case somebody tries to set the session variable using the cookie to hack the site
$_SESSION['user_id'] = 0; //in case somebody tries to set the session variable using the cookie to hack the site

$sql = "SELECT * FROM members WHERE email_address = '".$do_email."' AND pass_enc_sec = '".$do_pass."' AND disabled = 0 LIMIT 1";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);
if (mysql_num_rows($result) == 1) { //the following will log the user in if the cookie matches a record in the database

				// Setup session variables
				$_SESSION['member_id'] = $row['id'];
				$_SESSION['user_id'] = $row['id']; //new "user_" convention for all user variables - hence repeat
				$_SESSION['user_first_name'] = $row['first_name'];
				$_SESSION['user_last_name'] = $row['last_name'];
				$_SESSION['user_ug_school_id'] = $row['ug_school_id'];
				$_SESSION['user_is_analyst'] = $row['analyst'];
				$_SESSION['user_is_member'] = $row['member'];
				$_SESSION['user_is_um'] = $row['upper_management'];
				$_SESSION['user_is_board'] = $row['board_manager'];
				$_SESSION['user_is_alumni'] = $row['alumni'];
				$_SESSION['user_is_admin'] = $row['admin'];
				$_SESSION['user_gps_position_held'] = $row['gps_position_held'];

				// Add log
				$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('user_login_remember_me','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".$_SERVER['QUERY_STRING']."','".$_SERVER['REMOTE_ADDR']."')";
				$result = mysql_query($sql) or die(mysql_error());
}
}

//are we logged in?
if (is_numeric($_SESSION['member_id'])) { $logged_in = 1; $smarty->assign('logged_in', $logged_in); }

//Do quick check that account has not been disabled or admin priviledges changed since log in
if ($logged_in == 1) {
require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");
$sql = "SELECT disabled, admin FROM members WHERE id = '".$_SESSION['user_id']."' LIMIT 1";
$results = mysql_query($sql) or die("FATAL ERROR - CONTACT ADMIN");
$row = mysql_fetch_array($results);
if ($row['disabled'] == 1) { header("Location: /members/login.php?disabled_in_session=1"); }
if ($row['admin'] == 1) { $_SESSION['user_is_admin'] = 1; } else { $_SESSION['user_is_admin'] = 0; }
}

//Portal (Recruiting - Permissions)
if ($_SESSION['user_is_member'] || $_SESSION['user_is_um']) { $can_app_respond = 1; } //Note: UM is part of the member base

//declare global scripts
$university = array();

function load_unis() {
global $university;
//load universities
$sql = "SELECT ID, SNAME, FNAME FROM universities";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$university[$row[0]]['sname'] = $row[1];
$university[$row[0]]['fname'] = $row[2];
}
}

/////////////////////////////////////////////////////////////////////

///////////////////////   SECOND FUNCTION   /////////////////////////

/////////////////////////////////////////////////////////////////////

//We can assume log in is required within certain areas
if (stristr($_SERVER['PHP_SELF'], '/portal/')) { $login_required = 1; }

//And we can assume admin required within certain areas
if (stristr($_SERVER['PHP_SELF'], '/portal/admin/')) { $admin_required = 1; }

//Now controller is loaded, first check for login requirement
if (!$logged_in && $login_required) { header("Location: /members/login.php?session_expired=1&referrer=".$_SERVER['PHP_SELF']."&id=".$_GET['id']); exit; }

//Now controller is loaded, check user is not logged in with invalid session variables (this bug has occurred - cause unknown)
if ($logged_in && $_SESSION['user_id'] == 0) { header("Location: /members/login.php?action=logout"); exit; }

//Now controller is loaded, first check for login requirement
if (!$_SESSION['user_is_admin'] && $admin_required) { header("Location: /portal/admin_required.php"); exit; }

//Check if in recruiting area
if (stristr($_SERVER['PHP_SELF'], '/portal/recruiting/') && $_SESSION['user_is_analyst'] == 1) { header("Location: /portal/"); exit; }

?>