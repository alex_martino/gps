<?php
session_start();
?>
<!DOCTYPE html>

<head>
<title>
</title>
<style>
body { margin: 0; font-family: Arial }

.progress-outer {
    background: #333;
    -webkit-border-radius: 13px;
    height: 20px;
    width: 300px;
    padding: 3px;
    margin: 17px auto 0 auto;
}

.progress-inner {
    background: #95B9C7;
    height: 18px;
    -webkit-border-radius: 9px;
    text-align: center;
    color: #000;
    font-weight: bold;
    vertical-align: bottom;
    padding-top: 2px;
    font-size: 14px;
}
.paid_total {
    float: right;
    padding: 4px 0 0 0;
    color: #061667;
    font-size: 14px;
}
.nobody {
    width: 260px;
    margin: 34px auto 0 auto;
    color: #061667;
    font-size: 13px;
}
</style>
<body>

<?php
// Include site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/config.php");

if (!$_SESSION['user_is_admin']) { die("Hack attempt"); }

$group = $_GET['group'];
$payitem_usr = $_GET['payitem'];

switch ($group) {

case "analyst":
case "member":
case "alumni":

  //get list of expenses from db for group, e.g. Conference, Annual dues: 2012, etc & create array
  $sql = 'SELECT IF(p.annual_dues_flag =1,CONCAT(  "Annual dues: ", p.description ),p.description) AS Item FROM payments as p LEFT JOIN members m ON m.id = p.user_id WHERE m.'.$group.' = 1 AND p.description <> "Test" GROUP BY Item ORDER BY p.created DESC';
  $result = mysql_query($sql) or die(mysql_error());
  while ($row = mysql_fetch_array($result)) { $payitem[] = $row[0]; }

  //check if user specified an item
  if (isset($payitem_usr) && array_search($payitem_usr, $payitem) > 0) {
	$the_payitem = $payitem_usr;
  }
  else {
	$the_payitem = $payitem[0];
  }

  $sql = 'SELECT p.status, sum(p.amount) as total FROM payments p JOIN members m on m.id = p.user_id WHERE m.'.$group.' = 1 AND IF(p.annual_dues_flag,CONCAT(  "Annual dues: ", p.description ),p.description) = "'.$the_payitem.'" AND p.status = "paid"';
  $result = mysql_query($sql) or die(mysql_error());
  while ($row = mysql_fetch_array($result)) {
  $paystat['paid'] = $row['total'];
  }

  $sql = 'SELECT p.status, sum(p.amount) as total FROM payments p JOIN members m on m.id = p.user_id WHERE m.'.$group.' = 1 AND IF(p.annual_dues_flag,CONCAT(  "Annual dues: ", p.description ),p.description) = "'.$the_payitem.'" AND p.status <> "paid"';
  $result = mysql_query($sql) or die(mysql_error());
  while ($row = mysql_fetch_array($result)) {
  $paystat['notpaid'] = $row['total'];
  }

break;

}

  $paystat['total'] = $paystat['paid'] + $paystat['notpaid'];
  if ($paystat['total'] > 0) { $paystat['progress'] = round($paystat['paid']/$paystat['total']*100); } else { $paystat['progress'] = "-1"; }

if (count($payitem) > 0) {
	$html = '<div class="paid_total">$'.round($paystat['paid']).' / $'.round($paystat['total']).'</div>';
	$html .= '<form name="select_item" action="'.$_SERVER['PHP_SELF'].'" method="GET">';
	$html .= '<input type="hidden" name="group" value="'.$group.'">';
	$html .= '<select name="payitem" id="progress_'.$group.'_sel" onChange="this.form.submit();">';
	
	foreach ($payitem as $value) {
	$html .= '<option value="'.$value.'"';
	if ($value == $payitem_usr) { $html .= ' SELECTED'; }
	$html .= '>'.$value.'</option>';
	}
	
	$html .= '</select>';
	$html .= '</form>';
	
	$html .= '<div class="progress-outer" id="progress_'.$group.'_con"><div class="progress-inner" id="progress_'.$group.'"></div></div>';
	
	$html .= '<script>';
	$html .= 'document.getElementById("progress_'.$group.'").style.width = "'.$paystat['progress'].'%";';
	$html .= 'document.getElementById("progress_'.$group.'").innerHTML = "'.$paystat['progress'].'%";';
	
	if ($paystat['progress'] == -1) { $html .= 'document.getElementById("progress_'.$group.'_con").style.display = "none"; }'; }
	else if ($paystat['progress'] == "0") { $html .= 'document.getElementById("progress_'.$group.'").style.color = "#fff"; document.getElementById("progress_'.$group.'").style.marginLeft = "143px";'; }
	else if ($paystat['progress'] <= "25") { $html .= 'document.getElementById("progress_'.$group.'").style.background = "#F43114";'; }
	else if ($paystat['progress'] <= "50") { $html .= 'document.getElementById("progress_'.$group.'").style.background = "#F89923";'; }
	else if ($paystat['progress'] <= "70") { $html .= 'document.getElementById("progress_'.$group.'").style.background = "#FAD643";'; }
	else if ($paystat['progress'] <= "90") { $html .= 'document.getElementById("progress_'.$group.'").style.background = "#B5F82A";'; }
	
	$html .= '</script>';
	
	echo $html;
}

else {

echo "<div class='nobody'>Nobody in this group has been invoiced!</div>";

}

?>
</body>
</html>