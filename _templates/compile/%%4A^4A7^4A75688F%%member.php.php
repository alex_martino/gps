<?php /* Smarty version 2.6.17, created on 2012-10-16 07:18:10
         compiled from /home/gpscom/public_html/_pages/membership/member.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', '/home/gpscom/public_html/_pages/membership/member.php', 29, false),)), $this); ?>
<div><img src="/content_files/headers/members.gif" alt="" width="800" height="90"></div>
<div>

<?php if ($this->_tpl_vars['member']['hidden'] != 1): ?>
<?php if ($this->_tpl_vars['can_edit']): ?>
<div class="editlink">
<a href="/portal/network/view/overview.php?id=<?php echo $this->_tpl_vars['member']['id']; ?>
&edit=1">Edit Biography</a>
</div>
<?php endif; ?>
	<?php if ($this->_tpl_vars['member']['founding_member'] == 1 || $this->_tpl_vars['member']['chapter_founder'] == 1): ?>
		<h2>Global Platinum Securities Founding Member</h2>
	<?php elseif ($this->_tpl_vars['member']['alumni'] == 1): ?>
		<h2>GPS Alumni - Class of <?php echo $this->_tpl_vars['member']['graduation_year']; ?>
</h2>
	<?php elseif ($this->_tpl_vars['member']['member'] == 1): ?>
		<h2>Global Platinum Securities Member</h2>
	<?php elseif ($this->_tpl_vars['member']['advisory_board'] == 1): ?>
		<h2>Global Platinum Securities Advisory Board Member</h2>
	<?php elseif ($this->_tpl_vars['member']['upper_management'] == 1): ?>
		<h2>Global Platinum Securities Upper Management Member</h2>
	<?php elseif ($this->_tpl_vars['member']['board_manager'] == 1): ?>
		<h2>Global Platinum Securities Board Manager Member</h2>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['member']['current_photo'] != '' && $this->_tpl_vars['logged_in'] == 1): ?>
		<div style="float: left; margin-bottom: 5px; margin-right: 10px;"><img src="<?php echo $this->_tpl_vars['member']['current_photo']; ?>
" width="150" alt="" /></div>
	<?php endif; ?>
	<p><strong><span style="font-size: 16px;"><?php echo $this->_tpl_vars['member']['first_name']; ?>
 <?php echo $this->_tpl_vars['member']['last_name']; ?>
</span><br />
		<?php if ($this->_tpl_vars['member']['gps_position_held'] != ''): ?><?php echo $this->_tpl_vars['member']['gps_position_held']; ?>
<br /><?php endif; ?>
		<?php echo $this->_tpl_vars['member']['undergraduate_school']; ?>
</strong></p>
	<?php echo ((is_array($_tmp=$this->_tpl_vars['member']['biography'])) ? $this->_run_mod_handler('replace', true, $_tmp, '<p>', '<p style="text-align:justify">') : smarty_modifier_replace($_tmp, '<p>', '<p style="text-align:justify">')); ?>

	
<?php else: ?>

<h2>Confidential</h2>

<p>This member profile is private.</p>

<?php endif; ?>
</div>