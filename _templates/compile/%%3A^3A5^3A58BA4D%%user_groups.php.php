<?php /* Smarty version 2.6.17, created on 2012-11-07 11:26:09
         compiled from /home/gpscom/public_html/_pages/portal/admin/user_groups.php */ ?>
<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>

<div class="editlink">
<?php if ($this->_tpl_vars['is_editing']): ?>
<a href="#" onClick="update_groups(); return false;">Save</a> |
<a href="/portal/admin/user_groups.php">Cancel</a>
<?php else: ?>
<a href="/portal/admin/user_groups.php?edit=1">Edit</a>
<?php endif; ?>
</div>
<h2>User Groups</h2>

<?php if ($this->_tpl_vars['is_editing']): ?>
<script type="text/javascript">
	window.onload = function() {
		var list = document.getElementById("analysts");
		DragDrop.makeListContainer( list );
		list.onDragOver = function() { this.style["border"] = "1px dashed #AAA"; };
		list.onDragOut = function() {this.style["border"] = "1px solid white"; };
		
		list = document.getElementById("members");
		DragDrop.makeListContainer( list );
		list.onDragOver = function() { this.style["border"] = "1px dashed #AAA"; };
		list.onDragOut = function() {this.style["border"] = "1px solid white"; };

		list = document.getElementById("alumni");
		DragDrop.makeListContainer( list );
		list.onDragOver = function() { this.style["border"] = "1px dashed #AAA"; };
		list.onDragOut = function() {this.style["border"] = "1px solid white"; };
	};
</script>
<style>
ul.boxy { cursor:move; }
</style>
<?php endif; ?>

<div class="listdiv">
<div class="title">Analysts</div>
<div class="thelist">
<ul id="analysts" class="sortable boxy" style="margin-left: 1em;">
<?php $_from = $this->_tpl_vars['group']['analysts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<li><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
<i><?php echo $this->_tpl_vars['i']['id']; ?>
</i></li>
<?php endforeach; endif; unset($_from); ?>
</ul>
</div>
</div>

<div class="listdiv">
<div class="title">Members</div>
<div class="thelist">
<ul id="members" class="sortable boxy" style="margin-right: 1em;">
<?php $_from = $this->_tpl_vars['group']['members']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<li><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
<i><?php echo $this->_tpl_vars['i']['id']; ?>
</i></li>
<?php endforeach; endif; unset($_from); ?>
</ul>
</div>
</div>

<div class="listdiv">
<div class="title">Alumni</div>
<div class="thelist">
<ul id="alumni" class="sortable boxy" style="margin-right: 1em;">
<?php $_from = $this->_tpl_vars['group']['alumni']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<li><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
<i><?php echo $this->_tpl_vars['i']['id']; ?>
</i></li>
<?php endforeach; endif; unset($_from); ?>
</ul>
</div>
</div>

<div style="clear:both">&nbsp;</div>

</div>