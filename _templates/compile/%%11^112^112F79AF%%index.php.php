<?php /* Smarty version 2.6.17, created on 2012-10-16 12:38:00
         compiled from /home/gpscom/public_html/_pages/portal/vote/index.php */ ?>
<div><img src="/content_files/headers/vote.gif" width="800" height="90"></div>
<div>
<?php if (( $this->_tpl_vars['add_success'] )): ?>
<B>Survey successfully added!</b><BR><BR>
<?php endif; ?>
<?php if ($this->_tpl_vars['previous']): ?>
<h2>Past Surveys</h2>
<p><i>The below surveys are no longer active.</i></p>
<?php else: ?>
<h2>Current Surveys</h2>
<?php endif; ?>
<?php if (( $this->_tpl_vars['user_can_add_survey'] && $this->_tpl_vars['previous'] != 1 )): ?>
<div style="width:100%; text-align: center">
<span class="button default strong"><input type="button" value="Add Survey" class="submit" id="addbutton" onClick="showform(); return false;" /></span>
</div>
<?php endif; ?>

<?php if (! $this->_tpl_vars['nosurveys']): ?>

<?php $_from = $this->_tpl_vars['surveys']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<?php $this->assign('myid', $this->_tpl_vars['i']['id']); ?>
<?php $this->assign('catid', $this->_tpl_vars['i']['cat_id']-1); ?>
<div style="margin-top:10px">

<form class="vote" method="post" action="index.php?pollid=<?php echo $this->_tpl_vars['i']['id']; ?>
">
<fieldset>
<legend><?php echo $this->_tpl_vars['categories'][$this->_tpl_vars['catid']]['name']; ?>
</legend>

<?php if (( $this->_tpl_vars['already_voted'][$this->_tpl_vars['myid']] || $this->_tpl_vars['previous'] == 1 )): ?>
<div style="float: right; font-size: 11px; color: #15428b">
<?php echo $this->_tpl_vars['difftext'][$this->_tpl_vars['myid']]; ?>

</div>
<p class="question">
<?php echo $this->_tpl_vars['i']['question']; ?>

<?php if (( $this->_tpl_vars['user_can_download_survey'] )): ?>
<a href="/portal/admin/surveys.php?download=<?php echo $this->_tpl_vars['myid']; ?>
"><img src="/content_files/images/icons/download.gif" style="width: 15px; height: 15px"></a>
<?php endif; ?>
</p>

<div style="float: left; line-height: 1.6">
<?php $_from = $this->_tpl_vars['vresults'][$this->_tpl_vars['myid']]['option']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['z'] => $this->_tpl_vars['y']):
?>
<?php echo $this->_tpl_vars['y']['name']; ?>
<BR>
<?php endforeach; endif; unset($_from); ?>
</div>
<div style="float: left; margin-left: 10px">
<?php $_from = $this->_tpl_vars['vresults'][$this->_tpl_vars['myid']]['option']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['z'] => $this->_tpl_vars['y']):
?>
<?php if ($this->_tpl_vars['y']['total'] > 0): ?>
<div style="height: 15px; width: <?php echo $this->_tpl_vars['y']['width']; ?>
px; background: <?php echo $this->_tpl_vars['y']['color']; ?>
; color:<?php echo $this->_tpl_vars['y']['fcolor']; ?>
; margin: 3px 10px 4px 0; text-align: right"><div style="margin-right: 2px"><?php echo $this->_tpl_vars['y']['percentage']; ?>
%</div></div>
<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>
</div>
<div style="clear:both">
<BR>
<?php if (( $this->_tpl_vars['vresults'][$this->_tpl_vars['myid']]['expiry'] < 604800 )): ?>
<div style="float:right">
	<span class="button default strong"><input type="button" value="+1 Day" class="button" onClick="extend_survey('<?php echo $this->_tpl_vars['myid']; ?>
',1);" /></span>
	<span class="button default strong"><input type="button" value="+1 Week" class="button" onClick="extend_survey('<?php echo $this->_tpl_vars['myid']; ?>
',7);" /></span>
</div>
<?php endif; ?>
Total Votes: <?php echo $this->_tpl_vars['vresults'][$this->_tpl_vars['myid']]['total_votes']; ?>

</div>
<?php else: ?>
<div style="float: right; font-size: 11px; color: #15428b">
<?php echo $this->_tpl_vars['difftext'][$this->_tpl_vars['myid']]; ?>

</div>
<p class="question">
<?php echo $this->_tpl_vars['i']['question']; ?>

</p>
<?php if (( $this->_tpl_vars['i']['multiple'] != 1 )): ?>
<p>
<?php $_from = $this->_tpl_vars['options'][$this->_tpl_vars['myid']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['m'] => $this->_tpl_vars['j']):
?>
<input type="radio" name="vote" value="<?php echo $this->_tpl_vars['m']; ?>
" /> <?php echo $this->_tpl_vars['j']['name']; ?>
<br />
<?php endforeach; endif; unset($_from); ?>
	</p>
<?php else: ?>
<p>
<?php $_from = $this->_tpl_vars['options'][$this->_tpl_vars['myid']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['m'] => $this->_tpl_vars['j']):
?>
<input type="checkbox" name="vote[]" value="<?php echo $this->_tpl_vars['m']; ?>
" /> <?php echo $this->_tpl_vars['j']['name']; ?>
<br />
<?php endforeach; endif; unset($_from); ?>
	</p>
<?php endif; ?>

	<p>
<span class="button default strong"><input type="submit" value="Vote" class="submit" /></span>
	</p>
<?php endif; ?>

	</fieldset>

	</form>
</div>
<?php endforeach; endif; unset($_from); ?>
<?php elseif ($this->_tpl_vars['previous']): ?>
<p>There are no old surveys yet.</p>
<?php else: ?>
<p>There are no active surveys.</p>
<?php endif; ?>



<?php if (( $this->_tpl_vars['user_can_add_survey'] )): ?>
<div style="clear:both">&nbsp;</div>
    <div id="page_screen">

        &nbsp;
    </div>


<div id="addform" style="display:none; border-radius:15px; -moz-border-radius: 15px; height: 340px">
<?php if ($this->_tpl_vars['error']): ?>
<div class="error" id="error" style="text-align: center; width: 100%; margin-top: 5px"><?php echo $this->_tpl_vars['errormsg']; ?>
</div>
<?php endif; ?>
<form name="addform" action="index.php?addsurvey=1" method="POST">
<div class="network_block_edit">
<div class="dataform">
<div class="data_title_r">
Question<BR>
Category<BR>
Deadline Date<BR>
Deadline Time
</div>
<div style="float:left" class="data_data_edit">
<input type="text" name="question" class="edittextw" value="" MAXLENGTH=255><BR>
<select name="category">
<option value="0">** Please Select **</option>
<?php $_from = $this->_tpl_vars['categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']['id']; ?>
"><?php echo $this->_tpl_vars['i']['name']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select><BR>
<select name="deadline_month">
<?php $_from = $this->_tpl_vars['months']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']['0']; ?>
"><?php echo $this->_tpl_vars['i']['1']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select>
<select name="deadline_day">
<?php $_from = $this->_tpl_vars['days']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select>
<select name="deadline_year">
<?php $_from = $this->_tpl_vars['years']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select>
<BR>
<select name="deadline_hour">
<?php $_from = $this->_tpl_vars['hours']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select>
:
<select name="deadline_minute">
<?php $_from = $this->_tpl_vars['minutes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select>
EST
</div>
<BR>
<hr class="editline">
<div class="data_title_r">
Response 1<BR>
Response 2<BR>
Response 3<BR>
Response 4<BR>
Response 5
</div>
<div style="float:left" class="data_data_edit">
<input type="text" name="response1" class="edittextw" value="<?php echo $this->_tpl_vars['post']['response1']; ?>
" MAXLENGTH=50><BR>
<input type="text" name="response2" class="edittextw" value="<?php echo $this->_tpl_vars['post']['response2']; ?>
" MAXLENGTH=50><BR>
<input type="text" name="response3" class="edittextw" value="<?php echo $this->_tpl_vars['post']['response3']; ?>
" MAXLENGTH=50><BR>
<input type="text" name="response4" class="edittextw" value="<?php echo $this->_tpl_vars['post']['response4']; ?>
" MAXLENGTH=50><BR>
<input type="text" name="response5" class="edittextw" value="<?php echo $this->_tpl_vars['post']['response5']; ?>
" MAXLENGTH=50>
</div>
<hr class="editline">
<div class="data_title_r">
Type
</div>
<div style="float:left" class="data_data_edit">
<select name="type" style="margin-bottom: 5px" onChange="update_explain();">
<option value="radio">One-response only</option>
<option value="checkbox">Multiple responses</option>
</select>
<span id="type_explain" style="font-size:11px">I.e. user can select just one of the above responses</span>
</div>
<hr class="editline">
<div style="float:left;"><input type="button" value="Cancel" onClick="hideform();"></div>
<div style="float:right"><input type="submit" value="Save"></div>
</div>

</div>
</form>
</div>

<script>
document.addform.deadline_month.value = "<?php echo $this->_tpl_vars['today']['month']; ?>
";
document.addform.deadline_day.value = "<?php echo $this->_tpl_vars['today']['day']; ?>
";
document.addform.deadline_hour.value = "19";
document.addform.deadline_minute.value = "00";
<?php if ($this->_tpl_vars['error']): ?>
showform();
<?php endif; ?>
function extend_survey(id,length) {
	if (length == 1) { length_str = "1 day"; }
	else if (length == 7) { length_str = "1 week"; }
	if (confirm("Are you sure you want to extend the vote by "+length_str+"?")) {
		window.location = "./?extend_survey="+id+"&extend="+length;
	}
}
</script>

<?php endif; ?>

</div>