<?php /* Smarty version 2.6.17, created on 2012-10-16 13:27:34
         compiled from /home/gpscom/public_html/_pages/portal/network/view/overview.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'capitalize', '/home/gpscom/public_html/_pages/portal/network/view/overview.php', 24, false),array('modifier', 'date_format', '/home/gpscom/public_html/_pages/portal/network/view/overview.php', 186, false),array('modifier', 'count', '/home/gpscom/public_html/_pages/portal/network/view/overview.php', 276, false),)), $this); ?>
<div><img src="/content_files/headers/network.gif" width="800" height="90">

<?php if ($this->_tpl_vars['not_viewable']): ?>
<div style="margin-top: 15px; margin-left: 10px">
<p>This profile is not available. <a href="javascript:history.go(-1)">Go back.</a></p>

<?php else: ?>
<div id="network_t_menu">
  <ul>
<?php $_from = $this->_tpl_vars['network_t_menu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
    <li<?php if ($this->_tpl_vars['i']['current'] == '1'): ?> id="current"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['i']['url']; ?>
?id=<?php echo $this->_tpl_vars['profile']['id']; ?>
"><?php echo $this->_tpl_vars['i']['name']; ?>
</a></li>
<?php endforeach; endif; unset($_from); ?>
  </ul>
</div>
</div>

<?php if ($this->_tpl_vars['is_editing']): ?>
<script>
			window.addEvent('load', function(){
				
				// Autocomplete initialization
				var t1 = new TextboxList('hometowns', {unique: true, plugins: {autocomplete: {}}});
<?php $_from = $this->_tpl_vars['tags']['hometown']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ttype'] => $this->_tpl_vars['i']):
?>
t1.add('<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
');
<?php endforeach; endif; unset($_from); ?>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t1.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_hometown.php', onSuccess: function(r){
					t1.plugins['autocomplete'].setValues(r);
					t1.container.removeClass('textboxlist-loading');
					document.getElementById('savelink').innerHTML = '<a href="#" onClick="document.do_edit.submit(); return false;">Save</a>';
				}}).send();	
			});

</script>
<script language="javascript" type="text/javascript" src="/scripts/tinymce/tiny_mce.js"></script>

<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode: 'textareas',
		theme: 'simple',
		editor_selector: 'mceEditor'
	});
</script>
<?php endif; ?>

<div style="margin-top: 25px">

<?php if ($this->_tpl_vars['can_edit']): ?>
<div class="editlink">
<?php if ($this->_tpl_vars['is_editing']): ?>
<span id="savelink">Please wait...</span> |
<a href="overview.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
">Cancel</a>
<?php else: ?>
<a href="overview.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
&edit=1">Edit</a>
<?php endif; ?>
</div>
<?php endif; ?>

<h2><?php if ($this->_tpl_vars['profile']['id'] == '102'): ?>Lord <?php endif; ?><?php if ($this->_tpl_vars['profile']['id'] == '96'): ?>Lady <?php endif; ?><?php echo $this->_tpl_vars['profile']['first_name']; ?>
 <?php echo $this->_tpl_vars['profile']['last_name']; ?>
</h2>

<div style="float:left" style="width: 150px">

<?php if ($this->_tpl_vars['display_photo']): ?>

<img name="profile_img" id="profile_img" src="<?php echo $this->_tpl_vars['profile']['current_photo']; ?>
<?php if ($_GET['profile_update']): ?>?<?php echo time(); ?>
<?php endif; ?>" width="150" alt="Photo of <?php echo $this->_tpl_vars['profile']['first_name']; ?>
 <?php echo $this->_tpl_vars['profile']['last_name']; ?>
" />

<?php if ($this->_tpl_vars['profile']['school_photo']): ?>
<script>
function time_back() {
$('profile_img').src='<?php echo $this->_tpl_vars['profile']['school_photo']; ?>
?<?php echo time(); ?>
';
document.getElementById("time_back").style.display="none";
document.getElementById("time_forward").style.display="block";
return false;
}
function time_forward() {
$('profile_img').src='<?php echo $this->_tpl_vars['profile']['current_photo']; ?>
';
document.getElementById("time_forward").style.display="none";
document.getElementById("time_back").style.display="block";
return false;
}

</script>
<div style="width: 100%; text-align: center; margin: 5px 0 0 0" id="time_back"><a href="#" onClick="time_back();">Time Travel</a></div>
<div style="width: 100%; text-align: center; margin: 5px 0 0 0; display: none" id="time_forward"><a href="#" onClick="time_forward();">Return to the Future</a></div>
<?php endif; ?>

<?php else: ?>

<img src="/images/male.png" width="150" alt="Photo missing" />

<?php endif; ?>

</div>

<?php if (( $this->_tpl_vars['is_editing'] )): ?>

<?php if (( $this->_tpl_vars['error'] )): ?>
<div class="error"><?php echo $this->_tpl_vars['errormsg']; ?>
</div>
<?php endif; ?>

<form name="do_edit" action="overview.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
&edit=1" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
<input type="hidden" name="submitted" value="1">

<div class="network_data" style="width: 580px">

		<fieldset>
		<legend>Hometowns</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">
			<input type="text" name="hometowns" value="" id="hometowns" />
<BR>
		<p class="note" style="margin-top: 3px">Anywhere you call home.. E.g. London, UK or New York, NY</p>
</div>
</div>

<BR><BR>

</fieldset>

</div>

<div class="network_data" style="width: 580px; float: right; margin-right: 5px">

		<fieldset>
		<legend>Public Biography</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">
<textarea id="biography" name="biography" style="width: 500px; height: 200px" class="mceEditor"><?php echo $this->_tpl_vars['profile']['biography']; ?>
</textarea>
<BR>
		<p class="note" style="margin-top: 3px">Your biography is visible on your <a href="/membership/member.php?id=<?php echo $this->_tpl_vars['profile']['id']; ?>
">public profile</a>.</p>
</div>
</div>

<BR><BR>

</fieldset>

</div>

<div class="network_data" style="width: 580px; float: right; margin-right: 5px">

		<fieldset>
		<legend>Photos</legend>
		<p class="note">Ideally images should be around 200px in width and in 'portrait' layout.</p>
		<p style="float: left;">Current Photo:<br />
			<input type="file" id="current_photo" name="current_photo" value="" style="width: 230px;" />
		</p>
		<p style="float: right;">School Photo:<br />
			<input type="file" id="school_photo" name="school_photo" value="" style="width: 230px;" />
		</p>
		<br style="clear: both;" />
		<div style="float: left; width: 230px;">
			<?php if (strlen ( $this->_tpl_vars['profile']['current_photo'] ) > 0): ?>
			<img src="<?php echo $this->_tpl_vars['profile']['current_photo']; ?>
" width="150" alt="" />
			<div style="text-align:center; width:150px; padding-top: 6px">Delete? <input type="checkbox" name="current_photo_del" value=1></div><?php endif; ?>
		</div>
		<div style="float: right; width: 230px;">
			<?php if (strlen ( $this->_tpl_vars['profile']['school_photo'] ) > 0): ?>
			<img src="<?php echo $this->_tpl_vars['profile']['school_photo']; ?>
" width="150" alt="" />
			<div style="text-align:center; width:150px; padding-top: 6px">Delete? <input type="checkbox" name="current_school_del" value=1></div><?php endif; ?>
		</div>
		<br style="clear: both;" />
		</fieldset>

</div>

<div class="network_data" style="width: 580px; float: right; margin-right: 5px">

		<fieldset>
		<legend>Birth Date</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">
<select name="birthday_day">
<option value=0 SELECTED>Day</option>
<?php $_from = $this->_tpl_vars['days']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']; ?>
" <?php if (((is_array($_tmp=$this->_tpl_vars['profile']['birth_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d") : smarty_modifier_date_format($_tmp, "%d")) == $this->_tpl_vars['i'] && $this->_tpl_vars['profile']['birth_date'] != "0000-00-00"): ?> SELECTED<?php endif; ?>><?php echo $this->_tpl_vars['i']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select>

<select name="birthday_month">
<option value=0 SELECTED>Month</option>
<?php $_from = $this->_tpl_vars['months']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']['0']; ?>
" <?php if (((is_array($_tmp=$this->_tpl_vars['profile']['birth_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m") : smarty_modifier_date_format($_tmp, "%m")) == $this->_tpl_vars['i']['0'] && $this->_tpl_vars['profile']['birth_date'] != "0000-00-00"): ?> SELECTED<?php endif; ?>><?php echo $this->_tpl_vars['i']['1']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select>

<select name="birthday_year">
<option value=0 SELECTED>Year</option>
<?php $_from = $this->_tpl_vars['years']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']; ?>
" <?php if (((is_array($_tmp=$this->_tpl_vars['profile']['birth_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y") : smarty_modifier_date_format($_tmp, "%Y")) == $this->_tpl_vars['i'] && $this->_tpl_vars['profile']['birth_date'] != "0000-00-00"): ?> SELECTED<?php endif; ?>><?php echo $this->_tpl_vars['i']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select>
</div>
</div>

<BR><BR>

</fieldset>

</div>
</form>

<?php else: ?>

<div class="network_data">

<?php if (( $this->_tpl_vars['profile']['id'] == 173 )): ?>
<div id="relationship_status" class="network_block_top">
<div style="float:left" class="data_title">
Relationship Status
</div>
<div style="float:left" class="data_data">
In a secret relationship with <a href="http://www.gps100.com/portal/network/view/overview.php?id=143">Raina 'Roofie' Gandhi</a>
</div>
</div>

<?php elseif (( $this->_tpl_vars['profile']['id'] == 143 )): ?>
<div id="relationship_status" class="network_block_top">
<div style="float:left" class="data_title">
Relationship Status
</div>
<div style="float:left" class="data_data">
In a secret relationship with <a href="http://www.gps100.com/portal/network/view/overview.php?id=173">Thomas 'Stud' Gorman</a>
</div>
</div>

<?php else: ?>

<div id="position" class="network_block_top">
<div style="float:left" class="data_title">
Position
</div>
<div style="float:left" class="data_data">
<?php echo $this->_tpl_vars['profile']['title']; ?>

</div>
</div>

<?php endif; ?>

<BR>
<div id="university" class="network_block">
<div style="float:left" class="data_title">
University
</div>
<div style="float:left" class="data_data">
<a href="/portal/network/search.php?s=1&uni=<?php echo $this->_tpl_vars['profile']['ug_school_id']; ?>
"><?php echo $this->_tpl_vars['profile']['uni_fname']; ?>
</a> | <a href="/portal/network/search.php?s=1&year=<?php echo $this->_tpl_vars['profile']['graduation_year']; ?>
"><?php echo $this->_tpl_vars['profile']['graduation_year']; ?>
</a>
</div>
</div>

<?php if ($this->_tpl_vars['profile']['current_employer']): ?>
<BR><BR>
<div id="employer" class="network_block">
<div style="float:left" class="data_title">
Employer
</div>
<div style="float:left" class="data_data">
<?php echo $this->_tpl_vars['profile']['current_employer']; ?>

</div>
</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['tags']['hometown']): ?>
<BR><BR>
<div id="hometowns" class="network_block">
<div style="float:left" class="data_title">
Hometown<?php if (( count($this->_tpl_vars['tags']['hometown']) > 1 )): ?>s<?php endif; ?>
</div>
<div style="float:left" class="data_data">
<?php $_from = $this->_tpl_vars['tags']['hometown']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>
<span class="button red small"><a href="../search.php?qs=1&ttype=hometown&tag=<?php echo $this->_tpl_vars['i']['tag']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</a></span>
<?php endforeach; endif; unset($_from); ?>
</div>
</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['profile']['birth_date'] != "0000-00-00"): ?>
<BR><BR>
<div id="Birthday" class="network_block">
<div style="float:left" class="data_title">
Birthday
</div>
<div style="float:left" class="data_data">
<?php if (((is_array($_tmp=$this->_tpl_vars['profile']['birth_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y") : smarty_modifier_date_format($_tmp, "%Y")) != '0'): ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['profile']['birth_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%B %e, %Y") : smarty_modifier_date_format($_tmp, "%B %e, %Y")); ?>

<?php else: ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['profile']['birth_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%B %e") : smarty_modifier_date_format($_tmp, "%B %e")); ?>

<?php endif; ?>
</div>
</div>
<BR><BR>
<?php endif; ?>

</div>

<?php endif; ?>

<div style="clear:both">&nbsp;</div>

<?php endif; ?>

</div>