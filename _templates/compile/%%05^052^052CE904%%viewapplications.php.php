<?php /* Smarty version 2.6.17, created on 2012-11-28 11:54:51
         compiled from /home/gpscom/public_html/_pages/portal/recruiting/viewapplications.php */ ?>
<div><img src="/content_files/headers/recruiting_<?php echo $this->_tpl_vars['sname']; ?>
.gif" width="800" height="90"></div>
<div>
<div class="filter_menu">
View:
<a <?php if ($this->_tpl_vars['display'] == 'all'): ?> class="this_filter" <?php endif; ?> href="viewapplications.php?display=all&cycle=<?php echo $this->_tpl_vars['cycle']; ?>
">All</a> |
<a <?php if ($this->_tpl_vars['display'] == 'P'): ?> class="this_filter" <?php endif; ?> href="viewapplications.php?display=P&cycle=<?php echo $this->_tpl_vars['cycle']; ?>
">Pending</a> |
<a <?php if ($this->_tpl_vars['display'] == 'R'): ?> class="this_filter" <?php endif; ?> href="viewapplications.php?display=R&cycle=<?php echo $this->_tpl_vars['cycle']; ?>
">Rejected</a>
<?php $_from = $this->_tpl_vars['top_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>
 | <a href="viewapplications.php?display=<?php echo $this->_tpl_vars['i']['count']; ?>
&cycle=<?php echo $this->_tpl_vars['cycle']; ?>
" class="<?php echo $this->_tpl_vars['i']['class']; ?>
">Round <?php echo $this->_tpl_vars['i']['count']; ?>
</a>
<?php endforeach; endif; unset($_from); ?>
 | <a <?php if ($this->_tpl_vars['display'] == 'S'): ?> class="this_filter" <?php endif; ?>
href="viewapplications.php?display=S&cycle=<?php echo $this->_tpl_vars['cycle']; ?>
">Successful</a>
</div>

<?php if ($this->_tpl_vars['result_num'] > 0): ?>
<div style="float:left" id="datalist">
<table style="border-spacing:0" class="applications">
<tr>
<th style="width: 180px">Name</th>

<?php if (( $this->_tpl_vars['f_email'] )): ?><th style="width: 90px">Email</th><?php endif; ?>
<?php if (( $this->_tpl_vars['f_number'] )): ?><th style="width: 90px">Number</th><?php endif; ?>
<?php if (( $this->_tpl_vars['f_date'] )): ?><th style="width: 90px">Date</th><?php endif; ?>
<?php if (( $this->_tpl_vars['f_status'] )): ?><th style="width: 50px">Status</th><?php endif; ?>
<?php if (( $this->_tpl_vars['f_actions'] )): ?><th style="width: 200px">Actions</th><?php endif; ?>
</tr>

<?php $_from = $this->_tpl_vars['rows']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>
<tr class="<?php echo $this->_tpl_vars['i']['class']; ?>
">
<td><?php echo $this->_tpl_vars['i']['fname']; ?>
 <?php echo $this->_tpl_vars['i']['sname']; ?>
</td>
<?php if ($this->_tpl_vars['i']['email']): ?> <td><?php echo $this->_tpl_vars['i']['email']; ?>
</td><?php endif; ?>
<?php if ($this->_tpl_vars['i']['number']): ?> <td><?php echo $this->_tpl_vars['i']['number']; ?>
</td><?php endif; ?>
<?php if ($this->_tpl_vars['i']['apptime']): ?> <td><?php echo $this->_tpl_vars['i']['apptime']; ?>
</td><?php endif; ?>
<?php if ($this->_tpl_vars['i']['status']): ?> <td><?php echo $this->_tpl_vars['i']['status']; ?>
</td><?php endif; ?>
<?php if ($this->_tpl_vars['i']['actions']): ?>
<td><input type="button" value="View" onClick="javascript:window.location.href='viewapplicant.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
';">
<input type="button" value="Print" onClick="javascript:CallPrint('<?php echo $this->_tpl_vars['i']['id']; ?>
');">
<?php if ($this->_tpl_vars['can_respond_to_these']): ?>
<input type="button" value="Respond" onClick="javascript:window.location.href='respond.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
'">
<?php endif; ?>
</td>
<?php endif; ?>
</tr>
<?php endforeach; endif; unset($_from); ?>

</table>
</div>
<?php endif; ?>
<br style="clear:both">
</div>