<?php /* Smarty version 2.6.17, created on 2012-10-16 08:25:05
         compiled from /home/gpscom/public_html/_pages/contact/index.php */ ?>
<div><img src="/content_files/headers/contact.gif" width="800" height="90"></div>
<div>
	<form name="form" action="/scripts/formmail.php" method="post">
		<table class="td_padding" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td width="100">Prefix</td>
		<td><select name="prefix">
				<option value="">Select One</option>
				<option value="Mr.">Mr.</option>
				<option value="Mrs.">Mrs.</option>
				<option value="Miss.">Miss.</option>
				<option value="Ms.">Ms.</option>
			</select>
		</td>
		</tr>
		<tr>
		<td width="100">First Name</td>
		<td><input type="text" name="firstname" style="width: 225px;">
		</td>
		</tr>
		<tr>
		<td width="100">Last Name</td>
		<td><input type="text" name="lastname" style="width: 225px;">
		</td>
		</tr>
		<tr>
		<td width="100">Suffix</td>
		<td><input type="text" name="suffix" style="width: 225px;">
		</td>
		</tr>
		<tr>
		<td width="100">Street Address</td>
		<td><input type="text" name="address1" style="width: 225px;">
		</td>
		</tr>
		<tr>
		<td width="100">Street Address 2</td>
		<td><input type="text" name="address2" style="width: 225px;">
		</td>
		</tr>
		<tr>
		<td width="100">City</td>
		<td><input type="text" name="city" style="width: 225px;">
		</td>
		</tr>
		<tr>
		<td width="100">State</td>
		<td><select name="state" style="width: 225px;">
				<option value="">Select One</option>
				<option value="AL">Alabama(AL)</option>
				<option value="AK">Alaska(AK)</option>
				<option value="AZ">Arizona(AZ)</option>
				<option value="AR">Arkansas(AR)</option>
				<option value="CA">California(CA)</option>
				<option value="CO">Colorado(CO)</option>
				<option value="CT">Connecticut(CT)</option>
				<option value="DE">Delaware(DE)</option>
				<option value="DC">District of Columbia(DC)</option>
				<option value="FL">Florida(FL)</option>
				<option value="GA">Georgia(GA)</option>
				<option value="HI">Hawaii(HI)</option>
				<option value="ID">Idaho(ID)</option>
				<option value="IL">Illinois(IL)</option>
				<option value="IN">Indiana(IN)</option>
				<option value="IA">Iowa(IA)</option>
				<option value="KS">Kansas(KS)</option>
				<option value="KY">Kentucky(KY)</option>
				<option value="LA">Louisiana(LA)</option>
				<option value="ME">Maine(ME)</option>
				<option value="MD">Maryland(MD)</option>
				<option value="MA">Massachusetts(MA)</option>
				<option value="MI">Michigan(MI)</option>
				<option value="MN">Minnesota(MN)</option>
				<option value="MS">Mississippi(MS)</option>
				<option value="MO">Missouri(MO)</option>
				<option value="MT">Montana(MT)</option>
				<option value="NE">Nebraska(NE)</option>
				<option value="NV">Nevada(NV)</option>
				<option value="NH">New Hampshire(NH)</option>
				<option value="NJ">New Jersey(NJ)</option>
				<option value="NM">New Mexico(NM)</option>
				<option value="NY">New York(NY)</option>
				<option value="NC">North Carolina(NC)</option>
				<option value="ND">North Dakota(ND)</option>
				<option value="OH">Ohio(OH)</option>
				<option value="OK">Oklahoma(OK)</option>
				<option value="OR">Oregon(OR)</option>
				<option value="PA">Pennsylvania(PA)</option>
				<option value="RI">Rhode Island(RI)</option>
				<option value="SC">South Carolina(SC)</option>
				<option value="SD">South Dakota(SD)</option>
				<option value="TN">Tennessee(TN)</option>
				<option value="TX">Texas(TX)</option>
				<option value="UT">Utah(UT)</option>
				<option value="VT">Vermont(VT)</option>
				<option value="VA">Virginia(VA)</option>
				<option value="WA">Washington(WA)</option>
				<option value="WV">West Virginia(WV)</option>
				<option value="WI">Wisconsin(WI)</option>
				<option value="WY">Wyoming(WY)</option>
			</select>
		</td>
		</tr>
		<tr>
		<td width="100">Zip</td>
		<td><input type="text" name="zip" style="width: 225px;">
		</td>
		</tr>
		<tr>
		<td width="100">Phone</td>
		<td><input type="text" name="phone" style="width: 225px;">
		</td>
		</tr>
		<tr>
		<td width="100">Email</td>
		<td><input type="text" name="email" style="width: 225px;">
		</td>
		</tr>
		<tr>
		<td width="100">Topic of Message</td>
		<td><select name="topic" style="width: 225px;">
				<option value="">Select One</option>
				<option value="Basic Materials">Basic Materials</option>
				<option value="Consumer Discretionary">Consumer Discretionary</option>
				<option value="Consumer Staples">Consumer Staples</option>
				<option value="Energy">Energy</option>
				<option value="Financials">Financials</option>
				<option value="Healthcare">Healthcare</option>
				<option value="Industrials">Industrials</option>
				<option value="Information Technology">Information Technology</option>
				<option value="Telecom">Telecom</option>
				<option value="General Information/Comments">General Information/Comments</option>
				<option value="Other">Other</option>
			</select>
		</td>
		</tr>
		<tr>
		<td width="100">Message Subject</td>
		<td><input type="text" name="message_subject" style="width: 225px;">
		</td>
		</tr>
		<tr>
		<td width="100">Message</td>
		<td><textarea name="message" style="width: 225px;" cols="23"></textarea>
		</td>
		</tr>
		<tr>
		<td>&nbsp;</td>
		<td><label>
			<input type="checkbox" name="receive_prospectus" value="Yes">
			Also, please send me a prospectus</label>
		</td>
		</tr>
		<tr>
		<td>&nbsp;</td>
		<td><a href="javascript:submitform('form');" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('send','','/content_files/images/buttons/submit_over.gif',0)"><img name="send" border="0" src="/content_files/images/buttons/submit_out.gif" width="66" height="17"></a></td>
		</tr>
		</table>
		<input type="hidden" name="recipient" value="info@gps100.com">
		<input type="hidden" name="subject" value="GPS Contact Information">
		<input type="hidden" name="redirect" value="/contact/thank_you.php">
	</form>
</div>