<?php /* Smarty version 2.6.17, created on 2012-10-16 16:23:19
         compiled from /home/gpscom/public_html/_pages/membership/founding_members.php */ ?>
<div><img src="/content_files/headers/founding_members.gif" width="800" height="90"></div>
<div>
	<h2>Global Platinum Securities Founding Student Members</h2>
	<?php $_from = $this->_tpl_vars['founding_student_members']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['member']):
?>
		<div style="float: left; height: 62px; margin-bottom: 15px; width: 253px;">
			<?php if ($this->_tpl_vars['member']['biography'] != ''): ?><a href="/membership/member.php?member_type=2&amp;id=<?php echo $this->_tpl_vars['member']['id']; ?>
"><?php endif; ?><strong><?php echo $this->_tpl_vars['member']['first_name']; ?>
 <?php echo $this->_tpl_vars['member']['last_name']; ?>
</strong><?php if ($this->_tpl_vars['member']['biography'] != ''): ?></a><?php endif; ?><br />
			<?php if ($this->_tpl_vars['member']['gps_position_held'] != ''): ?><strong><?php echo $this->_tpl_vars['member']['gps_position_held']; ?>
</strong><br /><?php endif; ?>
			<?php if ($this->_tpl_vars['member']['current_employer'] != ''): ?><?php echo $this->_tpl_vars['member']['current_employer']; ?>
<br /><?php endif; ?>
			<?php echo $this->_tpl_vars['member']['undergraduate_school']; ?>

		</div>
	<?php endforeach; endif; unset($_from); ?>
	<div style="clear: both;"></div>
	
	<h2>Global Platinum Securities Chapter Founders</h2>
	<?php $_from = $this->_tpl_vars['founding_chapter_members']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['member']):
?>
		<div style="float: left; height: 38px; margin-bottom: 15px; width: 253px;">
			<?php if ($this->_tpl_vars['member']['biography'] != ''): ?><a href="/membership/member.php?member_type=2&amp;id=<?php echo $this->_tpl_vars['member']['id']; ?>
"><?php endif; ?><strong><?php echo $this->_tpl_vars['member']['first_name']; ?>
 <?php echo $this->_tpl_vars['member']['last_name']; ?>
</strong><?php if ($this->_tpl_vars['member']['biography'] != ''): ?></a><?php endif; ?><br />
			<?php if ($this->_tpl_vars['member']['current_employer'] != ''): ?><?php echo $this->_tpl_vars['member']['current_employer']; ?>
<br /><?php endif; ?>
			<?php echo $this->_tpl_vars['member']['undergraduate_school']; ?>

		</div>
	<?php endforeach; endif; unset($_from); ?>
	<div style="clear: both;"></div>
</div>