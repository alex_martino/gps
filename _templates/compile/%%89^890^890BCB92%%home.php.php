<?php /* Smarty version 2.6.17, created on 2012-10-16 06:22:18
         compiled from /home/gpscom/public_html/_templates/home.php */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="google-site-verification" content="08XbOJ3unLgivbPHTxHpRZ17GT0gX-8DOf4IhvsFrmk" />
<title><?php echo $this->_tpl_vars['page_title']; ?>
</title>

<?php if (( $this->_tpl_vars['secure_url'] != 1 && $this->_tpl_vars['logged_in'] == 1 )): ?>
<!-- begin feedback script -->
<script type="text/javascript">
reformal_wdg_w    = "713";
reformal_wdg_h    = "460";
reformal_wdg_domain    = "gpsportal";
reformal_wdg_mode    = 0;
reformal_wdg_title   = "Portal Feedback";
reformal_wdg_ltitle  = "Feedback";
reformal_wdg_lfont   = "";
reformal_wdg_lsize   = "15px";
reformal_wdg_color   = "#FFA000";
reformal_wdg_bcolor  = "#516683";
reformal_wdg_tcolor  = "#FFFFFF";
reformal_wdg_align   = "left";
reformal_wdg_waction = 0;
reformal_wdg_vcolor  = "#9FCE54";
reformal_wdg_cmline  = "#E0E0E0";
reformal_wdg_glcolor  = "#105895";
reformal_wdg_tbcolor  = "#FFFFFF";
 
reformal_wdg_bimage = "bea4c2c8eb82d05891ddd71584881b56.png";
 
</script>

<script type="text/javascript" language="JavaScript" src="http://idea.informer.com/tabn2v4.js?domain=gpsportal"></script><noscript><a href="http://gpsportal.idea.informer.com">GPS Portal feedback</a> <a href="http://idea.informer.com"> Powered by <img src="http://widget.idea.informer.com/tmpl/images/widget_logo.jpg" /></a></noscript>
<! -- end feedback script -->
<?php endif; ?>

<link rel="stylesheet" type="text/css" href="/scripts/ext-2.0/resources/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/css/styles.css" />
<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => ($this->_tpl_vars['document_root'])."/includes/head.inc.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>

</head>
<body>
<div id="main_container">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td width="250"><div style="margin-left: 14px;"><a href="/"><img src="/images/logo.gif" width="233" height="64" alt="Global Platinum Security" /></a></div></td>
		<td>
			<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => ($this->_tpl_vars['document_root'])."/includes/login_box.inc.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>

			<!-- REMOVED SEARCH AS NO LONGER SUBSCRIBED <?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => ($this->_tpl_vars['document_root'])."/includes/search_box.inc.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>
 -->
		</td>
		</tr>
		</table>
		<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => ($this->_tpl_vars['document_root'])."/includes/top_menu.inc.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>

		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['page_file']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</td>
	<td style="border-left: 1px solid #FFFFFF; width: 50px;"><img src="/images/side_background.gif" width="50" height="465" alt="" /></td>
	</tr>
	</table>
	<div id="footer_container">
		<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => ($this->_tpl_vars['document_root'])."/includes/footer.inc.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>

	</div>
</div>
<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => ($this->_tpl_vars['document_root'])."/includes/google_analytics.inc.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>

</body>
</html>