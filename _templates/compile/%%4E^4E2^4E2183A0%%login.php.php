<?php /* Smarty version 2.6.17, created on 2012-10-16 15:41:22
         compiled from /home/gpscom/public_html/_pages/members/login.php */ ?>
<div><img src="/content_files/headers/members.gif" width="800" height="90" alt="members" /></div>
<div>
	<h2>GPS Member Login</h2>

<?php if ($this->_tpl_vars['session_expired'] == 1): ?>
Sorry, your session has expired. Please log in again.
<?php endif; ?>

<?php if ($this->_tpl_vars['update_required'] == 'true'): ?>
You are required to update your password. Please check your email for further instructions.<BR>
If you have trouble please contact: admin@gps100.com.
<?php else: ?>

	<p style="color: #FF0000; text-align: center;"><?php echo $this->_tpl_vars['error_message']; ?>
</p>
	<form id="form" action="<?php echo $this->_tpl_vars['php_self']; ?>
?referrer=<?php echo $this->_tpl_vars['referrer']; ?>
" method="post">
		<table align="center">
		<tr>
		<td align="right"><strong>Email Address:</strong></td>
		<td><input type="text" id="l_email_address" name="email_address" value="<?php echo $_POST['email_address']; ?>
" />
		</td>
		</tr>
		<tr>
		<td align="right"><strong>Password:</strong></td>
		<td><input type="password" id="l_password" name="password" value="<?php echo $_POST['password']; ?>
" onKeyPress="return submitenter(this,event)" />
		</td>
		</tr>
		<tr>
		<td>&nbsp;</td>
		<td style="padding-top: 2px; padding-bottom: 2px"><input type="checkbox" name="remember_me" value="1" onKeyPress="return submitenter(this,event)" /> Remember me</td>
		</tr>
		<tr>
		<td>&nbsp;</td>
		<td><a href="javascript: submitform('form');" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('login','','/content_files/images/buttons/login_over.gif',1)"><img name="login" border="0" src="/content_files/images/buttons/login_out.gif" width="57" height="17" alt="login" /></a></td>
		</tr>
		</table>
		<input type="hidden" id="action" name="action" value="login" /><br>
		<div style="text-align: center">(<a href="/members/passreset.php?request_reset=1">Forgotten Password?</a>)</div>
	</form>

<?php endif; ?>
</div>