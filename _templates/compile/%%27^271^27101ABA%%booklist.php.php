<?php /* Smarty version 2.6.17, created on 2012-10-24 12:56:59
         compiled from /home/gpscom/public_html/_pages/portal/booklist.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strlen', '/home/gpscom/public_html/_pages/portal/booklist.php', 33, false),array('modifier', 'urlencode', '/home/gpscom/public_html/_pages/portal/booklist.php', 39, false),array('modifier', 'explode', '/home/gpscom/public_html/_pages/portal/booklist.php', 43, false),array('modifier', 'count', '/home/gpscom/public_html/_pages/portal/booklist.php', 63, false),)), $this); ?>
<div><img src="/content_files/headers/booklist.gif" width="800" height="90"></div>
<div>
	<style>
	#sortable { list-style-type: none; margin: 0; padding: 0; width: 758px; }
	#sortable li { list-style-image: none; margin: 0 0 10px 0; font-size: 1.4em; height: 55px }
	#sortable div.position { float: left; width: 50px; text-align: center; font-size: 2em; background: #091c5a; color: #fff; padding-top: 7px; height: 48px }
	#sortable div.bookname { float: left; margin: 18px 0 0 30px; width: 240px }
	#sortable div.actions { float: left; width: 320px; margin: 0 0 0 10px }
	#sortable div.actions div.text { float: left; margin: 22px 0 0 10px; font-size: 12px; color: #091c5a }
	#sortable div.actions div.text a { color: #516683; text-decoration: underline }
	.bookworm { width: 50px; height: 50px; margin: 2px 0 0 8px; float: left }
	#sortable div.who { float: left; padding: 2px 0 0 0; width: 104px }
	#sortable div.who img { float: left; width: 25px; height: 25px; margin: 0 1px 1px 0 }
	#sortable div.who .more { float: left; font-size: 11px; margin: 6px 0 0 4px }
	</style>
	<script>
	$(function() {
		$( "#sortable" ).sortable({ disabled:true; });
		$( "#sortable" ).disableSelection();
	});
	</script>
<div style="height: 10px">&nbsp;</div>
<div style="float: left; padding: 5px 0 0 0; font-size: 15px; font-weight: bold"><?php echo $_SESSION['user_first_name']; ?>
's Top 10 Progress:
</div>
<div class="progress-outer" style="float: left; margin: 0 0 0 20px"><div class="progress-inner" style="width: <?php echo $this->_tpl_vars['topcount']; ?>
%"><?php echo $this->_tpl_vars['topcount']; ?>
%</div></div>
<div style="float: left; margin: 5px 0 0 0; text-align: right; width: 225px"><a href="/portal/network/view/interests.php?id=<?php echo $_SESSION['user_id']; ?>
">Your other interests...</a></div>
<div style="clear: both; height: 30px">&nbsp;</div>

<ul id="sortable">
<?php $_from = $this->_tpl_vars['booklist']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?>
	<li class="ui-state-default">
		<div class="position"><?php echo $this->_tpl_vars['k']+1; ?>
</div>
		<div class="bookname"<?php if (((is_array($_tmp=$this->_tpl_vars['i']['name'])) ? $this->_run_mod_handler('strlen', true, $_tmp) : strlen($_tmp)) > 29): ?> style="margin: 8px 0 0 30px;"<?php endif; ?>><?php echo $this->_tpl_vars['i']['name']; ?>
</div>
		<div class="actions">
		<?php if (( $this->_tpl_vars['i']['read_flag'] )): ?>
		<img src="/images/book-worm.gif" class="bookworm">
		<div class="text">You've read this!</div>
		<?php else: ?>
		<div class="text"><a href="booklist.php?add_to_list=<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['name'])) ? $this->_run_mod_handler('urlencode', true, $_tmp) : urlencode($_tmp)); ?>
">I've read this too!</a></div>
		<?php endif; ?>
		</div>
		<div class="who">
		<?php $this->assign('users', ((is_array($_tmp=",")) ? $this->_run_mod_handler('explode', true, $_tmp, $this->_tpl_vars['i']['users']) : explode($_tmp, $this->_tpl_vars['i']['users']))); ?>
		<?php $this->assign('users_fullnames', ((is_array($_tmp=",")) ? $this->_run_mod_handler('explode', true, $_tmp, $this->_tpl_vars['i']['user_fullname']) : explode($_tmp, $this->_tpl_vars['i']['user_fullname']))); ?>
		<?php $this->assign('more', 0); ?>
		<?php $_from = $this->_tpl_vars['users']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['l'] => $this->_tpl_vars['uid']):
?>
		    <?php if ($this->_tpl_vars['l'] < 5): ?>
		    <a href="/portal/network/view/interests.php?id=<?php echo $this->_tpl_vars['uid']; ?>
">
		    <img src="/content_files/member_photos/<?php echo $this->_tpl_vars['uid']; ?>
-cropped.jpg" alt="" title="<?php echo $this->_tpl_vars['users_fullnames'][$this->_tpl_vars['l']]; ?>
">
		    </a>
		    <?php else: ?>
		    <?php $this->assign('more', $this->_tpl_vars['more']+1); ?>
		    <?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		<div class="more">
			<a href="javascript:void(0);" onClick="popup_display_users('<?php $_from = $this->_tpl_vars['users']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['j']):
?>,<?php echo $this->_tpl_vars['j']; ?>
<?php endforeach; endif; unset($_from); ?>','Readers of <?php echo $this->_tpl_vars['i']['name']; ?>
');">
				<?php if (( $this->_tpl_vars['more'] > 1 )): ?>
				+ <?php echo $this->_tpl_vars['more']; ?>
 others
				<?php elseif (( $this->_tpl_vars['more'] > 0 )): ?>
				+ 1 other
			</a>
			<?php else: ?>
				<div style="float: left; width: 74px; text-align: right"><?php echo count($this->_tpl_vars['users']); ?>
 total</div>
		    <?php endif; ?>
		    </div>
		</div>
	</li>
<?php endforeach; endif; unset($_from); ?>
</ul>

</div>