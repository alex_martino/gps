<?php /* Smarty version 2.6.17, created on 2012-12-07 17:50:16
         compiled from /home/gpscom/public_html/_pages/portal/admin/payments_invoices.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', '/home/gpscom/public_html/_pages/portal/admin/payments_invoices.php', 33, false),array('modifier', 'date_format', '/home/gpscom/public_html/_pages/portal/admin/payments_invoices.php', 36, false),array('modifier', 'capitalize', '/home/gpscom/public_html/_pages/portal/admin/payments_invoices.php', 38, false),)), $this); ?>
<div><img src="/content_files/headers/payments.gif" width="800" height="90"></div>
<script>
function invoice_open(id) {
window.open("https://www.gps100.com/includes/payments/invoice_details.php?id="+id,"invoice","width=900, height=600, left="+((screen.width/2)-450)+",top="+((screen.height/2)-330));
}
</script>
<div>
<h2>Invoices</h2>
<?php if ($this->_tpl_vars['error'] > 0): ?>
<div style="color:navy">
<?php echo $this->_tpl_vars['errormsg']; ?>

</div>
<BR>
<?php endif; ?>

<table class="admin_table" style="width: 100%; border: 1px solid black">

<tr style="border-bottom: 1px solid black">

<th style="width: 20px">ID</th>
<th>User</th>
<th>Description</th>
<th>Amount</th>
<th style="width: 100px">Created</th>
<th>Updated</th>
<th>Status</th>
</tr>

<?php $_from = $this->_tpl_vars['invoices']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?>

<tr class="row" onClick="invoice_open('<?php echo $this->_tpl_vars['i']['id']; ?>
');">
<td><?php echo $this->_tpl_vars['i']['id']; ?>
</td>
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['name'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 30, "...", true) : smarty_modifier_truncate($_tmp, 30, "...", true)); ?>
</td>
<td><?php if (( $this->_tpl_vars['i']['annual_dues_flag'] )): ?>Annual dues: <?php endif; ?><?php echo $this->_tpl_vars['i']['description']; ?>
</td>
<td><?php echo $this->_tpl_vars['i']['amount']; ?>
</td>
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['created'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d/%m/%y") : smarty_modifier_date_format($_tmp, "%d/%m/%y")); ?>
</td>
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['updated'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d/%m/%y") : smarty_modifier_date_format($_tmp, "%d/%m/%y")); ?>
</td>
<td><b><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['status'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</b></td>
</tr>


<?php endforeach; endif; unset($_from); ?>

</table>

</div>