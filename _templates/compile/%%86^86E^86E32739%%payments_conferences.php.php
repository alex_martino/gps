<?php /* Smarty version 2.6.17, created on 2013-02-10 11:31:37
         compiled from /home/gpscom/public_html/_pages/portal/admin/payments_conferences.php */ ?>
<div><img src="/content_files/headers/payments.gif" width="800" height="90"></div>
<script>
function invoice_open(id) {
window.open("https://www.gps100.com/includes/payments/invoice_details.php?id="+id,"invoice","width=900, height=600, left="+((screen.width/2)-450)+",top="+((screen.height/2)-330));
}
function validate_form() {
	conference = document.getElementById("conference");
	analyst_price = document.getElementById("analyst_price");
	member_price = document.getElementById("member_price");
	alumni_price = document.getElementById("alumni_price");
	conference_user = document.getElementById("conference_user");
	
	if (conference.value == 0) {
		conference.style.color = 'red';
		return false;
	}
	else {
		conference.style.color = 'black';
	}
	
	if ((parseInt(analyst_price.value) + parseInt(member_price.value) + parseInt(alumni_price.value)) == 0) {
		analyst_price.style.color = 'red';
		member_price.style.color = 'red';
		alumni_price.style.color = 'red';
		return false;
	}
	else {
		analyst_price.style.color = 'black';
		member_price.style.color = 'black';
		alumni_price.style.color = 'black';
	}

	if (conference_user.value.length == 0) {
		document.getElementById("names_desc").style.color = 'red';
		return false;
	}
	else {
		document.getElementById("names_desc").style.color = 'black';
	}
	
	
}
</script>
<div>
<h2>Conferences</h2>
<?php if ($this->_tpl_vars['error'] > 0): ?>
<div style="color:navy">
<?php echo $this->_tpl_vars['errormsg']; ?>

</div>
<BR>
<?php endif; ?>

<?php if (( $this->_tpl_vars['no_conferences'] == 1 )): ?>
No conferences have been invoiced yet.
<?php else: ?>
<style>
th { font-weight: bold; width: 60px }
</style>
<table style="width: 700px">
<tr><th style="width: 100px">Name</th><th>Paid</th><th>Unpaid</th><th>Overdue</th><th>First Invoice</th><th>Last Payment</th></tr>
<?php $_from = $this->_tpl_vars['conference_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
	<tr>
		<td><?php echo $this->_tpl_vars['i']['name']; ?>
</td>
		<td>$<?php echo $this->_tpl_vars['i']['paid']; ?>
</td>
		<td>$<?php echo $this->_tpl_vars['i']['unpaid']; ?>
</td>
		<td>$<?php echo $this->_tpl_vars['i']['overdue']; ?>
</td>
		<td><?php echo $this->_tpl_vars['i']['first_invoice']; ?>
</td>
		<td><?php echo $this->_tpl_vars['i']['last_payment']; ?>
</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php endif; ?>
<BR><BR>
<span class="button default strong"><input type="button" value="Add Conference" class="submit" id="addbutton" onClick="showform(); return false;" /></span>
</div>

<div style="clear:both">&nbsp;</div>
    <div id="page_screen">

        &nbsp;
    </div>

<div id="addform" style="display:none; border-radius:15px; -moz-border-radius: 15px; height: 340px">
<form name="addform" action="payments_conferences.php?add_conference=1" method="POST" onSubmit="return validate_form()">
<div class="network_block_edit" style="margin-top: 0">
<div class="dataform">
<div class="data_title_r" style="padding-top: 3px">
Conference
</div>
<div style="float:left" class="data_data_edit">
<select name="conference" id="conference">
<option value="0">** Please Select **</option>
<option value="2012 (Spring)">2012 (Spring)</option>
<option value="2012 (Summer)">2012 (Summer)</option>
<option value="2012 (Fall)">2012 (Fall)</option>
</select>
</div>
<BR>
<hr class="editline">
<b>Price</b>
<div>
<div style="float: left; width: 150px;">Analysts <input type="text" name="analyst_price" id="analyst_price" class="edittextw" value="0" MAXLENGTH=25 style="width: 40px; text-align: center"></div>
<div style="float: left; width: 150px;">Members <input type="text" name="member_price" id="member_price" class="edittextw" value="0" MAXLENGTH=25 style="width: 40px; text-align: center"></div>
<div style="float: left; width: 150px;">Alumni <input type="text" name="alumni_price" id="alumni_price" class="edittextw" value="0" MAXLENGTH=25 style="width: 40px; text-align: center"></div>
</div>
<BR>
<hr class="editline">
<div id="names_desc">Type the attendees name in the field below...</div>
<script>
			window.addEvent('load', function(){
				// Autocomplete with poll the server as you type
				var t1 = new TextboxList('conference_user', {unique: true, plugins: {autocomplete: {
					minLength: 1,
					queryRemote: true, onlyFromValues: true,
					remote: {url: '/includes/autocomplete_members.php?no_image=1&hidden=1'}
				}}});
			});
</script>
<style>
.textboxlist { width: 565px }
</style>
<div style="margin: 10px 0 10px 0; height: 150px; overflow-y: scroll">
	<input type="text" id="conference_user" name="conference_user">
</div>

<hr class="editline">
<div style="float:left;"><input type="button" value="Cancel" onClick="hideform();"></div>
<div style="float:right"><input type="submit" value="Next"></div>
</div>

</div>
</form>
</div>