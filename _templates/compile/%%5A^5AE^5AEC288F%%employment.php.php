<?php /* Smarty version 2.6.17, created on 2012-10-16 16:24:10
         compiled from /home/gpscom/public_html/_pages/portal/network/view/employment.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'capitalize', '/home/gpscom/public_html/_pages/portal/network/view/employment.php', 64, false),array('modifier', 'substr', '/home/gpscom/public_html/_pages/portal/network/view/employment.php', 99, false),array('modifier', 'date_format', '/home/gpscom/public_html/_pages/portal/network/view/employment.php', 99, false),)), $this); ?>
<div><img src="/content_files/headers/network.gif" width="800" height="90">

<?php if ($this->_tpl_vars['not_viewable']): ?>
<div style="margin-top: 15px; margin-left: 10px">
<p>This profile is not available. <a href="javascript:history.go(-1)">Go back.</a></p>

<?php else: ?>
<div id="network_t_menu">
  <ul>
<?php $_from = $this->_tpl_vars['network_t_menu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
    <li<?php if ($this->_tpl_vars['i']['current'] == '1'): ?> id="current"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['i']['url']; ?>
?id=<?php echo $this->_tpl_vars['profile']['id']; ?>
"><?php echo $this->_tpl_vars['i']['name']; ?>
</a></li>
<?php endforeach; endif; unset($_from); ?>
  </ul>
</div>
</div>
<div style="margin-top: 25px">

<?php if ($this->_tpl_vars['can_edit']): ?>
<div class="editlink">
<?php if ($this->_tpl_vars['is_editing']): ?>
<a href="#" onClick="document.do_edit.submit(); return false;">Save</a> |
<a href="employment.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
">Cancel</a>
<?php else: ?>
<a href="employment.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
&edit=1">Edit</a>
<?php endif; ?>
</div>
<?php endif; ?>

<h2><?php echo $this->_tpl_vars['profile']['first_name']; ?>
 <?php echo $this->_tpl_vars['profile']['last_name']; ?>
</h2>

<div style="float:left" style="width: 150px">

<?php if ($this->_tpl_vars['display_photo']): ?>

<img name="profile_img" src="<?php echo $this->_tpl_vars['profile']['current_photo']; ?>
" width="150" alt="Photo of <?php echo $this->_tpl_vars['profile']['first_name']; ?>
 <?php echo $this->_tpl_vars['profile']['last_name']; ?>
"
<?php if ($this->_tpl_vars['profile']['school_photo']): ?>
<?php endif; ?>
/>

<?php else: ?>

<img src="/images/male.png" width="150px" alt="Photo missing" />

<?php endif; ?>

<?php if ($this->_tpl_vars['linkedin']): ?>

<div style="width: 150px; text-align: center; padding: 5px 0 0 0">
<a href="<?php echo $this->_tpl_vars['profile']['linkedin']; ?>
"><img src="/content_files/images/icons/linkedin.png" style="width: 18px; height: 18px"></a>
</div>



<?php endif; ?>

<?php if ($this->_tpl_vars['is_editing']): ?>
<script>
			window.addEvent('load', function(){
				
				// Autocomplete initialization
				var t1 = new TextboxList('employer_add', {unique: true, max: 1, plugins: {autocomplete: {}}});
<?php $_from = $this->_tpl_vars['tags']['sector']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ttype'] => $this->_tpl_vars['i']):
?>
t1.add('<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
');
<?php endforeach; endif; unset($_from); ?>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t1.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_jobs.php', onSuccess: function(r){
					t1.plugins['autocomplete'].setValues(r);
					t1.container.removeClass('textboxlist-loading');
				}}).send();
			});
</script>
<BR>
<div id="addbutton"<?php if ($this->_tpl_vars['error']): ?> style="display:none"<?php endif; ?> style="text-align: center">
<form name="addjob">
&nbsp; <input type="button" value="Add Job" onClick="showform();">
</form>
</div>
<?php endif; ?>

</div>

<div class="network_data" style="width: 580px">

<?php if (! $this->_tpl_vars['jobs']): ?>
No jobs listed
<?php endif; ?>

	<?php $_from = $this->_tpl_vars['jobs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<div style="clear:left">
<div style="float:left" class="data_title">
<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['i']['start'])) ? $this->_run_mod_handler('substr', true, $_tmp, 0, 7) : substr($_tmp, 0, 7)))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %Y") : smarty_modifier_date_format($_tmp, "%b %Y")); ?>
 -
<?php if ($this->_tpl_vars['i']['current']): ?>Present<?php else: ?>
<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['i']['end'])) ? $this->_run_mod_handler('substr', true, $_tmp, 0, 7) : substr($_tmp, 0, 7)))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %Y") : smarty_modifier_date_format($_tmp, "%b %Y")); ?>

<?php endif; ?>
</div>
<div class="data_data">
<b style="font-size: 15px"><?php echo $this->_tpl_vars['i']['employer']; ?>
</b>
<?php if ($this->_tpl_vars['i']['division']): ?>|<?php endif; ?> <span style="font-size: 14px"><?php echo $this->_tpl_vars['i']['division']; ?>
</span>
<?php if ($this->_tpl_vars['is_editing']): ?>
<a href="#" onClick="show_edit_form('<?php echo $this->_tpl_vars['i']['id']; ?>
','<?php echo $this->_tpl_vars['i']['employer']; ?>
','<?php echo $this->_tpl_vars['i']['division']; ?>
','<?php echo $this->_tpl_vars['i']['title']; ?>
','<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['i']['start'])) ? $this->_run_mod_handler('substr', true, $_tmp, 0, 7) : substr($_tmp, 0, 7)))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m") : smarty_modifier_date_format($_tmp, "%m")); ?>
','<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['i']['start'])) ? $this->_run_mod_handler('substr', true, $_tmp, 0, 7) : substr($_tmp, 0, 7)))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y") : smarty_modifier_date_format($_tmp, "%Y")); ?>
','<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['i']['end'])) ? $this->_run_mod_handler('substr', true, $_tmp, 0, 7) : substr($_tmp, 0, 7)))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m") : smarty_modifier_date_format($_tmp, "%m")); ?>
','<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['i']['end'])) ? $this->_run_mod_handler('substr', true, $_tmp, 0, 7) : substr($_tmp, 0, 7)))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y") : smarty_modifier_date_format($_tmp, "%Y")); ?>
','<?php echo $this->_tpl_vars['i']['current']; ?>
','<?php echo $this->_tpl_vars['i']['i_id']; ?>
');"><img src="/content_files/images/edit.png" class="edit"></a>
<a href="employment.php?id=<?php echo $this->_tpl_vars['profile']['id']; ?>
&deljob=<?php echo $this->_tpl_vars['i']['id']; ?>
"><img src="/content_files/images/delete.gif" class="delete"
onclick="return confirm('Are you sure you want to delete this job?')"></a>
<?php endif; ?><BR>
<i style="font-size: 13px; line-height: 13px"><?php echo $this->_tpl_vars['i']['title']; ?>
</i><BR><BR>
</div>
</div>
<?php endforeach; endif; unset($_from); ?>

<?php if ($this->_tpl_vars['is_editing']): ?>
<form name="do_edit" action="employment.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
" method="POST">
<input type="hidden" name="submitted" value=1>
<div style="clear:both">
<div style="float:left" class="data_title">
LinkedIn URL:
</div>
<div style="float:left" class="job_info">
<input type="text" name="linkedinurl" value="<?php if ($this->_tpl_vars['profile']['linkedin']): ?><?php echo $this->_tpl_vars['profile']['linkedin']; ?>
<?php else: ?>Your Public LinkedIn URL<?php endif; ?>" style="width: 350px" onClick="this.value='';" onFocus="this.value='';">
</div>
</div>
  <?php if ($this->_tpl_vars['jobs']): ?>
<BR><BR>
<div style="clear:both">
<div style="float:left" class="data_title">
Current Job:
</div>
<div style="float:left" class="job_info">
<select name="currentjob">
<option value="0">* None *</option>
<?php $_from = $this->_tpl_vars['jobs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']['id']; ?>
" <?php if (( $this->_tpl_vars['i']['id'] == $this->_tpl_vars['profile']['current_job_id'] )): ?>SELECTED<?php endif; ?>><?php echo $this->_tpl_vars['i']['employer']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select><BR>
<p class="note">This job will show in search results and in your public profile</p>
<BR>
</div>
</div>
  <?php endif; ?>

</form>
<?php endif; ?>

</div>
<div style="clear:both">&nbsp;</div>

<?php if ($this->_tpl_vars['is_editing']): ?>

    <div id="page_screen">
        &nbsp;
    </div>

<div id="addform" style="display:<?php if ($this->_tpl_vars['error']): ?>block<?php else: ?>none<?php endif; ?>;border-radius:15px; -moz-border-radius: 15px">
<?php if ($this->_tpl_vars['error']): ?>
<div class="error" id="error" style="text-align: center; width: 100%; margin-top: 5px"><?php echo $this->_tpl_vars['errormsg']; ?>
</div>
<?php endif; ?>
<form name="edit_profile" action="/portal/network/view/employment.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
&edit=1" method="POST">
<div class="network_block_edit">
<div class="dataform" style="border-bottom: 0; padding-bottom: 0">
<div class="data_title_r">
Employer
</div>
<div style="float:left" class="data_data_edit">
<input type="text" name="employer" class="edittextw" value="" id="employer_add"><BR>
</div>
</div>
<div class="dataform">
<div class="data_title_r">
Division<BR>
Title<BR>
Industry<BR>
Start Date<BR>
End Date<BR>
Current Job?
</div>
<div style="float:left" class="data_data_edit">
<input type="text" name="division" class="edittextw" value="<?php echo $this->_tpl_vars['do_division']; ?>
"><BR>
<input type="text" name="title" class="edittextw" value="<?php echo $this->_tpl_vars['do_title']; ?>
"><BR>
<select name="industry">
<option value="0">** Please Select **</option>
<?php $_from = $this->_tpl_vars['industries']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']['id']; ?>
"><?php echo $this->_tpl_vars['i']['name']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select><BR>
<input type="text" name="startmonth" id="startmonth" class="edittext" size=2 value="<?php if (isset ( $this->_tpl_vars['do_start_month'] )): ?><?php echo $this->_tpl_vars['do_start_month']; ?>
<?php else: ?>MM<?php endif; ?>" onClick="this.value='';">
<input type="text" name="startyear" id="startyear" class="edittext" size=4 value="YYYY" onClick="this.value='';"><BR>
<div style="display:inline" id="end"><input type="text" name="endmonth" class="edittext" size=2 value="MM" onClick="this.value='';">
<input type="text" name="endyear" class="edittext" size=4 value="YYYY" onClick="this.value='';"><BR></div>
<input type="checkbox" name="current" value="1" onClick="togglecurrent();">
</div>
<hr class="editline">
<div style="float:left;"><input type="button" value="Cancel" onClick="hideform();"></div>
<div style="float:right"><input type="submit" value="Add Job"></div>
</div>

</div>
</form>
</div>

<div id="editform" style="display:<?php if ($this->_tpl_vars['edit_error']): ?>block<?php else: ?>none<?php endif; ?>; border-radius:15px; -moz-border-radius: 15px">
<form name="edit_job" action="/portal/network/view/employment.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
&edit=1&e_job=1" method="POST">
<input type="hidden" name="id" value="">
<div class="network_block_edit" style="background-color:#ededed">
<div class="dataform">
<div class="data_title_r">
Employer<BR>
Division<BR>
Title<BR>
Industry<BR>
Start Date<BR>
End Date<BR>
Current Job?
</div>
<div style="float:left" class="data_data_edit">
<input type="text" name="employer" class="edittextw" value="" id="employer_edit" readonly style="background: #ededed; border: 0"><BR>
<input type="text" name="division" class="edittextw" value=""><BR>
<input type="text" name="title" class="edittextw" value=""><BR>
<select name="industry">
<option value="0">** Please Select **</option>
<?php $_from = $this->_tpl_vars['industries']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']['id']; ?>
"><?php echo $this->_tpl_vars['i']['name']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select><BR>
<input type="text" name="startmonth" class="edittext" size=2 value="">
<input type="text" name="startyear" class="edittext" size=4 value=""><BR>
<div style="display:inline" id="edit_end"><input type="text" name="endmonth" class="edittext" size=2 value="" onClick="">
<input type="text" name="endyear" class="edittext" size=4 value="" onClick=""><BR></div>
<input type="checkbox" name="current" value="1" onClick="toggle_edit_current();">
</div>
<hr class="editline">
<div style="float:left;"><input type="button" value="Cancel" onClick="hide_edit_form();"></div>
<div style="float:right"><input type="submit" value="Save"></div>
</div>

</div>
</form>

<?php endif; ?>

</div>

<?php if ($this->_tpl_vars['error']): ?>
<script>
showform();
</script>
<?php endif; ?>

<?php endif; ?>