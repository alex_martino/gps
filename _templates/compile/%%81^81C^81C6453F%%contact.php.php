<?php /* Smarty version 2.6.17, created on 2012-10-16 16:24:23
         compiled from /home/gpscom/public_html/_pages/portal/network/view/contact.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', '/home/gpscom/public_html/_pages/portal/network/view/contact.php', 243, false),)), $this); ?>
<div><img src="/content_files/headers/network.gif" width="800" height="90">

<?php if ($this->_tpl_vars['not_viewable']): ?>
<div style="margin-top: 15px; margin-left: 10px">
<p>This profile is not available. <a href="javascript:history.go(-1)">Go back.</a></p>

<?php else: ?>
<div id="network_t_menu">
  <ul>
<?php $_from = $this->_tpl_vars['network_t_menu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
    <li<?php if ($this->_tpl_vars['i']['current'] == '1'): ?> id="current"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['i']['url']; ?>
?id=<?php echo $this->_tpl_vars['profile']['id']; ?>
"><?php echo $this->_tpl_vars['i']['name']; ?>
</a></li>
<?php endforeach; endif; unset($_from); ?>
  </ul>
</div>
</div>

<div style="margin-top: 25px">

<?php if ($this->_tpl_vars['can_edit']): ?>
<div class="editlink">
<?php if ($this->_tpl_vars['is_editing']): ?>
<a href="#" onClick="document.do_edit.submit(); return false;">Save</a> |
<a href="contact.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
">Cancel</a>
<?php else: ?>
<a href="contact.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
&edit=1">Edit</a>
<?php endif; ?>
</div>
<?php endif; ?>

<h2><?php echo $this->_tpl_vars['profile']['first_name']; ?>
 <?php echo $this->_tpl_vars['profile']['last_name']; ?>
</h2>

<div style="float:left" style="width: 150px">

<?php if ($this->_tpl_vars['display_photo']): ?>

<img name="profile_img" src="<?php echo $this->_tpl_vars['profile']['current_photo']; ?>
" width="150" alt="Photo of <?php echo $this->_tpl_vars['profile']['first_name']; ?>
 <?php echo $this->_tpl_vars['profile']['last_name']; ?>
"
<?php if ($this->_tpl_vars['profile']['school_photo']): ?>
<?php endif; ?>
/>

<?php else: ?>

<img src="/images/male.png" width="150" alt="Photo missing" />

<?php endif; ?>

</div>

<?php if (( $this->_tpl_vars['is_editing'] )): ?>

<form name="do_edit" action="contact.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
&edit=1" method="POST" accept-charset="utf-8">
<input type="hidden" name="submitted" value="1">

<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Gmail Address</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">

			<input type="text" name="gmail_address" value="<?php echo $this->_tpl_vars['profile']['gmail_address']; ?>
" id="gmail_address" style="width: 250px" />
<BR>
		<p class="note" style="margin-top: 3px">This is useful for when we need to share Google Docs</p>

<?php if ($this->_tpl_vars['errormsg']['gmail']): ?>
<div class="error" style="margin-left: 0"><?php echo $this->_tpl_vars['errormsg']['gmail']; ?>
</div>
<?php endif; ?>

</div>

</div>

<BR><BR>


		</fieldset>


		<fieldset>
		<legend>Contact Number</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">
<select name="contact_number_country">
<option value="225_1">United States (1)</option>
<option value="224_44">United Kingdom (44)</option>
<?php $_from = $this->_tpl_vars['countries']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']['id']; ?>
_<?php echo $this->_tpl_vars['i']['country_code']; ?>
" <?php if (( $this->_tpl_vars['profile']['contact_number_country_id'] == $this->_tpl_vars['i']['id'] )): ?>SELECTED<?php endif; ?>><?php echo $this->_tpl_vars['i']['country_name']; ?>
 (<?php echo $this->_tpl_vars['i']['country_code']; ?>
)</option>
<?php endforeach; endif; unset($_from); ?>
</select>
			<input type="text" name="contact_number" value="<?php echo $this->_tpl_vars['profile']['contact_number']; ?>
" id="contact_number" />

<?php if ($this->_tpl_vars['errormsg']['number']): ?>
<BR><BR><div class="error" style="margin-left: 0"><?php echo $this->_tpl_vars['errormsg']['number']; ?>
</div>
<?php endif; ?>

</div>
</div>

<BR><BR>

</fieldset>

		<fieldset>
		<legend>Skype Username</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">
			<input type="text" name="skype" value="<?php echo $this->_tpl_vars['profile']['skype']; ?>
" id="skype" />

<?php if ($this->_tpl_vars['errormsg']['skype']): ?>
<BR><BR><div class="error" style="margin-left: 0"><?php echo $this->_tpl_vars['errormsg']['skype']; ?>
</div>
<?php endif; ?>

</div>
</div>

<BR><BR>

</fieldset>

		<fieldset>
		<legend>Email Redirect (Not editable)</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">
			<input type="text" value="<?php echo $this->_tpl_vars['profile']['email_address']; ?>
" style="width: 250px" readonly />
<BR>
		<p class="note" style="margin-top: 3px">To update where this redirects, please contact the VP of IR</p>
</div>
</div>

<BR><BR>

</fieldset>

		<fieldset>
		<legend>Mailing Address</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">
			<textarea name="address" id="mailing_address" style="width: 400px; height: 70px"><?php echo $this->_tpl_vars['profile']['address']; ?>
</textarea>
<BR><BR>
			<input type="checkbox" name="address_private" value="1"<?php if (( $this->_tpl_vars['profile']['address_private'] )): ?> checked<?php endif; ?>> Make address confidential (UM & Board only)
</div>
</div>

<BR><BR>

</fieldset>

		<fieldset>
		<legend>Alternative Email</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">

			<input type="text" name="alternative_email_1" value="<?php echo $this->_tpl_vars['profile']['alternative_email_1']; ?>
" id="alternative_email_1" style="width: 250px" />
<BR>
		<p class="note" style="margin-top: 3px">If you would like to be able to send in articles from another email address, add the email here.</p>

<?php if ($this->_tpl_vars['errormsg']['alternative_email_1']): ?>
<div class="error" style="margin-left: 0"><?php echo $this->_tpl_vars['errormsg']['alternative_email_1']; ?>
</div>
<?php endif; ?>

</div>

</div>

<BR><BR>


		</fieldset>

</div>

</form>

<?php else: ?>

<div class="network_data">

<?php if ($this->_tpl_vars['profile']['email_address']): ?>
<div id="sectors" class="network_block_top">
<div style="float:left" class="data_title">
Email
</div>
<div style="float:left" class="data_data">
<?php echo $this->_tpl_vars['profile']['email_address']; ?>

</div>
</div>
<BR><BR>
<?php endif; ?>

<?php if ($this->_tpl_vars['profile']['gmail_address']): ?>
<div id="sectors" class="network_block">
<div style="float:left" class="data_title">
Gmail
</div>
<div style="float:left" class="data_data">
<?php echo $this->_tpl_vars['profile']['gmail_address']; ?>

</div>
</div>
<BR><BR>
<?php endif; ?>

<?php if ($this->_tpl_vars['profile']['contact_number']): ?>
<div id="sectors" class="network_block">
<div style="float:left" class="data_title">
Contact Number
</div>
<div style="float:left" class="data_data">
<?php if (( $this->_tpl_vars['profile']['valid_us_number'] )): ?>
<?php echo $this->_tpl_vars['profile']['valid_us_number']; ?>

<?php else: ?>
<?php echo $this->_tpl_vars['profile']['contact_number_country_code']; ?>
 <?php echo $this->_tpl_vars['profile']['contact_number']; ?>

<?php endif; ?>
</div>
</div>
<BR><BR>
<?php endif; ?>

<?php if ($this->_tpl_vars['profile']['skype']): ?>
<div id="sectors" class="network_block">
<div style="float:left" class="data_title">
Skype
</div>
<div style="float:left" class="data_data">
<a href="skype:<?php echo $this->_tpl_vars['profile']['skype']; ?>
?call"><img src="http://download.skype.com/share/skypebuttons/buttons/call_green_transparent_70x23.png" style="border: none;" width="70" height="23" alt="Skype Me�!" /></a>
<a href="skype:<?php echo $this->_tpl_vars['profile']['skype']; ?>
?add"><img src="http://download.skype.com/share/skypebuttons/buttons/add_green_transparent_118x23.png" style="border: none;" width="118" height="23" alt="Add me to Skype" /></a>
</div>
</div>
<BR><BR>
<?php endif; ?>

<?php if ($this->_tpl_vars['profile']['address'] && ! $this->_tpl_vars['profile']['address_private']): ?>
<div id="address" class="network_block">
<div style="float:left" class="data_title">
Mailing Address
</div>
<div style="float:left" class="data_data">
<?php echo ((is_array($_tmp=$this->_tpl_vars['profile']['address'])) ? $this->_run_mod_handler('replace', true, $_tmp, "\n", "<BR>") : smarty_modifier_replace($_tmp, "\n", "<BR>")); ?>

</div>
</div>
<BR><BR>
<?php endif; ?>

<?php if ($this->_tpl_vars['can_edit']): ?>
<div id="sectors" class="network_block">
<div style="float:left" class="data_title">
Alternative Email
</div>
<div style="float:left" class="data_data">
Only you can see this email address - <a href="contact.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
&edit=1">view it</a>.
</div>
</div>
<BR><BR>
<?php endif; ?>

</div>

<?php endif; ?>

<div style="clear:both">&nbsp;</div>

<?php endif; ?>

</div>