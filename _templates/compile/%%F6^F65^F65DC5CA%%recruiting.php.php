<?php /* Smarty version 2.6.17, created on 2012-10-16 08:20:53
         compiled from /home/gpscom/public_html/_pages/contact/recruiting.php */ ?>
<div><img src="/content_files/headers/contact.gif" alt="" width="800" height="90" /></div>
<div>
<p>All recruiting inquiries should be directed to the respective school. Please submit resumes, cover letters, and questions to one of the following email addresses:</p>
<ul>
<li><a href="mailto:recruitment.colorado@gps100.com">recruitment.colorado@gps100.com</a></li>
<li><a href="mailto:recruitment.dayton@gps100.com">recruitment.dayton@gps100.com</a></li>
<li><a href="mailto:recruitment.georgetown@gps100.com">recruitment.georgetown@gps100.com</a></li>
<li><a href="mailto:recruitment.harvard@gps100.com">recruitment.harvard@gps100.com</a></li>
<li><a href="mailto:recruitment.lse@gps100.com">recruitment.lse@gps100.com</a></li>
<li><a href="mailto:recruitment.mit@gps100.com">recruitment.mit@gps100.com</a></li>
<li><a href="mailto:recruitment.nyu@gps100.com">recruitment.nyu@gps100.com</a></li>
<li><a href="mailto:recruitment.penn@gps100.com">recruitment.penn@gps100.com</a></li>
</ul>
<p>&nbsp;</p>
</div>