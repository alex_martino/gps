<?php /* Smarty version 2.6.17, created on 2012-11-01 12:32:26
         compiled from /home/gpscom/public_html/_pages/members/passreset.php */ ?>
<div><img src="/content_files/headers/members.gif" width="800" height="90"></div>
<div>
	<h2>Password Reset</h2>

<?php if ($this->_tpl_vars['request_reset'] == 1): ?>
<?php if ($this->_tpl_vars['error'] == 1): ?>
<p style="color:red"><?php echo $this->_tpl_vars['error_message']; ?>
</p>
<?php endif; ?>
<?php if ($this->_tpl_vars['email_success'] != 1): ?>
<p>Please enter your GPS email address to request a password reset.</p>
<form id="request_reset" action="passreset.php?request_reset=1" method="post">
<input type="hidden" name="request_reset" value="1">
<input type="text" id="email" name="email" value="<?php echo $this->_tpl_vars['r_email']; ?>
" style="width: 180px"><BR>
<input type="submit" value="Submit">
</form>
<?php else: ?>
<p>Email found - please check your email for further instructions.</p>
<?php endif; ?>
<?php else: ?>

	<p style="color: #FF0000; text-align: left;">
		<?php echo $this->_tpl_vars['error_message']; ?>

	</p>
<?php if ($this->_tpl_vars['success'] == 1): ?>
<p>
Password successfully updated.
</p>
<?php elseif ($this->_tpl_vars['code_match'] == 1): ?>

	<form id="form" action="<?php echo $this->_tpl_vars['php_self']; ?>
?code=<?php echo $this->_tpl_vars['code']; ?>
&email=<?php echo $this->_tpl_vars['email']; ?>
" method="post" enctype="multipart/form-data">
		<fieldset>
		<legend>Password Change</legend>
<div style="float:left">
		<p style="float: left;">New Password:<br />
			<input type="password" id="new_password" name="new_password" value="<?php echo $this->_tpl_vars['new_password']; ?>
" style="width: 230px;" />
		</p>
		<br style="clear: both;" />
		<p style="float: left;">New Password Again:<br />
			<input type="password" id="new_password_2" name="new_password_2" value="<?php echo $this->_tpl_vars['new_password_2']; ?>
" style="width: 230px;" />
		</p>
</div>
<div style="float:left; margin-left: 30px; text-align: center; border: 1px dotted black; padding: 4px">
<b>Rules</b><BR>
6 - 14 characters<BR>
1+ lower case letters<BR>
1+ upper case letters<BR>
1+ numbers
</div>
		</fieldset>
<input type="submit" value="Submit New Password">
	</form>

<?php else: ?>

To reset your password you'll need a valid code.

<?php endif; ?>

<?php endif; ?>

</div>