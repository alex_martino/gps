<?php /* Smarty version 2.6.17, created on 2013-01-13 12:08:47
         compiled from /home/gpscom/public_html/_pages/portal/admin/industries.php */ ?>
<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>
<h2>Settings: Industries</h2>

<?php if (( $this->_tpl_vars['display'] == 'default' )): ?>
<table class="admin_table" style="border: 1px solid black; margin-top: 20px">
<tr style="border-bottom: 1px solid black">
<th>ID</th>
<th>Name</th>
</tr>
<?php $_from = $this->_tpl_vars['industries']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr class="row">
<td><?php echo $this->_tpl_vars['i']['id']; ?>
</td>
<td><?php echo $this->_tpl_vars['i']['name']; ?>
</td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<BR>
<input type="button" value="Add Industry" onClick="window.location='industries.php?display=add';">
<?php else: ?>
<?php if (( $this->_tpl_vars['display'] == 'add' ) && ( $this->_tpl_vars['error'] != 0 )): ?>
<div class="error" style="margin-left:0"><?php echo $this->_tpl_vars['errormsg']; ?>
</div>
<div style="clear:both">&nbsp;</div>
<?php endif; ?>
<form name="addindustry" action="industries.php" method="post">
<input type="hidden" name="addsubmitted" value="1">
<div style="float: left; line-height: 1.4; width: 140px;">
Industry Name:<BR><BR>
<input type="submit" value="Save">
</div>
<div style="float: left; line-height: 1.4; width: 155px;">
<input type="text" name="name" value="<?php echo $this->_tpl_vars['name']; ?>
">
</div>
<div style="float: left; line-height: 1.4">
E.g. Private Equity
</div>
</form>
<?php endif; ?>

</div>