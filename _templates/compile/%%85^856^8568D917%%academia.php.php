<?php /* Smarty version 2.6.17, created on 2012-10-16 16:24:09
         compiled from /home/gpscom/public_html/_pages/portal/network/view/academia.php */ ?>
<div><img src="/content_files/headers/network.gif" width="800" height="90">

<?php if ($this->_tpl_vars['not_viewable']): ?>
<div style="margin-top: 15px; margin-left: 10px">
<p>This profile is not available. <a href="javascript:history.go(-1)">Go back.</a></p>

<?php else: ?>

<div id="network_t_menu">
  <ul>
<?php $_from = $this->_tpl_vars['network_t_menu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
    <li<?php if ($this->_tpl_vars['i']['current'] == '1'): ?> id="current"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['i']['url']; ?>
?id=<?php echo $this->_tpl_vars['profile']['id']; ?>
"><?php echo $this->_tpl_vars['i']['name']; ?>
</a></li>
<?php endforeach; endif; unset($_from); ?>
  </ul>
</div>
</div>
<div style="margin-top: 25px">

<?php if ($this->_tpl_vars['can_edit']): ?>
<div class="editlink">
<?php if ($this->_tpl_vars['is_editing']): ?>
<a href="#" onClick="do_edit.submit(); return false;">Save</a> | <a href="academia.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
">Cancel</a>
<?php else: ?>
<a href="academia.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
&edit=1">Edit</a>
<?php endif; ?>
</div>
<?php endif; ?>

<h2><?php echo $this->_tpl_vars['profile']['first_name']; ?>
 <?php echo $this->_tpl_vars['profile']['last_name']; ?>
</h2>

<div style="float:left" style="width: 150px">

<?php if ($this->_tpl_vars['display_photo']): ?>

<img name="profile_img" src="<?php echo $this->_tpl_vars['profile']['current_photo']; ?>
" width="150" alt="Photo of <?php echo $this->_tpl_vars['profile']['first_name']; ?>
 <?php echo $this->_tpl_vars['profile']['last_name']; ?>
"
<?php if ($this->_tpl_vars['profile']['school_photo']): ?>
<?php endif; ?>
/>

<?php else: ?>

<img src="/images/male.png" width="150" alt="Photo missing" />

<?php endif; ?>

</div>

<?php if (( $this->_tpl_vars['is_editing'] )): ?>
<form name="do_edit" action="academia.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
&edit=1" method="POST">
<input type="hidden" name="submitted" value="1">

<div class="network_data">
		<fieldset>
		<legend>Undergraduate</legend>
		<p style="float: left;">University:<br />
			<?php echo $this->_tpl_vars['profile']['undergraduate_school']; ?>

		</p>
		<p style="float: left; clear:left">Graduation Year:<br />
			<input type="text" id="graduation_year" name="graduation_year" value="<?php echo $this->_tpl_vars['profile']['graduation_year']; ?>
" style="width: 40px;" />
		</p>
		<p style="float: left; clear:left">Major:<br />
			<input type="text" id="undergraduate_major" name="undergraduate_major" value="<?php echo $this->_tpl_vars['profile']['undergraduate_major']; ?>
" style="width: 230px;" />
		</p>
		</fieldset>
</div>

<div class="network_data">
		<fieldset>
		<legend>Graduate</legend>
		<p style="float: left;">Graduate School:<br />
			<input type="text" id="graduate_school" name="graduate_school" value="<?php echo $this->_tpl_vars['profile']['graduate_school']; ?>
" style="width: 230px;" />
		</p>
		<p style="float: left; clear:left">Graduate Major:<br />
			<input type="text" id="graduate_major" name="graduate_major" value="<?php echo $this->_tpl_vars['profile']['graduate_major']; ?>
" style="width: 230px;" />
		</p>
		</fieldset>
</div>

<?php else: ?>

<div class="network_data">

<div id="university">
<div style="float:left" class="data_title">
University
</div>
<div style="float:left" class="job_info">
<b style="font-size: 15px"><a href="/portal/network/search.php?s=1&uni=<?php echo $this->_tpl_vars['profile']['ug_school_id']; ?>
"><?php echo $this->_tpl_vars['profile']['uni_fname']; ?>
</a></b>
<span style="font-size: 15px"><?php if (( $this->_tpl_vars['profile']['graduation_year'] )): ?>| <a href="/portal/network/search.php?s=1&year=<?php echo $this->_tpl_vars['profile']['graduation_year']; ?>
"><?php echo $this->_tpl_vars['profile']['graduation_year']; ?>
</a></span><?php endif; ?>
<?php if (( $this->_tpl_vars['profile']['undergraduate_major'] )): ?>
<BR><i style="font-size: 13px"><?php echo $this->_tpl_vars['profile']['undergraduate_major']; ?>

<?php endif; ?>
</div>
</div>

<?php if (( $this->_tpl_vars['profile']['graduate_school'] )): ?><BR>
<div id="gradschool">
<div style="float:left" class="data_title">
Graduate School
</div>
<div style="float:left" class="job_info">
<b style="font-size: 15px"><?php echo $this->_tpl_vars['profile']['graduate_school']; ?>
</b>
<?php if (( $this->_tpl_vars['profile']['graduate_major'] )): ?>
<BR><i style="font-size: 13px"><?php echo $this->_tpl_vars['profile']['graduate_major']; ?>

<?php endif; ?>
</div>
</div>
<?php endif; ?>
<?php if (( ( $this->_tpl_vars['prof_des'] > 1 ) )): ?>
<BR><BR>
<div id="professional_designations" class="network_block">
<div style="float:left" class="data_title">
Professional Designations
</div>
<div style="float:left" class="data_data">
<?php echo $this->_tpl_vars['profile']['professional_designations']; ?>
<BR>
</div>
</div>
<?php endif; ?>

</div>

<?php endif; ?>

<div style="clear:both">&nbsp;</div>

<?php endif; ?>

</div>