<?php /* Smarty version 2.6.17, created on 2012-10-16 16:24:18
         compiled from /home/gpscom/public_html/_pages/portal/network/view/interests.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'capitalize', '/home/gpscom/public_html/_pages/portal/network/view/interests.php', 23, false),)), $this); ?>
<div><img src="/content_files/headers/network.gif" width="800" height="90">

<?php if ($this->_tpl_vars['not_viewable']): ?>
<div style="margin-top: 15px; margin-left: 10px">
<p>This profile is not available. <a href="javascript:history.go(-1)">Go back.</a></p>

<?php else: ?>
<div id="network_t_menu">
  <ul>
<?php $_from = $this->_tpl_vars['network_t_menu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
    <li<?php if ($this->_tpl_vars['i']['current'] == '1'): ?> id="current"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['i']['url']; ?>
?id=<?php echo $this->_tpl_vars['profile']['id']; ?>
"><?php echo $this->_tpl_vars['i']['name']; ?>
</a></li>
<?php endforeach; endif; unset($_from); ?>
  </ul>
</div>
</div>
<?php if ($this->_tpl_vars['is_editing']): ?>
<script>
			window.addEvent('load', function(){
				
				// Autocomplete initialization
				var t1 = new TextboxList('investors', {unique: true, plugins: {autocomplete: {}}});
<?php $_from = $this->_tpl_vars['tags']['investor']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ttype'] => $this->_tpl_vars['i']):
?>
t1.add('<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
');
<?php endforeach; endif; unset($_from); ?>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t1.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_investor.php', onSuccess: function(r){
					t1.plugins['autocomplete'].setValues(r);
					t1.container.removeClass('textboxlist-loading');
				}}).send();	

				// Autocomplete initialization
				var t2 = new TextboxList('philosophers', {unique: true, plugins: {autocomplete: {}}});

<?php $_from = $this->_tpl_vars['tags']['philosopher']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ttype'] => $this->_tpl_vars['i']):
?>
t2.add('<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
');
<?php endforeach; endif; unset($_from); ?>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t2.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_philosopher.php', onSuccess: function(r){
					t2.plugins['autocomplete'].setValues(r);
					t2.container.removeClass('textboxlist-loading');
				}}).send();	

				// Autocomplete initialization
				var t3 = new TextboxList('books', {unique: true, plugins: {autocomplete: {}}});
<?php $_from = $this->_tpl_vars['tags']['book']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ttype'] => $this->_tpl_vars['i']):
?>
t3.add('<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
');
<?php endforeach; endif; unset($_from); ?>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t3.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_book.php', onSuccess: function(r){
					t3.plugins['autocomplete'].setValues(r);
					t3.container.removeClass('textboxlist-loading');
				}}).send();

				// Autocomplete initialization
				var t4 = new TextboxList('movies', {unique: true, plugins: {autocomplete: {}}});
<?php $_from = $this->_tpl_vars['tags']['movie']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ttype'] => $this->_tpl_vars['i']):
?>
t4.add('<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
');
<?php endforeach; endif; unset($_from); ?>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t4.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_movie.php', onSuccess: function(r){
					t4.plugins['autocomplete'].setValues(r);
					t4.container.removeClass('textboxlist-loading');
				}}).send();

				// Autocomplete initialization
				var t5 = new TextboxList('television', {unique: true, plugins: {autocomplete: {}}});
<?php $_from = $this->_tpl_vars['tags']['television']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ttype'] => $this->_tpl_vars['i']):
?>
t5.add('<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
');
<?php endforeach; endif; unset($_from); ?>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t5.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_television.php', onSuccess: function(r){
					t5.plugins['autocomplete'].setValues(r);
					t5.container.removeClass('textboxlist-loading');
					document.getElementById('savelink').innerHTML = '<a href="#" onClick="document.do_edit.submit(); return false;">Save</a>';
				}}).send();
			
								
			});

function checkApostrophe(field) {
alert(field);
}

</script>

<?php endif; ?>
<div style="margin-top: 25px">

<?php if ($this->_tpl_vars['can_edit']): ?>
<div class="editlink">
<?php if ($this->_tpl_vars['is_editing']): ?>
<span id="savelink">Please wait...</span> |
<a href="interests.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
">Cancel</a>
<?php else: ?>
<a href="interests.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
&edit=1">Edit</a>
<?php endif; ?>
</div>
<?php endif; ?>

<h2><?php echo $this->_tpl_vars['profile']['first_name']; ?>
 <?php echo $this->_tpl_vars['profile']['last_name']; ?>
</h2>

<div style="float:left" style="width: 150px">

<?php if ($this->_tpl_vars['display_photo']): ?>

<img name="profile_img" src="<?php echo $this->_tpl_vars['profile']['current_photo']; ?>
" width="150" alt="Photo of <?php echo $this->_tpl_vars['profile']['first_name']; ?>
 <?php echo $this->_tpl_vars['profile']['last_name']; ?>
"
<?php if ($this->_tpl_vars['profile']['school_photo']): ?>
<?php endif; ?>
/>

<?php else: ?>

<img src="/images/male.png" width="150" alt="Photo missing" />

<?php endif; ?>
</div>

<div style="float: right; width: 610px">

<?php if ($this->_tpl_vars['error']): ?>
<div class="error"><?php echo $this->_tpl_vars['errormsg']; ?>
</div>
<?php endif; ?>

<?php if (( $this->_tpl_vars['is_editing'] )): ?>

<form name="do_edit" action="interests.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
&edit=1" method="POST" accept-charset="utf-8">
<input type="hidden" name="submitted" value="1">

<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Investors</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="investors" value="" id="investors" />
</div>
</div>

<BR><BR>


		</fieldset>
</div>
<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Philosophers</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="philosophers" value="" id="philosophers" />
</div>
</div>

<BR><BR>


		</fieldset>
</div>
<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Books</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="books" value="" id="books" />
</div>
</div>

<BR><BR>


		</fieldset>
</div>

<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Movies</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="movies" value="" id="movies" />
</div>
</div>

<BR><BR>


		</fieldset>
</div>

<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Television</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="television" value="" id="television" />
</div>
</div>

<BR><BR>


		</fieldset>
</div>

</form>

		
<?php else: ?>

<div class="network_data">

<?php if ($this->_tpl_vars['tags']['investor']): ?>
<div id="investors" class="network_block_top">
<div style="float:left" class="data_title">
Investors
</div>
<div style="float:left" class="data_data">
<?php $_from = $this->_tpl_vars['tags']['investor']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>
<span class="button black small"><a href="../search.php?qs=1&ttype=investor&tag=<?php echo $this->_tpl_vars['i']['tag']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</a></span>
<?php endforeach; endif; unset($_from); ?>
</div>
</div>
<div style="line-height:1px">&nbsp;</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['tags']['philosopher']): ?>
<div id="philosophers" class="network_block">
<div style="float:left" class="data_title">
Philosophers
</div>
<div style="float:left" class="data_data">
<?php $_from = $this->_tpl_vars['tags']['philosopher']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ttype'] => $this->_tpl_vars['i']):
?>
<span class="button red small"><a href="../search.php?qs=1&ttype=philosopher&tag=<?php echo $this->_tpl_vars['i']['tag']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</a></span>
<?php endforeach; endif; unset($_from); ?>
</div>
</div>
<div style="line-height:1px">&nbsp;</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['tags']['book']): ?>
<div id="books" class="network_block">
<div style="float:left" class="data_title">
Books
</div>
<div style="float:left" class="data_data">
<?php $_from = $this->_tpl_vars['tags']['book']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ttype'] => $this->_tpl_vars['i']):
?>
<span class="button blue small"><a href="../search.php?qs=1&ttype=book&tag=<?php echo $this->_tpl_vars['i']['tag']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</a></span>
<?php endforeach; endif; unset($_from); ?>
</div>
</div>
<div style="line-height:1px">&nbsp;</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['tags']['movie']): ?>
<div id="movies" class="network_block">
<div style="float:left" class="data_title">
Movies
</div>
<div style="float:left" class="data_data">
<?php $_from = $this->_tpl_vars['tags']['movie']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ttype'] => $this->_tpl_vars['i']):
?>
<span class="button green small"><a href="../search.php?qs=1&ttype=movie&tag=<?php echo $this->_tpl_vars['i']['tag']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</a></span>
<?php endforeach; endif; unset($_from); ?>
</div>
</div>
<div style="line-height:1px">&nbsp;</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['tags']['television']): ?>
<div id="television" class="network_block">
<div style="float:left" class="data_title">
Television
</div>
<div style="float:left" class="data_data">
<?php $_from = $this->_tpl_vars['tags']['television']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ttype'] => $this->_tpl_vars['i']):
?>
<span class="button black small"><a href="../search.php?qs=1&ttype=television&tag=<?php echo $this->_tpl_vars['i']['tag']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</a></span>
<?php endforeach; endif; unset($_from); ?>
</div>
</div>
<BR><BR>
<?php endif; ?>


</div>

<?php endif; ?>

</div> <!--end float right -->

<div style="clear:both">&nbsp;</div>

<?php endif; ?>

</div>