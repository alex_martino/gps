<?php /* Smarty version 2.6.17, created on 2012-10-16 18:35:27
         compiled from /home/gpscom/public_html/_pages/portal/research/index.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', '/home/gpscom/public_html/_pages/portal/research/index.php', 16, false),)), $this); ?>
<div><img src="/content_files/headers/research.gif" width="800" height="90"></div>
<div>
<p>Here you can find the current research pipeline. Use the menu on the left to browse by sector.</p>
<BR>
<h2>Current Research: Most recent companies</h2>
<table class="admin_table" style="border: 1px solid black">
<tr style="border-bottom: 1px solid black">
<th style="width: 100px">Entry Date</th>
<th style="width: 250px">Name</th>
<th style="width: 85px">Ticker</th>
<th style="width: 100px">Sector</th>
<th style="width: 200px">Person Researching</th>
</tr>
<?php $_from = $this->_tpl_vars['recently_added']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr class="row" onClick="window.location.href='research.php?sector=<?php echo $this->_tpl_vars['i']['sector_id']; ?>
&item=<?php echo $this->_tpl_vars['i']['id']; ?>
';">
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['entry_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m/%d/%y") : smarty_modifier_date_format($_tmp, "%m/%d/%y")); ?>
</td>
<td><?php echo $this->_tpl_vars['i']['company_name']; ?>

<td><?php echo $this->_tpl_vars['i']['ticker']; ?>

<td><?php echo $this->_tpl_vars['i']['sname']; ?>

<td><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
</td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
</div>