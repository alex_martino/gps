<?php /* Smarty version 2.6.17, created on 2012-10-16 19:49:09
         compiled from /home/gpscom/public_html/_pages/portal/network/search.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', '/home/gpscom/public_html/_pages/portal/network/search.php', 241, false),array('modifier', 'capitalize', '/home/gpscom/public_html/_pages/portal/network/search.php', 241, false),)), $this); ?>
<div><img src="/content_files/headers/network.gif" width="800" height="90"></div>
<div>

<?php if ($this->_tpl_vars['no_results']): ?>

<p>No results found - <a href="search.php">Search again</a></p>

<?php else: ?>

<?php if ($this->_tpl_vars['is_search']): ?>

<?php if ($this->_tpl_vars['search_type'] == 'by_year'): ?>

<h2>Search Results: <?php echo $this->_tpl_vars['search_title']; ?>
</h2>
<BR>
	<?php $_from = $this->_tpl_vars['unis']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['undergraduate_school'] => $this->_tpl_vars['members_array']):
?>
		<h2><?php echo $this->_tpl_vars['undergraduate_school']; ?>
</h2>

<?php $_from = $this->_tpl_vars['members_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>

<div style="float: left; height: 50px; margin-bottom: 15px; width: 253px;">
<div style="float:left; padding-right: 10px; width: 50px">
<?php if ($this->_tpl_vars['i']['cropped_photo']): ?>
<a href="view/overview.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
">
<img src="<?php echo $this->_tpl_vars['i']['cropped_photo']; ?>
">
</a>
<?php endif; ?>
</div>

				<a href="view/overview.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
"><strong><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
</strong></a><br />
				<?php if ($this->_tpl_vars['i']['current_employer'] != ''): ?><strong><?php echo $this->_tpl_vars['i']['current_employer']; ?>
</strong><br /><?php endif; ?>
				<?php echo $this->_tpl_vars['i']['undergraduate_school_s']; ?>

</div>

<?php endforeach; endif; unset($_from); ?>
<div style="clear: both;"></div>

	<?php endforeach; endif; unset($_from); ?>


<?php elseif ($this->_tpl_vars['search_type'] == 'by_uni'): ?>

<h2>Search Results: <?php echo $this->_tpl_vars['search_title']; ?>
</h2>
<BR>
	<?php $_from = $this->_tpl_vars['years']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['year'] => $this->_tpl_vars['members_array']):
?>
		<h2>Class of <?php echo $this->_tpl_vars['year']; ?>
</h2>

<?php $_from = $this->_tpl_vars['members_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>

<div style="float: left; height: 50px; margin-bottom: 15px; width: 253px;">
<div style="float:left; padding-right: 10px; width: 50px">
<?php if ($this->_tpl_vars['i']['cropped_photo']): ?>
<a href="view/overview.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
">
<img src="<?php echo $this->_tpl_vars['i']['cropped_photo']; ?>
">
</a>
<?php endif; ?>
</div>

				<a href="view/overview.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
"><strong><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
</strong></a><br />
				<?php if ($this->_tpl_vars['i']['current_employer'] != ''): ?><strong><?php echo $this->_tpl_vars['i']['current_employer']; ?>
</strong><br /><?php endif; ?>
				<?php echo $this->_tpl_vars['i']['undergraduate_school_s']; ?>

</div>

<?php endforeach; endif; unset($_from); ?>
<div style="clear: both;"></div>

	<?php endforeach; endif; unset($_from); ?>



<?php elseif ($this->_tpl_vars['search_type'] == 'by_tag'): ?>
<h2>Search Results: <?php echo $this->_tpl_vars['search_title']; ?>
</h2>
<h3>Sort: <a href="search.php?qs=1&ttype=<?php echo $this->_tpl_vars['ttype']; ?>
&tag=<?php echo $this->_tpl_vars['search_tag']; ?>
&sortby=uni">University</a> | <a href="search.php?qs=1&ttype=<?php echo $this->_tpl_vars['ttype']; ?>
&tag=<?php echo $this->_tpl_vars['search_tag']; ?>
&sortby=year">Year</a></h3>
<BR>
<?php if ($this->_tpl_vars['sortby'] == 'uni'): ?>
	<?php $_from = $this->_tpl_vars['unis']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['undergraduate_school'] => $this->_tpl_vars['members_array']):
?>
		<h2><?php echo $this->_tpl_vars['undergraduate_school']; ?>
</h2>

<?php $_from = $this->_tpl_vars['members_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>

<div style="float: left; height: 50px; margin-bottom: 15px; width: 253px;">
<div style="float:left; padding-right: 10px; width: 50px">
<?php if ($this->_tpl_vars['i']['cropped_photo']): ?>
<a href="view/overview.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
">
<img src="<?php echo $this->_tpl_vars['i']['cropped_photo']; ?>
">
</a>
<?php endif; ?>
</div>

				<a href="view/overview.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
"><strong><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
</strong></a><br />
				<?php if ($this->_tpl_vars['i']['current_employer'] != ''): ?><strong><?php echo $this->_tpl_vars['i']['current_employer']; ?>
</strong><br /><?php endif; ?>
				<?php echo $this->_tpl_vars['i']['undergraduate_school_s']; ?>

</div>

<?php endforeach; endif; unset($_from); ?>
<div style="clear: both;"></div>

	<?php endforeach; endif; unset($_from); ?>
<?php endif; ?>

<?php if ($this->_tpl_vars['sortby'] == 'year'): ?>
	<?php $_from = $this->_tpl_vars['years']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['year'] => $this->_tpl_vars['members_array']):
?>
		<h2>Class of <?php echo $this->_tpl_vars['year']; ?>
</h2>

<?php $_from = $this->_tpl_vars['members_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>

<div style="float: left; height: 50px; margin-bottom: 15px; width: 253px;">
<div style="float:left; padding-right: 10px; width: 50px">
<?php if ($this->_tpl_vars['i']['cropped_photo']): ?>
<a href="view/overview.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
">
<img src="<?php echo $this->_tpl_vars['i']['cropped_photo']; ?>
">
</a>
<?php endif; ?>
</div>

				<a href="view/overview.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
"><strong><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
</strong></a><br />
				<?php if ($this->_tpl_vars['i']['current_employer'] != ''): ?><strong><?php echo $this->_tpl_vars['i']['current_employer']; ?>
</strong><br /><?php endif; ?>
				<?php echo $this->_tpl_vars['i']['undergraduate_school_s']; ?>

</div>

<?php endforeach; endif; unset($_from); ?>

<div style="clear: both;"></div>

	<?php endforeach; endif; unset($_from); ?>

<?php endif; ?>

<?php elseif ($this->_tpl_vars['results_view'] == 1): ?>
<script>
var is_input = document.URL.indexOf('?');
var vars = "";
if (is_input != -1)
{ 
// Create variable from ? in the url to the end of the string
addr_str = document.URL.substring(is_input+1, document.URL.length);
for (count = 0; count < addr_str.length; count++) 
{

if (addr_str.charAt(count) == "&") 
// Write a line break for each & found
{ vars = vars + ("&");}

else 
// Write the part of the url 
{ vars = vars + (addr_str.charAt(count)); }
}}
</script>
<h2>Search Results: Custom Search</h2>
<h4>Criteria: <span style="font-weight: normal"><?php $_from = $this->_tpl_vars['criteria']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><?php echo $this->_tpl_vars['i']; ?>
, <?php endforeach; endif; unset($_from); ?></span></h4><BR>
<h3>Sort: <script>document.write("<a href='search.php?" + vars + "&sortby=uni'>");</script>University</a> | <script>document.write("<a href='search.php?" + vars + "&sortby=year'>");</script>Year</a></h3>
<BR>
<?php if ($this->_tpl_vars['sortby'] == 'uni'): ?>
	<?php $_from = $this->_tpl_vars['unis']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['undergraduate_school'] => $this->_tpl_vars['members_array']):
?>
		<h2><?php echo $this->_tpl_vars['undergraduate_school']; ?>
</h2>

<?php $_from = $this->_tpl_vars['members_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>

<div style="float: left; height: 50px; margin-bottom: 15px; width: 253px;">
<div style="float:left; padding-right: 10px; width: 50px">
<?php if ($this->_tpl_vars['i']['cropped_photo']): ?>
<a href="view/overview.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
">
<img src="<?php echo $this->_tpl_vars['i']['cropped_photo']; ?>
">
</a>
<?php endif; ?>
</div>

				<a href="view/overview.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
"><strong><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
</strong></a><br />
				<?php if ($this->_tpl_vars['i']['current_employer'] != ''): ?><strong><?php echo $this->_tpl_vars['i']['current_employer']; ?>
</strong><br /><?php endif; ?>
				<?php echo $this->_tpl_vars['i']['undergraduate_school_s']; ?>

</div>

<?php endforeach; endif; unset($_from); ?>
<div style="clear: both;"></div>

	<?php endforeach; endif; unset($_from); ?>
<?php endif; ?>

<?php if ($this->_tpl_vars['sortby'] == 'year'): ?>
	<?php $_from = $this->_tpl_vars['years']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['year'] => $this->_tpl_vars['members_array']):
?>
		<h2>Class of <?php echo $this->_tpl_vars['year']; ?>
</h2>

<?php $_from = $this->_tpl_vars['members_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>

<div style="float: left; height: 50px; margin-bottom: 15px; width: 253px;">
<div style="float:left; padding-right: 10px; width: 50px">
<?php if ($this->_tpl_vars['i']['cropped_photo']): ?>
<a href="view/overview.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
">
<img src="<?php echo $this->_tpl_vars['i']['cropped_photo']; ?>
">
</a>
<?php endif; ?>
</div>

				<a href="view/overview.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
"><strong><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
</strong></a><br />
				<?php if ($this->_tpl_vars['i']['current_employer'] != ''): ?><strong><?php echo $this->_tpl_vars['i']['current_employer']; ?>
</strong><br /><?php endif; ?>
				<?php echo $this->_tpl_vars['i']['undergraduate_school_s']; ?>

</div>

<?php endforeach; endif; unset($_from); ?>

<div style="clear: both;"></div>

	<?php endforeach; endif; unset($_from); ?>

<?php endif; ?>

<?php endif; ?>

<?php else: ?>

<script>
			window.addEvent('load', function(){

				// Autocomplete with poll the server as you type
				var t1 = new TextboxList('quicksearch', {unique: true, max: 1, plugins: {autocomplete: {
					minLength: 1,
					queryRemote: true,
					remote: {url: '/includes/autocomplete_quicksearch.php'}
				}}});
				//t1.add('John Doe').add('Jane Roe');


			});

//declare filter arrays
lists = new Array();
var th_visible = 0;
var rowcount = 1;

var masterlist = ["f_employer", "f_gender", "f_gps_stage", "f_graduation_year", "f_expertise", "f_hobby", "f_hometown", "f_industry", "f_sector", "f_undergraduate_school"];

lists['f_employer'] = new Array();
lists['f_employer'][0] = 'Current Employer';
lists['f_employer'][1] = '=';
lists['f_employer'][2] = '<select id="f_employer" class="cs wide"><?php $_from = $this->_tpl_vars['employers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo $this->_tpl_vars['i']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</option><?php endforeach; endif; unset($_from); ?></select>';
lists['f_employer'][3] = 0;

lists['f_expertise'] = new Array();
lists['f_expertise'][0] = 'Research Experience';
lists['f_expertise'][1] = '=';
lists['f_expertise'][2] = '<select id="f_expertise" class="cs wide"><?php $_from = $this->_tpl_vars['tags']['expertise']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('replace', true, $_tmp, ', ', '_') : smarty_modifier_replace($_tmp, ', ', '_')); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</option><?php endforeach; endif; unset($_from); ?></select>';
lists['f_expertise'][3] = 0;

lists['f_gender'] = new Array();
lists['f_gender'][0] = 'Gender';
lists['f_gender'][1] = '=';
lists['f_gender'][2] = '<select id="f_gender" class="cs wide"><option value="male">Male</option><option value="female">Female</option></select>';
lists['f_gender'][3] = 0;

lists['f_gps_stage'] = new Array();
lists['f_gps_stage'][0] = 'GPS Status';
lists['f_gps_stage'][1] = '=';
lists['f_gps_stage'][2] = '<select id="f_gps_stage" class="cs wide"><option value="alumni">Alumni</option><option value="analyst">Analyst</option><option value="member">Member</option></select>';
lists['f_gps_stage'][3] = 0;

lists['f_graduation_year'] = new Array();
lists['f_graduation_year'][0] = 'Graduation Year';
lists['f_graduation_year'][1] = '<select id="f_graduation_year_operator" class="cs"><option value=">">></option><option value="=">=</option><option value="<"><</option></select>';
lists['f_graduation_year'][2] = '<select id="f_graduation_year" class="cs wide"><?php $_from = $this->_tpl_vars['year']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value=<?php echo $this->_tpl_vars['i']; ?>
><?php echo $this->_tpl_vars['i']; ?>
</option><?php endforeach; endif; unset($_from); ?></select>';
lists['f_graduation_year'][3] = 0;

lists['f_hobby'] = new Array();
lists['f_hobby'][0] = 'Hobby';
lists['f_hobby'][1] = '=';
lists['f_hobby'][2] = '<select id="f_hobby" class="cs wide"><?php $_from = $this->_tpl_vars['tags']['hobbies']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('replace', true, $_tmp, ', ', '_') : smarty_modifier_replace($_tmp, ', ', '_')); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</option><?php endforeach; endif; unset($_from); ?></select>';
lists['f_hobby'][3] = 0;

lists['f_hometown'] = new Array();
lists['f_hometown'][0] = 'Hometown';
lists['f_hometown'][1] = '=';
lists['f_hometown'][2] = '<select id="f_hometown" class="cs wide"><?php $_from = $this->_tpl_vars['tags']['hometowns']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('replace', true, $_tmp, ', ', '_') : smarty_modifier_replace($_tmp, ', ', '_')); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</option><?php endforeach; endif; unset($_from); ?></select>';
lists['f_hometown'][3] = 0;

lists['f_industry'] = new Array();
lists['f_industry'][0] = 'Current Industry';
lists['f_industry'][1] = '=';
lists['f_industry'][2] = '<select id="f_industry" class="cs wide"><?php $_from = $this->_tpl_vars['industries']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo $this->_tpl_vars['i']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</option><?php endforeach; endif; unset($_from); ?></select>';
lists['f_industry'][3] = 0;

lists['f_sector'] = new Array();
lists['f_sector'][0] = 'Sector';
lists['f_sector'][1] = '=';
lists['f_sector'][2] = '<select id="f_sector" class="cs wide"><?php $_from = $this->_tpl_vars['tags']['sectors']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('replace', true, $_tmp, ', ', '_') : smarty_modifier_replace($_tmp, ', ', '_')); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</option><?php endforeach; endif; unset($_from); ?></select>';
lists['f_sector'][3] = 0;

lists['f_undergraduate_school'] = new Array();
lists['f_undergraduate_school'][0] = 'Undergraduate School';
lists['f_undergraduate_school'][1] = '=';
lists['f_undergraduate_school'][2] = '<select id="f_undergraduate_school" class="cs wide"><?php $_from = $this->_tpl_vars['uni']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value=<?php echo $this->_tpl_vars['i']['id']; ?>
><?php echo $this->_tpl_vars['i']['sname']; ?>
</option><?php endforeach; endif; unset($_from); ?></select>';
lists['f_undergraduate_school'][3] = 0;


filter_added = new Array(); //define filter_added array

function add_filter() {
var the_filter = document.cs_form.filterlist.value;

// check not null
if (the_filter != 0) {

// check for headers row
if (th_visible != 1) {
document.getElementById("cs_headers").style.visibility = "visible";
document.getElementById('searchbutton1').style.display = "block";
th_visible = 1;
}

//if (filter_added[the_filter] != 1) {

var x=document.getElementById('cs_table').insertRow(-1);

var a=x.insertCell(0);
var b=x.insertCell(1);
var c=x.insertCell(2);
var d=x.insertCell(3);

a.className = "cs_a";
b.className = "cs_a";
c.className = "cs_a";
d.className = "cs_a";

a.innerHTML = lists[the_filter][0]
b.innerHTML = lists[the_filter][1].replace('" class="cs', '['+lists[the_filter][3]+']" onChange="hide_results();" class="cs');
c.innerHTML = lists[the_filter][2].replace('" class="cs', '['+lists[the_filter][3]+']" onChange="hide_results();" class="cs');
d.innerHTML = '<a href="#" onClick="remove_filter(this,\''+the_filter+'\');return false;"><img src="/content_files/images/delete.png"></a>';

rowcount++; //increase row count by 1
lists[the_filter][3]++;


filter_added[the_filter] = 1; //set variable to identify filter

//} else { alert("Filter already added!"); } //end if filter already added

} else { alert("Please select a filter from the dropdown box"); } //end if filter not null

hide_results();

} //end add_filter function

function remove_filter(mylink,the_filter) {
document.getElementById('cs_table').getElementsByTagName('tbody')[0].removeChild(mylink.parentNode.parentNode);
if (document.getElementById('cs_table').getElementsByTagName('tr').length == 1) { document.getElementById("cs_headers").style.visibility = "hidden"; th_visible = 0; document.getElementById('searchbutton1').style.display = "none"; }
rowcount--;
lists[the_filter][3]--;
hide_results();
             }

function remove_all() {
  if (confirm("Are you sure you want to delete all filters?")) {
var rownum = document.getElementById("cs_table").rows.length;
while (rownum>1) { document.getElementById("cs_table").deleteRow(rownum-1); rownum--; }
document.getElementById("cs_headers").style.visibility = "hidden"; th_visible = 0; document.getElementById('searchbutton1').style.display = "none";
hide_results();
  }
}

//AJAX Code
function ajaxSearch(){

//define variables

var varlist = "";
var thatone = "";

for (i=0;i<masterlist.length;i++) {

for (j=0;j<lists[masterlist[i]][3];j++) {
var thisone = masterlist[i] + "[" + j + "]";
var thisop = masterlist[i] + "_operator" + "[" + j + "]";
thatone = thatone + thisone + ",";
if (document.getElementById(thisop) != null) {
varlist = varlist + document.getElementById(thisone).value + "_" + document.getElementById(thisop).value + ",";
} else {
varlist = varlist + document.getElementById(thisone).value + ",";
}

//check for operator
}
}

do_criteria = thatone.split(',');
do_values = varlist.split(',');

do_s_submit = "";

for (var i = 0; i < do_criteria.length; i++)
{
if (do_values[i].length > 0) { do_s_submit = do_s_submit + do_criteria[i] + "=" + do_values[i] + "&"; }
}

do_s_submit = do_s_submit.slice(0,do_s_submit.length-1); //remove final ampersand

	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
// Create a function that will receive data sent from the server
ajaxRequest.onreadystatechange = function(){
		document.getElementById("cs-results").innerHTML = "Please wait...";
	if(ajaxRequest.readyState == 4){
		document.getElementById("cs-results").innerHTML = ajaxRequest.responseText;
	}
}

var do_ss_submit = "search.php?cs=1&" + do_s_submit;

ajaxRequest.open("GET", do_ss_submit, true);
ajaxRequest.send(null);

}

function view_results() {
document.getElementById("data_selection_grid").style.display = "block";
}

function hide_results() {
document.getElementById("data_selection_grid").style.display = "none";
document.getElementById("cs-results").innerHTML = "";
}

function download_results(format) {
	var rquery = "";
        var elem = document.getElementById('cs_results').elements;
        for(var i = 0; i < elem.length; i++)
        {

if (elem[i].checked) { rquery += elem[i].name + "=" + elem[i].value + "&"; }

        } 

if (rquery != "") {

var downloadpage = "search.php?cs=1&download=1&format=" + format + "&" + rquery + do_s_submit;
window.location=downloadpage;

}

else {
alert("Please select some fields.");
}

}

	function toggle(fields) {
		t_fields = new Array();
		t_fields = fields.split(",");
		boxnum = t_fields.length;
		if(document.getElementsByName(t_fields[0])[0].checked) {
for (var i = 1; i < boxnum; i++) { document.getElementsByName(t_fields[i])[0].checked = true; }
		}
		else {
for (var i = 1; i < boxnum; i++) { document.getElementsByName(t_fields[i])[0].checked = false; }
		}
	}

function view_online(ids) {
var onlineurl = "search.php?cs=1&results_view=1&" + do_s_submit + "&ids=" + ids;
window.location = onlineurl;
}

</script>

<h2>Popular Searches</h2>
Class:<?php $_from = $this->_tpl_vars['years']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['year'] => $this->_tpl_vars['members_array']):
?> <a href="search.php?s=1&year=<?php echo $this->_tpl_vars['year']; ?>
"><?php echo $this->_tpl_vars['year']; ?>
</a>&nbsp; <?php endforeach; endif; unset($_from); ?><BR>
University:<?php $_from = $this->_tpl_vars['uni']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?> <a href="search.php?s=1&uni=<?php echo $this->_tpl_vars['i']['id']; ?>
"><?php echo $this->_tpl_vars['i']['sname']; ?>
</a>&nbsp; <?php endforeach; endif; unset($_from); ?><BR>
Sector:<?php $_from = $this->_tpl_vars['sectors']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?> <a href="search.php?qs=1&ttype=sector&tag=<?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('replace', true, $_tmp, '&', '%26') : smarty_modifier_replace($_tmp, '&', '%26')); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</a>&nbsp; <?php endforeach; endif; unset($_from); ?><BR>
<BR><BR>
<h2>Quick Search</h2>
<form id="qs_form" name="qs_form" action="search.php?qs=1" method="POST">
<div class="form_friends">
<div style="float:left"><input type="text" name="quicksearch" value="" id="quicksearch" /></div>
<div style="float:left; margin: 0 0 0 5px; padding-top: 2px"><span class="button default strong"><input type="submit" value="Search"></span></div>
</div>
</form>
<div style="clear:both"><BR></div><BR>
<h2>Custom Search</h2>
<div class="cs_container">
<form name="cs_form" id="cs_form">
<select name="filterlist" class="filterlist">
<option value="0" SELECTED>** Select One **</option>
<option value="f_employer">Current Employer</option>
<option value="f_industry">Current Industry</option>
<option value="f_gender">Gender</option>
<option value="f_gps_stage">GPS Status</option>
<option value="f_graduation_year">Graduation Year</option>
<option value="f_hobby">Hobby</option>
<option value="f_hometown">Hometown</option>
<option value="f_expertise">Research Experience</option>
<option value="f_sector">Sector</option>
<option value="f_undergraduate_school">Undergraduate School</option>
</select>
<div style="margin: 0 0 0 5px; padding-top: 4px; display: inline">
<span class="button default small strong"><input type="button" value="Add Filter" class="add_filter" onClick="add_filter();"></span>
</div>
<table id="cs_table" border="1">
<tr id="cs_headers"><th>Criteria</th><th style="width: 50px">Operator</th><th>Value</th><th style="width: 30px"><a href=# onClick="remove_all();return false;"><img src="/content_files/images/bin.gif"></a></th></tr>
</table>
<BR>
<div id="searchbutton1" style="display:none">
<span class="button default strong"><input type="button" onClick="ajaxSearch();" value="Search Database"></span>
<BR><BR>
<div id="cs-results" style="background: #ededed; border: 0; width: 750px"></div>
</div>
</form>
</div>

<div id="data_selection_grid">
<form name="cs_results" id="cs_results">
<div class="hrs"><hr></div>

<div class="searchmenu" id="searchmenu">
<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="basic" value=1 checked onClick="toggle('basic,r[first_name],r[last_name]');"> Basic</div>
	<div class="submenu">
	<input type="checkbox" name="r[first_name]" value=1 checked> First Name<BR>
	<input type="checkbox" name="r[last_name]" value=1 checked> Last Name
	</div>
</div>
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="contact" value=1 onClick="toggle('contact,r[email_address],r[contact_number_country_code],r[contact_number],r[hometown],r[skype],r[address]');"> Contact</div>
	<div class="submenu">
	<input type="checkbox" name="r[email_address]" value=1> Email Address<BR>
	<input type="checkbox" name="r[contact_number_country_code]" value=1> Country Code<BR>
	<input type="checkbox" name="r[contact_number]" value=1> Contact Number<BR>
	<input type="checkbox" name="r[hometown]" value=1> Hometowns<BR>
	<input type="checkbox" name="r[skype]" value=1> Skype<BR>
	<input type="checkbox" name="r[address]" value=1> Mailing Address
	</div>
</div>
</div>
<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="gps" value=1 onClick="toggle('gps,r[gps_stage],r[gps_position_held],r[alumni_mentor],r[member_advocate],r[mentees],r[sector],r[pitch],r[expertise]');"> GPS</div>
	<div class="submenu">
	<input type="checkbox" name="r[gps_stage]" value=1 DISABLED> <span style="text-decoration: line-through">GPS Status</span><BR>
	<input type="checkbox" name="r[gps_position_held]" value=1> GPS Position Held<BR>
	<input type="checkbox" name="r[alumni_mentor]" value=1> Alumni Mentor<BR>
	<input type="checkbox" name="r[member_advocate]" value=1> Member Advocate<BR>
	<input type="checkbox" name="r[mentees]" value=1 DISABLED> <span style="text-decoration: line-through">Mentees</span><BR>
	<input type="checkbox" name="r[sector]" value=1> Sectors<BR>
	<input type="checkbox" name="r[pitch]" value=1> Pitches<BR>
	<input type="checkbox" name="r[expertise]" value=1> Research Experience
	</div>
</div>
</div>
<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="academia" value=1 onClick="toggle('academia,r[undergraduate_school],r[undergraduate_major],r[graduation_year],r[graduate_school],r[graduate_major]');"> Academia</div>
	<div class="submenu">
	<input type="checkbox" name="r[undergraduate_school]" value=1> Undergraduate School<BR>
	<input type="checkbox" name="r[undergraduate_major]" value=1> Undergraduate Major<BR>
	<input type="checkbox" name="r[graduation_year]" value=1> Graduation Year<BR>
	<input type="checkbox" name="r[graduate_school]" value=1> Graduate School<BR>
	<input type="checkbox" name="r[graduate_major]" value=1> Graduate Major
	</div>
</div><div class="searchsection">
<div class="searchcat"><input type="checkbox" name="jobs" value=1 onClick="toggle('jobs,r[current_employer],r[current_role],r[current_title]');"> Jobs</div>
	<div class="submenu">
	<input type="checkbox" name="r[current_employer]" value=1> Current Employer<BR>
	<input type="checkbox" name="r[current_industry]" value=1> Current Industry<BR>
	<input type="checkbox" name="r[current_role]" value=1 DISABLED> <span style="text-decoration: line-through">Current Role</span><BR>
	<input type="checkbox" name="r[current_title]" value=1 DISABLED> <span style="text-decoration: line-through">Current Title</span>
	</div>
</div>
</div>
<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="activities" value=1 onClick="toggle('activities,r[philanthropy],r[entrepreneurship],r[hobby]');"> Activities</div>
	<div class="submenu">
	<input type="checkbox" name="r[philanthropy]" value=1> Philanthropy<BR>
	<input type="checkbox" name="r[entrepreneurship]" value=1> Entrepreneurship<BR>
	<input type="checkbox" name="r[hobby]" value=1> Hobbies
	</div>
</div><div class="searchsection">
<div class="searchcat"><input type="checkbox" name="interests" value=1 onClick="toggle('interests,r[investor],r[philosopher],r[book],r[movie],r[television]');"> Interests</div>
	<div class="submenu">
	<input type="checkbox" name="r[investor]" value=1> Investors<BR>
	<input type="checkbox" name="r[philosopher]" value=1> Philosophers<BR>
	<input type="checkbox" name="r[book]" value=1> Books<BR>
	<input type="checkbox" name="r[movie]" value=1> Movies<BR>
	<input type="checkbox" name="r[television]" value=1> TV
	</div>
</div></div>
<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="misc" value=1 onClick="toggle('misc,r[birth_date],r[gmail_address],r[biography],r[gender]');"> Misc</div>
	<div class="submenu">
	<input type="checkbox" name="r[biography]" value=1> Biography<BR>
	<input type="checkbox" name="r[birth_date]" value=1> Birthday<BR>
	<input type="checkbox" name="r[gender]" value=1> Gender<BR>
	<input type="checkbox" name="r[gmail_address]" value=1> Gmail Address
	</div>
</div>
</div>
<div style="clear:both" class="hrs"><hr></div>
<?php if (( $this->_tpl_vars['user_is_admin'] || $this->_tpl_vars['user_is_board'] || $this->_tpl_vars['user_is_um'] )): ?>
<span style="margin-right: 20px"><a href="javascript:void(0);" onClick="download_results('excel');"><img src="/content_files/images/icons/excel.png"></a></span>
<a href="javascript:void(0);" onClick="alert('Not implemented yet.'); return false;"><img src="/content_files/images/icons/word.png"></a>
<?php endif; ?>
</div>

</form>

<div style="clear:both">&nbsp;</div>


<?php endif; ?>

<?php endif; ?>





</div>