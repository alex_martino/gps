<?php /* Smarty version 2.6.17, created on 2012-10-16 06:29:29
         compiled from /home/gpscom/public_html/_pages/portal/admin/activity_log.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', '/home/gpscom/public_html/_pages/portal/admin/activity_log.php', 46, false),array('modifier', 'truncate', '/home/gpscom/public_html/_pages/portal/admin/activity_log.php', 48, false),array('modifier', 'replace', '/home/gpscom/public_html/_pages/portal/admin/activity_log.php', 237, false),array('modifier', 'wordwrap', '/home/gpscom/public_html/_pages/portal/admin/activity_log.php', 237, false),)), $this); ?>
<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>

<?php if (! $this->_tpl_vars['popup']): ?>

<script>
function showlog(id) {
window.location='activity_log.php?popup='+id;
}

	function toggle(fields) {
		t_fields = new Array();
		t_fields = fields.split(",");
		boxnum = t_fields.length;
		if(document.getElementsByName(t_fields[0])[0].checked) {
for (var i = 1; i < boxnum; i++) { document.getElementsByName(t_fields[i])[0].checked = true; }
		}
		else {
for (var i = 1; i < boxnum; i++) { document.getElementsByName(t_fields[i])[0].checked = false; }
		}
	}
</script>

<h2>Activity Log</h2>
<?php if ($this->_tpl_vars['error'] > 0): ?>
<div style="color:navy">
<?php echo $this->_tpl_vars['errormsg']; ?>

</div>
<BR>
<?php endif; ?>

<p class="note">Click on a log for more details...</p>

<?php echo $this->_tpl_vars['pagination']; ?>

<BR>
<table class="admin_table" style="width: 700px; border: 1px solid black">
<tr style="border-bottom: 1px solid black">
<th>Date</th>
<th>Time</th>
<th>Type</th>
<th>User</th>
<th>IP Address</th>
</tr>
<?php $_from = $this->_tpl_vars['logs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['i']):
?>
<tr class="row" onClick="showlog(<?php echo $this->_tpl_vars['i']['id']; ?>
);">
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%D") : smarty_modifier_date_format($_tmp, "%D")); ?>
</td>
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%H:%M:%S") : smarty_modifier_date_format($_tmp, "%H:%M:%S")); ?>
</td>
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['type'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, "...", true) : smarty_modifier_truncate($_tmp, 20, "...", true)); ?>
</td>
<td><?php if ($this->_tpl_vars['i']['user']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['user'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, "...", 'TRUE', 'TRUE') : smarty_modifier_truncate($_tmp, 20, "...", 'TRUE', 'TRUE')); ?>
<?php else: ?>-<?php endif; ?></td>
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['ip_address'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, "...", true) : smarty_modifier_truncate($_tmp, 20, "...", true)); ?>
</td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<BR>
<h2>Filters</h2>
<BR>
<form name="filterform" action="activity_log.php" method="GET">
<input type="hidden" name="submitted" value="1">
<div class="searchmenu" id="searchmenu">

<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="admin" value=1 checked onClick="toggle('admin,r[admin_add],r[admin_delete],r[admin_loginas],r[admin_user_add]');"> Admin</div>
	<div class="submenu">
	<input type="checkbox" name="r[admin_add]" value=1 checked> Add<BR>
	<input type="checkbox" name="r[admin_delete]" value=1 checked> Delete<BR>
	<input type="checkbox" name="r[admin_loginas]" value=1 checked> Log In As<BR>
	<input type="checkbox" name="r[admin_user_add]" value=1 checked> User Add
	</div>
</div>
</div>

<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="db" value=1 checked onClick="toggle('db,r[db_download],r[db_edit],r[db_search]');"> Database</div>
	<div class="submenu">
	<input type="checkbox" name="r[db_download]" value=1 checked> DB Download<BR>
	<input type="checkbox" name="r[db_edit]" value=1 checked> DB Edit<BR>
	<input type="checkbox" name="r[db_search]" value=1 checked> DB Search
	</div>
</div>
</div>

<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="dms" value=1 checked onClick="toggle('dms,r[dms_file_delete],r[dms_file_download],r[dms_file_upload],r[dms_folder_create],r[dms_folder_delete]');"> Documents</div>
	<div class="submenu">
	<input type="checkbox" name="r[dms_file_delete]" value=1 checked> File Delete<BR>
	<input type="checkbox" name="r[dms_file_download]" value=1 checked> File Download<BR>
	<input type="checkbox" name="r[dms_file_upload]" value=1 checked> File Upload<BR>
	<input type="checkbox" name="r[dms_folder_create]" value=1 checked> Folder Create<BR>
	<input type="checkbox" name="r[dms_folder_delete]" value=1 checked> Folder Delete
	</div>
</div>
</div>

<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="research" value=1 checked onClick="toggle('research,r[research_add],r[research_delete],r[research_update]');"> Research</div>
	<div class="submenu">
	<input type="checkbox" name="r[research_add]" value=1 checked> Add<BR>
	<input type="checkbox" name="r[research_delete]" value=1 checked> Delete<BR>
	<input type="checkbox" name="r[research_update]" value=1 checked> Update
	</div>
</div>
</div>

<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="resume" value=1 checked onClick="toggle('resume,r[resume_download],r[resume_search],r[resume_upload]');"> Resumes</div>
	<div class="submenu">
	<input type="checkbox" name="r[resume_download]" value=1 checked> Download<BR>
	<input type="checkbox" name="r[resume_search]" value=1 checked> Search<BR>
	<input type="checkbox" name="r[resume_upload]" value=1 checked> Upload
	</div>
</div>
</div>

<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="survey" value=1 checked onClick="toggle('survey,r[survey_add],r[survey_download],r[survey_vote]');"> Surveys</div>
	<div class="submenu">
	<input type="checkbox" name="r[survey_add]" value=1 checked> Add<BR>
	<input type="checkbox" name="r[survey_download]" value=1 checked> Download<BR>
	<input type="checkbox" name="r[survey_vote]" value=1 checked> Vote
	</div>
</div>
</div>

<div class="searchcol">
<div class="searchsection">
<div class="searchcat"><input type="checkbox" name="user" value=1 checked onClick="toggle('user,r[user_login],r[user_login_fail],r[profile_update],r[user_pw_change],r[user_pw_reset_req],r[user_login_remember_me]');"> Users</div>
	<div class="submenu">
	<input type="checkbox" name="r[user_login]" value=1 checked> Login<BR>
	<input type="checkbox" name="r[user_login_fail]" value=1 checked> Login Fail<BR>
	<input type="checkbox" name="r[profile_update]" value=1 checked> Profile Update<BR>
	<input type="checkbox" name="r[user_pw_change]" value=1 checked> PW Change<BR>
	<input type="checkbox" name="r[user_pw_reset_req]" value=1 checked> PW Reset Request<BR>
	<input type="checkbox" name="r[user_login_remember_me]" value=1 checked> Remember Me Login
	</div>
</div>
</div>

<div style="clear: both"><span class="button white strong"><input type="submit" value="Update Filters"></span></div>

</div>
</form>

<script>
// set up check boxes
count = document.filterform.elements.length;
    for (i=0; i < count; i++) 
	{
document.filterform.elements[i].checked = 0;
}

var somethingchecked = 0;

<?php $_from = $this->_tpl_vars['fieldsi']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>
document.getElementsByName('r[<?php echo $this->_tpl_vars['i']; ?>
]')[0].checked = true;
somethingchecked = 1;
<?php endforeach; endif; unset($_from); ?>

if (somethingchecked != 1) {
    for (i=0; i < count; i++) 
	{
document.filterform.elements[i].checked = 1;
}
}

//check boss checkboxes if children are checked
if(document.getElementsByName('r[admin_add]')[0].checked && document.getElementsByName('r[admin_delete]')[0].checked && document.getElementsByName('r[admin_loginas]')[0].checked && document.getElementsByName('r[admin_user_add]')[0].checked) {
document.getElementsByName('admin')[0].checked = true
}
if(document.getElementsByName('r[db_download]')[0].checked && document.getElementsByName('r[db_edit]')[0].checked && document.getElementsByName('r[db_search]')[0].checked) {
document.getElementsByName('db')[0].checked = true
}
if(document.getElementsByName('r[dms_file_delete]')[0].checked && document.getElementsByName('r[dms_file_download]')[0].checked && document.getElementsByName('r[dms_file_upload]')[0].checked && document.getElementsByName('r[dms_folder_create]')[0].checked && document.getElementsByName('r[dms_folder_delete]')[0].checked) {
document.getElementsByName('dms')[0].checked = true
}
if(document.getElementsByName('r[research_add]')[0].checked && document.getElementsByName('r[research_delete]')[0].checked && document.getElementsByName('r[research_update]')[0].checked) {
document.getElementsByName('research')[0].checked = true
}

if(document.getElementsByName('r[resume_download]')[0].checked && document.getElementsByName('r[resume_search]')[0].checked && document.getElementsByName('r[resume_upload]')[0].checked) {
document.getElementsByName('resume')[0].checked = true
}

if(document.getElementsByName('r[survey_add]')[0].checked && document.getElementsByName('r[survey_download]')[0].checked && document.getElementsByName('r[survey_vote]')[0].checked) {
document.getElementsByName('survey')[0].checked = true
}
if(document.getElementsByName('r[user_login]')[0].checked && document.getElementsByName('r[user_login_fail]')[0].checked && document.getElementsByName('r[profile_update]')[0].checked && document.getElementsByName('r[user_pw_change]')[0].checked && document.getElementsByName('r[user_pw_reset_req]')[0].checked && document.getElementsByName('r[user_login_remember_me]')[0].checked) {
document.getElementsByName('user')[0].checked = true
}

</script>

<div style="clear:both">&nbsp;</div>

<?php else: ?>
<h2>Activity Log: Details</h2>
<style>
.title { font-weight: bold; width: 80px }
.field, .title { padding: 3px 0 }
</style>
<table>
<tr>
<td class="title">Date:</td>
<td class="field"><?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%D") : smarty_modifier_date_format($_tmp, "%D")); ?>
</td>
</tr>
<tr>
<td class="title">Time:</td>
<td class="field"><?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%H:%M:%S") : smarty_modifier_date_format($_tmp, "%H:%M:%S")); ?>
</td>
</tr>
<tr>
<td class="title">Action:</td>
<td class="field"><?php echo $this->_tpl_vars['popup']['type']; ?>
</td>
</tr>
<tr>
<td class="title">User:</td>
<td class="field"><?php echo $this->_tpl_vars['popup']['user']; ?>
</td>
</tr>
<tr>
<td class="title">IP Address:</td>
<td class="field"><?php echo $this->_tpl_vars['popup']['ip_address']; ?>
</td>
</tr>
<?php if (( $this->_tpl_vars['popup']['type'] == 'db_edit' )): ?>
<?php $_from = $this->_tpl_vars['db_edit_fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr>
<td class="title"><?php echo $this->_tpl_vars['i']['col']; ?>
:</td>
<td class="field">"<?php echo $this->_tpl_vars['i']['oldval']; ?>
" to "<?php echo $this->_tpl_vars['i']['newval']; ?>
" (<a href="http://www.gps100.com/portal/network/view/overview.php?id=<?php echo $this->_tpl_vars['i']['rowkey']; ?>
">User: <?php echo $this->_tpl_vars['i']['rowkey']; ?>
</a>)</td>
</tr>
<?php endforeach; endif; unset($_from); ?>
<?php else: ?>
<tr>
<td class="title">Data:</td>
<td class="field"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['popup']['data'])) ? $this->_run_mod_handler('replace', true, $_tmp, ":::::", "<BR><BR>") : smarty_modifier_replace($_tmp, ":::::", "<BR><BR>")))) ? $this->_run_mod_handler('wordwrap', true, $_tmp, 110, "<BR>", 'TRUE') : smarty_modifier_wordwrap($_tmp, 110, "<BR>", 'TRUE')); ?>
</td>
</tr>
<?php endif; ?>
</table>
<hr>
<b>Interpretation:</b>
<?php if (( $this->_tpl_vars['popup']['type'] == 'admin_add' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 added <?php echo $this->_tpl_vars['popup']['data']; ?>
 as an admin at <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%l:%M%p") : smarty_modifier_date_format($_tmp, "%l:%M%p")); ?>
 on <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %B %Y") : smarty_modifier_date_format($_tmp, "%e %B %Y")); ?>
.

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'admin_delete' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 removed <?php echo $this->_tpl_vars['popup']['data']; ?>
's admin priviledges at <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%l:%M%p") : smarty_modifier_date_format($_tmp, "%l:%M%p")); ?>
 on <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %B %Y") : smarty_modifier_date_format($_tmp, "%e %B %Y")); ?>
.

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'admin_loginas' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 logged in as <?php echo $this->_tpl_vars['popup']['uname']; ?>
 at <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%l:%M%p") : smarty_modifier_date_format($_tmp, "%l:%M%p")); ?>
 on <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %B %Y") : smarty_modifier_date_format($_tmp, "%e %B %Y")); ?>
.

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'db_download' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 downloaded custom search results at <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%l:%M%p") : smarty_modifier_date_format($_tmp, "%l:%M%p")); ?>
 on <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %B %Y") : smarty_modifier_date_format($_tmp, "%e %B %Y")); ?>
.


<?php elseif (( $this->_tpl_vars['popup']['type'] == 'hack_attempt_db_download' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 attempted to hack the custom search results script to download data he/she was not entitled to.

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'db_edit' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 made changes to the database using admin priviledges.

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'dms_file_delete' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 deleted the file: "<?php echo $this->_tpl_vars['popup']['file_name']; ?>
"

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'dms_file_download' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 downloaded the file: "<?php echo $this->_tpl_vars['popup']['file_name']; ?>
"

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'dms_file_upload' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 uploaded the file: "<?php echo $this->_tpl_vars['popup']['file_name']; ?>
"

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'dms_folder_create' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 created the folder: "<?php echo $this->_tpl_vars['popup']['folder_name']; ?>
"

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'dms_folder_delete' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 deleted the folder: "<?php echo $this->_tpl_vars['popup']['folder_name']; ?>
"

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'profile_update' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 updated the <a href="/portal/network/view/<?php echo $this->_tpl_vars['popup']['data']; ?>
.php?id=<?php echo $this->_tpl_vars['popup']['uid']; ?>
"><?php echo $this->_tpl_vars['popup']['data']; ?>
</a> area of their profile at <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%l:%M%p") : smarty_modifier_date_format($_tmp, "%l:%M%p")); ?>
 on <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %B %Y") : smarty_modifier_date_format($_tmp, "%e %B %Y")); ?>
.

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'survey_add' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 added the survey "<?php echo $this->_tpl_vars['popup']['survey_name']; ?>
" at <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%l:%M%p") : smarty_modifier_date_format($_tmp, "%l:%M%p")); ?>
 on <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %B %Y") : smarty_modifier_date_format($_tmp, "%e %B %Y")); ?>
.

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'survey_download' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 downloaded the survey results for "<?php echo $this->_tpl_vars['popup']['survey_name']; ?>
" at <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%l:%M%p") : smarty_modifier_date_format($_tmp, "%l:%M%p")); ?>
 on <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %B %Y") : smarty_modifier_date_format($_tmp, "%e %B %Y")); ?>
.

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'survey_vote' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 voted on the survey "<?php echo $this->_tpl_vars['popup']['survey_name']; ?>
" at <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%l:%M%p") : smarty_modifier_date_format($_tmp, "%l:%M%p")); ?>
 on <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %B %Y") : smarty_modifier_date_format($_tmp, "%e %B %Y")); ?>
.

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'user_login' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 logged in at <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%l:%M%p") : smarty_modifier_date_format($_tmp, "%l:%M%p")); ?>
 on <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %B %Y") : smarty_modifier_date_format($_tmp, "%e %B %Y")); ?>
.

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'user_login_fail' )): ?>
Someone failed to log into the account with email address "<b><?php echo $this->_tpl_vars['popup']['data']; ?>
</b>" at <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%l:%M%p") : smarty_modifier_date_format($_tmp, "%l:%M%p")); ?>
 on <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %B %Y") : smarty_modifier_date_format($_tmp, "%e %B %Y")); ?>
.
<?php if (( strlen ( $this->_tpl_vars['popup']['user'] ) > 1 )): ?>
The user who corresponds with this email address is <b><?php echo $this->_tpl_vars['popup']['user']; ?>
</b>.
<?php endif; ?>

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'user_pw_change' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 changed their password at <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%l:%M%p") : smarty_modifier_date_format($_tmp, "%l:%M%p")); ?>
 on <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %B %Y") : smarty_modifier_date_format($_tmp, "%e %B %Y")); ?>
.

<?php elseif (( $this->_tpl_vars['popup']['type'] == 'user_login_remember_me' )): ?>
<?php echo $this->_tpl_vars['popup']['user']; ?>
 logged in using a "remember me" cookie at <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%l:%M%p") : smarty_modifier_date_format($_tmp, "%l:%M%p")); ?>
 on <?php echo ((is_array($_tmp=$this->_tpl_vars['popup']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %B %Y") : smarty_modifier_date_format($_tmp, "%e %B %Y")); ?>
.

<?php else: ?>
No interpretation available.
<?php endif; ?>
<hr>
<BR>
<a href="javascript:history.go(-1);">Back</a>

<?php endif; ?>

</div>