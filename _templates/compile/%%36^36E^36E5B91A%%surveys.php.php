<?php /* Smarty version 2.6.17, created on 2012-11-07 11:26:28
         compiled from /home/gpscom/public_html/_pages/portal/admin/surveys.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', '/home/gpscom/public_html/_pages/portal/admin/surveys.php', 19, false),)), $this); ?>
<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>
<h2>Surveys</h2>

<table class="admin_table" style="border: 1px solid black; margin-top: 20px">
<tr style="border-bottom: 1px solid black">
<th>ID</th>
<th>Deadline</th>
<th>Question</th>
<th>Votes</th>
<th>Creator</th>
<th><img src="/content_files/images/icons/excel.png" style="width:14px; height: 14px"></th>
<th><img src="/content_files/images/bin.gif" style="width:14px; height: 14px"></th>
</tr>
<?php $_from = $this->_tpl_vars['surveys']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['i']):
?>
<tr>
<td><?php echo $this->_tpl_vars['i']['id']; ?>
</td>
<td><?php echo $this->_tpl_vars['i']['deadline']; ?>
</td>
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['question'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 60) : smarty_modifier_truncate($_tmp, 60)); ?>
</td>
<td><?php echo $this->_tpl_vars['i']['total']; ?>
</td>
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['user'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 20) : smarty_modifier_truncate($_tmp, 20)); ?>
</td>
<td><a href="surveys.php?download=<?php echo $this->_tpl_vars['i']['id']; ?>
"><img src="/content_files/images/icons/download.gif" style="width:14px; height: 14px"></a></td>
<td><a href="surveys.php?del=<?php echo $this->_tpl_vars['i']['id']; ?>
"><img src="/content_files/images/delete.png" style="width: 12px; height: 12px; margin-top: 2px" onClick="javascript:return confirm('Are you sure you wish to permanently delete this survey?');"></a></td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>

<div style="clear:both">&nbsp;</div>
</div>