//These scripts are for the recruiting application
function confirmdelete(cycle) {
	var answer = confirm("Are you sure you want to delete this recruiting cycle?")
	if (answer){
		window.location = 'applications.php?cycle='+cycle+'&delete=1';
	}
	else{

	}
}
function CallPrint(strid)
{
var idurl = 'printapp.php?id='+strid;
var WinPrint = window.open(idurl,'app','left=0,top=0,width=700,height=500,toolbar=0,scrollbars=0,status=0');
}
function filter() {
var f_email = 0; var f_number = 0; var f_date = 0; var f_status = 0; var f_actions = 0;
if (document.filters.f_email.checked == true) { var f_email = 1; }
if (document.filters.f_number.checked == true) { var f_number = 1; }
if (document.filters.f_date.checked == true) { var f_date = 1; }
if (document.filters.f_status.checked == true) { var f_status = 1; }
if (document.filters.f_actions.checked == true) { var f_actions = 1; }
var str = "&f_email="+f_email+"&f_number="+f_number+"&f_date="+f_date+"&f_status="+f_status+"&f_actions="+f_actions;
//var loc = "viewapplications.php?<?php echo "cycle=".$cycle."&display=".$display; ?>"+str;
document.location.href=loc;
}
function PrintData(){
try{
var oIframe = document.getElementById('ifrmPrint');
var oContent = document.getElementById('datalist').innerHTML;
document.getElementById('ifrmPrint').style.visibility = 'visible';
var oDoc = (oIframe.contentWindow || oIframe.contentDocument);
if (oDoc.document) oDoc = oDoc.document;
oDoc.write("<head><title>title</title>");
oDoc.write("</head><body onload='this.focus(); this.print();'>");
oDoc.write(oContent + "</body>");
oDoc.close();
}
catch(e){
self.print();
}
document.getElementById('ifrmPrint').style.visibility = 'hidden';
}

//Users popup
function popup_display_users(user_list,title) {
if (typeof title === 'undefined') { title = "GPSers"; }
timestamp = new Date().getTime();
$(".ui-dialog-content").dialog("close"); //close all previous dialogs
$('body').append('<div id="display_users_'+timestamp+'" title="'+title+'" class="display_users"><iframe SRC="/includes/display_users.php?users='+user_list+'"></div>');
	$("#display_users_"+timestamp).dialog({ width: 500, height: 500, resizable: false, draggable: false });
}