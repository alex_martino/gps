function hideform() {
document.getElementById('addform').style.display = "none";
document.getElementById('addbutton').style.display = "block";
//document.getElementById('error').style.display = "none";
LightenPage();
}

function showform() {
document.getElementById('addbutton').style.display = "none";
//document.getElementById('error').style.display = "block";
document.getElementById('addform').style.display = "block";
    DarkenPage();
    ShowPanel('addform');
}

visible = 0;
function togglecurrent() {
if (visible == 0) {
visible = 1;
document.getElementById('end').style.visibility='hidden'
}
else {
visible = 0;
document.getElementById('end').style.visibility='visible'
}
}

function toggle_edit_current() {
if (edit_visible == 0) {
edit_visible = 1;
document.getElementById('edit_end').style.visibility='hidden';
}
else {
edit_visible = 0;
document.getElementById('edit_end').style.visibility='visible';
}
}

function ShowPanel(form)
{
    var popup_panel = document.getElementById(form);

    // w is a width of the newsletter panel
    w = 650;
    // h is a height of the newsletter panel
    h = 340;

    // get the x and y coordinates to center the newsletter panel
    xc = Math.round((document.body.clientWidth/2)-(w/2))
    yc = Math.round((document.body.clientHeight/2)-(h/2))

    // show the newsletter panel
    popup_panel.style.left = xc + "px";
    popup_panel.style.top  = yc + "px";
    popup_panel.style.display = 'block';
}

// this function puts the dark screen over the entire page
function DarkenPage()
{
    var page_screen = document.getElementById('page_screen');
    page_screen.style.height = document.body.parentNode.scrollHeight + 'px';
    page_screen.style.display = 'block';
}

// this function removes the dark screen and the page is light again
function LightenPage()
{
    var page_screen = document.getElementById('page_screen');
    page_screen.style.display = 'none';
}

function update_explain() {
if (document.addform.type.value == "radio") {
document.getElementById('type_explain').innerHTML = 'I.e. user can select just one of the above responses';
}
else {
document.getElementById('type_explain').innerHTML = 'I.e. user can select several of the above responses';
}
}

// ]]>