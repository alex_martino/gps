function hideform() {
document.getElementById('addform').style.display = "none";
document.getElementById('addbutton').style.display = "block";
//document.getElementById('error').style.display = "none";
LightenPage();
}

function showform() {
document.getElementById('addbutton').style.display = "none";
//document.getElementById('error').style.display = "block";
document.getElementById('addform').style.display = "block";
    DarkenPage();
    ShowPanel('addform');
}

visible = 0;
function togglecurrent() {
if (visible == 0) {
visible = 1;
document.getElementById('end').style.visibility='hidden'
}
else {
visible = 0;
document.getElementById('end').style.visibility='visible'
}
}

function hide_edit_form() {
    // hide the newsletter panel
    var popup_panel = document.getElementById('editform');
    popup_panel.style.display = 'none';
    // lighten the page again
    LightenPage();
}

function show_edit_form(id,employer,division,title,startmonth,startyear,endmonth,endyear,current,i_id) {
document.getElementById('editform').style.display = "block";
document.edit_job.id.value = id;
document.edit_job.employer.value = employer;
document.edit_job.division.value = division;
document.edit_job.title.value = title;
document.edit_job.industry.value = i_id;
document.edit_job.startmonth.value = startmonth;
document.edit_job.startyear.value = startyear;
document.edit_job.endmonth.value = endmonth;
document.edit_job.endyear.value = endyear;
if (current == 1) {
document.edit_job.current.checked = 1;
document.getElementById('edit_end').style.visibility='hidden';
edit_visible = 1;
}
else {
document.edit_job.current.checked = 0;
document.getElementById('edit_end').style.visibility='visible';
edit_visible = 0;
}
    DarkenPage();
    ShowPanel('editform');
}

function toggle_edit_current() {
if (edit_visible == 0) {
edit_visible = 1;
document.getElementById('edit_end').style.visibility='hidden';
}
else {
edit_visible = 0;
document.getElementById('edit_end').style.visibility='visible';
}
}

function ShowPanel(form)
{
    var popup_panel = document.getElementById(form);

    // w is a width of the newsletter panel
    w = 650;
    // h is a height of the newsletter panel
    h = 500;

    // get the x and y coordinates to center the newsletter panel
    xc = Math.round((document.body.clientWidth/2)-(w/2))
    yc = Math.round((document.body.clientHeight/2)-(h/2))

    // show the newsletter panel
    popup_panel.style.left = xc + "px";
    popup_panel.style.top  = yc + "px";
    popup_panel.style.display = 'block';
}

// this function puts the dark screen over the entire page
function DarkenPage()
{
    var page_screen = document.getElementById('page_screen');
    page_screen.style.height = document.body.parentNode.scrollHeight + 'px';
    page_screen.style.display = 'block';
}

// this function removes the dark screen and the page is light again
function LightenPage()
{
    var page_screen = document.getElementById('page_screen');
    page_screen.style.display = 'none';
}



// ]]>
