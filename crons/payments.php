<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

$now = date("Y-m-d H:i:s");
$due_date = date("Y-m-d", strtotime('+1 week'));
$year = date("Y");

//Mark overdue invoices and email

$sql = "SELECT m.id as user_id, m.first_name, m.last_name, m.email_address, m.analyst, m.member, m.alumni, m.graduation_year, p.id as invoice_id, p.amount, p.status, p.description, p.created, p.due_date FROM members m LEFT JOIN payments p on p.user_id = m.id WHERE p.due_date < '" . $now ."' AND p.status <> 'paid'";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {

//first mark invoice as overdue
$sql2 = "UPDATE payments SET status = 'overdue' WHERE id = '" . $row['invoice_id'] ."' LIMIT 1";
$result2 = mysql_query($sql2) or die(mysql_error());

//email user their invoice reminder
				//now email
				require_once "Mail.php";
				
				$from = "GPS Payments <payments@".$domain.">";
				$to = $row['email_address'];
				$subject = "GPS Invoice Reminder: Payment due";

				$boundary = md5(date('U'));
				
				$host = "localhost";
				$username = "payments@".$domain;
				$password = "202AlphaDog";

				$headers = array ('From' => $from,
				  'To' => $to,
				  'MIME-Version' => '1.0',
				  'Content-Type' => 'multipart/alternative; boundary='.$boundary,
				  'Subject' => $subject);
				$smtp = Mail::factory('smtp',
				  array ('host' => $host,
				    'auth' => true,
				    'username' => $username,
				    'password' => $password));

				$text="hi";
				$html="<b>Hi</b>";

				 $body = "This is a MIME encoded message."; 
 
				 $body .= "\r\n\r\n--" . $boundary . "\r\n";
				 $body .= "Content-type: text/plain;charset=utf-8\r\n\r\n";
				 $body .= "Dear " . $row['first_name'].",\r\n\r\n";
				 $body .= "Your invoice from the " . date("jS F Y", strtotime($row['created'])) . " is now due.\r\n";
				 $body .= "Please log into http://www.gps100.com to make payment.\r\n\r\n";
				 $body .= "Thank you,\r\n";
				 $body .= "The GPS Board";

				 $body .= "\r\n\r\n--" . $boundary . "\r\n";
				 $body .= "Content-type: text/html;charset=utf-8\r\n\r\n";
				 $body .= file_get_contents("http://www.gps100.com/includes/payments/invoice_details.php?email_invoice=1&id=".$row['invoice_id']);

				 $body .= "\r\n\r\n--" . $boundary . "--";

				$mail = $smtp->send($to, $headers, $body);

				if (PEAR::isError($mail)) {
				  $error++; $errormsg = "Email Error";
				 } else {
				  $email_success=1;
				 }

echo "Payment reminder sent for: ".$row['first_name'] . " " . $row['last_name']."<BR>";

}

//End mark overdue invoices and email

//Automatically generate annual dues invoices and email to users

//Get dues structure
$fee['analyst'] = "0";
$fee['member'] = "25";
$fee['alumni_disc'] = "150";
$fee['alumni_full'] = "250";

//get list of all analysts, members & alumni (not 2005 class)
$sql = "SELECT m.id, m.first_name, m.last_name, m.email_address, m.analyst, m.member, m.alumni, m.graduation_year, p.amount, p.status, p.description FROM members m LEFT JOIN payments p on p.user_id = m.id WHERE m.disabled != 1 AND (p.annual_dues_flag = '1' OR p.annual_dues_flag IS NULL) AND m.analyst = 0 AND m.graduation_year != '2005'";
$result = mysql_query($sql) or die(mysql_error());

while ($row = mysql_fetch_array($result)) { //loop: loop through each GPSer

$uid = $row['id'];

//check payment not already created for this user for this year
$sql2 = "SELECT * FROM payments WHERE user_id = '$uid' AND annual_dues_flag = '1' AND description = '$year' LIMIT 1";
$result2 = mysql_query($sql2) or die(mysql_error());

if (mysql_num_rows($result2) != 1) { //check if payment for this year exists

	//Determine if user is near dues cap
	$sql4 = "SELECT m.non_portal_dues_paid, ( SELECT SUM( p.amount ) FROM payments p WHERE p.user_id =  '$uid' AND p.annual_dues_flag =  '1' ) AS annual_dues_invoiced, d.cap_amount FROM members AS m JOIN dues_cap d ON m.dues_cap_id = d.id WHERE m.id =  '$uid' LIMIT 1";
	$result4 = mysql_query($sql4);
	while ($row4 = mysql_fetch_array($result4)) {
		$total_annual_dues_invoiced = $row4['non_portal_dues_paid'] + $row4['annual_dues_invoiced'];
		$cap_amount = $row4['cap_amount'];
	}

	$total_dues_remaining = max( ($cap_amount - $total_annual_dues_invoiced) , 0);

	//calculate appropriate fee
	if ($row['analyst']) { $amount = $fee['analyst']; }
	else if ($row['member']) { $amount = $fee['member']; }
	else if ($row['alumni'] && $row['graduation_year'] >= ($year-2)) { $amount = $fee['alumni_disc']; }
	else { $amount = $fee['alumni_full']; }

	// Ensure the amount to be invoiced is not greater than the dues remaining
	$amount = min($amount, $total_dues_remaining);

	if ($amount > 0) {

		//add invoice to database
		$sql1 = "INSERT INTO payments (user_id, annual_dues_flag, description, amount, due_date, status, created, updated) VALUES ('$uid', '1', '$year', '$amount', '$due_date', 'unpaid', '$now', '$now')";
		$result1 = mysql_query($sql1) or die(mysql_error());

		//Get invoice ID
		$sql3 = "SELECT id FROM payments WHERE created = '".$now."' AND user_id = '".$uid."' LIMIT 1";
		$result3 = mysql_query($sql3) or die(mysql_error());
		$row3 = mysql_fetch_array($result3);
		$invoice_id = $row3['id'];

		//email user their invoice
						//now email
						require_once "Mail.php";
						
						$from = "GPS Payments <payments@".$domain.">";
						$to = $row['email_address'];
						$subject = "GPS Invoice: Annual Dues " . $year;

						$boundary = md5(date('U'));
						
						$host = "localhost";
						$username = "payments@".$domain;
						$password = "202AlphaDog";

						$headers = array ('From' => $from,
						  'To' => $to,
						  'MIME-Version' => '1.0',
						  'Content-Type' => 'multipart/alternative; boundary='.$boundary,
						  'Subject' => $subject);
						$smtp = Mail::factory('smtp',
						  array ('host' => $host,
						    'auth' => true,
						    'username' => $username,
						    'password' => $password));

						$text="hi";
						$html="<b>Hi</b>";

						 $body = "This is a MIME encoded message."; 
		 
						 $body .= "\r\n\r\n--" . $boundary . "\r\n";
						 $body .= "Content-type: text/plain;charset=utf-8\r\n\r\n";
						 $body .= "Dear " . $row['first_name'].",\r\n\r\n";
						 $body .= "You have an GPS invoice due.\r\n";
						 $body .= "Please log into http://www.gps100.com to make payment.\r\n\r\n";
						 $body .= "Thank you,\r\n";
						 $body .= "The GPS Board";

						 $body .= "\r\n\r\n--" . $boundary . "\r\n";
						 $body .= "Content-type: text/html;charset=utf-8\r\n\r\n";
						 $body .= file_get_contents("http://www.gps100.com/includes/payments/invoice_details.php?email_invoice=1&id=".$invoice_id);

						 $body .= "\r\n\r\n--" . $boundary . "--";
						 
						$mail = $smtp->send($to, $headers, $body);

						if (PEAR::isError($mail)) {
						  $error++; $errormsg = "Email Error";
						 } else {
						  $email_success=1;
						 }
						 

		echo "Payment added for: ".$row['first_name'] . " " . $row['last_name']."<BR>";

	} // end check amount is > 0

} //end check payment exists

else {

//payment exists for this user

}



} //end loop

//End automatically generate annual invoices and email to users


?>