<?php

//This script is run as a cron weekly
//The goal is to:
//1. Retrieve the articles not yet sent out from the database
//2. Group & order them as appropriate, then push them into an array
//3. Build the email
//4. Send out the email to the subscribers

//0. Connect to the MySQL server and load 2-way encryption lib
require_once("../config.php");
include_once("../includes/2-way-encryption.php");

//1. Retrieve the articles not yet sent out from the database
$sql = "SELECT id FROM articles WHERE sent = 0 AND invalid = 0";
$result = mysql_query($sql) or die(mysql_error());
$the_ids = array();
while($row = mysql_fetch_array($result)) {
$the_ids[] = $row[0];
}
foreach ($the_ids as $value) {
$token = num_encode($value);
$sql = "UPDATE articles SET token='".$token."' WHERE id='".$value."' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
}

//2. Group & order them as appropriate, then push them into an array
	//alumni
		$sql = "SELECT m.id, m.first_name, m.last_name, m.ug_school_id, m.graduation_year, m.alumni, a.url, a.description, a.token, u.SNAME as school_name FROM members as m LEFT JOIN articles as a ON m.id = a.user_id LEFT JOIN universities as u ON m.ug_school_id = u.ID WHERE a.invalid = 0 AND a.sent = 0 AND m.alumni = 1 ORDER BY a.sent DESC";
		$result = mysql_query($sql) or die(mysql_error());
		while($row=mysql_fetch_array($result)) {
		$articles['alumni'][] = $row;
		}

	//members
		$sql = "SELECT m.id, m.first_name, m.last_name, m.ug_school_id, m.graduation_year, m.member, a.url, a.description, a.token, u.SNAME as school_name FROM members as m LEFT JOIN articles as a ON m.id = a.user_id LEFT JOIN universities as u ON m.ug_school_id = u.ID WHERE a.invalid = 0 AND a.sent = 0 AND m.member = 1 ORDER BY a.sent DESC";
		$result = mysql_query($sql) or die(mysql_error());
		while($row=mysql_fetch_array($result)) {
		$articles['members'][] = $row;
		}

	//analysts
		$sql = "SELECT m.id, m.first_name, m.last_name, m.ug_school_id, m.graduation_year, m.analyst, a.url, a.description, a.token, u.SNAME as school_name FROM members as m LEFT JOIN articles as a ON m.id = a.user_id LEFT JOIN universities as u ON m.ug_school_id = u.ID WHERE a.invalid = 0 AND a.sent = 0 AND m.analyst = 1 ORDER BY a.sent DESC";
		$result = mysql_query($sql) or die(mysql_error());
		while($row=mysql_fetch_array($result)) {
		$articles['analysts'][] = $row;
		}

		$article_count = count($articles['alumni']) + count($articles['members']) + count($articles['analysts']);

if ($article_count > 0) { //check there are articles

//3. Build the email
				$boundary = md5(date('U'));

				 $bodym = "This is a MIME encoded message.";
				 $body = "";
 
				 $bodym .= "\r\n\r\n--" . $boundary . "\r\n";
				 $bodym .= "Content-type: text/plain;charset=utf-8\r\n\r\n";
				 $bodym .= "Dear GPSer,\r\n\r\n";
				 $bodym .= "Each week we send out an email digest, contributions are submitted by members.\r\n\r\n";
				 $bodym .= "To view the latest digest, please view this email in a modern email program, or turn on HTML.\r\n\r\n";
				 $bodym .= "Thank you,\r\n";
				 $bodym .= "Benjamin Wigoder";

				 $bodym .= "\r\n\r\n--" . $boundary . "\r\n";
				 $bodym .= "Content-type: text/html;charset=utf-8\r\n\r\n";
				 $body .= file_get_contents("http://www.gps100.com/includes/articles/digest_header.php");

if(count($articles['members']) > 0) {
				 $body .= file_get_contents("http://www.gps100.com/includes/articles/digest_title.php?title=".urlencode("Member Newsbites"));
				 $body .= '
                                                    <!-- article -->
                                                    <tr>
                                                        <td class="copy" valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #2b2b2b; line-height: 18px;"> <br />
				 ';
				foreach ($articles['members'] as $key=>$value) {
				 $body .= '
								<table cellspacing="0" border="0" cellpadding="0" width="415">
								   <tr>
									<td valign="middle" width="355">
									<p style="margin: 0;">
									<b>'.$value['description'].'</b> - <a href="http://www.gps100.com/digests/read.php?token='.$value['token'].'" target="_blank" style="color: #002868; text-decoration: none"><span style="color: #002868">Read more</span></a><BR>
									<i>'.$value['first_name'].' '.$value['last_name'].', '.$value['school_name'].' '.$value['graduation_year'].'</i>
									</p>
									</td>
									<td valign="middle" align="right">
									';
									if (file_exists($_SERVER['DOCUMENT_ROOT'].'/content_files/member_photos/'.$value['id'].'-cropped.jpg')) {
										$body .= '<img src="http://www.gps100.com/content_files/member_photos/'.$value['id'].'-cropped.jpg">';
									}
									else {
										$body .= '<img src="http://www.gps100.com/images/male_thumb.png">';
									}
									$body .= '
									</td>
								   </tr>
								</table>
							<br />
				 ';

				}
				 $body .= '
                                                        </td>
                                                    </tr>
                                                    <!-- / article -->
				 ';


}

if(count($articles['analysts']) > 0) {
				 $body .= file_get_contents("http://www.gps100.com/includes/articles/digest_title.php?title=".urlencode("Analyst Newsbites"));
				 $body .= '
                                                    <!-- article -->
                                                    <tr>
                                                        <td class="copy" valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #2b2b2b; line-height: 18px;"> <br />
				 ';
				foreach ($articles['analysts'] as $key=>$value) {
				 $body .= '
								<table cellspacing="0" border="0" cellpadding="0" width="415">
								   <tr>
									<td valign="middle" width="355">
									<p style="margin: 0;">
									<b>'.$value['description'].'</b> - <a href="http://www.gps100.com/digests/read.php?token='.$value['token'].'" target="_blank" style="color: #002868; text-decoration: none"><span style="color: #002868">Read more</span></a><BR>
									<i>'.$value['first_name'].' '.$value['last_name'].', '.$value['school_name'].' '.$value['graduation_year'].'</i>
									</p>
									</td>
									<td valign="middle" align="right">
									';
									if (file_exists($_SERVER['DOCUMENT_ROOT'].'/content_files/member_photos/'.$value['id'].'-cropped.jpg')) {
										$body .= '<img src="http://www.gps100.com/content_files/member_photos/'.$value['id'].'-cropped.jpg">';
									}
									else {
										$body .= '<img src="http://www.gps100.com/images/male_thumb.png">';
									}
									$body .= '
									</td>
								   </tr>
								</table>
							<br />
				 ';

				}
				 $body .= '
                                                        </td>
                                                    </tr>
                                                    <!-- / article -->
				 ';


}

if(count($articles['alumni']) > 0) {
				 $body .= file_get_contents("http://www.gps100.com/includes/articles/digest_title.php?title=".urlencode("Alumni Newsbites"));
				 $body .= '
                                                    <!-- article -->
                                                    <tr>
                                                        <td class="copy" valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #2b2b2b; line-height: 18px;"> <br />
				 ';
				foreach ($articles['alumni'] as $key=>$value) {
				 $body .= '
								<table cellspacing="0" border="0" cellpadding="0" width="415">
								   <tr>
									<td valign="middle" width="355">
									<p style="margin: 0;">
									<b>'.$value['description'].'</b> - <a href="http://www.gps100.com/digests/read.php?token='.$value['token'].'" target="_blank" style="color: #002868; text-decoration: none"><span style="color: #002868">Read more</span></a><BR>
									<i>'.$value['first_name'].' '.$value['last_name'].', '.$value['school_name'].' '.$value['graduation_year'].'</i>
									</p>
									</td>
									<td valign="middle" align="right">
									';
									if (file_exists($_SERVER['DOCUMENT_ROOT'].'/content_files/member_photos/'.$value['id'].'-cropped.jpg')) {
										$body .= '<img src="http://www.gps100.com/content_files/member_photos/'.$value['id'].'-cropped.jpg">';
									}
									else {
										$body .= '<img src="http://www.gps100.com/images/male_thumb.png">';
									}
									$body .= '
									</td>
								   </tr>
								</table>
							<br />
				 ';

				}
				 $body .= '
                                                        </td>
                                                    </tr>
                                                    <!-- / article -->
				 ';


}


				 $body .= file_get_contents("http://www.gps100.com/includes/articles/digest_footer.php");


// Uncomment to debug
//die($body);

				//write digest to database
				$sql = "INSERT INTO articles_digests (time_made, content, article_count) VALUES ('".date("Y-m-d H:i:s")."', '".mysql_real_escape_string($body)."', '".$article_count."')";
				$result = mysql_query($sql) or die(mysql_error());;

				$body = $bodym . $body;


				 $body .= "\r\n\r\n--" . $boundary . "--";

//5. Mark articles as sent - move ahead as it appears this gets killed because of script timeout
$datenow = date('Y-m-d H:i:s');
$sql2 = "UPDATE articles SET time_sent='".$datenow."', sent='1' WHERE id IN(".implode($the_ids,',').")";
$result2 = mysql_query($sql2) or die(mysql_error());

//4. Send out the email to the subscribers

				require_once "Mail.php";
				
				$from = "GPS Admin <admin@gps100.com>";
				$subject = "GPS Weekly Digest";

				$host = "localhost";
				$username = "articles@gps100.com";
				$password = "dfgJD3211Dk";

$sql = "SELECT email_address FROM members WHERE disabled = 0 AND articles_digest = 1";
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array($result)) { //start email loop

				$to = $row['email_address'];

				$headers = array ('From' => $from,
				  'To' => $to,
				  'MIME-Version' => '1.0',
				  'Content-Type' => 'multipart/alternative; boundary='.$boundary,
				  'Subject' => $subject,
				  'Date' => gmdate("D, d M Y H:i:s")." UT"
				  );
				$smtp = Mail::factory('smtp',
				  array ('host' => $host,
				    'auth' => true,
				    'username' => $username,
				    'password' => $password));

				$mail = $smtp->send($to, $headers, $body);

				if (PEAR::isError($mail)) {
				  die("Email Error");
				 } else {
				  $email_success=1;
				 }

} //end email loop

} //end check there are articles

?>