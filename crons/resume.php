<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

/////////////////////////////////////////////////////////////
////////// Segment 1: Email users without a resume //////////
/////////////////////////////////////////////////////////////

//load up users without a resume
$sql = "SELECT id, first_name, last_name, email_address FROM members WHERE (analyst = 1 OR member = 1) AND resume_uploaded = 0 AND disabled = 0";
$result = mysql_query($sql) or die(mysql_error());
while ($row=mysql_fetch_array($result)) { //start loop

				//now email
				require_once "Mail.php";
				
				$from = "GPS Board of Managers <admin@".$domain.">";
				$to = $row['first_name']. " ". $row['last_name'] . "<" . $row['email_address'] . ">";

				$subject = "Resume Required";
				$body = $row['first_name'].",\n\n";
				$body .= "Please upload your resume to the GPS portal here: http://www.gps100.com/portal/toolbox/resume_upload.php";
				$body .= "\n\nIt is important we have a copy of your resume so that we can use it for recruiting events.";
				$body .= "\n\nThanks,\n";
				$body .= "GPS Board of Managers";
				
				$host = "localhost";
				$username = "admin@".$domain;
				$password = "101dalmations";

				$headers = array ('From' => $from,
				  'To' => $to,
				  'Subject' => $subject);
				$smtp = Mail::factory('smtp',
				  array ('host' => $host,
				    'auth' => true,
				    'username' => $username,
				    'password' => $password));

				$mail = $smtp->send($to, $headers, $body);

				if (PEAR::isError($mail)) {
				  $error++; $errormsg = "Email Error";
				 } else {
				  $email_success=1;
				 }

} //end loop

/////////////////////////////////////////////////////////////
///// Segment 2: Email users with an out of date resume /////
/////////////////////////////////////////////////////////////

//This script is run every Saturday, so we use 1 week ranges for dates.

//dates to check resume up-to-date - format: "dd", "mm"
  //pre internship
    $check_dates[] = array("01", "06");
    $check_dates[] = array("02", "06");
    $check_dates[] = array("03", "06");
    $check_dates[] = array("04", "06");
    $check_dates[] = array("05", "06");
    $check_dates[] = array("06", "06");
    $check_dates[] = array("07", "06");

  //post-internship, pre-job start
    $check_dates[] = array("22", "08");
    $check_dates[] = array("23", "08");
    $check_dates[] = array("24", "08");
    $check_dates[] = array("25", "08");
    $check_dates[] = array("26", "08");
    $check_dates[] = array("27", "08");
    $check_dates[] = array("28", "08");

  //5th January - New year
    $check_dates[] = array("05", "01");
    $check_dates[] = array("06", "01");
    $check_dates[] = array("07", "01");
    $check_dates[] = array("08", "01");
    $check_dates[] = array("09", "01");
    $check_dates[] = array("10", "01");
    $check_dates[] = array("11", "01");

//today's date
$today_day = date("d");
$today_month = date("m");

foreach ($check_dates as $key => $value) {
if ($value[0] == $today_day && $value[1] == $today_month) {
$do_update=1;
}
}

if ($do_update) {

//load up users with a resume, which hasn't been updated in the last 3 months

$today=date("Y-m-d h:i:s");

$sql = "SELECT id, first_name, last_name, email_address FROM members WHERE (analyst = 1 OR member = 1) AND resume_uploaded = 1 AND (DATEDIFF('$today',resume_date) > 90) AND disabled = 0";
$result = mysql_query($sql) or die(mysql_error());
while ($row=mysql_fetch_array($result)) { //start loop

				//now email
				require_once "Mail.php";
				
				$from = "GPS Board of Managers <admin@".$domain.">";
				$to = $row['first_name']. " ". $row['last_name'] . "<" . $row['email_address'] . ">";

				$subject = "Resume Update Required";
				$body = $row['first_name'].",";
				$body .= "\n\nWe've noticed that your resume hasn't been updated for a while.";
				$body .= "\n\nPlease take a moment to update your resume here: http://www.gps100.com/portal/toolbox/resume_upload.php";
				$body .= "\n\nIt is important we have a recent copy of your resume so that we can use it for recruiting events.";
				$body .= "\n\nThanks,\n";
				$body .= "GPS Board of Managers";
				
				$host = "localhost";
				$username = "admin@".$domain;
				$password = "101dalmations";

				$headers = array ('From' => $from,
				  'To' => $to,
				  'Subject' => $subject);
				$smtp = Mail::factory('smtp',
				  array ('host' => $host,
				    'auth' => true,
				    'username' => $username,
				    'password' => $password));

				$mail = $smtp->send($to, $headers, $body);

				if (PEAR::isError($mail)) {
				  $error++; $errormsg = "Email Error";
				 } else {
				  $email_success=1;
				 }


} //end loop

} //end if do update

?>