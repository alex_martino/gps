<?php

//script will fetch an email identified by $msgid, and parse the its parts into an
//array $partsarray
//structure of array:
//$partsarray[<name of part>][<attachment/text>]
//if attachment- subarray is [filename][binary data]
//if text- subarray is [type of text(HTML/PLAIN)][text string]

//i.e.
//$partsarray[3.1][attachment][filename]=filename of attachment in part 3.1
//$partsarray[3.1][attachment][binary]=binary data of attachment in part 3.1
//$partsarray[2][text][type]=type of text in part 2
//$partsarray[2][text][string]=decoded text string in part 2
//$partsarray[not multipart][text][string]=decoded text string in message that isn't multipart

function parsepart($p,$i){
    global $link,$msgid,$partsarray,$c_messages;
    //where to write file attachments to:
    $filestore = '[full/path/to/attachment/store/(chmod777)]';

    //fetch part
    $part=imap_fetchbody($link,$msgid,$i);
    //if type is not text
    if ($p->type!=0){
        //DECODE PART        
        //decode if base64
        if ($p->encoding==3)$part=base64_decode($part);
        //decode if quoted printable
        if ($p->encoding==4)$part=quoted_printable_decode($part);
        //no need to decode binary or 8bit!
        
        //get filename of attachment if present
        $filename='';
        // if there are any dparameters present in this part
        if (count($p->dparameters)>0){
            foreach ($p->dparameters as $dparam){
                if ((strtoupper($dparam->attribute)=='NAME') ||(strtoupper($dparam->attribute)=='FILENAME')) $filename=$dparam->value;
                }
            }
        //if no filename found
        if ($filename==''){
            // if there are any parameters present in this part
            if (count($p->parameters)>0){
                foreach ($p->parameters as $param){
                    if ((strtoupper($param->attribute)=='NAME') ||(strtoupper($param->attribute)=='FILENAME')) $filename=$param->value;
                    }
                }
            }
        //write to disk and set partsarray variable
        if ($filename!=''){
            $partsarray[$i][attachment] = array('filename'=>$filename,'binary'=>$part);
            $fp=fopen($filestore.$filename,"w+");
            fwrite($fp,$part);
            fclose($fp);
            }
    //end if type!=0        
    }
    
    //if part is text
    else if($p->type==0){
        //decode text
        //if QUOTED-PRINTABLE
        if ($p->encoding==4) $part=quoted_printable_decode($part);
        //if base 64
        if ($p->encoding==3) $part=base64_decode($part);
        
        //OPTIONAL PROCESSING e.g. nl2br for plain text
        //if plain text

//        if (strtoupper($p->subtype)=='PLAIN')1;

	if (strtoupper($p->subtype)=='PLAIN')1 {
		$c_messages[$i]['body'] = $part;
	}

        //if HTML
        else if (strtoupper($p->subtype)=='HTML')1;
        $partsarray[$i][text] = array('type'=>$p->subtype,'string'=>$part);
    }
    
    //if subparts... recurse into function and parse them too!
    if (count($p->parts)>0){
        foreach ($p->parts as $pno=>$parr){
            parsepart($parr,($i.'.'.($pno+1)));            
            }
        }
return;
}

?>