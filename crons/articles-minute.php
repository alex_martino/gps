<?php

//This script is run as a cron every minute
//The goal is to:
//1. Log into the articles@gps100.com email box
//2. Read the emails in the mailbox and push to array
//3. Filter all fields for malicious code
//4. Filter which emails are valid
//5. Run a check on the sender email and see if we can find a match in our DB, if not reject
//6. Store in DB
//7. Notify senders that their articles have been added
//8. Notify senders that their articles have been rejected

//0. Connect to the MySQL server to use mysql_real_escape_string later
require_once("../config.php");

//1. Log into the articles@gps100.com email box
$mailbox = imap_open("{mail.gps100.com/novalidate-cert:143}INBOX", "articles@gps100.com", "dfgJD3211Dk");

//2. Read the emails received in the last minute and push to array

if ($mailbox) {

    $num_msg = imap_num_msg($mailbox);
    if ($num_msg > 0) {
         
        //Loop through all messages, filter data, store in db, then delete the emails
        for ($i=$num_msg; $i>0; $i--) {

            $headers = imap_header($mailbox, $i);

	    $sender = $headers->from;
	    foreach ($sender as $id => $object) {
	    $sender_name = $object->personal;
	    $sender_email = $object->mailbox . "@" . $object->host;
	    }

		$messages[$i]['subject'] = trim($headers->Subject);
		$messages[$i]['body'] = imap_body($mailbox, $i);
		$messages[$i]['sender']['email'] = $sender_email;
		$messages[$i]['sender']['name'] = $sender_name;
		$messages[$i]['datetime'] = date("Y-m-d H:i:s", $headers->udate);

	//Parse body
		unset($boundary); unset($matches); unset($email_segments);

		$matches = array();
		preg_match('#Content-Type: multipart\/[^;]+;\s*boundary="([^"]+)"#i', $messages[$i]['body'], $matches);
		list(, $boundary) = $matches;

		$email_segments = explode('--' . $boundary, $messages[$i]['body']);

		array_shift($email_segments); // drop everything before the first boundary

		foreach ($email_segments as $segment)
			{
			  if (stristr($segment, "Content-Type: text/plain") !== false)
			  {
			    $c_messages[$i]['body'] = preg_replace('/Content-(Type|ID|Disposition|Transfer-Encoding):.*?\r\n/is', "", $segment);

			  }

			}

		//We remove everything before the first "new line"
		$pos = strpos($c_messages[$i]['body'],"\n");
		$c_messages[$i]['body'] = substr($c_messages[$i]['body'],$pos);

		$pos = strpos($c_messages[$i]['body'],"\r");
		$c_messages[$i]['body'] = substr($c_messages[$i]['body'],$pos);

		//remove any white space
		$c_messages[$i]['body'] = rtrim($c_messages[$i]['body']);

		//We remove everything after the first "new line"
		if (stristr($c_messages[$i]['body'],"\n\r")) {
		$pos = strpos($c_messages[$i]['body'],"\n\r");
		$c_messages[$i]['body'] = substr($c_messages[$i]['body'],0,$pos);
		}

		//Replace an \r\n breaks with a space
		$c_messages[$i]['body'] = str_replace("\r\n"," ",$c_messages[$i]['body']);
		$c_messages[$i]['body'] = str_replace("\r"," ",$c_messages[$i]['body']);
		$c_messages[$i]['body'] = trim($c_messages[$i]['body']);

		//If last character is an "=", remove
		if (substr($c_messages[$i]['body'],-1) == "=") {
			$c_messages[$i]['body'] = substr($c_messages[$i]['body'], 0, strlen($c_messages[$i]['body'])-1);
		}

		//If body text is too long, truncate
		if (strlen($c_messages[$i]['body']) > 403) {
		$c_messages[$i]['body'] = substr($c_messages[$i]['body'], 0, 400)."...";
		}


//3. Filter all fields for malicious code

	//Now check the data is valid & filter
		$c_messages[$i]['subject'] = mysql_real_escape_string(filter_var($messages[$i]['subject'], FILTER_VALIDATE_URL));
		$c_messages[$i]['body'] = mysql_real_escape_string($c_messages[$i]['body']);
		$c_messages[$i]['sender']['email'] = mysql_real_escape_string(filter_var($messages[$i]['sender']['email'], FILTER_VALIDATE_EMAIL));
		$c_messages[$i]['sender']['name'] = mysql_real_escape_string($messages[$i]['sender']['name']);
		$c_messages[$i]['datetime'] = mysql_real_escape_string($messages[$i]['datetime']);

//4. Filter which emails are valid - we provide a reason to the user if invalid

	//Check for invalid emails
		//If URL not valid
		if ($c_messages[$i]['subject'] != $messages[$i]['subject']) {
			$c_messages[$i]['invalid'] = 1;
			$c_messages[$i]['invalid_reason'] .= "Subject of the email must contain a valid link. ";
		}

		//If link description not valid
		if (strlen($c_messages[$i]['body']) < 1) {
			$c_messages[$i]['invalid'] = 1;
			$c_messages[$i]['invalid_reason'] .= "The body of the email should contain a description of the link. ";
		}

//5. Run a check on the sender email and see if we can find a match in our DB

		$sql = "SELECT id FROM members WHERE (email_address = '" . $c_messages[$i]['sender']['email'] . "' OR gmail_address = '" . $c_messages[$i]['sender']['email'] . "' OR alternative_email_1 = '" . $c_messages[$i]['sender']['email'] . "') AND disabled = 0 LIMIT 1";
		$result = mysql_query($sql) or die(mysql_error());
		$resultnum = mysql_num_rows($result);
		if ($resultnum > 0) {
			while($row = mysql_fetch_array($result)) {
				$c_messages[$i]['sender']['user_id'] = $row[0];
			}
		}
		else { //email not found
			$c_messages[$i]['invalid'] = 1;
			$c_messages[$i]['invalid_reason'] .= "The email you sent the article from is not associated with your GPS account. Please log into the Portal and add " . $c_messages[$i]['sender']['email'] . " to your account (in the contact section of your profile).";
		}

	//Mark email for deletion
	imap_delete($mailbox, $i);

	}

//6. Store in DB

		$sql = array();

		foreach ($c_messages as $key => $value) {
			$sql[] = '("'.$value['subject'].'", "'.$value['body'].'", "'.$value['sender']['name'].'", "'.$value['sender']['email'].'", "'.$value['datetime'].'", "'.$value['sender']['user_id'].'", "'.$value['invalid'].'", "'.$value['invalid_reason'].'")';
		}

		$result = mysql_query('INSERT INTO articles (url, description, sender_name, sender_email, time_received, user_id, invalid, invalid_reason) VALUES '.implode(',', $sql)) or die(mysql_error());

//7. Notify senders that their articles have been added &&
//8. Notify senders that their articles have been rejected


				require_once "Mail.php";
				
				$from = "GPS Admin <admin@".$domain.">";


		foreach ($c_messages as $key => $value) {

				$to = $value['sender']['email'];
				$body = "Dear ".$value['sender']['name'].",";

			if ($value['invalid'] != 1) {

				$subject = "Thank you for the article!";

				$body .= "\n\nThank you for taking the time to contribute an article. It will appear in the next digest.";
				$body .= "\n\nPlease feel free to submit more articles!";
				$body .= "\n\nBest wishes,\nBenjamin Wigoder";

			}

			else {

				$subject = "There was a problem with your article submission!";

				$body .= "\n\nThere was a problem with your article submission.";
				$body .= "\n\n".$value['invalid_reason'];
				$body .= "\n\nPlease feel free to retry by sending another email to articles@gps100.com";
				$body .= "\n\nIf you need help just hit reply to this email to ask me a question.";
				$body .= "\n\nBest wishes,\nBenjamin Wigoder";


			}


				//complete the mail send

				$host = "localhost";
				$username = "admin@gps100.com";
				$password = "101dalmations";

				$headers = array ('From' => $from,
				  'To' => $to,
				  'Subject' => $subject);
				$smtp = Mail::factory('smtp',
				  array ('host' => $host,
				    'auth' => true,
				    'username' => $username,
				    'password' => $password));

				$mail = $smtp->send($to, $headers, $body);

				if (PEAR::isError($mail)) {
				  $error++; $errormsg = "Email Error";
				 } else {
				  $emailsuccess=1;
				 }


		} //end foreach c_messages...


    } else {
        echo "No messages in mailbox";
    }


	//now empty the mailbox
	imap_expunge($mailbox);
	imap_close($mailbox);

} else {
    echo "Cannot open mailbox.";
}


?>