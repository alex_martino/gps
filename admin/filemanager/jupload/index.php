<?php

session_start();
include('../class/FileManager.php');

$container = $_REQUEST['fmContainer'];

if($container && isset($_SESSION[$container])) {
	$FileManager = unserialize($_SESSION[$container]);
	include('jupload.php');

	$allowFileTypes = is_array($FileManager->allowFileTypes) ? join('/', $FileManager->allowFileTypes) : '';
	$specificHeaders = array();

	if($FileManager->authUser != '') {
		$specificHeaders[] = 'Authorization: Basic ' . base64_encode($FileManager->authUser . ':' . $FileManager->authPassword);
	}

	$appletParameters = array(
		'maxFileSize' => '2G',
		'archive' => 'wjhk.jupload.jar',
		'width' => 520,
		'height' => 250,
		'lookAndFeel' => 'system',
		'lang' => $FileManager->language,
		'allowedFileExtensions' => $allowFileTypes,
		'specificHeaders' => join('\n', $specificHeaders),
		'afterUploadURL' => 'javascript:parent.document.fmJavaUpload.submit()'
	);

	$classParameters = array(
		'demo_mode' => false,
		'allow_subdirs' => false,
		'destdir' => $FileManager->getUploadDir()
	);

	$juploadPhpSupportClass = new JUpload($appletParameters, $classParameters);
}

?>
<html>
<head>
<!--JUPLOAD_JSCRIPT-->
<link rel="stylesheet" href="../css/filemanager.css" type="text/css">
</head>
<body class="fmTD3" style="margin:0px; padding:0px">
<div align="center">
<?php if(!isset($FileManager)) print "ERROR: session not found!"; ?>
<!--JUPLOAD_APPLET-->
</div>
</body>
</html>