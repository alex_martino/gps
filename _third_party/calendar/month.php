<?php 
define('BASE', $_SERVER['DOCUMENT_ROOT'] . '/_third_party/calendar/');

$current_view = "month";

require_once(BASE . 'functions/ical_parser.php');
require_once(BASE . 'functions/list_functions.php');
require_once(BASE . 'functions/template.php');

header("Content-Type: text/html; charset=$charset");

if ($minical_view == 'current') $minical_view = 'month';

ereg ("([0-9]{4})([0-9]{2})([0-9]{2})", $_REQUEST['getdate'], $day_array2);
$this_day 				= $day_array2[3]; 
$this_month 			= $day_array2[2];
$this_year 				= $day_array2[1];

$unix_time 				= strtotime($getdate);
$today_today 			= date('Ymd', time() + $second_offset); 
$tomorrows_date 		= date('Ymd', strtotime("+1 day",  $unix_time));
$yesterdays_date 		= date('Ymd', strtotime("-1 day",  $unix_time));
$sidebar_date 			= localizeDate($dateFormat_week_list, $unix_time);

// find out next month
$next_month_month 		= ($this_month + 1 == '13') ? '1' : ($this_month + 1);
$next_month_day 		= $this_day;
$next_month_year 		= ($next_month_month == '1') ? ($this_year + 1) : $this_year;
while (!checkdate($next_month_month,$next_month_day,$next_month_year)) $next_month_day--;
$next_month_time 		= mktime(0, 0, 0,$next_month_month,$next_month_day,$next_month_year);

// find out last month
$prev_month_month 		= ($this_month-1 == '0') ? '12' : ($this_month-1);
$prev_month_day 		= $this_day;
$prev_month_year 		= ($prev_month_month == '12') ? ($this_year-1) : $this_year;
while (!checkdate($prev_month_month,$prev_month_day,$prev_month_year)) $prev_month_day--;
$prev_month_time 		= mktime(0, 0, 0,$prev_month_month,$prev_month_day,$prev_month_year);

$next_month 			= date("Ymd", $next_month_time);
$prev_month 			= date("Ymd", $prev_month_time);
$display_date 			= localizeDate ($dateFormat_month, $unix_time);
$parse_month 			= date("Ym", $unix_time);
$first_of_month 		= $this_year.$this_month."01";
$start_month_day 		= dateOfWeek($first_of_month, $week_start_day);
$thisday2 				= localizeDate($dateFormat_week_list, $unix_time);
$num_of_events2 		= 0;

// select for calendars
$list_icals 	= display_ical_list(availableCalendars($ALL_CALENDARS_COMBINED));
$list_years 	= list_years();
$list_months 	= list_months();
$list_weeks 	= list_weeks();
$list_jumps 	= list_jumps();
$list_calcolors = list_calcolors();
$list_icals_pick = display_ical_list(availableCalendars($ALL_CALENDARS_COMBINED), TRUE);

$page = new Page(BASE . 'templates/month.tpl');

$page->replace_files(array(
	'header'			=> BASE . 'templates/header.tpl',
	'event_js'			=> BASE . 'functions/event.js',
	'footer'			=> BASE . 'templates/footer.tpl',
	'sidebar'           => BASE . 'templates/sidebar.tpl'
));

$page->replace_tags(array(
	'charset'			=> $charset,
	'cal'				=> $cal,
	'getdate'			=> $getdate,
	'getcpath'			=> "&cpath=$cpath",
	'cpath'             => $cpath,
	'calendar_name'		=> $cal_displayname,
	'display_date'		=> $display_date,
	'default_path'		=> '',
	'next_month' 		=> $next_month,
	'prev_month'	 	=> $prev_month,
	'show_goto' 		=> '',
	'list_jumps' 		=> $list_jumps,
	'list_icals' 		=> $list_icals,
	'list_icals_pick'	=> $list_icals_pick,
	'list_years' 		=> $list_years,
	'list_months' 		=> $list_months,
	'list_weeks' 		=> $list_weeks,
	'legend'	 		=> $list_calcolors,
	'current_view'		=> $current_view,
	'style_select' 		=> $style_select,
	'sidebar_date'		=> $sidebar_date,
	'l_goprint'			=> $lang['l_goprint'],
	'l_calendar'		=> $lang['l_calendar'],
	'l_legend'			=> $lang['l_legend'],
	'l_tomorrows'		=> $lang['l_tomorrows'],
	'l_jump'			=> $lang['l_jump'],
	'l_todo'			=> $lang['l_todo'],
	'l_day'				=> $lang['l_day'],
	'l_week'			=> $lang['l_week'],
	'l_month'			=> $lang['l_month'],
	'l_year'			=> $lang['l_year'],
	'l_subscribe'		=> $lang['l_subscribe'],
	'l_download'		=> $lang['l_download'],
	'l_this_months'		=> $lang['l_this_months'],
	'l_search'			=> $lang['l_search'],
	'l_pick_multiple'	=> $lang['l_pick_multiple']		
));
	
if ($this_months_events == 'yes') {	
	$page->monthbottom($page);
} else {
	$page->nomonthbottom($page);
}

$page->draw_subscribe($page);
$page->output();
?>