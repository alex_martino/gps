<?php
define('BASE', $_SERVER['DOCUMENT_ROOT'] . '/_third_party/calendar/');

$current_view = 'year';

require_once(BASE . 'functions/ical_parser.php');
require_once(BASE . 'functions/list_functions.php');
require_once(BASE . 'functions/template.php');

header("Content-Type: text/html; charset=$charset");

ereg ("([0-9]{4})([0-9]{2})([0-9]{2})", $getdate, $day_array2);
$this_day 	= $day_array2[3]; 
$this_month = $day_array2[2];
$this_year 	= $day_array2[1];
$next_year 	= strtotime ("+1 year", strtotime($getdate));
$next_year 	= date ("Ymd", $next_year);
$prev_year 	= strtotime ("-1 year", strtotime($getdate));
$prev_year 	= date ("Ymd", $prev_year);

$sidebar_date 		= localizeDate($dateFormat_day, strtotime($getdate));

// For the side months
ereg ("([0-9]{4})([0-9]{2})([0-9]{2})", $getdate, $day_array2);
$this_day 	= $day_array2[3]; 
$this_month = $day_array2[2];
$this_year 	= $day_array2[1];

// select for calendars
$list_icals 	= display_ical_list(availableCalendars($ALL_CALENDARS_COMBINED));
$list_years 	= list_years();
$list_months 	= list_months();
$list_weeks 	= list_weeks();
$list_jumps 	= list_jumps();
$list_calcolors = '<img src="images/allday_dot.gif" alt=" " width="11" height="10" border="0" />'.$lang['l_all_day'].'<br/><img src="images/event_dot.gif" alt=" " width="11" height="10" border="0" />'.$lang['l_event']."<br/>"; 

$list_icals_pick = display_ical_list(availableCalendars($ALL_CALENDARS_COMBINED), TRUE);

$page = new Page(BASE . 'templates/year.tpl');

$page->replace_files(array(
	'header'			=> BASE . 'templates/header.tpl',
	'event_js'			=> BASE . 'functions/event.js',
	'footer'			=> BASE . 'templates/footer.tpl',
    'sidebar'           => BASE . 'templates/sidebar_year.tpl'
));

$page->replace_tags(array(
	'event_js'			=> '',
	'current_view'		=> $current_view,
	'charset'			=> $charset,
	'default_path'		=> '',
	'cal'				=> $cal,
	'getcpath'			=> "&cpath=$cpath",
    'cpath'             => $cpath,
	'getdate'			=> $getdate,
	'calendar_name'		=> $cal_displayname,
	'display_date'		=> $this_year,
	'sidebar_date'		=> $sidebar_date,
	'this_year'			=> $this_year,
	'next_day' 			=> $next_day,
	'next_week' 		=> $next_week,
	'prev_day'	 		=> $prev_day,
	'prev_week'	 		=> $prev_week,
	'next_year'			=> $next_year,
	'prev_year'			=> $prev_year,
	'show_goto' 		=> '',
	'list_icals' 		=> $list_icals,
	'list_icals_pick' 		=> $list_icals_pick,
	'list_years' 		=> $list_years,
	'list_months' 		=> $list_months,
	'list_weeks' 		=> $list_weeks,
	'list_jumps' 		=> $list_jumps,
	'legend'	 		=> $list_calcolors,
	'style_select' 		=> $style_select,
	'l_goprint'			=> $lang['l_goprint'],
	'l_calendar'		=> $lang['l_calendar'],
	'l_legend'			=> $lang['l_legend'],
	'l_tomorrows'		=> $lang['l_tomorrows'],
	'l_jump'			=> $lang['l_jump'],
	'l_todo'			=> $lang['l_todo'],
	'l_day'				=> $lang['l_day'],
	'l_week'			=> $lang['l_week'],
	'l_month'			=> $lang['l_month'],
	'l_year'			=> $lang['l_year'],
	'l_search'				=> $lang['l_search'],
	'l_subscribe'		=> $lang['l_subscribe'],
	'l_download'		=> $lang['l_download'],
	'l_pick_multiple'	=> $lang['l_pick_multiple']
));

$page->tomorrows_events($page);
$page->draw_subscribe($page);	
$page->output();
?>
