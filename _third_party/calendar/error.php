<?php
define('BASE', $_SERVER['DOCUMENT_ROOT'] . '/_third_party/calendar/');

require_once(BASE . 'functions/template.php');

function error($error_msg='There was an error processing the request.', $file='NONE', $error_base='./') {
	global $language, $enable_rss, $lang, $charset, $default_path;
	if (!isset($lang['l_error_title']))		$lang['l_error_title'] = 'Error!';
	if (!isset($lang['l_error_window']))	$lang['l_error_window'] = 'There was an error!';
	if (!isset($lang['l_error_calendar']))	$lang['l_error_calendar'] = 'The calendar "%s" was being processed when this error occurred.';
	if (!isset($lang['l_error_back']))		$lang['l_error_back'] = 'Please use the "Back" button to return.';
	if (!isset($enable_rss))				$enable_rss = 'no';
		
	$error_calendar 	= sprintf($lang['l_error_calendar'], $file);
	$current_view 		= 'error';
	$display_date 		= $lang['l_error_title'];
	$calendar_name 		= $lang['l_error_title'];
	
	if (empty($default_path)) {
		if (isset($_SERVER['HTTPS']) || strtolower($_SERVER['HTTPS']) == 'on' ) {
			$default_path = 'https://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].substr($_SERVER['PHP_SELF'],0,strpos($_SERVER['PHP_SELF'],'/rss/'));
		} else {
			$default_path = 'http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].substr($_SERVER['PHP_SELF'],0,strpos($_SERVER['PHP_SELF'],'/rss/'));
		}
	}
	
	$page = new Page(BASE.'templates/error.tpl');
	
	$page->replace_files(array(
		'header'			=> BASE . 'templates/header.tpl',
		'footer'			=> BASE . 'templates/footer.tpl',
		'sidebar'           => ''
	));

	$page->replace_tags(array(
		'version'			=> $phpicalendar_version,
		'default_path'		=> $default_path.'/',
		'cal'				=> $cal,
		'getdate'			=> $getdate,
		'charset'			=> $charset,
		'calendar_name'		=> $calendar_name,
		'display_date'		=> $display_date,
		'event_js'			=> '',
		'error_msg'	 		=> $error_msg,
		'error_calendar' 	=> $error_calendar,
		'generated'	 		=> $generated,
		'l_error_back'		=> $lang['l_error_back'],
		'l_error_window'	=> $lang['l_error_window']	
	));
		
	$page->output();
}

?>