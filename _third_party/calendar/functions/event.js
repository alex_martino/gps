<script language="JavaScript" type="text/javascript">
	document.popup_data = new Array();
	
	function openEventWindow(num, obj) {
		// populate the hidden form
		var data = document.popup_data[num];

		var template = new Ext.Template(
			'<div>'
			+ '<p>{event_text} - <span class="V9">(<i>{event_times}</i>)</span></p>'
			+ '<p>{description}</p>'
			+ '</div>'
		);
		template.compile();
	
		var popup = new Ext.Window({
			width: 500,
			height: 400,
			autoScroll: true,
			bodyStyle: "padding: 5px;",
			renderTo: document.body
		});
		
		// Send a request to get the event data as a json object
		var connection = new Ext.data.Connection().request({
			url: "event.php",
			method: "POST",
			params: {date: data.date, time: data.time, uid: data.uid, cpath: data.cpath, event_data: data.event_data},
			success: function(o) {
				var response = Ext.util.JSON.decode(o.responseText);
				
				response.event_text = unescape(String(response.event_text).replace(/\+/g, " "));
				response.event_times = unescape(String(response.event_times).replace(/\+/g, " "));
				response.description = unescape(String(response.description).replace(/\+/g, " "));
				response.cal_title_full = unescape(String(response.cal_title_full).replace(/\+/g, " "));
				
				popup.setTitle(response.cal_title_full);
				
				template.overwrite(popup.body, response);
				
				popup.show(obj);
			}
		});
	}
	
	function EventData(date, time, uid, cpath, event_data) {
		this.date = date;
		this.time = time;
		this.uid = uid;
		this.cpath = cpath;
		this.event_data = event_data;
	}
</script>